# Grids used in Chapter 5

There are two categories of grids: (i) Grids used in Sec. 5.4.1 whose filename is setup DXXX_RYYY_D/N, with XXX being the horizontal discretization length (m), YYY being the radius of the outer domain (km) and N or D standing for the Dirichlet or Neumann case. (ii) The second category has filenames of type: GEOMETRY_INITSCENARIO_EXTENSION.dgf. Here GEOMETRY depicts either the complex or generic geometry. INITSCENARIO stands for the initialization scenario. NOINIT means that the grid does not contain initialized primary variables at the nodes. EXTENSION can be either extend which means that a domain extension has been applied to the grid and NOEXTEND which means that there is no circular domain extension. For the two layer grids and the generic geometry grids can be created using the Matlab-script `genericsolling.m` in `matlab-functions/dgfGridFunctions'.

**Files:**

- **grid-CH5-BrineDisp.tbz2** Zip (BZIP2) file containing all grids used in Chapter 5. Extract: `tar -jxvf grid-CH5-BrineDisp.tbz2`

