
% Plot the CO2 storage efficiency in kgCO2/m2 over Gr as well as the amount
% of trapped CO2 also in terms of storage efficiency
% and over the storage efficiency. The amount trapped is calculated with
% the Land model

clear all;
%Plot name
plotName = 'Fig_4_08_plotTrap';

%Load database
load('databaseGr.mat')

%Load results for 20 shallow simulations below 2500m. The data points
%are chosen to cover 20 different equi-distant Gr values covering the whole range of Gr values.
load('../Sec-4-4-2-Comparison/Plot-4-6-Gr-Okwen/shallow_Inj0.1MTperA_20_Gr/shallowSelection.mat', 'indexGr');
indexShallow20 = indexGr;
load('../Sec-4-4-2-Comparison/Plot-4-6-Gr-Okwen/shallow_Inj0.1MTperA_20_Gr/co2MassGrTest.mat', 'massTrapped', 'co2Mass');
massTrapped20 = massTrapped;
totalMass20 = co2Mass;

%Load results for 10 shallow simulations below 2500m. The data points
%are chosen to cover 10 different equi-distant Gr values covering the whole range of Gr values.
%The values are different to the 20 data points selected above.
load('../Sec-4-4-2-Comparison/Plot-4-6-Gr-Okwen/shallow_Inj0.1MTperA_10_depth/shallowSelection.mat', 'indexGr');
indexShallow = indexGr;
load('../Sec-4-4-2-Comparison/Plot-4-6-Gr-Okwen/shallow_Inj0.1MTperA_10_depth/co2MassGrTest.mat', 'massTrapped', 'co2Mass');
massTrapped10 = massTrapped;
totalMass10 = co2Mass;



% load('deepInj0.1MTperA/deepSelection.mat','indexGr');
% indexDeep = indexGr;
% load('deepInj0.1MTperA/co2MassGrTest.mat','co2Mass');
% massDeep = co2Mass;
% deltaPressureMaxDeep = deltaPressureMax;

shallowDeepIndex = [indexShallow20 indexShallow];
shallowDeepMass = [totalMass20 totalMass10];
shallowDeepMassTrapped = [massTrapped20 massTrapped10];
%shallowDeepPres = [deltaPressureMaxShallow20 deltaPressureMaxShallow];


reservoirThickness = 24; %m
reservoirArea = pi*1000^2; %m2
co2DomainDensity = shallowDeepMass.*48./reservoirArea; %kg/m2
co2TrappingDensity = shallowDeepMassTrapped.*48./reservoirArea; %kg/m2
injectionRate = 0.1e9/365/24/3600; % kg/s
wellDiameter = 0.3; %m
injectionArea = pi*(wellDiameter/2)^2*reservoirThickness; %m2
specificInjectionRate = injectionRate/injectionArea; %kg/s/m2
grPlot = gr_(shallowDeepIndex);

shallow = 900;
deep = 2000;
shallowStr = num2str(shallow);
deepStr = num2str(deep);


strInterpreter = 'latex';
strFontName = 'Times';
iFontSize = 12;
numberFontSize = 10;
strFontUnit = 'pixels';
iResolution = 150;
Fig = figure;
figure(Fig);
set(gcf, 'Units', 'centimeters');

    p = plot(grPlot, co2DomainDensity, 'x', ...
    grPlot, co2TrappingDensity, 'o');
    set(p(1), 'color', [0 0 1], 'MarkerSize', 7);
    set(p(2), 'color', [0 0.5 0], 'MarkerSize', 7);
set(p, 'LineWidth',2);
axis square;
axis([4e10 10e10 0 400]);
set(gca,'FontSize',numberFontSize, 'fontName', strFontName);
%h_leg = legend([shallowStr,'m',' - ',deepStr,'m'], ['below  ',deepStr,'m'],['above  ',shallowStr,'m'],'Location', 'SouthWest');
[h_leg, h_obj] = legend('Total CO$_2$ (mobile + residually trapped)~~', 'CO$_2$ residually trapped in reservoir','Location', 'SouthWest');
set(h_leg, 'color', 'none' );
% set(h_obj, 'Visible', 'on');
title('Injection Rate 0.1 Mt per year');
ylabel('SE [kgCO$_2$/m$^2$]');
xlabel('Gr [-]');
figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',iFontSize,'fontName',strFontName, 'Interpreter',strInterpreter) 

set(Fig,'position',[0 0, 13, 15])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,plotName,'epsc') 
