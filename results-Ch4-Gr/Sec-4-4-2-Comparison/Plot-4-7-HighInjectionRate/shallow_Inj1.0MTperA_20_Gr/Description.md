# Results

**Files:**

- **ShallowSelection_20_Gr.m** Picks 20 datapoints from the database with equidistant values of Gr covering the whole range of Gr values.
Creates the simulation folders and modifies and copies the Dumux input file (gravitationno.input) into each simulation folder.

- **ShallowSelection_20_Gr.m** Mat-file containing the index of the datapoint and the folder name.

- **gravitationno.input** Dumux input file template.

- **readCO2mass.m** Reads the CO2 mass from each simulation at the end of the simulation (i.e. when the spill point is reached)
The values are stored into 'co2MassGrTest.mat'.

- **co2MassGrTest.mat** Contains condensed simulation output.

- **ShallowIndex_X**  Simulation folder.
