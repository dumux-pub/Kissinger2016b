% Plot the CO2 storage efficiency in kgCO2/m2 over Gr and over the storage efficiency
% calculated with the Okwen-Method. Evaluate for two injection rates 0.1
% and 1 Mt per year

clear all;

%Plot name
plotName = 'Fig_4_07_plotVarInj.eps';

%Load database
load('databaseGr.mat')

%Load simulation results 

%Load results for 20 shallow simulations below 2500m. The data points
%are chosen to cover 20 different equi-distant Gr values covering the whole
%range of Gr values. Injection rate 0.1 Mt per year.
load('../Plot-4-6-Gr-Okwen/shallow_Inj0.1MTperA_20_Gr/shallowSelection.mat','indexGr');
indexShallowInjSmall = indexGr;
load('../Plot-4-6-Gr-Okwen/shallow_Inj0.1MTperA_20_Gr/co2MassGrTest.mat','co2Mass', 'deltaPressureMax', 'volumeCO2');
massShallowInjSmall = co2Mass;
volumeShallowInjSmall = volumeCO2;
deltaPressureMaxShallow20Small = deltaPressureMax;

%Load results for 10 shallow simulations below 2500m. The data points
%are chosen to cover ten different equi-distant depths within the depth
%range. Injection rate 1.0 Mt per year
load('shallow_Inj1.0MTperA_20_Gr/shallowSelection.mat','indexGr');
indexShallowInjLarge = indexGr;
load('shallow_Inj1.0MTperA_20_Gr/co2MassGrTest.mat','co2Mass', 'deltaPressureMax', 'volumeCO2');
massShallowInjLarge = co2Mass;
volumeShallowInjLarge = volumeCO2;
deltaPressureMaxShallowLarge = deltaPressureMax;


reservoirThickness = 24; %m
reservoirArea = pi*1000^2; %m2
co2DomainDensityInjSmall = massShallowInjSmall.*48./reservoirArea; %kg/m2
co2DomainDensityInjLarge = massShallowInjLarge.*48./reservoirArea; %kg/m2
injectionRateSmall = 0.1e9/365/24/3600; % kg/s
injectionRateLarge = 1e9/365/24/3600; % kg/s
wellDiameter = 0.3; %m
injectionArea = pi*(wellDiameter/2)^2*reservoirThickness; %m2
specificInjectionRateSmall = injectionRateSmall/injectionArea; %kg/s/m2
specificInjectionRateLarge = injectionRateLarge/injectionArea; %kg/s/m2
grPlot = gr_(indexShallowInjSmall);
Srw = 0.3;
porosity = 0.2;
% volumetric storage efficiency from simulations
eps_sim_small = volumeShallowInjSmall.*48./reservoirArea./reservoirThickness./porosity;
eps_sim_large = volumeShallowInjLarge.*48./reservoirArea./reservoirThickness./porosity;

% Storage efficiency according to Okwen
viscosityCO2 = viscosityCO2_(indexShallowInjSmall);
viscosityBrine = viscosityBrine_(indexShallowInjSmall);
viscosityRatio = viscosityBrine./viscosityCO2;
densityCO2 = densityCO2_(indexShallowInjSmall);
densityBrine = densityBrine_(indexShallowInjSmall);

tmp = 2*pi*9.81*1e-13*reservoirThickness^2*(densityBrine - densityCO2).*densityCO2./viscosityBrine;
Gr_nc_small = tmp/injectionRateSmall;
Gr_nc_large = tmp/injectionRateLarge;

eps_visc = (1-Srw)./viscosityRatio;

tmp = 1./((0.0324*viscosityRatio - 0.0952).*Gr_nc_small + (0.1778*viscosityRatio + 5.9682).*Gr_nc_small.^0.5 + 1.6962*viscosityRatio - 3.0472);
eps_grav_cor_small = 2*(1-Srw).*tmp;
tmp = 1./((0.0324*viscosityRatio - 0.0952).*Gr_nc_large + (0.1778*viscosityRatio + 5.9682).*Gr_nc_large.^0.5 + 1.6962*viscosityRatio - 3.0472);
eps_grav_cor_large = 2*(1-Srw).*tmp;

eps_small = eps_visc;
eps_small(Gr_nc_small > 0.5) = eps_grav_cor_small(Gr_nc_small > 0.5);
eps_large = eps_visc;
eps_large(Gr_nc_large > 0.5) = eps_grav_cor_large(Gr_nc_large > 0.5);

mtotal_small = reservoirThickness*porosity*densityCO2.*eps_small;
mtotal_large = reservoirThickness*porosity*densityCO2.*eps_large;



shallow = 1000;
deep = 2000;
shallowStr = num2str(shallow);
deepStr = num2str(deep);
injLargeStr = 'Injection Rate 1.0 Mt/yr~~~~';
injSmallStr = 'Injection Rate 0.1 Mt/yr';


strInterpreter = 'latex';
strFontName = 'Times';
iFontSize = 12;
numberFontSize = 10;
strFontUnit = 'pixels';
iResolution = 150;
Fig = figure;
figure(Fig);
set(gcf, 'Units', 'centimeters');


subplot(1,2,1)

%Plot for different injection rates and depths, the first five plot entries
%are dummies for creating the legend.
p = semilogx(grPlot(1), co2DomainDensityInjSmall(1),'x', grPlot(1)./10, co2DomainDensityInjLarge(1),'o', ...
    grPlot(1), co2DomainDensityInjSmall(1), '-', grPlot(1), co2DomainDensityInjSmall(1), '-', grPlot(1), co2DomainDensityInjSmall(1), '-',...
    grPlot(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow),...
    co2DomainDensityInjSmall(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow), 'x', ...
    grPlot(raw{2}(indexShallowInjSmall) > deep), co2DomainDensityInjSmall(raw{2}(indexShallowInjSmall) > deep), 'x',...
    grPlot(raw{2}(indexShallowInjSmall) < shallow), co2DomainDensityInjSmall(raw{2}(indexShallowInjSmall) < shallow), 'x', ...
    grPlot(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow)./10,...
    co2DomainDensityInjLarge(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow), 'o', ...
    grPlot(raw{2}(indexShallowInjSmall) > deep)./10, co2DomainDensityInjLarge(raw{2}(indexShallowInjSmall) > deep), 'o',...
    grPlot(raw{2}(indexShallowInjSmall) < shallow)./10, co2DomainDensityInjLarge(raw{2}(indexShallowInjSmall) < shallow), 'o');
    set(p(1), 'color', 'k', 'MarkerSize', 7);
    set(p(2), 'color', 'k', 'MarkerSize', 7);
    set(p(3), 'color', [0 0 1]);
    set(p(4), 'color', [0 0.5 0]);
    set(p(5), 'color', [1 0 0]);
    set(p(6), 'color', [0 0 1], 'MarkerSize', 7);
    set(p(7), 'color', [0 0.5 0], 'MarkerSize', 7);
    set(p(8), 'color', [1 0 0], 'MarkerSize', 7);
    set(p(9), 'color', [0 0 1], 'MarkerSize', 7);
    set(p(10), 'color', [0 0.5 0], 'MarkerSize', 7);
    set(p(11), 'color', [1 0 0], 'MarkerSize', 7);
set(p, 'LineWidth',2);
axis square;
%axis([2e-3 5.5e-3 6 16]);
set(gca,'FontSize',numberFontSize, 'fontName', strFontName);
h_leg = legend(injSmallStr, injLargeStr, [shallowStr,'~m',' - ',deepStr,'~m'], ['deeper than ',deepStr,'~m'], ...
    ['shallower than  ',shallowStr,'~m'], 'Location', 'SouthWest');
% h_leg = legend([shallowStr,'m',' - ',deepStr,'m, ', injSmallStr], ['deeper than ',deepStr,'m, ',injSmallStr], ...
%     ['shallower than  ',shallowStr,'m, ', injSmallStr], ...
% [shallowStr,'m',' - ',deepStr,'m, ',injLargeStr], ['deeper than ',deepStr,'m, ',injLargeStr], ...
% ['shallower than  ',shallowStr,'m, ', injLargeStr],'Location', 'SouthOutside');
% h_leg2 = legend([shallowStr,'m',' - ',deepStr,'m'], ['deeper than ',deepStr,'m'],['shallower than  ',shallowStr,'m'],'Location', 'NorthWest');
title('Storage Efficiency over Gr');
ylabel('SE Simulation [kgCO$_2$/m$^2$]');
xlabel('Gr [-]');

figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',iFontSize,'fontName',strFontName, 'Interpreter',strInterpreter) 





subplot(1,2,2)


p = plot(mtotal_small(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow),...
    co2DomainDensityInjSmall(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow), 'x', ...
    mtotal_small(raw{2}(indexShallowInjSmall) > deep), co2DomainDensityInjSmall(raw{2}(indexShallowInjSmall) > deep), 'x',...
    mtotal_small(raw{2}(indexShallowInjSmall) < shallow), co2DomainDensityInjSmall(raw{2}(indexShallowInjSmall) < shallow), 'x',...
    mtotal_large(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow),...
    co2DomainDensityInjLarge(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow), 'o', ...
    mtotal_large(raw{2}(indexShallowInjSmall) > deep), co2DomainDensityInjLarge(raw{2}(indexShallowInjSmall) > deep), 'o',...
    mtotal_large(raw{2}(indexShallowInjSmall) < shallow), co2DomainDensityInjLarge(raw{2}(indexShallowInjSmall) < shallow), 'o');
    set(p(1), 'color', [0 0 1], 'MarkerSize', 7);
    set(p(2), 'color', [0 0.5 0], 'MarkerSize', 7);
    set(p(3), 'color', [1 0 0], 'MarkerSize', 7);
    set(p(4), 'color', [0 0 1], 'MarkerSize', 7);
    set(p(5), 'color', [0 0.5 0], 'MarkerSize', 7);
    set(p(6), 'color', [1 0 0], 'MarkerSize', 7);
set(p, 'LineWidth',2);
axis square;
%axis([2e-4 5.5e-4 14 26]);
set(gca,'FontSize',numberFontSize, 'fontName', strFontName);
%h_leg = legend([shallowStr,'m',' - ',deepStr,'m'], ['deeper than ',deepStr,'m'],['shallower than  ',shallowStr,'m'],'Location', 'SouthWest');
title('Storage Efficiency over Okwen-Method');
ylabel('SE Simulation [kgCO$_2$/m$^2$]');
xlabel('SE Okwen-Method [kgCO$_2$/m$^2$] ');
figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',iFontSize,'fontName',strFontName, 'Interpreter',strInterpreter) 
% 
% subplot(2,2,3)
% 
% p = plot(grPlot(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow),...
%     eps_sim_small(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow), 'x', ...
%     grPlot(raw{2}(indexShallowInjSmall) > deep), eps_sim_small(raw{2}(indexShallowInjSmall) > deep), 'x',...
%     grPlot(raw{2}(indexShallowInjSmall) < shallow), eps_sim_small(raw{2}(indexShallowInjSmall) < shallow), 'x', ...
%     grPlot(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow)./10,...
%     eps_sim_large(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow), 'o', ...
%     grPlot(raw{2}(indexShallowInjSmall) > deep)./10, eps_sim_large(raw{2}(indexShallowInjSmall) > deep), 'o',...
%     grPlot(raw{2}(indexShallowInjSmall) < shallow)./10, eps_sim_large(raw{2}(indexShallowInjSmall) < shallow), 'o');
%     set(p(1), 'color', [0 0 1], 'MarkerSize', 7);
%     set(p(2), 'color', [0 0.5 0], 'MarkerSize', 7);
%     set(p(3), 'color', [1 0 0], 'MarkerSize', 7);
%     set(p(4), 'color', [0 0 1], 'MarkerSize', 7);
%     set(p(5), 'color', [0 0.5 0], 'MarkerSize', 7);
%     set(p(6), 'color', [1 0 0], 'MarkerSize', 7);
% set(p, 'LineWidth',2);
% axis square;
% %axis([2e-3 5.5e-3 6 16]);
% set(gca,'FontSize',numberFontSize, 'fontName', strFontName);
% % h_leg = legend([shallowStr,'m',' - ',deepStr,'m, ', injSmallStr], ['deeper than ',deepStr,'m, ',injSmallStr], ...
% %     ['shallower than  ',shallowStr,'m, ', injSmallStr], ...
% % [shallowStr,'m',' - ',deepStr,'m, ',injLargeStr], ['deeper than ',deepStr,'m, ',injLargeStr], ...
% % ['shallower than  ',shallowStr,'m, ', injLargeStr],'Location', 'SouthOutside');
% %h_leg2 = legend([shallowStr,'m',' - ',deepStr,'m'], ['deeper than ',deepStr,'m'],['shallower than  ',shallowStr,'m'],'Location', 'NorthWest');
% title('Volumetric Storage Efficiency over Gr');
% ylabel('Volumetric Storage Efficiency [m^3/m^3]');
% xlabel('Gr [-]');
% figureHandle = gcf;
% set(findall(figureHandle,'type','text'),'fontSize',iFontSize,'fontName',strFontName) 
% 
% 
% subplot(2,2,4)
% 
% p = plot(eps_small(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow),...
%     eps_sim_small(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow), 'x', ...
%     eps_small(raw{2}(indexShallowInjSmall) > deep), eps_sim_small(raw{2}(indexShallowInjSmall) > deep), 'x',...
%     eps_small(raw{2}(indexShallowInjSmall) < shallow), eps_sim_small(raw{2}(indexShallowInjSmall) < shallow), 'x',...
%     eps_large(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow),...
%     eps_sim_large(raw{2}(indexShallowInjSmall) < deep & raw{2}(indexShallowInjSmall) > shallow), 'o', ...
%     eps_large(raw{2}(indexShallowInjSmall) > deep), eps_sim_large(raw{2}(indexShallowInjSmall) > deep), 'o',...
%     eps_large(raw{2}(indexShallowInjSmall) < shallow), eps_sim_large(raw{2}(indexShallowInjSmall) < shallow), 'o');
%     set(p(1), 'color', [0 0 1], 'MarkerSize', 7);
%     set(p(2), 'color', [0 0.5 0], 'MarkerSize', 7);
%     set(p(3), 'color', [1 0 0], 'MarkerSize', 7);
%     set(p(4), 'color', [0 0 1], 'MarkerSize', 7);
%     set(p(5), 'color', [0 0.5 0], 'MarkerSize', 7);
%     set(p(6), 'color', [1 0 0], 'MarkerSize', 7);
% set(p, 'LineWidth',2);
% axis square;
% %axis([2e-4 5.5e-4 14 26]);
% set(gca,'FontSize',numberFontSize, 'fontName', strFontName);
% %h_leg = legend([shallowStr,'m',' - ',deepStr,'m'], ['deeper than ',deepStr,'m'],['shallower than  ',shallowStr,'m'],'Location', 'SouthWest');
% title('Volumetric Storage estimation Simulation over Okwen et al.');
% ylabel('Volumetric Storage Efficiency Simulation [kg CO_2  /m^2]');
% xlabel('Volumetric Storage Efficiency Okwen et al. [kg CO_2  /m^2] ');
% figureHandle = gcf;
% set(findall(figureHandle,'type','text'),'fontSize',iFontSize,'fontName',strFontName) 



% p = plot(grPlot,co2DomainDensityInjSmall, 'bx' , ...
%     grPlot, co2DomainDensityInjLarge, 'rx');
% set(p, 'LineWidth',2);
% axis square;
% set(gca,'FontSize',numberFontSize, 'fontName', strFontName);
% h_leg = legend('Injection Rate 0.1 Mt per year', 'Injection Rate 1.0 Mt per year','Location', 'SouthWest');
% ylabel('CO_2 Domain Density [kg/m^3]');
% xlabel('Gr [-]');
% figureHandle = gcf;
% set(findall(figureHandle,'type','text'),'fontSize',iFontSize,'fontName',strFontName) 

set(Fig,'position',[0 0, 27, 12.5])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,plotName,'epsc') 
