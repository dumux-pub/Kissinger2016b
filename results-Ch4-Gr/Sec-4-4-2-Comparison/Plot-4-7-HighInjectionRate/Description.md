# Plot the simulation results against the values of Gr and the storage efficiency calculated with the Okwen-Method for an injection rate of 1.0 MT per year

**Files:**

- **plotVarInj_withOkwen.m** - Plot script for Fig. 4.7

- **shallow_Inj1.0MTperA_20_Gr** - Results for 20 shallow simulations below 2500m. The data points are chosen to cover ten different equi-distant Gr values covering the whole range of Gr values.
The simulation results are obtained using the high injection rate.
