% Pick data points at different equi-distant depths covering depths from 
% 2500 - 4000m and create input files and folders for simulations.

clear all;

%Load database
load('databaseGr.mat');

%Input file name
fileNameInput = 'gravitationno.input';

%Parameters to change in the input file
strParameter{1} = 'DepthTOR ';
strParameter{2} = 'Temperature ';
strParameter{3} = 'Salinity ';
strParameter{4} = 'PressureAtTOR ';
inputParameter(1,:) = raw{2}; %DepthBOR in m
inputParameter(2,:) = raw{3} + 273.15; %Temperature in Kelvin
inputParameter(3,:) = salinity;
inputParameter(4,:) = pressure;
noSimulation = 10;


%Select points for deep selection
noSimulation = 10;
minDepth = 2500;
maxDepth = 4000;
for i=0:noSimulation-1
    deepSimulation(i+1) = minDepth + i*(maxDepth-minDepth)/(noSimulation-1);
end

%Choose depth values closetst to the grSimulation values

for j=1:noSimulation
    for i=1:length(gr_)
        depthDiff(j,i) = 1e20; %Large dummy value
        if(raw{2}(i) > minDepth)
           depthDiff(j,i) = abs(raw{2}(i) - deepSimulation(j));
        end
    end
    [value(j), indexGr(j)] = min(depthDiff(j,:));
    sc(j,1) = gr_(indexGr(j));
    sc(j,2) = pressure(indexGr(j));
    sc(j,3) = raw{3}(indexGr(j))+273.15;
end

%---> Reading from Parameter file

fprintf('> > > > > Reading from Parameter File: %s ... \n',fileNameInput);
fid = fopen(fileNameInput, 'rt');
i=0;
while feof(fid) == 0
   i=i+1;
   line = fgetl(fid);
   InputFileData{i}=strcat(line);      
end
fclose(fid);


%Preparation for Parallel Run
for l=1:noSimulation
    %---> Changing of Properties in Input File Data
    
    %Run in separated folders
    SimulationDirName(l)=strcat({'./DeepIndex_'},num2str(indexGr(l)));
    mkdir(SimulationDirName{l});
    
    for i=1:1:length(InputFileData)-1
        
        %Change the parameters
        for j=1:length(strParameter)
            %string = InputFileData{i}(1:length(strParameter{j}));
            if (findstr(InputFileData{i},strParameter{j})~=0)  
                InputFileData{i}=num2str(inputParameter(j,indexGr(l)));
                InputFileData(i)=strcat({[strParameter{j},' = ']},InputFileData(i)); %Formating        
            end
        end
        %Change the simulation names
%         if (findstr(InputFileData{i}, 'Name')~=0)
%            InputFileData{i}=num2str(CollocationPointsBase(l,1));
%            InputFileData(i)=strcat({'Name = '}, 'Snapshot', num2str(l)); %Formating        
%         end
    end
    

    
    %---> Writing to Parameter File
    %fid = fopen(fileNameInput,'w+');
    fid = fopen(strcat(SimulationDirName{l},'/',fileNameInput),'w+');
    for i=1:1:length(InputFileData)
        fprintf(fid,'%s\n',InputFileData{i});
    end
    fclose(fid); 
    
end


save('deepSelection.mat', 'indexGr', 'SimulationDirName');
