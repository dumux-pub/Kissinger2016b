% Pick data points at different equi-distant values of Gr covering depths 
% from 2500 - 4000m and create input files and folders for simulations.

clear all;

%Load database
load('databaseGr.mat');

%Input file name
fileNameInput = 'gravitationno.input';

%Parameters to change in the input file
strParameter{1} = 'DepthTOR ';
strParameter{2} = 'Temperature ';
strParameter{3} = 'Salinity ';
strParameter{4} = 'PressureAtTOR ';
inputParameter(1,:) = raw{2}; %DepthBOR in m
inputParameter(2,:) = raw{3} + 273.15; %Temperature in Kelvin
inputParameter(3,:) = salinity;
inputParameter(4,:) = pressure;
noSimulation = 10;


%Select points for deep selection
noSimulation = 10;
minDepth = 2500;
maxDepth = 4000;
for i=0:noSimulation-1
    grRange = gr_(raw{2} > minDepth & raw{2} < maxDepth);
    indexGrRangeToAll = find(raw{2} > minDepth & raw{2} < maxDepth);
    grSimulation(i+1) = min(grRange) + i*(max(grRange)-min(grRange))/(noSimulation-1);
end

%Choose gr values closetst to the grSimulation values

for j=1:noSimulation
    for i=1:length(grRange)
                
        grDiff(j,i) = abs(grRange(i) - grSimulation(j));
       
    end
    [value(j), indexGrRange(j)] = min(grDiff(j,:));
    sc(j,1) = gr_(indexGrRangeToAll(indexGrRange(j)));
    sc(j,2) = pressure(indexGrRangeToAll(indexGrRange(j)));
    sc(j,3) = raw{3}(indexGrRangeToAll(indexGrRange(j)))+273.15;
end

%---> Reading from Parameter file

fprintf('> > > > > Reading from Parameter File: %s ... \n',fileNameInput);
fid = fopen(fileNameInput, 'rt');
i=0;
while feof(fid) == 0
   i=i+1;
   line = fgetl(fid);
   InputFileData{i}=strcat(line);      
end
fclose(fid);


%Preparation for Parallel Run
for l=1:noSimulation
    %---> Changing of Properties in Input File Data
    
    %Run in separated folders
    SimulationDirName(l)=strcat({'./DeepIndexGr_'},num2str(indexGrRangeToAll(indexGrRange(l))));
    mkdir(SimulationDirName{l});
    
    for i=1:1:length(InputFileData)-1
        
        %Change the parameters
        for j=1:length(strParameter)
            %string = InputFileData{i}(1:length(strParameter{j}));
            if (findstr(InputFileData{i},strParameter{j})~=0)  
                InputFileData{i}=num2str(inputParameter(j,indexGrRangeToAll(indexGrRange(l))));
                InputFileData(i)=strcat({[strParameter{j},' = ']},InputFileData(i)); %Formating        
            end
        end
        %Change the simulation names
%         if (findstr(InputFileData{i}, 'Name')~=0)
%            InputFileData{i}=num2str(CollocationPointsBase(l,1));
%            InputFileData(i)=strcat({'Name = '}, 'Snapshot', num2str(l)); %Formating        
%         end
    end
    

    
    %---> Writing to Parameter File
    %fid = fopen(fileNameInput,'w+');
    fid = fopen(strcat(SimulationDirName{l},'/',fileNameInput),'w+');
    for i=1:1:length(InputFileData)
        fprintf(fid,'%s\n',InputFileData{i});
    end
    fclose(fid); 
    
end

indexGr = indexGrRangeToAll(indexGrRange(:));

save('deepSelectionGr.mat', 'indexGr', 'SimulationDirName');
