# Results

**Files:**

- **DeepSelection_10_Gr.m** Picks Results for 10 deep simulations between 2500 and 4000m.
The data points are chosen to cover ten different equi-distant Gr values within the depth range.
Creates the simulation folders and modifies and copies the Dumux input file (gravitationno.input) into each simulation folder.

- **deepSelection.mat** Mat-file containing the index of the datapoint and the folder name.

- **gravitationno.input** Dumux input file template.

- **readCO2mass.m** Reads the CO2 mass from each simulation at the end of the simulation (i.e. when the spill point is reached)
The values are stored into 'co2MassGrTest.mat'.

- **co2MassGrTest.mat**  Contains condensed simulation output.

- **DeepIndexGr_X**  Simulation folder.