% Plot the CO2 storage efficiency in kgCO2/m2 over Gr and over the storage efficiency
% calculated with the Okwen-Method

clear all;
%Plot name
plotName = 'Fig_4_06_co2MassOverGrAndOkwen';

%Load database
load('databaseGr.mat')


%Load simulation results 

%Load results for 20 shallow simulations below 2500m. The data points
%are chosen to cover 20 different equi-distant Gr values covering the whole range of Gr values.
load('shallow_Inj0.1MTperA_20_Gr/shallowSelection.mat','indexGr');
indexShallow20 =indexGr;
load('shallow_Inj0.1MTperA_20_Gr/co2MassGrTest.mat','co2Mass', 'deltaPressureMax');
massShallow20 = co2Mass;
deltaPressureMaxShallow20 = deltaPressureMax;

%Load results for 10 shallow simulations below 2500m. The data points
%are chosen to cover 10 different equi-distant Gr values covering the whole range of Gr values.
%The values are different to the 20 data points selected above.
load('shallow_Inj0.1MTperA_10_Gr/shallowSelection.mat','indexGr');
indexShallow10 =indexGr;
load('shallow_Inj0.1MTperA_10_Gr/co2MassGrTest.mat','co2Mass', 'deltaPressureMax');
massShallow = co2Mass;
deltaPressureMaxShallow = deltaPressureMax;

%Load results for 10 deep simulations between 2500 and 4000m. The data points
%are chosen to cover ten different equi-distant depths within the depth range.
load('deep_Inj0.1MTperA_10_depth/deepSelection.mat','indexGr');
indexDeep = indexGr;
load('deep_Inj0.1MTperA_10_depth/co2MassGrTest.mat','co2Mass');
massDeep = co2Mass;
deltaPressureMaxDeep = deltaPressureMax;

%Load results for 10 deep simulations between 2500 and 4000m. The data points
%are chosen to cover ten different equi-distant Gr values within the depth range.
load('deep_Inj0.1MTperA_10_Gr/deepSelection.mat','indexGr');
indexDeepGr = indexGr';
load('deep_Inj0.1MTperA_10_Gr/co2MassGrTest.mat','co2Mass');
massDeepGr = co2Mass;
deltaPressureMaxDeepGr = deltaPressureMax;

shallowDeepIndex = [indexShallow20 indexShallow10 indexDeep indexDeepGr];
shallowDeepMass = [massShallow20 massShallow massDeep massDeepGr];
shallowDeepPres = [deltaPressureMaxShallow20 deltaPressureMaxShallow deltaPressureMaxDeep deltaPressureMaxDeepGr];

reservoirThickness = 24; %m
reservoirVolume = pi*1000^2*reservoirThickness; %m3
%co2DomainDensity = shallowDeepMass.*48./reservoirVolume; %kg/m3
co2DomainDensity = shallowDeepMass.*48./pi/1000^2; %kg/m2
injectionRate = 0.1e9/365/24/3600; % kg/s
wellDiameter = 0.3; %m
injectionArea = pi*(wellDiameter/2)^2*reservoirThickness; %m2
specificInjectionRate = injectionRate/injectionArea; %kg/s/m2
%grPlot = gr_(shallowDeepIndex).*(1e-13/specificInjectionRate);
grPlot = gr_(shallowDeepIndex);
Srw = 0.3;
porosity = 0.2;

% Calculate storage efficiency according to the Okwen-Method

%Get the viscosities and densities from the Gr-Grid
viscosityCO2Select = viscosityCO2_(shallowDeepIndex);
viscosityBrineSelect = viscosityBrine_(shallowDeepIndex);
viscosityRatio = viscosityBrineSelect./viscosityCO2Select;
densityCO2Select = densityCO2_(shallowDeepIndex);
densityBrineSelect = densityBrine_(shallowDeepIndex);

%Calculate the gravitational number after Nordbotten and Celia 2006
tmp = 2*pi*9.81*1e-13*reservoirThickness^2*(densityBrineSelect - densityCO2Select).*densityCO2Select./viscosityBrineSelect;
Gr_nc_small = tmp/injectionRate;


eps_visc = (1-Srw)./viscosityRatio;

tmp = 1./((0.0324*viscosityRatio - 0.0952).*Gr_nc_small + (0.1778*viscosityRatio + 5.9682).*Gr_nc_small.^0.5 + 1.6962*viscosityRatio - 3.0472);
eps_grav_cor_small = 2*(1-Srw).*tmp;


eps_small = eps_visc;
eps_small(Gr_nc_small > 0.5) = eps_grav_cor_small(Gr_nc_small > 0.5);

mtotal_Okwen = reservoirThickness*porosity*densityCO2Select.*eps_small;


% Plot the data of Gr and Okwen methods

shallow = 1000;
deep = 2000;
shallowStr = num2str(shallow);
deepStr = num2str(deep);


strInterpreter = 'latex';
strFontName = 'Times';
iFontSize = 12;
tickFontSize = 10;
Fig = figure;
figure(Fig);
set(gcf, 'Units', 'centimeters');

subplot(1,2,1)

p = plot(grPlot(raw{2}(shallowDeepIndex) < deep & raw{2}(shallowDeepIndex) > shallow),...
    co2DomainDensity(raw{2}(shallowDeepIndex) < deep & raw{2}(shallowDeepIndex) > shallow),'x', ...
    grPlot(raw{2}(shallowDeepIndex) > deep), co2DomainDensity(raw{2}(shallowDeepIndex) > deep), 'x',...
    grPlot(raw{2}(shallowDeepIndex) < shallow), co2DomainDensity(raw{2}(shallowDeepIndex) < shallow), 'x');
    set(p(1), 'color', [0 0 1], 'MarkerSize', 7);
    set(p(2), 'color', [0 0.5 0], 'MarkerSize', 7);
    set(p(3), 'color', [1 0 0], 'MarkerSize', 7);
set(p, 'LineWidth',2);
axis square;
set(gca,'FontSize',tickFontSize, 'fontName', strFontName);
title('Storage Efficiency over Gr');
ylabel('SE Simulation [kgCO$_2$/m$^2$]');
xlabel('Gr [-]');
figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',iFontSize,'fontName',strFontName) 
h_leg = legend([shallowStr,'~m',' - ',deepStr,'~m'], ['deeper than ',deepStr,'~m'],['shallower than  ',shallowStr,'~m~'], ...
    'FontSize', iFontSize,'Location', 'SouthWest');

subplot(1,2,2)

p = plot(mtotal_Okwen(raw{2}(shallowDeepIndex) < deep & raw{2}(shallowDeepIndex) > shallow),...
    co2DomainDensity(raw{2}(shallowDeepIndex) < deep & raw{2}(shallowDeepIndex) > shallow),'x', ...
    mtotal_Okwen(raw{2}(shallowDeepIndex) > deep), co2DomainDensity(raw{2}(shallowDeepIndex) > deep), 'x',...
    mtotal_Okwen(raw{2}(shallowDeepIndex) < shallow), co2DomainDensity(raw{2}(shallowDeepIndex) < shallow), 'x');
    set(p(1), 'color', [0 0 1], 'MarkerSize', 7);
    set(p(2), 'color', [0 0.5 0], 'MarkerSize', 7);
    set(p(3), 'color', [1 0 0], 'MarkerSize', 7);
set(p, 'LineWidth',2);
axis square;
set(gca,'FontSize',tickFontSize, 'fontName', strFontName);
%h_leg = legend([shallowStr,'m',' - ',deepStr,'m'], ['deeper than ',deepStr,'m'],['shallower than  ',shallowStr,'m'],'Location', 'SouthWest');
title('Storage Efficiency over Okwen-Method');
ylabel('SE Simulation [kgCO$_2$/m$^2$]');
xlabel('SE Okwen-Method [kgCO$_2$/m$^2$]');
figureHandle = gcf;
set(findall(figureHandle,'type','text'),'fontSize',iFontSize,'fontName',strFontName, 'Interpreter',strInterpreter)
axis([0,300,150,450])

set(Fig,'position',[0 0, 27, 12.5])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,plotName,'epsc') 
