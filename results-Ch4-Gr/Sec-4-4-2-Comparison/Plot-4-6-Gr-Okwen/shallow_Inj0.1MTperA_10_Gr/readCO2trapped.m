clear all;
% Calculate the mass of trapped CO2 using the simulation results and the
% Land-Model and save the results in co2MassGrTest.mat

%Load simulation directory names
load('shallowSelection.mat');

%Load mat-file containing the box volumes
load('../boxVolume.mat');

%Name of the variables for which the vtu-output is parsed
variableNameList{1} = 'pw';
variableNameList{2} = 'Sn';
variableNameList{3} = 'rhoN';
variableNameList{4} = 'porosity';

%Load database
load('../../Sec-4-4-1-Gr/databaseGr.mat', 'gr_', 'raw');
eps = 1e-6;
%Name of the mat-file where the trapped CO2 mass is stored
outputMatFile = 'co2MassGrTest.mat';

%Land model parametrs
StMax = 0.3;
SnMax = 0.7;
C = 1/StMax - 1/SnMax;

for simIter=1:length(SimulationDirName)
    matFileName{simIter} = [SimulationDirName{simIter},'.mat'];
%    Check if mat file exists and load it
   if(exist(matFileName{simIter}) == 2)
       load(matFileName{simIter});
   else
        vtuReader([SimulationDirName{simIter},'/grTest.pvd'],matFileName{simIter},variableNameList);
        load(matFileName{simIter});
    end
    for timeIter=1:length(ref_Time)
        nodeNo = 0;
        for coordIter=1:length(ref_Coordinates(1,1,:))
            nodeNo = nodeNo + 1;
            if(ref_Coordinates(timeIter, 1, coordIter) < 0.15 + eps && ...
                    ref_Coordinates(timeIter, 2, coordIter) < eps && ...
                    ref_Coordinates(timeIter, 3, coordIter) < 1 + eps)
                pressureMax(simIter, timeIter) = ref_pw(timeIter, coordIter);
            end
            
        end
    end
    %land model
       temp = 1 + C.*ref_Sn(3,:);
       St = ref_Sn(3,:)./temp;
       massTrapped(simIter) = sum(ref_porosity(3,:).*St.*ref_rhoN(3,:).*ref_boxVolume);
       volumeCO2(simIter) = sum(ref_porosity(3,:).*ref_Sn(3,:).*ref_boxVolume);
       
       deltaPressureMax(simIter) = max(pressureMax(simIter, :))-pressureMax(simIter, 1);
       grSimulation(simIter) = gr_(indexGr(simIter)); 
       depthSimulation(simIter) = raw{2}(indexGr(simIter));
    
end

save(outputMatFile, 'deltaPressureMax', '-append','massTrapped', '-append', 'volumeCO2', '-append');

% Fig = figure;
% figure(Fig);
% 
% dastr = num2str(depthSimulation');
% %dastr2000 = num2str(depth2000');
% plothandle2 = plot(deltaPressureMax/1e5, grSimulation, 'rx');
%   %                co2Mass2000, grSimulation2000, 'bx', 'MarkerSize', 10);
% text( deltaPressureMax/1e5,   grSimulation, dastr, 'fontsize', 12);
% %text( co2Mass2000,   grSimulation2000, dastr2000, 'fontsize', 12);
% %, co2Mass2000, grSimulation2000, 'bx');
% %set(line, 'Color', [0. 0. 0.]+0.2, 'LineWidth', 2, 'LineStyle','-', 'Marker', 'none');
% grid on;
% 
% %set(gca,'FontSize',BFS)
% % title('Gr über M_{eff}','FontWeight', 'bold');
% set(gca,'FontSize',12);
% ylabel('Gr, [-]');
% xlabel('Max Pressure, [bar]');
% set(gca,'Color',[1 1 1]);
% set(gcf,'Color',[1 1 1]);
% 
% set(Fig,'position',[0 0, 400 400])
% set(Fig,'PaperPositionMode','Auto')
% saveas(Fig,fileNameOutput,'epsc')


