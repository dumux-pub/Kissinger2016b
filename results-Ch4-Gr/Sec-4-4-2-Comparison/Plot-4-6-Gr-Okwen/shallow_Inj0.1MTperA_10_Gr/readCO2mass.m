
clear all;
%Evaluate the simulations and store the mass obtained from simulation output
%files into the mat-file "co2MassGrTest.mat".

%Load database
load('databaseGr.mat')
%Name of the simulation output files
fileNameSimulationOutout = 'out.log';
%Load the simulation names
load('shallowSelection.mat');
%Array of simulation directories
dirName = SimulationDirName;


% Read the simulation out log files and store the target variable mass CO2
% in domain at the end of the simulation into co2Mass
for i=1:length(dirName)
    %grSimulationTest(i) = str2num(dirName{i}(length('./Gr_')+1:end));
    cd(dirName{i});
    system(['grep Storage ', fileNameSimulationOutout, ' > temp']);
    fid = fopen('temp', 'r');
    out{i} = textscan(fid, '%s %f %f','delimiter', ' ');
    co2Mass(i) = out{i}{3}(length(out{i}{3}));
    fclose(fid);
    system('rm temp');
    cd ..;
    densityCO2Simulation(i) = densityCO2_(indexGr(i));
    viscosityCO2Simulation(i) = viscosityCO2_(indexGr(i));
    depthSimulation(i) = raw{2}(indexGr(i));
    tempSimulation(i) = raw{3}(indexGr(i)) + 273.15;
    grSimulation(i) = gr_(indexGr(i));
end

save('co2MassGrTest','indexGr', 'co2Mass');

% display(abs(grSimulationTest-grSimulation));
% j=1;
% k=1;
% for i=1:length(co2Mass)
%     if(tempSimulation(i)/depthSimulation(i) > 0.323 )
%         grSimulation1000(j) = grSimulation(i);
%         co2Mass1000(j) = co2Mass(i);
%         depth1000(j) = depthSimulation(i);
%         j=j+1;
%     else
%         grSimulation2000(k) = grSimulation(i);
%         co2Mass2000(k) = co2Mass(i);
%         depth2000(k) = depthSimulation(i);
%         k=k+1;
%     end
% end
% 
% Fig1 = figure;
% figure(Fig1);
% 
% subplot(2,2,1);
% dastr1000 = num2str(depth1000');
% dastr2000 = num2str(depth2000');
% plothandle = plot(co2Mass1000, grSimulation1000, 'rx', ...
%                   co2Mass2000, grSimulation2000, 'bx', 'MarkerSize', 10);
% text( co2Mass1000,   grSimulation1000, dastr1000, 'fontsize', 12);
% text( co2Mass2000,   grSimulation2000, dastr2000, 'fontsize', 12);
% , co2Mass2000, grSimulation2000, 'bx');
% set(line, 'Color', [0. 0. 0.]+0.2, 'LineWidth', 2, 'LineStyle','-', 'Marker', 'none');
% grid on;
% 
% set(gca,'FontSize',BFS)
% title('Gr über M_{eff}','FontWeight', 'bold');
% set(gca,'FontSize',12);
% ylabel('Gr, [-]');
% xlabel('CO_2 Mass, [kg]');
% set(gca,'Color',[1 1 1]);
% set(gcf,'Color',[1 1 1]);
% 
% 
% subplot(2,2,2);
% set(line, 'Color', [0. 0. 0.]+0.2, 'LineWidth', 2, 'LineStyle','-', 'Marker', 'none');
% grid on;
% plot(co2Mass, depthSimulation, 'x');
% ylabel('Depth, [m]');
% xlabel('CO_2 Mass, [kg]');
% set(gca,'Color',[1 1 1]);
% set(gcf,'Color',[1 1 1]);
% 
% subplot(2,2,3)
% plot(co2Mass, grVcrConstSimulation, 'rx');
% set(line, 'Color', [0. 0. 0.]+0.2, 'LineWidth', 2, 'LineStyle','-', 'Marker', 'none');
% grid on;
% ylabel('Gr vcr=const, [-]');
% xlabel('CO_2 Mass, [kg]');
% set(gca,'Color)',[1 1 1]);
% set(gcf,'Color',[1 1 1]);
% 
% subplot(2,2,4);
% set(line, 'Color', [0. 0. 0.]+0.2, 'LineWidth', 2, 'LineStyle','-', 'Marker', 'none');
% grid on;
% plot(co2Mass, viscosityCO2Simulation, 'x');
% ylabel('ViscosityCO2, [kg/m^3]');
% xlabel('CO_2 Mass, [kg]');
% set(gca,'Color',[1 1 1]);
% set(gcf,'Color',[1 1 1]);
% 
% 
% set(Fig1,'position',[0 0, 400 400])
% set(Fig1,'PaperPositionMode','Auto')
% saveas(Fig1,fileNameOutput,'epsc')

