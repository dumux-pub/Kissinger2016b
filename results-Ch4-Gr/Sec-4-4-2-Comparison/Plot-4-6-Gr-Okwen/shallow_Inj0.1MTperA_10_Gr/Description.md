# Results

**Files:**

- **ShallowSelection.mat** Mat-file containing the index of the datapoint and the folder name.

- **gravitationno.input** Dumux input file template.

- **readCO2mass.m** Reads the CO2 mass from each simulation at the end of the simulation (i.e. when the spill point is reached)
The values are stored into 'co2MassGrTest.mat'.

- **readCO2masstrapped.m** Calculates the amount of trapped CO2 from the simulation results using the Land model.
The script also Calculates the maximum pressure during the injection. 
The values are stored into 'co2MassGrTest.mat'.

- **co2MassGrTest.mat**  Contains condensed simulation output.

- **ShallowIndex_X**  Simulation folder.