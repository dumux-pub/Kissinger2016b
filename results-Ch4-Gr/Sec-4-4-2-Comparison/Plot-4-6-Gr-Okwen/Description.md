# Plot the simulation results against the values of Gr and the storage efficiency calculated with the Okwen-Method

**Files:**

- **plotSelectedPoints_withOkwen.m** Plot script for Fig. 4.6

- **shallow_Inj0.1MTperA_20_Gr** Results for 20 shallow simulations below 2500m. The data points are chosen to cover ten different equi-distant Gr values covering the whole range of Gr values.

- **shallow_Inj0.1MTperA_10_depth** Results for 10 shallow simulations below 2500m. The data points are chosen to cover ten different equi-distant Gr values covering the whole range of Gr values. 
The values are different to the 20 simulation above.

- **deep_Inj0.1MTperA_10_depth** Results for 10 deep simulations between 2500 and 4000m. The data points are chosen to cover ten different equi-distant depths within the depth range.

- **deep_Inj0.1MTperA_10_Gr**  Results for 10 deep simulations between 2500 and 4000m. The data points are chosen to cover ten different equi-distant Gr values within the depth range.
