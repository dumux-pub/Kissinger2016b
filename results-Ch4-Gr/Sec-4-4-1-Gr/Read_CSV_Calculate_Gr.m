% Script for determining the gravitaion number with parameters depth,
% temperature and salinity out of a given csv table
%
%Density and viscosity are dependent on pressure and temperature in the 
%reservoir as well as on the composition of the fluids, 
%e.g. salt content of the brine phase. 
%The temperature can be  directly obtained from the above data. 
%However, to obtain the pressure and salinity, some assumptions have 
%to be made. Hydrostatic conditions are assumed for the pressure 
%calculations. The pressure is then dependent on the weight of the 
%overlying fluid column. A mean density of the fluid column is determined 
%by calculating an average density of the fluid column assuming a linear 
%increase of the density with depth. As the actual pressures are unknown 
%and highly uncertain, this is assumed to be a reasonable estimate. 
%The salinity can thus also be determined from the calculated 
%brine density at the data point.

clear all;

% Input file
fileNameInput = 'SK_sm_Attribute.csv';

% Output files
fileNameOutput = 'databaseGr.csv';
outMatFileName = 'databaseGr.mat';

%Gravity
g = 9.81; %[m/s2]


% Read the input file and store the columns in vectors
% raw{1} = pointID 
% raw{2} = depth [m]
% raw{3} = temperature [°C] 
% raw{4} = salt concentration [kg/m3]

fid = fopen(fileNameInput, 'r');
%header = textscan(fid, '%s %s %s %s', 'delimiter', ';');
raw = textscan(fid, '%d %f %f %f', 'HeaderLines', 1, 'delimiter', ';');
fclose(fid);



% initial values for pressure and salinity
densityBrineInit = 1000; % kg/m3
for i=1:length(raw{1})
    pressure(i) = densityBrineInit* g* raw{2}(i);
    salinity(i) = raw{4}(i)/ densityBrineInit;
    temperature(i) = raw{3}(i) + 273.15;
    densityCO2_(i) = densityCO2(temperature(i), pressure(i)/1e6);
    viscosityCO2_(i) = viscosityCO2(temperature(i), pressure(i));
    gr_(i) = grNumberConstMassFlux(densityBrineInit, densityCO2_(i), 1, viscosityCO2_(i), 1);
end

error = 1e-5;
eps = 1;
counter = 0;

while(counter < 15)     
    

    for i=1:length(raw{1})
               
        %densityWater_(i) = density_water_batzle(temperature(i) - 273.15, pressure(i)/1e6);
        densityBrine_(i) = densityBrine(temperature(i), pressure(i), salinity(i));
        viscosityBrine_(i) = viscosityBrine(temperature(i), pressure(i), salinity(i));
        densityCO2_(i) = densityCO2(temperature(i), pressure(i)/1e6);
        viscosityCO2_(i) = viscosityCO2(temperature(i), pressure(i));
        grOld_(i) = gr_(i);

        gr_(i) = grNumberConstMassFlux(densityBrine_(i), densityCO2_(i), 1, viscosityCO2_(i), 1);
        eps(i) = abs(gr_(i)/grOld_(i));
     
        pressure(i) =  (densityBrineInit+densityBrine_(i))/2* g* raw{2}(i);
        salinity(i) = raw{4}(i)/ densityBrine_(i);   
    end
    counter = counter + 1;
    fprintf('Iteration Number: %d \n', counter);
    fprintf('Max Error: %f \n', max(eps));
    fprintf('Min Error: %f \n', min(eps));
%     ex_pres(counter) = pressure(1000);
%     ex_sal(counter) = salinity(1000);
%     ex_gr(counter) = gr_(1000);
%     ex_densityBrine(counter) = densityBrine_(1000);
%     ex_densityCO2(counter) = densityCO2_(1000);
end

save(outMatFileName, 'raw', 'pressure', 'salinity', 'temperature', 'densityCO2_', ...
    'viscosityCO2_', 'densityBrine_', 'viscosityBrine_', 'gr_');




