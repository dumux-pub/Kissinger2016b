# Calculate the initial fluid propertiies from the original database of depth, temperature and salt concentration values of the Middle Buntsandstein in the North German Basin.

**FILES:**

- **SK_sm_Attribute.csv**  - CSV file containing depth, temperature and salt concentration data as well as point IDs from the ARCGIS shapefile.

- **Read_CSV_Calculate_Gr.m** - Matlab script for calculating pressure, salinity, density CO2, density Brine, viscosity CO2, viscosity Brine and Gr.

- **databaseGr.mat** - mat file containg all values calculated above as well as original database 

- **Fig4_03_AllConstRel3d.m** - Plot script for generating Fig. 4.3
