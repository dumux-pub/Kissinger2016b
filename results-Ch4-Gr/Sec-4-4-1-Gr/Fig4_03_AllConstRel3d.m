% Plot script for Figure 4.3 showing densities and viscosities of CO2 and
% brine over temperature, pressure and salinity as well as the datapoints
% of the database.

clear all;
%Load database
load('databaseGr.mat');
%Plot name
fileNameOutput = 'allConstRel3d.eps';


strInterpreter = 'latex';
strFontName = 'Times';
iFontSize = 13;
numberFontSize = 13;
strFontUnit = 'pixels';
iResolution = 150;
Fig = figure;
figure(Fig);
set(gcf, 'Units', 'centimeters');

%density CO2
subplot(2,2,1)

TMax = max(temperature);
TMin = min(temperature);
pMax = max(pressure);
pMin = min(pressure);
TCrit = 304.25;
PCrit = 7.38e6;
densityCrit = densityCO2(TCrit, PCrit/1e6);

[T_plot, p_plot] = ...
    meshgrid(TMin:(TMax-TMin)/100:TMax, ...
    pMin:(pMax-pMin)/100:pMax);

density_plot = arrayfun(@densityCO2, T_plot, p_plot/1e6);
viscosity_plot = arrayfun(@viscosityCO2, T_plot, p_plot);

surf(T_plot,p_plot/1e6,density_plot,'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','phong')
%daspect([5 5 1])
axis tight;
hold on;
plot3(temperature,pressure/1e6, densityCO2_,'.','MarkerSize',15, 'MarkerEdgeColor'...
    , 'r') %nonuniform
zlabel('Density CO_2 [kg/m^3]')
ylabel('Pressure [MPa]');
xlabel('Temperature [K]');
view(-20,30)
camlight left


%viscosity CO2
subplot(2,2,2)


surf(T_plot,p_plot/1e6,viscosity_plot,'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','phong')
%daspect([5 5 1])
axis tight;
hold on;
plot3(temperature,pressure/1e6, viscosityCO2_,'.','MarkerSize',15, 'MarkerEdgeColor'...
    , 'r') %nonuniform
zlabel('Dynamic Viscosity CO_2 [Pa s]')
ylabel('Pressure [MPa]');
xlabel('Temperature [K]');
% hold on;
% plot3(temperature(indexShallow(:)'), pressure(indexShallow(:)'), viscosityCO2_(indexShallow(:)'),'.','MarkerSize',30, 'MarkerEdgeColor'...
%     , 'k') %nonuniform
% hold on;
% plot3(temperature(indexDeep(:)'), pressure(indexDeep(:)'), viscosityCO2_(indexDeep(:)'),'.','MarkerSize',30, 'MarkerEdgeColor'...
%     , 'b') %nonuniform
view(-20,30)
camlight left



%density Brine
subplot(2,2,3)
TMax = max(temperature);
TMin = min(temperature);
salMax = max(salinity);
salMin = min(salinity);


T = 290:450;
Sal = [0.1 0.2 0.3];
P = 1e7;

for salIter=1:length(Sal)
    for TIter=1:length(T)
        densityBrineFct(salIter, TIter) = densityBrine(T(TIter), P, Sal(salIter));
    end
end

[T_plot, sal_plot] = ...
    meshgrid(TMin:(TMax-TMin)/100:TMax, ...
    salMin:(salMax-salMin)/100:salMax);
p = mean(pressure)/1e6;

for i=1:length(T_plot)
    for j=1:length(sal_plot)
        densityWater = density_water_batzle(T_plot(i,j)-273.15, p);
        density_plot(i,j) = density_brine_batzle(...
            T_plot(i,j)-273.15, p, sal_plot(i,j),densityWater);
        viscosity_plot(i,j) = viscosityBrine(T_plot(i,j), p, sal_plot(i,j));
    end
end
surf(T_plot,sal_plot,density_plot,'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','phong')
% daspect([5 5 1])
axis tight;
zlabel('Density Brine [kg/m^3]')
ylabel('Salinity [-]');
xlabel('Temperature [K]');
hold on;
% Calculate brine density for data points with constant pressure
% for dataIter=1:length(temperature)
%     densityWaterPConst_ = density_water_batzle(temperature(dataIter)-237.15, p);
%     densityBrinePConst_ = density_brine_batzle(temperature(dataIter)-237.15, p, salinity(dataIter), densityWaterPConst_);
%     plot3(temperature(dataIter),salinity(dataIter), densityBrinePConst_,'.','MarkerSize',15, 'MarkerEdgeColor'...
%         , 'r') %nonuniform
%     hold on;
% end
% plot3(TCrit, PCrit, densityCrit,'.','MarkerSize',20, 'MarkerEdgeColor'...
%     , 'k') %nonuniform
 view(-20,30)
 camlight left
 
 
%viscosity Brine
subplot(2,2,4)

set(gcf, 'Units', 'centimeters');

surf(T_plot,sal_plot,viscosity_plot,'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','phong')
%daspect([5 5 1])
axis tight;
hold on;
plot3(temperature,salinity, viscosityBrine_,'.','MarkerSize',15, 'MarkerEdgeColor'...
    , 'r') %nonuniform
zlabel('Dynamic Viscosity Brine [Pa s]')
ylabel('Salinity [-]');
xlabel('Temperature [K]');
%hold on;
% plot3(TCrit, PCrit, nsityCrit,'.','MarkerSize',20, 'MarkerEdgeColor'...
%     , 'k') %nonuniform
view(-20,30)
camlight left

set(Fig,'position',[0 0, 25, 25])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,fileNameOutput,'epsc') 
