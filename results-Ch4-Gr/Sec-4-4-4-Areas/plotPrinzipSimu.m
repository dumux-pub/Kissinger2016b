% Plot the CO2 storage efficiency in kgCO2/m2 over Gr for three areas with
% a low, medium, and high average value of Gr

clear all;

% Load mat file containing average values of pressure, temperature, 
% densities, viscosities, Gr, and salinity for each area
load('allAreas.mat');
fileNameOutput = 'prinzipSimulation.eps';
area = pi*1000^2; % area up to spill point

strInterpreter = 'latex';
strFontName = 'Times';
iFontSize = 13;
numberFontSize = 13;
strFontUnit = 'pixels';
iResolution = 150;
Fig = figure;
figure(Fig);
set(gcf, 'Units', 'centimeters');

subplot(1,2,1)

% Gr values from areas
gr = [gr_(1), gr_(1), gr_(1);...
    gr_(2), gr_(2), gr_(2);...
    gr_(3), gr_(3), gr_(3)];
 
% Mean mass and standard deviation obtained from Monte-Carlo simulations
% Column 1: Mean - standard deviation
% Column 2: Mean
% Column 3: Mean + standard deviation
 meff = [2.8632e6,    3.5509e6,    4.2385e6;... % Row 1: Area 3
      0.7703e7,    1.3264e7,    1.8825e7;...  % Row 2: Area 2
      1.2429e7,    1.8670e7,    2.4910e7];  % Row 3: Area 1
  
GrStr = ['   Large Gr'; '  Medium Gr'; '  Small Gr '];
areaStr = ['  Area 3'; '  Area 2'; '  Area 1'];
  
meanMeff = meff(:, 2)*48;
stdMeff = (meff(:, 2) - meff(:, 1))*48;

errorbar(gr(:, 2), meanMeff/area, stdMeff/area, 'rx', 'LineWidth', 2, 'MarkerSize', 10);
set(gca,'FontSize',12);
ylabel('Storage Efficiency , [kg/m^2]');
xlabel('Gr [-]');
text(gr(:, 2), meanMeff/area, areaStr, 'fontsize', 12);
set(gca,'Color',[1 1 1]);
set(gcf,'Color',[1 1 1]);
grid on;


subplot(1,2,2)

gr = [1, 2, 3];
% Mean mass obtained from simulations of constant Gr case and area-specific
% Gr case
meff = [ 4.3699e6,    1.3264e7, 1.6470e7;... % Row 1: Means constant Gr case
      3.5509e6,   1.3264e7 ,   1.8670e7];    % Row 2: Means area-specifc Gr case
  
 plot(gr,fliplr(meff(1,:))/area*48,'k-o', ...
    gr,fliplr(meff(2,:))/area*48,'r-x',...
    'LineWidth', 2, 'MarkerSize', 10);

set(gca, 'XTick',1:3, 'XTickLabel',{'Area 1' 'Area 2' 'Area 3'})
text(gr, fliplr(meff(2,:))/area*48,flipud(GrStr), 'fontsize', 12);
set(gca,'FontSize',12);
axis([0.5 3.5 0 400]);
ylabel('Storage Efficiency , [kg/m^2]');
legend('Constant Gr for all areas', 'Area-specific Gr','Location', 'NorthEast');
grid on;

set(Fig,'position',[0 0, 26, 12])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,fileNameOutput,'epsc') 