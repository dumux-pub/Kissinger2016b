# Results of the Monte-Carlo simulations

**Files:**

- **runAPC.m** Matlab-script for running the arbitrary polynomial chaos expansion (aPC).
It creates the snapshot folders and modifies and copies the Dumux input file (gravitationno.input) into each snapshot folder.

- **evalAPC.m** Evaluates the simulation results, builds the polynomial and runs Monte-Carlo simulations on this polynomial.
As an output storage efficiency and the standard deviation are displayed.

- **mat-files** The mat-files are required for storing the relevant data for the aPC.

- **Snapshot_X** Folder for simulations requried for setting up the polynomial. If the simulations need to be reproduced copy 
the executalbe (gravitationno) into each folder.

- **gravitationno.input** Input file with area specific pressure, temperature and salinity.
