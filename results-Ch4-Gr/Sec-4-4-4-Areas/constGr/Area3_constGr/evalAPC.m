clear all;
%Calculate the coefficients for the polynomial and run Monte-Carlo
%simulations. This script displays the mean CO2 mass as well as the
%standard deviations

fileNameSimulationOutout = 'out.log';
load('matrix.mat');
PolynomialBasisFileName{1} = 'PolynomialBasis_1.mat';
%PolynomialBasisFileName{2} = 'PolynomialBasis_2.mat';
N = length(PolynomialBasisFileName);
P = length(SnapshotDirName);

%Read the simulation out log files and store the target variable mass CO2
%in domain at the end of the simulation into co2Mass
for i=1:length(SnapshotDirName)
    cd(SnapshotDirName{i});
    system(['grep Storage ', fileNameSimulationOutout, ' > temp']);
    fid = fopen('temp', 'r');
    raw{i} = textscan(fid, '%s %f %f','delimiter', ' ');
    co2Mass(i) = raw{i}{3}(length(raw{i}{3}));
    fclose(fid);
    system('rm temp');
    cd ..;
end

%Use the previously calculated moments matrix and the solution vector
%co2Mass to calculate the coeffcients of the polynomial
coeff = CoeffMatrix\co2Mass';

%Polinomial MC+PCM Computation
MCsize=size(MC_Vector);
MCsize=MCsize(2);

for j=1:1:P;
    for ii=1:1:N;     
        load(PolynomialBasisFileName{ii},'Polynomial');
        MC_Poly(ii,j,:)=polyval(Polynomial(1+PolynomialDegree(j,ii),length(Polynomial):-1:1), ...
            MC_Vector(ii,:));       
    end
end

%display(MC_Poly);


for l=1:MCsize;
    TargetVarMC(l)=0;
    for j=1:1:P;
        multi=1;
        for ii=1:1:N;
            multi=multi*MC_Poly(ii,j,l);
        end
        TargetVarMC(l)=TargetVarMC(l)+coeff(j)*multi;
    end
end
%checkUnique(TargetVarMC);
% display(co2Mass);
%MC = [[MC_Vector'; CollocationPointsBase], [TargetVarMC'/1e8; co2Mass'/1e8]];
%display(MC);

meanTargetVarMC = mean(TargetVarMC);
stdTargetVarMC = std(TargetVarMC);
meandTargetVarMCMinusStd = meanTargetVarMC - stdTargetVarMC;
meandTargetVarMCPlusStd = meanTargetVarMC + stdTargetVarMC;
display([meandTargetVarMCMinusStd meanTargetVarMC meandTargetVarMCPlusStd]);


%----- Numerical Computation TargetVariable CDF
for k = 1:1:MCsize;
    relSum(k)=(k-1)/(MCsize)+1/(2*MCsize);
end
sortTargetVarMC =  sort(TargetVarMC);

%plot(sortTargetVarMC,relSum,'--');
plot(sortTargetVarMC,relSum,'--', meanTargetVarMC, 0.5, 'x');
set(line, 'Color', [0. 0. 0.]+0.2, 'LineWidth', 2, 'LineStyle','-', 'Marker', 'none');
grid on;
%set(gca,'FontSize',BFS)
title('CDF of CO_2 Mass','FontWeight', 'bold');
%set(gca,'FontSize',LFS)
xlabel('CO_2 Mass, [kg]');
ylabel('Probability');
set(gca,'Color',[1 1 1]);
set(gcf,'Color',[1 1 1]);
set(gcf,'Position',[100 200 600 400]);
hold on;

