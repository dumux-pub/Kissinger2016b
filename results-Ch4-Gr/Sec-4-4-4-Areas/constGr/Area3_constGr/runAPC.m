% ========================================================================
% Arbitraty Integrative Probabilistic Collocation Method
% Author: Dr. Sergey Oladyshkin
% SRC SimTech,Universitaet Stuttgart, Pfaffenwaldring 61, 70569 Stuttgart
% E-mail: Sergey.Oladyshkin@iws.uni-stuttgart.de
% ========================================================================
% modified by Lena Walter
% modified by Alexander Kissinger
% changes for parallel runs

clear all;

%Filenames:

%Filename of data points
%fileNameDataPoints = 'thickn_poro_südostbrandenburg_1.csv';

%Filename for dumux parameter file

fileNameInput = 'gravitationno.input';
%Filename for the executable
%fileNameExecutable = 'gravitationno';

%FilenameGridFile
%fileNameGridFile = 'pieceofcake.dgf';

%Filename of the mat-file for storing collocation points base and the
%matrix
fileNameOutput = 'matrix.mat';


%Parameters:

N=1; %Number of Uncertanties and Design Parameters
d=3; %d-order polynomial
P=factorial(N+d)/(factorial(N)*factorial(d)); %Total number of terms


%Data distribution for Gr principal simulations
%Uniform distribution between:
%Porosity
 
%Vector reservoir thickness
%  MC_Vector(1,:)=raw{2};  
%  strParameter{1} = 'Thickness';
%Vector porosity
 MC_Vector(1,:)= unifrnd(0.04,0.08,1,1000);
 strParameter{1} = 'ReservoirPorosity';
 
fprintf('\n');
fprintf('______________________________________________________________________________________________________________\n');
fprintf('______________________________________________________________________________________________________________\n');
fprintf('\n> > > > > Arbitraty Integrative Probabilistic Collocation Method \n');
fprintf('\n> > > > > aPC.: massive stochastic model reduction \n');
fprintf('\n> > > > > Module A: Preparation for parallel Snapshot Simulations\n');
fprintf('\n');
tic
 
 [CoeffMatrix, CollocationPointsBase, PolynomialDegree] = createMatrixAndCollocationPoints(N, d, MC_Vector);
 
 
 
%-------------------------------------------------------------------------------------------
%---------- I. P. C. M.: Snapshot Computation: Deterministic Computational Model Call ------
%-------------------------------------------------------------------------------------------
fprintf('\n> > > > > aPC: Snapshots Computation ...\n');
fprintf('\n . . . . . . . . . . . . . . . . . . . . . . . . . . .\n\n\n');

%---> Reading from Parameter file

fprintf('> > > > > aPC: Reading from Parameter File: %s ... \n',fileNameInput);
fid = fopen(fileNameInput, 'rt');
i=0;
while feof(fid) == 0
   i=i+1;
   line = fgetl(fid);
   InputFileData{i}=strcat(line);      
end
fclose(fid);
%Preparation for Parallel Run
for l=1:P;
    %---> Changing of Properties in Input File Data
    
    %Run in separated folders
    SnapshotDirName(l)=strcat({'./Snapshot_'},num2str(l));
    mkdir(SnapshotDirName{l});
    
    for i=1:1:length(InputFileData)-1
        
        %Change the parameters
        for j=1:N
            if (findstr(InputFileData{i},strParameter{j})~=0)
                InputFileData{i}=num2str(CollocationPointsBase(l,j));
                InputFileData(i)=strcat({[strParameter{j},' = ']},InputFileData(i)); %Formating        
            end
        end
        %Change the simulation names
        if (findstr(InputFileData{i}, 'Name')~=0)
           InputFileData{i}=num2str(CollocationPointsBase(l,1));
           InputFileData(i)=strcat({'Name = '}, 'Snapshot', num2str(l)); %Formating        
        end
    end
    

    
    %---> Writing to Parameter File
    %fid = fopen(fileNameInput,'w+');
    fid = fopen(strcat(SnapshotDirName{l},'/',fileNameInput),'w+');
    for i=1:1:length(InputFileData)
        fprintf(fid,'%s\n',InputFileData{i});
    end
    fclose(fid);
    
    %Copy the Executable, Run and Grid File
    %copyfile(fileNameExecutable,SnapshotDirName{l});
    %copyfile(fileNameGridFile,SnapshotDirName{l});
    %copyfile(fileNameRunScript,SnapshotDirName{l});
    
    %Start the simulation
    %system(['./',  SnapshotDirName{l},'/',fileNameRunScript]);
    
    
end
fprintf('Writing output mat file \n');
save(fileNameOutput ,'SnapshotDirName', 'CoeffMatrix', ...
'CollocationPointsBase','PolynomialDegree', 'MC_Vector');

 