clear all;
%This script displays the mean CO2 mass as well as the
%standard deviations. No polynomial is used the mean CO2 mass and the
%standard deviation of it are directly calculated from the nine data
%points.

clear all;
fileNameSimulationOutout = 'out.log';
load('matrix.mat');
fileNameOutput = '~/doc/CO2brim/co2brim/treffen/121128Hannover/mittleregr.eps';

%Read the simulation out log files and store the target variable mass CO2
%in domain at the end of the simulation into co2Mass
for i=1:length(SnapshotDirName)
    cd(SnapshotDirName{i});
    system(['grep Storage ', fileNameSimulationOutout, ' > temp']);
    fid = fopen('temp', 'r');
    raw{i} = textscan(fid, '%s %f %f','delimiter', ' ');
    co2Mass(i) = raw{i}{3}(length(raw{i}{3}));
    fclose(fid);
    system('rm temp');
    cd ..;
end

TargetVarMC = co2Mass;
MCsize = length(co2Mass);
meanTargetVarMC = mean(TargetVarMC);
stdTargetVarMC = std(TargetVarMC);
meandTargetVarMCMinusStd = meanTargetVarMC - stdTargetVarMC;
meandTargetVarMCPlusStd = meanTargetVarMC + stdTargetVarMC;
display([meandTargetVarMCMinusStd meanTargetVarMC meandTargetVarMCPlusStd]);

%----- Numerical Computation TargetVariable CDF
for k = 1:1:MCsize;
    relSum(k)=(k-1)/(MCsize)+1/(2*MCsize);
end
sortTargetVarMC =  sort(TargetVarMC);


Fig1 = figure;
figure(Fig1);
%plot(sortTargetVarMC,relSum,'--');
plot(sortTargetVarMC,relSum,'--', ...
    sortTargetVarMC,relSum,'kx',...
    'MarkerSize', 10);
%set(line, 'Color', [0. 0. 0.]+0.2, 'LineWidth', 2, 'LineStyle','-', 'Marker', 'none');
grid on;
line([1 1],[0 1],'LineWidth',2);
line([meanTargetVarMC-stdTargetVarMC meanTargetVarMC-stdTargetVarMC],...
    [0 1],'LineWidth',2,'Color', 'r');
line([meanTargetVarMC+stdTargetVarMC meanTargetVarMC+stdTargetVarMC],...
    [0 1],'LineWidth',2,'Color', 'r');

%set(gca,'FontSize',BFS)
title('Summenkurve der CO_2 Masse','FontWeight', 'bold', 'FontSize', 12);
set(gca,'FontSize',12);
xlabel('CO_2 Masse , [kg]');
ylabel('Wahrscheinlichkeit, [-]');
set(gca,'Color',[1 1 1]);
set(gcf,'Color',[1 1 1]);

set(gcf,'Position',[0 0 400 400]);
set(Fig1,'PaperPositionMode','Auto')
saveas(Fig1,fileNameOutput,'epsc')



