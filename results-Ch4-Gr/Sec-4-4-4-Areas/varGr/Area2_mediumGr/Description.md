# Results of the Monte-Carlo simulations. Here no arbitrary polynomial chaos expansion (aPC) is performed. Instead nine simulations for the nine data values for porosity and thickness are directly simulated.

**Files:**

- **runAPC.m** Matlab-script creates the snapshot folders and modifies and copies the Dumux input file (gravitationno.input) into each snapshot folder.
It uses the data from 'thickn_poro_südostbrandenburg_1.csv' to prepare the simulation folder.

- **evalAPC.m** Evaluates the simulation results. As an output storage efficiency and the standard deviation are displayed.

- **matrix.mat** Contains the simulation folder names.

- **thickn_poro_südostbrandenburg_1.csv** Contains data on porosity and thickness for Area 2.

- **Snapshot_X** Folder for simulations requried for setting up the polynomial. 
If the simulations need to be reproduced copy the executalbe (gravitationno) into each folder.

- **gravitationno.input** Input file with area specific pressure, temperature and salinity.
