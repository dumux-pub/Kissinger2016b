% Cake cake grid
clear;

pathdgf = 'pieceofcake.dgf';
pathvtk = 'pieceofcake.vtk';
R = 5000; % Domain radius
A = 2*pi/48; % Domain angle
Z = 35; % Domain length in z direction
nRBSP = 100; %approximate no of elements on R before spill point
nRASP = 10; %approximate no of elements on R after spill point
nSlice = 2; %no of slices
nZ = 40; %approximate no of elements in Z direction
minCellSizeRBSP = 1; %Minimum cell size in R at the well
maxCellSizeRBSP = 10; %Max cell size in R before the spill point
minCellSizeRASP = 10; %Minimum cell size in Rafter the spill point
maxCellSizeRASP = 500; %Max cell size in R after the spill point
cellSizeZ = Z/nZ;
wellRadius = 0.15; %Well radius at the tip of the cake slice
radiusSpillPoint = 1000; %Radius of the spill point from the well
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%dr Vector
dr1 = logSpacer(nRBSP, radiusSpillPoint, minCellSizeRBSP, maxCellSizeRBSP, 0);
dr2 = logSpacer(nRASP, R, minCellSizeRASP, maxCellSizeRASP, radiusSpillPoint + minCellSizeRASP);
dr = [dr1, dr2];

%da Vector
da = 0:A/nSlice:A;

%Z-Vector
dz = 0:Z/nZ:Z;

%Create the dgf file
dgfWriterCake(pathdgf, dr, da, dz, wellRadius);

%Create the vtk file
vtkWriterCake(pathvtk, dr, da, dz, wellRadius);

noNodes = length(dr)*length(da)*length(dz);
display(noNodes);
