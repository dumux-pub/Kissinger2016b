clear all;
%This script simply reads the data pairs for porosity and reservoir
%thickness from a csv file and generates nine simulation input files. The
%aPC is not used for this case. The matrix.mat is used to store the
%simulation names.

%Filenames:

%Filename of data points
fileNameDataPoints = 'thickn_poro_südostbrandenburg_1.csv';

%Filename for dumux parameter file

%[filePathInput, fileNameInput, ext] = fileparts('../gravitationno.input');
fileNameInput = 'gravitationno.input';

%Filename for the executable
%fileNameExecutable = 'gravitationno';

%FilenameGridFile
%fileNameGridFile = 'pieceofcake.dgf';

%Filename of the mat-file for storing collocation points base and the
%matrix
fileNameOutput = 'matrix.mat';


%Parameters:

N=2; %Number of Uncertanties and Design Parameters
%d=3; %d-order polynomial
%P=factorial(N+d)/(factorial(N)*factorial(d)); %Total number of terms


%Data distribution for Gr principal simulations
fid = fopen(fileNameDataPoints, 'r');
raw = textscan(fid, '%d %f %f', 'HeaderLines', 1, 'delimiter', ';');
 
%Vector reservoir thickness
 MC_Vector(1,:)=raw{2};  
 strParameter{1} = 'Thickness =';
%Vector porosity
 MC_Vector(2,:)=raw{3}/100;
 strParameter{2} = 'ReservoirPorosity =';

% fprintf('\n> > > > > Snapshots Computation ...\n');
% fprintf('\n . . . . . . . . . . . . . . . . . . . . . . . . . . .\n\n\n');

%---> Reading from Parameter file

fprintf('> > > > > aPC: Reading from Parameter File: %s ... \n',fileNameInput);
fid = fopen(fileNameInput, 'rt');
i=0;
while feof(fid) == 0
   i=i+1;
   line = fgetl(fid);
   InputFileData{i}=strcat(line);      
end
fclose(fid);
%Preparation for Parallel Run
for l=1:length(MC_Vector);
    %---> Changing of Properties in Input File Data
    
    %Run in separated folders
    SnapshotDirName(l)=strcat({'./Snapshot_'},num2str(l));
    mkdir(SnapshotDirName{l});
    
    for i=1:1:length(InputFileData)-1
        
        %Change the parameters
        for j=1:N
            if (findstr(InputFileData{i},strParameter{j})~=0)
                InputFileData{i}=num2str(MC_Vector(j,l));
                InputFileData(i)=strcat({strParameter{j}},InputFileData(i)); %Formating        
            end
        end
        %Change the simulation names
        if (findstr(InputFileData{i}, 'Name')~=0)
           %InputFileData{i}=num2str(CollocationPointsBase(l,1));
           InputFileData(i)=strcat({'Name = '}, 'Snapshot', num2str(l)); %Formating        
        end
    end
    

    
    %---> Writing to Parameter File
    %fid = fopen(fileNameInput,'w+');
    fid = fopen(strcat(SnapshotDirName{l},'/',fileNameInput),'w+');
    for i=1:1:length(InputFileData)
        fprintf(fid,'%s\n',InputFileData{i});
    end
    fclose(fid);
    
    %Copy the Executable, Run and Grid File
    %copyfile(fileNameExecutable,SnapshotDirName{l});
    %copyfile(fileNameGridFile,SnapshotDirName{l});
    %copyfile(fileNameRunScript,SnapshotDirName{l});
    
    %Start the simulation
    %system(['./',  SnapshotDirName{l},'/',fileNameRunScript]);
    
    
end
fprintf('Writing output mat file \n');
save(fileNameOutput ,'SnapshotDirName','MC_Vector');

 