# Plot the simulation results for the three areas using Monte-Carlo simulations for two cases: variable Gr and constant Gr (i.e. variable fluid properties and constant fluid properties)

**Files:**

- **allAreas.mat** Mat-file containing initial fluid properties of the three selected areas.

- **plotPrinzipSimu.m** Plot-script for Fig. 4.9.

- **varGr** Folder containing results for the three areas for the variable Gr case.

- **constGr** Folder containing results for the three areas for the constant Gr case.
