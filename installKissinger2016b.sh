#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### DUNE modules
git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-istl.git
git clone -b releases/2.4 https://gitlab.dune-project.org/core/dune-localfunctions.git

### alugrid
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
cd dune-alugrid
git checkout b97734c1a40f965ed551f6ff04b6fe4933ee045e
cd ..

### dumux
git clone -b releases/2.8 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git

git clone -b master https://git.iws.uni-stuttgart.de/dumux-pub/Kissinger2016b.git

### apply patches
cd dumux
patch -p0 < ../Kissinger2016b/dumux-Kissinger2016b/appl/co2/co2brim/solling/dfm.patch
patch -p0 < ../Kissinger2016b/dumux-Kissinger2016b/appl/co2/co2brim/solling/episode.patch
patch -p0 < ../Kissinger2016b/dumux-Kissinger2016b/appl/co2/co2brim/solling/gridcreator.patch
cd ..

### dunecontrol
./dune-common/bin/dunecontrol --opts=Kissinger2016b.opts all
