# Readme for the dumux-pub module of the Dissertation of Alexander Kissinger entitled _Basin-Scale Site Screening and Investigation of Possible Impacts of CO2 Storage on Subsurface Hydrosystems_

The aim of this repository is to make it easier to reproduce the main plots and simulations of the dissertation. For your convenience please find the [BibTex entry here](https://git.iws.uni-stuttgart.de/dumux-pub/Kissinger2016b/blob/master/kissinger-etal2016.bib).

==============================

# Installation

You can build the module just like any other DUNE module. 
For building and running the executables, go to the folders containing
the sources listed above. For the basic dependencies see dune-project.org.

The easiest way is to use the installKissinger2016b.sh in this folder here.
You might want to look at it before execution [here](https://git.iws.uni-stuttgart.de/dumux-pub/Kissinger2016b/installKissinger2016b.sh). Create a new folder containing the script and execute it.

You might want to proceed as below in a terminal:
- `mkdir kissinger2016`
- `cd kissinger2016`
- `wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Kissinger2016b/raw/master/Kissinger2016b.opts` (if this does not work, copy the **Kissinger2016b.opts** into this folder)
- `wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Kissinger2016b/raw/master/installKissinger2016b.sh` (if this does not work, copy the **installKissinger2016b.sh** into this folder)
- `chmod u+x installKissinger2016b.sh`
- `./installKissinger2016b.sh`

===============================

## Installation details

The script ./installKissinger2016b.sh will extract all necessary modules and apply all necessary patches which are given below:

When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.

| Module | Branch | Revision number |
| ------ | ------ | --------------- |
| dune-common | releases/2.4 | 7c4f09c116e0f7a053e84935cc5c4d9e0e0adb5a |
| dune-geometry | releases/2.4 | 2bf6adc505050c132135ab680e480c8eabd5f87b |
| dune-grid | releases/2.4 | 4d0c95d0df6be8b36d9e3dcfc381c207e9a8e771 |
| dune-istl | releases/2.4 | f40f33096c811f21e2760b3b6b8bb0c67ac1da28 |
| dune-localfunctions | releases/2.4 | d00f8be5a05a775febe572f30681120b6f626acd |
| dune-alugrid | master | b97734c1a40f965ed551f6ff04b6fe4933ee045e |
| dumux | releases/2.8 | 2521d491a1ac5a7a41c4a3a53737eccf523610c8 |

dumux patched with:
- dfm.patch
- episode.patch
- gridcreator.patch

Patches can be found in **dumux-Kissinger2016b/appl/co2/co2brim/solling**
apply `patch -p0 < PATCHNAME.patch` in dumux 2.8 folder

===============================

# Matlab-scripts for output

Both the results of Chapter 4 and 5 use a number of Matlab-scripts to create the Figures given in the dissertation from the simulation output. In order to run these scripts a number of functions, which are given in  the folder **matlab-functions** have to be included in the Matlab search path. Most conveniently the path of this folder can be added to the Matlab **startup.m**: `addpath(genpath('PATH/TO/MATLAB/FUNCTIONS'),genpath('PATH/TO/GR/DATABASE'));`

Here the first entry specifies the path to the folder **matlab-functions** and the second entry specifies the path the Gr-database.

==============================

# Folder Structure

- **dumux-Kissinger2016b** Contains the dune module where the executables can be compiled.

- **grid-Ch4-Gr** Contains the grid used in Chapter 4.

- **grid-Ch5-BrineDisp** Contains the grid used in Chapter 5.

- **matlab-functions** Contains necessary Matlab-functions for pre- and post-processing.

- **results-Ch4-Gr** Contains results for Chapter 4.

- **results-Ch5-BrineDisp** Contains results for Chapter 5.

All subfolders are structured according to the section numbering used in the dissertation and the required results should therefore be easy to find. Where necessary description files (**Description.md**) have been created to explain the files within a folder.

==============================

# Reproducing results for Chapter 4

- All simulation results are prepared in the **results-Ch4-Gr** folder with configured parameter input files.
- The result plots can be obtained without running the simulations again as all relevant outout is given in the simulation output files (**out.log**). The Matlab-scripts can be run to create the plots.
- Compile executable "gravitationno" in your cmake build directory, i.e. `dumux-kissinger2016/build-cmake/appl/co2/co2brim/gravitationno`, then copy the executable to the appropriate location where the simulation-specific input file is given and run the executable: ./gravitationno

==============================

## Interpretation of output

The main target variable the mass of CO2 when the CO2 plume has reached the spill point. The simulation output on the console looks as follows after a certain time step:

`Storage: 9.54725e+09 1.49961e+07`

Here the second value it the mass of CO2 in the domain in kg.

==============================

# Reproducing results for Chapter 5

- All scenarios are prepared in the **results-Ch5-BrineDisp** folder with configured parameter input files.
- The result plots can be obtained without running the simulations again as all relevant outout is given in the simulation output files (**out.log**). The Matlab-scripts can be run to create the plots.
- As the simulations in Chapter 5 hava a very high computational demand it is suggested to run these simulations on a cluster having suffucient memory (more than 8GB per process).
- Unpack grids in the **grid-Ch5-BrineDisp** folder, i.e. `tar -jxvf grid-CH5-BrineDisp.tbz2`
- Compile executable **solling** in your cmake build directory, i.e. **dumux-kissinger2016/build-cmake/appl/co2/co2brim/solling**
- Default model is complex geometry with water injection
- If you want to use the generic or two-layer geometry or CO2 instead of brine injection, change the macros `GEOMETRY` and `MODELTYPE` given in the file **solling.cc** (**dumux-kissinger2016//appl/co2/co2brim/solling/solling.cc**) and recompile.
- Copy executables into the scenario specific sub-folder of the results folder that contains an input file (**solling.input**)
- Most model parameters can be changed in the input files if wanted
- run: `./solling` for generic geometry run: `./solling -ParameterFile solling_generic.input`
- For saltpool problem compile (Sec. 5.4.2) compile executable **saltpool** in your cmake build directory, i.e. **dumux-kissinger2016/build-cmake/appl/co2/co2brim/saltpool**, then copy and run the executable. 

================================

## Some abreviations used in this work for the geological layers within the code and input files

| Abbreviation | Layer |
| -------- | -------- |
| quar1 | Top most layer Quaternary 1 |
| quar2 | Second layer from top Quaternary 2 |
| ter | Tertiary Post-Rupelian (Miozän) |
| rc | Rupelian clay barrier |
| oli | Tertiary post-Rupelian |
| kreide | Cretaceous |
| obs | Upper Buntsandstein |
| mbs | Upper Middle Buntsandstein |
| sol | Solling - injection horizon |
| umbs | Lower Middle Buntsandstein |
| ubs | Lower Buntsandstein |
| zec | Permian Zechstein |
| tz | Fault zone |

================================

## Episodes

The model uses for episodes which can be controlled through the input file

- **Episode 1** Pressure initialization - Concentrations are held constant in the domain and the pressure is allowed to evolve according to the brine density dependent on pressure, salt concentration and temperature. 
This takes place within one time step. The mass is reduced by the "MassReductionFactor" in the input file in order decrease the storage term and quickly obtain a stationary solution.

- **Episode 2** Salt initialization - The salt is allowed to distribute according to the boundary conditions of the system. The parameter "InitializationOnly" (if set true)  in the input file allows for a grid file (dgf) to be written which contains results for the pressure and salt distribution. This distribution can be used for various injection runs.

- **Episode 3** Injection period - Injection starts.

- **Episode 4** Post-injection period - time after injection as specified in the input file

==================================

## Interpretation of output

- After every episode vtu output is written. Optional output for time steps can be set in the input file using "TimeStepOutput" parameter
- Output after every time step is written into the console. This output can be used for creating the 2D plots over time as done with the various Matlab-scripts.
- The term "flow around salt wall" is equivalent to flow over fault zone as used in the dissertation
- The term "flow across Rupel" is equivalent to flow over the intact Rupel, excluding the fault zone and the hydrogeological windows

==================================

## Exemplary output with explanation:

Time [s]: 3153600006.222222;  
Time step size [s]: 21756245.15233088;  
Storage PV 0 [mol Brine]: 2.561261449489266e+17;  
Storage PV1 [mol NaCl]: 9775140286297026;  
Maximum difference between last and current time step PV0 [Pa]: 2362.151332728565;  
Located at PV0: 715081.3800000001 5966603.93 2678;  
Maximum difference between last and current time step PV1 [-]: 9.573448490007852e-06;  
Located at PV1: 733638.63 5999376.31 2350;  
Injection Pw [Pa]: 17157200.58487338;  
Measurement Point 1 Pw [Pa]: 29687240.78991911;  
Measurement Point 2 Pw [Pa]: 19701057.45628934;  
Total mass flow around salt wall [kg/s]: -0.5304106270569433;  
Salt flow around salt wall [kgNaCl/s]: -0.1181669553463467;  
Volume flow around salt wall [m3/s]: -0.0004559866913020654;  
Total mass flow across Rupel [kg/s]: -0.3194185283035135;  
Salt flow across Rupel [kgNaCl/s]: -0.1167015168537419;  
Volume flow across Rupel [m3/s]: -0.0002971878852282452;  
Total mass flow across windows Rupel [kg/s]: -0.5798006234798538;  
Salt flow across windows Rupel [kgNaCl/s]: -0.01057867862513299;  
Volume flow across windows Rupel [m3/s]: -0.0005728746319864103;  
Total mass flow into target aquifers [kg/s]: -1.429629778840319;  
Salt flow into target aquifers [kgNaCl/s]: -0.2454471508252229;  
Volume flow into target aquifers [m3/s]: -0.001326049208516719;  
Total mass flow across lateral boundary [kg/s]: 0.01001248235499519;  
Salt flow across lateral boundary [kgNaCl/s]: -0.00134896410806145;  
Volume flow across lateral boundary [m3/s]: 1.135104008407905e-05;  
Total mass flow into Quar2 [kg/s]: -1.431029392957535;  
Salt flow into Quar2 [kgNaCl/s]: -0.1681637257059847;  
Volume flow into Quar2 [m3/s]: -0.001325826453642238;  
Total mass flow into Quar1 [kg/s]: -1.430871887729429;  
Salt flow into Quar1 [kgNaCl/s]: -0.1608726537274452;  
Volume flow into Quar1 [m3/s]: -0.001314906020357363;  
