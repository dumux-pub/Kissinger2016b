# Simulations for the injection and post-injection period for the 1p1c and the 1p2c model.

Each folder contains an model-specific input file and an output file (out.log) with the results. The reference simulation uses the grid obtained from the initialization period as an initial condition. On this grid the initialized primary variables are stored.

**Files:**

- **plot_1c_vs_2c_brine.m** Plot script for Fig. 5.21

- **reference** Reference scenario with 1p2c model.

- **bar1e-18** Reference scenario with 1p1c model.

