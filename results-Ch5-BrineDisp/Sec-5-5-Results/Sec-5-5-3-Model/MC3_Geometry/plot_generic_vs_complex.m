%Plot the flux into the target aquifers.
%Comparison generic 1p2c vs complex 1p2c
%Calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the number of log-files to be evaluated
% - divide the flux by the injection rate to make the flux dimensionless

clear all;
plotName = 'Fig-5-24-generic_vs_complex_brine';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;
fluxLateralIdx = 6;
M1Idx = 7;
M2Idx = 8;
initTimeIdx = 9;

eps = 1e4;

%Volumetric injection rate [m3/s], injection rate [kg/s]
injectionRate = 24.5136; % [kg/s] brine injection rate reference case
injectionDensity = 1078; % [kg/m3] initial brine density injection point reference case
volInjectionRate = injectionRate/injectionDensity; % [m3/s]

%array of logfiles to read
logFileName{1} = 'bar1e-18/out.log';
logFileName{2} = 'reference/out.log';
% logFileName{2} = 'highpermfault.log';
% logFileName{3} = 'generic_nosalt.log';
% logFileName{3} = 'generic_nosalt.log';
% logFileName{4} = 'complex_nosalt.log';
titel{1} = 'Total volume flow';
titel{2} = 'Flow over fault zone and windows';
titel{3} = 'Pressure at M1 - 6.2 km from IP';
titel{4} = 'Pressure at M2 - 13.5 km from IP';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
strList{fluxFaultIdx} = 'Volume flow around salt wall [m3/s]:';
strList{fluxRupelIdx} = 'Volume flow across Rupel [m3/s]:';
strList{fluxHolesIdx} = 'Volume flow across windows Rupel [m3/s]:';
strList{fluxTotalIdx} = 'Volume flow into target aquifers [m3/s]:';
strList{M1Idx} = 'Measurement Point 1 Pw [Pa]:';
strList{M2Idx} = 'Measurement Point 2 Pw [Pa]:';
strList{initTimeIdx} = 'Initialization took:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    output = outputReader(strList, logFileName{logIter});
    tinit(logIter) = output{initTimeIdx};
    tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
    time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter))/365/24/3600;
    fluxTotal{logIter} = (output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx})))*(-1)/volInjectionRate;
    fluxHoles{logIter} = (output{fluxHolesIdx}(tInitIdx(logIter):length(output{fluxHolesIdx})))*(-1)/volInjectionRate;
    fluxFault{logIter} = (output{fluxFaultIdx}(tInitIdx(logIter):length(output{fluxFaultIdx})))*(-1)/volInjectionRate;
    fluxRupel{logIter} = (output{fluxRupelIdx}(tInitIdx(logIter):length(output{fluxRupelIdx})))*(-1)/volInjectionRate;
    M1{logIter} = output{M1Idx}(tInitIdx(logIter):length(output{M1Idx})) - output{M1Idx}(tInitIdx(logIter));
    M2{logIter} = output{M2Idx}(tInitIdx(logIter):length(output{M2Idx})) - output{M2Idx}(tInitIdx(logIter));
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements foound for search patterns');
    end
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
set(gcf, 'Units', 'centimeters');
Fig = figure(...
    'PaperUnits','centimeters', ...
    'PaperPosition',[0 , 0, 19, 17]);
%'PaperType','a4letter',...
%'PaperOrientation','Portrait',...
%'PaperUnits','centimeters', ...
%'ActivePositionProperty','Position', ....
%'PaperPosition',[0 , 0, 15.89, 15.89]);
%'PaperPosition',[2.54 , 2.54, 18.43, 18.43]);

co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0.5 0 0.5;
      0 0 0];
  
%symbols  
sy{1} = '-';
sy{2} = '--';
sy{3} = ':';
sy{4} = '-.';

for subplotIter=1:4
    
    subplot(2,2,subplotIter);
    if(subplotIter == 1)
        fluxplot = plot(time{1}(time{1}<=timePlot), fluxTotal{1}(time{1}<=timePlot), sy{1},...
            time{2}(time{2}<=timePlot), fluxTotal{2}(time{2}<=timePlot), sy{2});
        h_title = title(titel{subplotIter});
        h_leg = legend('Generic', 'Complex','Location', 'NorthEast');
    end
    if(subplotIter == 2)
        fluxplot = plot(time{1}(time{1}<=timePlot), fluxHoles{1}(time{1}<=timePlot),sy{1}, ...
            time{2}(time{2}<=timePlot), fluxHoles{2}(time{2}<=timePlot),sy{2}, ...
            time{1}(time{1}<=timePlot), fluxFault{1}(time{1}<=timePlot),sy{3}, ...
            time{2}(time{2}<=timePlot), fluxFault{2}(time{2}<=timePlot), sy{4});
        h_title = title(titel{subplotIter});
        set(fluxplot(1), 'color', co(1,:));
        set(fluxplot(2), 'color', co(2,:));
        set(fluxplot(3), 'color', co(1,:));
        set(fluxplot(4), 'color', co(2,:));
        h_leg = legend('Generic: h. windows', 'Complex: h. windows','Generic: fault zone', 'Complex: fault zone','Location', 'NorthEast');
    end
    if(subplotIter == 3)
        fluxplot = plot(time{1}(time{1}<=timePlot), M1{1}(time{1}<=timePlot)/1e5, sy{1},...
            time{2}(time{2}<=timePlot), M1{2}(time{2}<=timePlot)/1e5, sy{2});
        h_title = title(titel{subplotIter});
        axis([0.0,100,0.0,10])
        h_leg = legend('Generic', 'Complex','Location', 'NorthEast');
    end
    if(subplotIter == 4)
        fluxplot = plot(time{1}(time{1}<=timePlot), M2{1}(time{1}<=timePlot)/1e5,sy{1}, ...
            time{2}(time{2}<=timePlot), M2{2}(time{2}<=timePlot)/1e5, sy{2});
        h_title = title(titel{subplotIter});    
        axis([00.,100,0.0,0.5])
        h_leg = legend('Generic', 'Complex','Location', 'NorthEast');
        
    end
    set(h_title,'Interpreter',strInterpreter, 'FontSize', iFontSize)
    if(subplotIter < 3)
        ylabel('Vol. flow/injection rate [-]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
        axis([0,100,-0.1,1.0])
    else
        ylabel('Pressure increase [bar]', 'FontSize', iFontSize, 'Interpreter',strInterpreter);
        if(subplotIter == 3)
            axis([0.0,100,0.0,10])
        else
            axis([00.,100,0.0,1])
        end
    end
    xlabel('Time [Years]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
    set(fluxplot, 'LineWidth',1.5);
    %Set the font size of the axis ticks
    set(gca,'FontSize',tickFontSize)
    set(gca, 'XTick', [0, 20, 40, 60, 80, 100]);
    
    axis square;
%     grid on;
    set(h_leg,'Interpreter',strInterpreter, 'FontSize', iFontSize);
end
  
  display('Error at injection end, generic vs complex injection');
  display(1-max(fluxTotal{1}(time{1}<=timePlot))/max(fluxTotal{2}(time{2}<=timePlot)))

saveas(Fig,plotName,'epsc')
%This function fixes the issue of badly exported dots in the line style
fix_dottedline([plotName, '.eps']);