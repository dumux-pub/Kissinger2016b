# Simulations for the injection and post-injection period for the generic and complex geometry both carried out with the 1p2c model.

Each folder contains an model-specific input file and an output file (out.log) with the results. The simulation using the complex geometry use the grid obtained from the initialization period as an initial condition. On this grid the initialized primary variables are stored. The simulation with the generic geometry use a simplified grid which does not contain initialized primary variables.

**Files:**

- **plot_generic_vs_complex.m** Plot script for Fig. 5.24

- **reference** Reference scenario with the complex geometry 1p2c model.

- **bar1e-18** Reference scenario with generic geometry 1p2c model. To compile model for generic geometry change solling.cc: `#define GEOMETRY 1` and compile.

