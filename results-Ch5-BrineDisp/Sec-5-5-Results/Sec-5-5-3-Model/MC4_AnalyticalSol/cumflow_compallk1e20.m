%Integrate the flow into the target aquifers (cummulative flow) for 50
%years of injection. The cummulative flow is shown in Table 5.6.
%Comparison of all models for Upper Buntsandsteni permeability of k=1e-20m2
%Calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the number of log-files to be evaluated
% - Integrate the flow into the target aquifers and divide it by the
% cummulatve injected volume over 50 years to make it dimensionless

clear all;
plotName{1} = 'compall_k1e20';

%define the indices for result and search lists
timeIdx = 1;
fluxTotalIdx = 2;
fluxFaultIdx = 3;
fluxHolesIdx = 4;
initTimeIdx = 5;

%Volumetric injection rate [m3/s], injection rate [kg/s]
injectionRate = 24.5136; % [kg/s] brine injection rate reference case
injectionDensity = 1078; % [kg/m3] initial brine density injection point reference case
volInjectionRate = injectionRate/injectionDensity; % [m3/s]
injectedVolume = volInjectionRate*50*365*24*3600; % [m3]

eps = 1e4;
%array of logfiles to read
logFileName{1} = 'bar1e-20_CO2Inj/out.log';
logFileName{2} = 'bar1e-20_reference/out.log';
logFileName{3} = 'bar1e-20_NoSalttransport/out.log';
logFileName{4} = 'bar1e-20_GenericGeometry/out.log';

% Get analytical solution from .fig for the averaged injection layer
openfig('analyticalD_average.fig', 'invisible');
data=get(gca,'Children');
xAnalyticalD_average=get(data,'XData')*365*24*3600; % [s] get the x data and convert to s
%get the y data,
%multiply with volumetric injection rate and density of 1p2c reference at injection
yAnalyticalD_average=get(data,'YData')*volInjectionRate; % [m3] convert to volume rate

% % Get analytical solution from .fig for the non-averaged injection layer
% openfig('analyticalD_noaverage.fig', 'invisible');
% data=get(gca,'Children');
% xAnalyticalD_noaverage=get(data,'XData'); %get the x data 
% %get the y data,
% %multiply with volumetric injection rate and density of 1p2c reference at injection
% yAnalyticalD_noaverage=get(data,'YData'); 

titel{1} = 'Total flow $k = 10^{-20}$ m$^2$';
titel{2} = 'Flow over fault zone $k = 10^{-20}$ m$^2$';

legendStr{1} = '2p3c';
legendStr{2} = '1p2c';
legendStr{3} = '1p1c';
legendStr{4} = 'generic';
legendStr{5} = 'analytic: $D_{inj}=0.161';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
strList{fluxFaultIdx} = 'Volume flow around salt wall [m3/s]:';
strList{fluxHolesIdx} = 'Volume flow across windows Rupel [m3/s]:';
strList{fluxTotalIdx} = 'Volume flow into target aquifers [m3/s]:';
strList{initTimeIdx} = 'Initialization took:';

t50yr = 50*365*24*3600;
%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    output = outputReader(strList, logFileName{logIter});
    tinit(logIter) = output{initTimeIdx};
    tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
    time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter)); % in seconds
    flux{logIter}{fluxTotalIdx} = output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx}))* (-1);
    cumflow{logIter}{fluxTotalIdx} = trapz(time{logIter}((time{logIter} -t50yr)<eps),...
        flux{logIter}{fluxTotalIdx}((time{logIter} -t50yr)<eps))/injectedVolume; %integrate total flow for 50years and divide by injected volume
    flux{logIter}{fluxFaultIdx} = output{fluxFaultIdx}(tInitIdx(logIter):length(output{fluxFaultIdx}))* (-1);
    cumflow{logIter}{fluxFaultIdx} = trapz(time{logIter}((time{logIter} -t50yr)<eps), ...
        flux{logIter}{fluxFaultIdx}((time{logIter} -t50yr)<eps))/injectedVolume; %integrate flow over fault zone for 50years and divide by injected volume
    flux{logIter}{fluxHolesIdx} = output{fluxHolesIdx}(tInitIdx(logIter):length(output{fluxHolesIdx}))* (-1);
    cumflow{logIter}{fluxHolesIdx} = trapz(time{logIter}((time{logIter} -t50yr)<eps), ...
        flux{logIter}{fluxHolesIdx}((time{logIter} -t50yr)<eps))/injectedVolume; %integrate flow over windows for 50years and divide by injected volume
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements foound for search patterns');
    end
    if(logIter == 3)
        %Base flow 1p1c:
        baseflow1p1c(fluxFaultIdx) = output{fluxFaultIdx}(tInitIdx(3))*-1*t50yr/injectedVolume;
        baseflow1p1c(fluxHolesIdx) = output{fluxHolesIdx}(tInitIdx(3))*-1*t50yr/injectedVolume;
    end
end

%integrate the averaged analytical solution
 cumflow{length(logFileName)+1}{fluxTotalIdx}=trapz(xAnalyticalD_average, yAnalyticalD_average)/injectedVolume;
 cumflow{length(logFileName)+1}{fluxFaultIdx}=trapz(xAnalyticalD_average, yAnalyticalD_average)/injectedVolume;
 cumflow{length(logFileName)+1}{fluxHolesIdx}=0;
 
 %display values in console
display('[Model] [Total volume] [Fault zone] [windows]');
for logIter = 1:length(logFileName)+1
    cumflow{logIter}{1}=legendStr{logIter};
    display(cumflow{logIter});
end
cumflow{3}{1}=[legendStr{3}, ' (no base flow)'];
cumflow{3}{fluxFaultIdx}=cumflow{3}{fluxFaultIdx}-baseflow1p1c(fluxFaultIdx);
cumflow{3}{fluxHolesIdx}=cumflow{3}{fluxHolesIdx}-baseflow1p1c(fluxHolesIdx);
display(cumflow{3});