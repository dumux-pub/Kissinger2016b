%Plot the flux into the target aquifers.
%Comparison of all models for Upper Buntsandsteni permeability of k=1e-20m2
%Calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the number of log-files to be evaluated
% - divide the flux by the injection rate to make the flux dimensionless

clear all;
plotName{1} = 'compall_k1e20';

%define the indices for result and search lists
timeIdx = 1;
fluxTotalIdx = 2;
fluxFaultIdx = 3;
fluxHolesIdx = 4;
initTimeIdx = 5;

%Volumetric injection rate [m3/s], injection rate [kg/s]
injectionRate = 24.5136; % [kg/s] brine injection rate reference case
injectionDensity = 1078; % [kg/m3] initial brine density injection point reference case
volInjectionRate = injectionRate/injectionDensity; % [m3/s]

eps = 1e4;
%array of logfiles to read
logFileName{1} = 'bar1e-20_CO2Inj/out.log';
logFileName{2} = 'bar1e-20_reference/out.log';
logFileName{3} = 'bar1e-20_NoSalttransport/out.log';
logFileName{4} = 'bar1e-20_GenericGeometry/out.log';

% Get analytical solution from .fig for the averaged injection layer
openfig('analyticalD_average.fig', 'invisible');
data=get(gca,'Children');
xAnalyticalD_average=get(data,'XData'); %get the x data 
%get the y data,
%multiply with volumetric injection rate and density of 1p2c reference at injection
yAnalyticalD_average=get(data,'YData'); 

% Get analytical solution from .fig for the non-averaged injection layer
openfig('analyticalD_noaverage.fig', 'invisible');
data=get(gca,'Children');
xAnalyticalD_noaverage=get(data,'XData'); %get the x data 
%get the y data,
%multiply with volumetric injection rate and density of 1p2c reference at injection
yAnalyticalD_noaverage=get(data,'YData'); 

titel{1} = 'Total flow $k = 10^{-20}$ m$^2$';
titel{2} = 'Flow over fault zone $k = 10^{-20}$ m$^2$';

legendStr{1} = '2p3c';
legendStr{2} = '1p2c';
legendStr{3} = '1p1c';
legendStr{4} = 'generic';
legendStr{5} = 'analytic: $D_{inj}=0.161';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
strList{fluxFaultIdx} = 'Volume flow around salt wall [m3/s]:';
strList{fluxHolesIdx} = 'Volume flow across windows Rupel [m3/s]:';
strList{fluxTotalIdx} = 'Volume flow into target aquifers [m3/s]:';
strList{initTimeIdx} = 'Initialization took:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    output = outputReader(strList, logFileName{logIter});
    tinit(logIter) = output{initTimeIdx};
    tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
    time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter))/365/24/3600; % in years
    flux{logIter}{fluxTotalIdx} = output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx}))* (-1)/volInjectionRate;
    flux{logIter}{fluxFaultIdx} = output{fluxFaultIdx}(tInitIdx(logIter):length(output{fluxFaultIdx}))* (-1)/volInjectionRate;
    flux{logIter}{fluxHolesIdx} = output{fluxHolesIdx}(tInitIdx(logIter):length(output{fluxHolesIdx}))* (-1)/volInjectionRate;
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements foound for search patterns');
    end
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
Fig = figure();
set(gcf, 'Units', 'centimeters');
%specify color vector
co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0.5 0 0.5;
      0 0 0
      0.5 0.5 0.5];
  
%symbols  
sy{1} = '-';
sy{2} = '--';
sy{3} = ':';
sy{4} = '-.';
  
%Map flux index to subplotiter
fluxIter = [fluxTotalIdx, fluxFaultIdx];

for subplotIter=1:2
    
    subplot(1,2,subplotIter);
    %Loop over the log-files
    for logIter = 1:length(logFileName)
        fluxplot = plot(time{logIter}(time{logIter}<=timePlot), ...
            flux{logIter}{fluxIter(subplotIter)}(time{logIter}<=timePlot), sy{logIter}, 'color', co(logIter,:));
        set(fluxplot, 'LineWidth',1.5);
        hold on;
    end
    fluxplot = plot(xAnalyticalD_average, yAnalyticalD_average, 'color', co(logIter+1,:));
    h_title = title(titel{subplotIter});
    set(fluxplot, 'LineWidth',1.5);
    
    if(subplotIter==2)
        fluxplot = plot(xAnalyticalD_noaverage, yAnalyticalD_noaverage, 'color', co(logIter+2,:));
        h_title = title(titel{subplotIter});
        set(fluxplot, 'LineWidth',1.5);
        x1 = 30;
        y1 = 0.83;
        txt1 = 'Zeidouni: $D_{inj}=0.913$';
        text(x1,y1,txt1, 'Interpreter',strInterpreter, 'FontSize', tickFontSize)
    end

    set(h_title,'Interpreter',strInterpreter, 'FontSize', iFontSize)
    ylabel('Vol. flow/injection rate [-]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
    xlabel('Time [Years]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
    %Set the font size of the axis ticks
    set(gca,'FontSize',tickFontSize)
    axis([0,100,-0.1,1.0])
    set(gca, 'XTick', [0, 20, 40, 60, 80, 100]);
    axis square;
%     grid on;
end
%     h_leg = legend(legendStr, 'Location', 'NorthEast');
%     set(h_leg,'Interpreter',strInterpreter, 'FontSize', iFontSize);
    hold off;

    set(gcf, 'Units','centimeters', 'Position',[0 0 19 17])
    set(gcf, 'PaperPositionMode','auto')
    saveas(Fig,plotName{1},'epsc')
        %This function fixes the issue of badly exported dots in the line style
    fix_dottedline([plotName{1}, '.eps']);

