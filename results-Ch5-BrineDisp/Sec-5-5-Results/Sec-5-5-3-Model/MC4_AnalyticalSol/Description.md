# Simulations for the injection and post-injection period for the all model types including the analytical Zeidouni-Method.

Each folder contains an model-specific input file and an output file (out.log) with the results. The simulation using the complex-geometry 1p2c or 2p3c models use the grid obtained from the initialization period as an initial condition. On this grid the initialized primary variables are stored.

**Files:**

- **plot_compallk1e18.m** Plot script for Fig. 5.26.

- **plot_compallk1e20.m** Plot script for Fig. 5.26.

- **cumflow_compallk1e18.m** Script for obtaining values for Table 5.6.

- **cumflow_compallk1e20.m** Script for obtaining values for Table 5.6.

- **Faultleakage_multilayer.m** Script for obtaining results for the analytical Zeidouni-Method for the case when the Middle Buntsandstein values are not averaged.

- **Faultleakage_multilayer_averagae.m** Script for obtaining results for the analytical Zeidouni-Method for the case when the Middle Buntsandstein values are averaged into one layer.

- **analyticalD_average.fig** Results for the analytical Zeidouni-Method for the case when the Middle Buntsandstein values are averaged into one layer.

- **analyticalD_noaverage.fig** Results for the analytical Zeidouni-Method for the case when the Middle Buntsandstein values are not averaged into one layer.

- **bar1e-XX_Y** Simulation folders with different Upper Buntsandstein barrier permeability (*XX*) and for different models (Y).
