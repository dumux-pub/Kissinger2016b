# Simulations for the injection and post-injection period for the 2p3c and the 1p2c model.

Each folder contains an model-specific input file and an output file (out.log) with the results. The 2p3c and the 1p2c simulation use the grid obtained from the initialization period as an initial condition. On this grid the initialized primary variables are stored.

**Files:**

- **plot_2p3c_vs_1p2c_brine.m** Plot script for Fig. 5.22

- **reference** Reference scenario with 1p2c model.

- **bar1e-18** Reference scenario with 2p3c model. To compile 2p3c model change solling.cc: `#define MODELTYPE 2` and compile.

