%Plot the flux into the target aquifers.
%Comparison 2p3c vs 1p2c
%Calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the number of log-files to be evaluated
% - divide the flux by the injection rate to make the flux dimensionless

clear all;
plotName = 'Fig-5-22-2p3c_vs_1p2c_brine';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;
fluxLateralIdx = 6;
initTimeIdx = 7;

%Volumetric injection rate [m3/s], injection rate [kg/s]
injectionRate = 24.5136; % [kg/s] brine injection rate reference case
injectionDensity = 1078; % [kg/m3] initial brine density injection point reference case
volInjectionRate = injectionRate/injectionDensity; % [m3/s]

eps = 1e4;

%array of logfiles to read
% logFileName{1} = '2p3c.log';
logFileName{1} = 'bar1e-18/out.log';
logFileName{2} = 'reference/out.log';
titel{1} = 'Total volume flow';
titel{2} = 'Flow over fault zone and windows';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
strList{fluxFaultIdx} = 'Volume flow around salt wall [m3/s]:';
strList{fluxRupelIdx} = 'Volume flow across Rupel [m3/s]:';
strList{fluxHolesIdx} = 'Volume flow across windows Rupel [m3/s]:';
strList{fluxTotalIdx} = 'Volume flow into target aquifers [m3/s]:';
strList{initTimeIdx} = 'Initialization took:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    output = outputReader(strList, logFileName{logIter});
    tinit(logIter) = output{initTimeIdx};
    if(isempty(max(find(abs(output{timeIdx} - tinit(logIter)) < eps))))
        error('Could not find specified initial time.');
    end
    tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
    time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter))/365/24/3600;
    fluxTotal{logIter} = output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx}))*-1/volInjectionRate;
    fluxHoles{logIter} = output{fluxHolesIdx}(tInitIdx(logIter):length(output{fluxHolesIdx}))*-1/volInjectionRate;
    fluxFault{logIter} = output{fluxFaultIdx}(tInitIdx(logIter):length(output{fluxFaultIdx}))*-1/volInjectionRate;
    fluxRupel{logIter} = output{fluxRupelIdx}(tInitIdx(logIter):length(output{fluxRupelIdx}))*-1/volInjectionRate;
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements foound for search patterns.');
    end
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
set(gcf, 'Units', 'centimeters');
Fig = figure(...
    'PaperUnits','centimeters', ...
    'PaperPosition',[0 , 0, 19, 17]);
%'PaperType','a4letter',...
%'PaperOrientation','Portrait',...
%'PaperUnits','centimeters', ...
%'ActivePositionProperty','Position', ....
%'PaperPosition',[0 , 0, 15.89, 15.89]);
%'PaperPosition',[2.54 , 2.54, 18.43, 18.43]);


co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0.5 0 0.5;
      0 0 0];
  
%symbols  
sy{1} = '-';
sy{2} = '--';
sy{3} = ':';
sy{4} = '-.';

for subplotIter=1:2
    
    subplot(2,2,subplotIter);
    if(subplotIter == 1)
        fluxplot = plot(time{1}(time{1}<=timePlot), fluxTotal{1}(time{1}<=timePlot),sy{1}, ...
            time{2}(time{2}<=timePlot), fluxTotal{2}(time{2}<=timePlot), sy{2});
             h_title = title(titel{subplotIter});
        h_leg = legend('2p3c', '1p2c','Location', 'NorthEast');
    end
    if(subplotIter == 2)
        fluxplot = plot(time{1}(time{1}<=timePlot), fluxHoles{1}(time{1}<=timePlot), sy{1},...
            time{2}(time{2}<=timePlot), fluxHoles{2}(time{2}<=timePlot), sy{2},...
            time{1}(time{1}<=timePlot), fluxFault{1}(time{1}<=timePlot), sy{3},...
            time{2}(time{2}<=timePlot), fluxFault{2}(time{2}<=timePlot), sy{4});

        h_title = title(titel{subplotIter});
        set(fluxplot(1), 'color', co(1,:));
        set(fluxplot(2), 'color', co(2,:));
        set(fluxplot(3), 'color', co(1,:));
        set(fluxplot(4), 'color', co(2,:));
        h_leg = legend('2p3c: h. windows', '1p2c: h. windows','2p3c: fault zone', '1p2c: fault zone','Location', 'NorthEast');
    end
    if(subplotIter == 3)
        fluxplot = plot(time{1}(time{1}<=timePlot), fluxFault{1}(time{1}<=timePlot), ...
            time{2}(time{2}<=timePlot), fluxFault{2}(time{2}<=timePlot));
        h_title = title(titel{subplotIter});
    end
    if(subplotIter == 4)
        fluxplot = plot(time{1}(time{1}<=timePlot), fluxRupel{1}(time{1}<=timePlot), ...
            time{2}(time{2}<=timePlot), fluxRupel{2}(time{2}<=timePlot));
        h_title = title(titel{subplotIter});
        
    end
    set(fluxplot, 'LineWidth',1.5);
    set(h_title,'Interpreter',strInterpreter, 'FontSize', iFontSize)
    ylabel('Vol. flow/injection rate [-]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
    xlabel('Time [Years]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
    %Set the font size of the axis ticks
    set(gca,'FontSize',tickFontSize)
    axis([0,100,-0.1,1.0])
    axis square;
%     grid on;
    set(h_leg,'Interpreter',strInterpreter, 'FontSize', iFontSize);
end

  display('Max Error, CO2 vs water injection');
  display(1-max(fluxTotal{2}(time{2}<=timePlot))/max(fluxTotal{1}(time{1}<=timePlot)))

saveas(Fig,plotName,'epsc')
%This function fixes the issue of badly exported dots in the line style
fix_dottedline([plotName, '.eps']);