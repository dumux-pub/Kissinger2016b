# Simulation folders for the initialization period for the 1p2c model.

Each folder contains the input file for running the initialization period of a specific scenario as given in Fig. 5.16. The main simulation output is a dgf-grid where the primary variables for each degree of freedom are stored. These grids (containing the primary variables) are stored in *Kissinger2016b/grid-Ch5-BrineDisp* with their respective scenario names. The results for the injection period are calculated in *Kissinger2016b/results-Ch5-BrineDisp/Sec-5-5-Results/Sec-5-5-2-Scenario* based on these grids.

**Files:**

- **reference** Sceanrio 1 in Fig. 5.16

- **grad_1-0** Sceanrio 2 in Fig. 5.16

- **grad_2-0** Sceanrio 3 in Fig. 5.16

- **disp_large** Sceanrio 4 in Fig. 5.16

- **disp_small** Sceanrio 5 in Fig. 5.16

- **low-perm** Sceanrio 6 in Fig. 5.16

- **no_diff** Sceanrio 7 in Fig. 5.16

