%Plot the flux into the target aquifers.
%Scenario: variable boundary conditions.
%Calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the nuber of log-files to be evaluated
% - divide the flux by the injection rate to make the flux dimensionless

clear all;
plotName = 'Fig-5-17-bccompare';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;
fluxLateralIdx = 6;
initTimeIdx = 7;


eps = 1e4;
injectionRate = 24.9;
%array of logfiles to read
logFileName{1} = 'reference/out.log';
logFileName{2} = 'bc_noflow/out.log';
logFileName{3} = 'bc_dirichlet/out.log';

titel{1} = 'Total mass flow';
titel{2} = 'Mass flow over fault zone';


%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
strList{fluxFaultIdx} = 'Total mass flow around salt wall [kg/s]:';
strList{fluxRupelIdx} = 'Total mass flow across Rupel [kg/s]:';
strList{fluxHolesIdx} = 'Total mass flow across windows Rupel [kg/s]:';
strList{fluxTotalIdx} = 'Total mass flow into target aquifers [kg/s]:';
strList{initTimeIdx} = 'Initialization took:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    output = outputReader(strList, logFileName{logIter});
    tinit(logIter) = output{initTimeIdx};
    tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter)) < eps));
    time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter))/365/24/3600;
    fluxTotal{logIter} = (output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx})))*(-1)/injectionRate;
    fluxHoles{logIter} = (output{fluxHolesIdx}(tInitIdx(logIter):length(output{fluxHolesIdx})))*(-1)/injectionRate;
    fluxFault{logIter} = (output{fluxFaultIdx}(tInitIdx(logIter):length(output{fluxFaultIdx})))*(-1)/injectionRate;
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements found for search patterns');
    end
end

%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
Fig = figure();
set(gcf, 'Units', 'centimeters');

%colors
co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0.5 0 0.5;
      0 0 0];
  
%symbols  
sy{1} = '-';
sy{2} = '--';
sy{3} = ':';
sy{4} = '-.';


 for subplotIter=1:2
     
     subplot(2,2,subplotIter);
     
     if(subplotIter == 1)
         for logIter = 1:length(logFileName)
             fluxplot = plot(time{logIter}(time{logIter}<=timePlot), fluxTotal{logIter}(time{logIter}<=timePlot), ...
                 sy{logIter},'color',co(logIter,:));
             set(fluxplot, 'LineWidth',1.5);
             hold on;
         end
         hold off;
     end
     if(subplotIter == 2)
         for logIter = 1:length(logFileName)
             fluxplot = plot(time{logIter}(time{logIter}<=timePlot), fluxFault{logIter}(time{logIter}<=timePlot), ...
                 sy{logIter},'color',co(logIter,:));
             set(fluxplot, 'LineWidth',1.5);
             hold on;
         end
         hold off;
     end
     if(subplotIter == 2)
         h_leg = legend('Infinite aquifer',...
             'No flow', ...
             'Dirichlet', 'Location', 'NorthEast');
         set(h_leg,'Interpreter',strInterpreter, 'FontSize', iFontSize);
     end
h_title = title(titel{subplotIter},'FontSize', iFontSize,'Interpreter',strInterpreter);
ylabel('Mass flow / injection rate [-]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
xlabel('Time [years]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
%Set the font size of the axis ticks
set(gca,'FontSize',tickFontSize)
axis([0,100,-0.1,1])
axis square;
ax = gca;
set(ax, 'YTick', [0, 0.2, 0.4, 0.6, 0.8, 1.0]);
set(gca, 'XTick', [0, 20, 40, 60, 80, 100]);
% grid on;
     
 end
% set(gcf, 'PaperPositionMode', 'manual', ...
%     'PaperUnits','centimeters', ...
%     'papersize',[18,18], ...
%     'PaperPosition',[0 , 0, 18, 18]);
set(gcf, 'Units','centimeters', 'Position',[0 0 19 17])
set(gcf, 'PaperPositionMode','auto')

saveas(Fig,plotName,'epsc')
%This function fixes the issue of badly exported dots in the line style
fix_dottedline([plotName, '.eps']);
