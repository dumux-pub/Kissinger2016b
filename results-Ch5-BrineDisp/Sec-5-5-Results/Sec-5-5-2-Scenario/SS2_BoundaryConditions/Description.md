# Simulations for the injection and post-injection period for the 1p2c model for different scenarios with variable boundary conditions.

Each folder contains an scenario-specific input file and an output file (out.log) with the results. The simulations use the grid obtained from the initialization period of the reference model as an initial condition. On this grid the initialized primary variables are stored.

**Files:**

- **plot_flux_bc.m** Plot script for Fig. 5.17.

- **reference** The reference scenario uses the grid extension (infinite aquifer boundary conditions).

- **bc_dirichlet** This scenario uses Dirichlet conditions for the lateral inner domain boundary (no extended grid used).

- **bc_neumann** This scenario uses no-flow conditions for the lateral inner domain boundary (no extended grid used).

