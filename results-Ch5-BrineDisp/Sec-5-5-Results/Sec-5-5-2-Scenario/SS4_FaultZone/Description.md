# Simulations for the injection and post-injection period for the 1p2c model for different scenarios with different permeabilities of the fault zone.

Each folder contains an scenario-specific input file and an output file (out.log) with the results. The simulations use the grid obtained from the initialization period of the reference model as an initial condition. On this grid the initialized primary variables are stored.

**Files:**

- **plot_flux_vartz.m** Plot script for Fig. 5.19.

- **plot_matrixweight.m** Plot script for Fig. 5.20.

- **reference** Used in Fig. 5.20. The reference scenario has a matrix weighting factor of 0.0.

- **mw1.0** Used in Fig. 5.20. This scenario uses a matrix weighting factor of 1.0.

- **bar1e-18** Used in Fig. 5.19. Contains the simulation folders 10 simulations with variable fault-zone permeability with a permeability of the Upper Buntsandstein barrier of 1e-18 m^2. Each simulation folder contains a command (*run.sh*) for running the simulation with the appropriate parameters.

- **bar1e-20** Used in Fig. 5.19. Contains the simulation folders 10 simulations with variable fault-zone permeability with a permeability of the Upper Buntsandstein barrier of 1e-20 m^2. Each simulation folder contains a command (*run.sh*) for running the simulation with the appropriate parameters.