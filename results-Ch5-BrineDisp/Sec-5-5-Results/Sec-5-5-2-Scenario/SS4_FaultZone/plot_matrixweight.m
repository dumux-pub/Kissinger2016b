%Plot the flux into the target aquifers.
%Scenario variable matrix weight factor
%Calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the number of log-files to be evaluated
% - divide the flux by the injection rate to make the flux dimensionless

clear all;
plotName = 'Fig-5-20-varmw';

%define the indices for result and search lists
timeIdx = 1;
fluxVolFaultIdx = 2;
fluxSaltFaultIdx = 3;
initTimeIdx = 4;

eps = 1e4;
%Volumetric injection rate [m3/s], injection rate [kg/s]
injectionRate = 24.5136; % [kg/s] brine injection rate reference case
injectionDensity = 1078; % [kg/m3] initial brine density injection point reference case
volInjectionRate = injectionRate/injectionDensity; % [m3/s]

%array of logfiles to read
logFileName{1} = 'mw1.0/out.log';
logFileName{2} = 'reference/out.log';
titel{1} = 'Volume flow over fault zone';
titel{2} = 'Salt flow over fault zone';

ylabelStr{1} = 'Vol. flow/injection rate [-]';
ylabelStr{2} = 'Salt flow [kg/s]';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
strList{fluxVolFaultIdx} = 'Volume flow around salt wall [m3/s]:';
strList{fluxSaltFaultIdx} = 'Salt flow around salt wall [kgNaCl/s]:';
strList{initTimeIdx} = 'Initialization took:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    output = outputReader(strList, logFileName{logIter});
    tinit(logIter) = output{initTimeIdx};
    if(isempty(max(find(abs(output{timeIdx} - tinit(logIter)) < eps))))
        error('Could not find specified initial time.');
    end
    tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
    time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter))/365/24/3600;
    fluxFault{logIter}{fluxVolFaultIdx} = output{fluxVolFaultIdx}(tInitIdx(logIter):length(output{fluxVolFaultIdx}))*-1/volInjectionRate;
    fluxFault{logIter}{fluxSaltFaultIdx} = output{fluxSaltFaultIdx}(tInitIdx(logIter):length(output{fluxSaltFaultIdx}))*-1;
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements foound for search patterns.');
    end
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
set(gcf, 'Units', 'centimeters');
Fig = figure(...
    'PaperUnits','centimeters', ...
    'PaperPosition',[0 , 0, 17, 12]);

fluxIter = [fluxVolFaultIdx, fluxSaltFaultIdx];

%specify color vector
%colors
co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0.5 0 0.5;
      0 0 0];
  
%symbols  
sy{1} = '-';
sy{2} = '--';
sy{3} = ':';
sy{4} = '-.';

for subplotIter=1:length(fluxIter)
    
    subplot(1,2,subplotIter);
    
    for logIter = 1:length(logFileName)
        fluxplot = plot(time{logIter}(time{logIter}<=timePlot), ...
            fluxFault{logIter}{fluxIter(subplotIter)}(time{logIter}<=timePlot), sy{logIter},'color', co(logIter,:));
        set(fluxplot, 'LineWidth',1.5);
        hold on;
    end
    h_title = title(titel{subplotIter});
    set(h_title,'Interpreter',strInterpreter, 'FontSize', iFontSize)
    ylabel(ylabelStr{subplotIter}, 'FontSize', iFontSize,'Interpreter',strInterpreter);
    xlabel('Time [Years]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
    %Set the font size of the axis ticks
    set(gca,'FontSize',tickFontSize)
    axis([0,100,0.0,1.0])
    set(gca, 'XTick', [0, 20, 40, 60, 80, 100]);
    axis square;
%     grid on;
end
h_leg = legend('$W_m = 1$', '$W_m = 0$ (reference)','Location', 'NorthEast');
set(h_leg,'Interpreter',strInterpreter, 'FontSize', iFontSize);
hold off;

saveas(Fig,plotName,'epsc')
%This function fixes the issue of badly exported dots in the line style
fix_dottedline([plotName, '.eps']);
