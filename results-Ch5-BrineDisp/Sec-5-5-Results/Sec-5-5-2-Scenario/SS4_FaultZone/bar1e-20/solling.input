#############################################################
# Configuration file for solling1p2cni
#############################################################

#############################################################
# General Parameters
#############################################################
[TimeManager]
DtInitial = 1 # [s] initialization length
TEnd = 1e100 # [s] does not have any meaning for the simulation
#Restart = 

[Grid]
File = ../../../../../../grid-Ch5-BrineDisp/complex_initreference_extend.dgf
#File = initialization_pressure.dgf
#File = initialization_salt.dgf

#############################################################
# Problem parameters
#############################################################
[Problem]
Name = solling # [-] the name of the output files
InitializationOnly = false # specifiy whether the simulation should stop after the initialization period (second episode) and write dgf with results
InitializationPressureOnly = false # stop after the pressure initialization (first episode) and write dgf with results
ReadFromDGFPressureInitialization = true # Read the results of the pressure initialization from a dgf

# Episode management
SaltInitDT = 1 # [s] initial time step for salt initialization episode
SaltEpsLength = 1 # [s] 300000 years
InjectionEpsLength = 1.5768e9 # [s] 50 years
InjectionInitDT = 1e6 # [s] ca. 10 days, initial time step of the injection period
InjectionMaxDT = 1.5768e8 # [s] 5 years
PostInjectionEpsLength = 1.5768e9 # [s] 50 years
PostInjectionInitDT = 1e6 # [s] ca. 10 days, initial time step of the post-injection period
PostInjectionMaxDT = 1.5768e8 # [s] 5 years

# Output
EpisodeOutput = true # Write output at the end of every episode
TimeStepOutput = 10000 # Write output at every nth time step
TimeStepRestart = 10 # Write a restart file at the end of every nth time step 
DebugOutput = false # Write certain vtu output only if this value is true

# Initialization/Salt transport
MassReductionFactor = 1e-18 # porosity reduction factor for the pressure initialization period
EnableSaltTransport = true # if true the model is initalised with a linear salt gradient
XlNaClSoluteGrad = 1.5e-4 # [kgSalt/kgWater/m] gradient in z-direction for salt transport.
XlNaClSoluteMax = 0.3589 # [kgSalt/ kgWater] maximum solubility of salt at 20°C and 1 bar Source: http://en.wikipedia.org/wiki/Solubility_table
XlNaClSoluteMin = 0.0 # [kgSalt/ kgWater] freshwater salt molality 25 mg/kg
InitSaltTerQuar = true # also ter quar vertices will get an inital salt concentration

#stationarity bounds, determine when a stationary state has been reached and an the initialization episodes are terminated
StatDiffPress = 1e-1 # [Pa]
StatDiffSaltMoleFrac = 1e-7 # [molSalt/molBrine] 

# Boundary conditions
PressureTOR = 1e5 # Atmospheric pressure at top of reservoir
GWRegenerationRate = 3.17e-6 # [kg/s/sm] groundwater regeneration rate, corresponds to 100 mm/year --> 100[mm/year]*1000[kg/qm]/1000[mm/m]/3600[s/h]/24[h/d]/365[d/year]
Closed = true # close all lateral boundaries except outer domain lateral boundaries
ApplyGeothermalGradient = true # set a temperature gradient 0.03K/m
DirichletAtTop = false # set dirichlet at the top boundary

# Injection management
InjectionRate = 15.855 # [kg/s] ~ 0.5 MtCO2/year will be converted to equivalent brine injection rate
UseCO2VolumeEquivalentInjRate = true # states whether the injection rate should be converted to a CO2 equivalent rate

# Injection location coordinates for box model (nodal values)
InjCoor = 725779 5.97192e+06 1651.31
DeltaSphere = 5.

#Measurement point coordinates for box model (nodal values)
M1 = 731911 5.97138e+06 2809

M2 = 739306 5.97068e+06 1892

#Flux delineation x-coordinate between flux salt dome and flux Rupel holes
DCoorX = 730851.6875

#DFM-specifc parameter for assigning the weight of matrix for the transport equation if a fracture is present
TransportEqMatrixWeight = 0.0

#Injection location coordinates for cell centered model (element centers)
#XInjCoor =  725568 #725779 #7.26e+05 #725556.91
#YInjCoor =  5.97194e+06 #5.97192e+06	# 5.98e+06 #5971762.74	      
#DeltaXY = 100 # the delta for looking for a cell in x and y direction
#ZInjCoord = 1594.81 #1651.31
#DeltaZ = 5.

#Measurement point coordinates for cell centered model (element centers)
#M1x = 731700
#M1y = 5.9714e+06
#M1z = 2773.66

#M2x = 739095
#M2y = 5.97071e+06
#M2z = 1971.17

#############################################################						
# Spatial Parameters
#############################################################
[SpatialParams]
Swr = 0.2		# [-] residual saturation wetting phase
Snr = 0.05 		# [-] residual saturation non-wetting phase
BrooksCoreyPe = 0	# [Pa] entry pressure for Brooks Corey law
BrooksCoreyLambda = 2.0	# [-] lambda for Brooks Corey law
Compressibility = 4.5e-10 # [1/Pa] compressibility of the solid phase
LongDispersion = 0.0 # [m] Longitudinal dispersivity
TransDispersion = 0.0 # [m] Transversal dispersivity
InitialPermeability = 1e-20 # [sm] Use this permeability for all values below during the initialization period
HolesRupelClosed = false # Close all holes in the rupel, except where the the transition braks through
FractureOff = false # Assign no fractures
TzWidth = 50 # [m] Width of the transition zone

# Permeability and Porosity values for the different layers
PermQuarTop = 6.03e-11 # [sm]
PermQuar = 1e-12 # [sm]
PermTer = 1e-13 # [sm]
PermRC = 1e-18 # [sm]
PermOli = 1e-13 # [sm]
PermOBS = 1e-18 # [sm]
PermMBS = 1e-16 # [sm]
PermSol = 1.1e-13 # [sm]
PermUMBS = 1e-16 # [sm]
PermUBS = 1e-16 # [sm]
PermZec = 1e-20 # [sm]
PermKreide = 1e-14 # [sm] 
PermTZ = 1e-12 # [sm]

PorQuarTop = 0.2 # [-]
PorQuar = 0.2 # [-]
PorTer = 0.15 # [-]
PorRC = 0.1 # [-]
PorOli = 0.1 # [-]
PorOBS = 0.04 # [-]
PorMBS = 0.04 # [-]
PorSol = 0.2 # [-]
PorUMBS = 0.04 #[-]
PorUBS = 0.04 # [-]
PorZec = 1e-3 # [-]
PorKreide = 0.07 # [-]
PorTZ = 0.3 # [-]

####################################################################
# Fluid system
####################################################################
[FluidSystem]
NTemperature = 100		# [-] number of tabularization entries
NPressure = 400			# [-] number of tabularization entries
PressureLow = 1e5		# [Pa] low end for tabularization of fluid properties
PressureHigh = 4e7		# [Pa] high end for tabularization of fluid properties
TemperatureLow = 280 	# [K] low end for tabularization of fluid properties
TemperatureHigh = 420 	# [K] high end for tabularization of fluid properties

# Parameters for simplified EOS
UseSimpleRelations = false # use constant values for fluid compressibility and viscosity
Viscosity = 1e-3 # [Pa s] dynamic viscosity of water
CompressibilityFluid = 4.5e-10 # [1/Pa] compressibility of water
ReferenceDensity = 1000.0 # [kg/m3] reference density at reference pressure
ReferencePressure = 1e5 # [Pa] reference pressure
DiffusionCoefficient = 1.587e-9 # [m2/s] molecular diffusion coefficient

#######################################################################
# VTK
#####################################################################
[Vtk]
AddVelocity = true

[Implicit]
EnablePartialReassemble = true

[Newton]
MaxRelativeShift = 1e-3
ProceedWithoutConvergence = false
EnableChop = true
ScaleLinearSystem = false
WriteConvergence = false

[LinearSolver]
Verbosity = 1
MaxIterations = 500


