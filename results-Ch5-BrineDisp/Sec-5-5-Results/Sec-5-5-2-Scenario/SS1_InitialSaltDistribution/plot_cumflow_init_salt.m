%Plot the cummulative salt mass flow into different aquifers 
%Scenario variable initial salt distribution
%calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the nuber of log-files to be evaluated
% - Calculate the cummulative salt mass flow for different scenarios

clear all;
plotName{1} = 'Fig-5-16a-cumflow_salt_ta';
plotName{2} = 'Fig-5-16b-cumflow_salt_quar2';
plotName{3} = 'Fig-5-16c-cumflow_salt_quar1';

%define the indices for result and search lists
timeIdx = 1;
fluxTotalIdx = 2;
fluxQuar1Idx = 3;
fluxQuar2Idx = 4;
initTimeIdx = 5;

eps = 1e4;
%array of logfiles to read
logFileName{1} = 'reference/out.log';
logFileName{2} = 'init_grad1/out.log';
logFileName{3} = 'init_grad2/out.log';
logFileName{4} = 'init_displarge/out.log';
logFileName{5} = 'init_dispsmall/out.log';
logFileName{6} = 'init_lowperm/out.log';
logFileName{7} = 'init_nodiff/out.log';
titel{1} = 'Salt flow into target aquifers';
titel{2} = 'Salt flow into Quaternary 2';
titel{3} = 'Salt flow into Quaternary 1';

legendStr{1} = 'Injection';
legendStr{2} = 'No injection';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
% strList{fluxFaultIdx} = 'Salt Flux around salt wall:';
% strList{fluxRupelIdx} = 'Salt Flux Across Rupel:';
% strList{fluxHolesIdx} = 'Salt Flux Across Holes Rupel:';
strList{fluxTotalIdx} = 'Salt flow into target aquifers [kgNaCl/s]:';
strList{initTimeIdx} = 'Initialization took:';
strList{fluxQuar1Idx} = 'Salt flow into Quar1 [kgNaCl/s]:';
strList{fluxQuar2Idx} = 'Salt flow into Quar2 [kgNaCl/s]:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    output = outputReader(strList, logFileName{logIter});
    tinit(logIter) = output{initTimeIdx};
    tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
    time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter));
    flux{logIter}{fluxTotalIdx} = output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx}))* (-1);
    flux{logIter}{fluxQuar2Idx} = output{fluxQuar2Idx}(tInitIdx(logIter):length(output{fluxQuar2Idx}))* (-1);
    flux{logIter}{fluxQuar1Idx} = output{fluxQuar1Idx}(tInitIdx(logIter):length(output{fluxQuar1Idx}))* (-1);
    
    %calculate cumulative flow using the trapezoidal rule
    cumflow{logIter}{fluxTotalIdx} = trapz(time{logIter}, flux{logIter}{fluxTotalIdx})/1e9; %convert to MT
    cumflow{logIter}{fluxQuar2Idx} = trapz(time{logIter}, flux{logIter}{fluxQuar2Idx})/1e9;
    cumflow{logIter}{fluxQuar1Idx} = trapz(time{logIter}, flux{logIter}{fluxQuar1Idx})/1e9;
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements foound for search patterns');
    end
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
Fig = figure();
set(gcf, 'Units', 'centimeters');
%specify color vector
co = [0 0 1;
      0 0.5 0;
      1 0 0];
  
%Map flux index to subplotiter
fluxIter = [fluxTotalIdx, fluxQuar2Idx, fluxQuar1Idx];

for subplotIter=1:3
    
    %     subplot(2,2,subplotIter);
    
    %Loop over the log-files
    for logIter = 1:length(logFileName)
        fluxplot = plot(logIter, cumflow{logIter}{fluxIter(subplotIter)}, 'rx', ...
            logIter, flux{logIter}{fluxIter(subplotIter)}(1)*100*365*24*3600/1e9, 'ko');
        set(fluxplot, 'LineWidth',1.0);
        hold on;
        if(subplotIter==2)
            h_leg = legend(legendStr, 'Location', 'NorthEast');
            set(h_leg,'Interpreter',strInterpreter, 'FontSize', 9);
        end
    end
    hold off;
    h_title = title(titel{subplotIter},'FontSize', iFontSize);
    set(h_title,'Interpreter',strInterpreter)
    ylabel('Cum. salt mass [MT-NaCl]', 'FontSize', iFontSize, 'Interpreter',strInterpreter);
    xlabel('Scenario No.', 'FontSize', iFontSize, 'Interpreter',strInterpreter);
    %Set the font size of the axis ticks
    set(gca,'FontSize',tickFontSize)
    axis([0,7,0.0,2.5])
    %set(gca, 'YTick', 0:1:2e10);
    set(gca, 'XTick', 1:7);
    set(fluxplot, 'LineWidth',1);
    axis square;
    grid on;
    set(gcf, 'Units','centimeters', 'Position',[0 0 9 9])
    set(gcf, 'PaperPositionMode','auto')
    saveas(Fig,plotName{subplotIter},'epsc')
    
end



