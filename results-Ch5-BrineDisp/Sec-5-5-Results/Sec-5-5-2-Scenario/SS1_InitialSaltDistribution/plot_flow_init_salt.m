%Plot the salt mass flow into the target aquifers
%Scenario variable initial salt distribution
%calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the nuber of log-files to be evaluated
% - Get the salt flux

clear all;
plotName = 'Fig-5-15-vargrad_salt';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 3;
fluxRupelIdx = 5;
fluxHolesIdx = 4;
fluxTotalIdx = 2;
initTimeIdx = 6;

eps = 1e4;
%array of logfiles to read
logFileName{1} = 'init_grad1/out.log';
logFileName{2} = 'reference/out.log';
logFileName{3} = 'init_grad2/out.log';
logFileName{4} = 'noinj/out.log';
% titel{1} = 'Grad NaCl = 1.0*10$^{-4}$ kg-NaCl/kg-H2O';
% titel{2} = 'Grad NaCl = 1.5*10$^{-4}$ kg-NaCl/kg-H2O';
% titel{3} = 'Grad NaCl = 2.0*10$^{-4}$ kg-NaCl/kg-H2O';
titel{1} = 'Total salt flow';
titel{2} = 'Salt flow over fault zone';
titel{3} = 'Salt flow over h. windows';
titel{4} = 'Salt flow over Rupelian clay';

legContent{1} = 'Low';
legContent{2} = 'Medium (reference)';
legContent{3} = 'High';
legContent{4} = 'Medium no injection';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
% strList{fluxFaultIdx} = 'Salt Flux around salt wall:';
% strList{fluxRupelIdx} = 'Salt Flux Across Rupel:';
% strList{fluxHolesIdx} = 'Salt Flux Across Holes Rupel:';
strList{fluxTotalIdx} = 'Salt flow into target aquifers [kgNaCl/s]:';
strList{initTimeIdx} = 'Initialization took:';
strList{fluxHolesIdx} = 'Salt flow across windows Rupel [kgNaCl/s]:';
strList{fluxFaultIdx} = 'Salt flow around salt wall [kgNaCl/s]:';
strList{fluxRupelIdx} = 'Salt flow across Rupel [kgNaCl/s]:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    output = outputReader(strList, logFileName{logIter});
    tinit(logIter) = output{initTimeIdx};
    tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
    time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter))/365/24/3600;
    flux{logIter}{fluxTotalIdx} = output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx}))* (-1);
    flux{logIter}{fluxHolesIdx} = output{fluxHolesIdx}(tInitIdx(logIter):length(output{fluxHolesIdx}))* (-1);
    flux{logIter}{fluxFaultIdx} = output{fluxFaultIdx}(tInitIdx(logIter):length(output{fluxFaultIdx}))* (-1);
    flux{logIter}{fluxRupelIdx} = output{fluxRupelIdx}(tInitIdx(logIter):length(output{fluxRupelIdx}))* (-1);
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements foound for search patterns');
    end
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
Fig = figure();
set(gcf, 'Units', 'centimeters');
%specify color vector
%colors
co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0.5 0 0.5;
      0 0 0];
  
%symbols  
sy{1} = '-';
sy{2} = '--';
sy{3} = ':';
sy{4} = '-.';
  
%Map flux index to subplotiter
fluxIter = [fluxTotalIdx, fluxFaultIdx, fluxHolesIdx, fluxRupelIdx];

for subplotIter=1:4

    subplot(2,2,subplotIter);
    
    %Loop over the log-files
    for logIter = 1:length(logFileName)    
        fluxplot = plot(time{logIter}(time{logIter}<=timePlot), flux{logIter}{fluxIter(subplotIter)}(time{logIter}<=timePlot),...
            sy{logIter}, 'Color', co(logIter,:));
        set(fluxplot, 'LineWidth',1.0);
        hold on;
    end
    %hold off;
    h_title = title(titel{subplotIter},'FontSize', iFontSize);
    set(h_title,'Interpreter',strInterpreter)
    ylabel('Salt flow [kg-NaCl/s]', 'FontSize', iFontSize, 'Interpreter',strInterpreter);
    xlabel('Time [years]', 'FontSize', iFontSize, 'Interpreter',strInterpreter);
    %Set the font size of the axis ticks
    set(gca,'FontSize',tickFontSize)
    axis([0,100,0.0,1.2])
    set(gca, 'YTick', 0:0.2:1.2);
    set(gca, 'XTick', 0:20:100);
    set(fluxplot, 'LineWidth',1.5);
    axis square;
    %grid on;
    if(fluxIter(subplotIter)==fluxRupelIdx)
        h_leg = legend(legContent,'Location', 'North');
        set(h_leg,'Interpreter',strInterpreter, 'FontSize', tickFontSize);
    end
end

set(gcf, 'Units','centimeters', 'Position',[0 0 17 14])
set(gcf, 'PaperPositionMode','auto')
saveas(Fig,plotName,'epsc')
%This function fixes the issue of badly exported dots in the line style
fix_dottedline([plotName, '.eps']);

