# Simulations for the injection and post-injection period for the 1p2c model for different scenarios with a variable salt distribution prior to the injection.

Each folder contains an scenario-specific input file and an output file (out.log) with the results. The simulations use the grids obtained from the initialization period as an initial condition. On these grids the initialized primary variables are stored.

**Files:**

- **plot_flow_init_salt.m** Plot script for Fig. 5.15

- **plot_cumflow_init_salt.m** Plot script for Fig. 5.16

- **reference** Sceanrio 1 in Fig. 5.16

- **Initi_grad_1-0** Sceanrio 2 in Fig. 5.16

- **init_grad_2-0** Sceanrio 3 in Fig. 5.16

- **init_disp_large** Sceanrio 4 in Fig. 5.16

- **init_disp_small** Sceanrio 5 in Fig. 5.16

- **init_low-perm** Sceanrio 6 in Fig. 5.16

- **init_no_diff** Sceanrio 7 in Fig. 5.16

- **noinj** No injection scenario in Fig. 5.15

