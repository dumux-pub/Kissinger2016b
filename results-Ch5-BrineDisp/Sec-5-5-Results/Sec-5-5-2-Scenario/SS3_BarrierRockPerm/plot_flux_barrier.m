%Plot the flux into the target aquifers.
%Scenario: variable Upper Buntsandstein barrier permeability.
%Calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the number of log-files to be evaluated
% - divide the flux by the injection rate to make the flux dimensionless

clear all;
plotName = 'Fig-5-18-varbar';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 2;
fluxRupelIdx = 3;
fluxHolesIdx = 4;
fluxTotalIdx = 5;
fluxLateralIdx = 6;
initTimeIdx = 7;

eps = 1e4;
injectionRate = 24.9;
%array of logfiles to read
logFileName{1} = 'bar1e-17/out.log';
logFileName{2} = 'reference/out.log';
logFileName{3} = 'bar1e-19/out.log';
logFileName{4} = 'bar1e-20/out.log';
titel{1} = '$k = 10^{-17}$ m$^2$';
titel{2} = '$k = 10^{-18}$ m$^2$';
titel{3} = '$k = 10^{-19}$ m$^2$';
titel{4} = '$k = 10^{-20}$ m$^2$';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time [s]:';
strList{fluxFaultIdx} = 'Total mass flow around salt wall [kg/s]:';
strList{fluxRupelIdx} = 'Total mass flow across Rupel [kg/s]:';
strList{fluxHolesIdx} = 'Total mass flow across windows Rupel [kg/s]:';
strList{fluxTotalIdx} = 'Total mass flow into target aquifers [kg/s]:';
strList{fluxLateralIdx} = 'Total mass flow across lateral boundary [kg/s]:';
strList{initTimeIdx} = 'Initialization took:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileName)
    output = outputReader(strList, logFileName{logIter});
    tinit(logIter) = output{initTimeIdx};
    tInitIdx(logIter) = max(find(abs(output{timeIdx} - tinit(logIter) < eps)));
    time{logIter} = (output{timeIdx}(tInitIdx(logIter):length(output{timeIdx})) - tinit(logIter))/365/24/3600;
    fluxTotal{logIter} = (output{fluxTotalIdx}(tInitIdx(logIter):length(output{fluxTotalIdx})))*(-1)/injectionRate;
    fluxHoles{logIter} = (output{fluxHolesIdx}(tInitIdx(logIter):length(output{fluxHolesIdx})))*(-1)/injectionRate;
    fluxFault{logIter} = (output{fluxFaultIdx}(tInitIdx(logIter):length(output{fluxFaultIdx})))*(-1)/injectionRate;
    fluxLateral{logIter} = (output{fluxLateralIdx}(tInitIdx(logIter):length(output{fluxLateralIdx})))*(-1)/injectionRate;
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements foound for search patterns');
    end
end



%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100.1;
strInterpreter = 'latex';
iFontSize = 10;
tickFontSize = 9;
set(gcf, 'Units', 'centimeters');
Fig = figure(...
    'PaperUnits','centimeters', ...
     'PaperPosition',[0 , 0, 19, 17]); 
        %'PaperType','a4letter',...
        %'PaperOrientation','Portrait',...
        %'PaperUnits','centimeters', ...
        %'ActivePositionProperty','Position', ....
        %'PaperPosition',[0 , 0, 15.89, 15.89]); 
        %'PaperPosition',[2.54 , 2.54, 18.43, 18.43]); 
        
        %colors
co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0.5 0 0.5;
      0 0 0];
  
%symbols  
sy{1} = '-';
sy{2} = '--';
sy{3} = ':';
sy{4} = '-.';

for logIter = 1:length(logFileName)

    subplot(2,2,logIter);
    fluxplot = plot(time{logIter}(time{logIter}<=timePlot), fluxTotal{logIter}(time{logIter}<=timePlot),sy{1}, ...
        time{logIter}(time{logIter}<=timePlot), fluxFault{logIter}(time{logIter}<=timePlot),sy{2}, ...
        time{logIter}(time{logIter}<=timePlot), fluxHoles{logIter}(time{logIter}<=timePlot), sy{3});
    set(fluxplot, 'LineWidth',1.5);
    h_title = title(titel{logIter},'FontSize', iFontSize,'Interpreter',strInterpreter);
    ylabel('Mass flow / injection rate [-]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
    xlabel('Time [years]', 'FontSize', iFontSize,'Interpreter',strInterpreter);
    set(gca, 'XTick', [0, 20, 40, 60, 80, 100]);
    if(logIter==1)
        h_leg = legend('Total mass flow', ...
                       'Flow over fault zone', ...
                       'Flow over hydro. window','Location', 'NorthEast');
        set(h_leg,'Interpreter',strInterpreter, 'FontSize', 8);   
    end
    axis([0,100,-0.1,1])
    %Set the font size of the axis ticks
    set(gca,'FontSize',tickFontSize);
    axis square;
%     grid on;   
end

%set(Fig,'PaperPositionMode','Auto')
saveas(Fig,plotName,'epsc')
%This function fixes the issue of badly exported dots in the line style
fix_dottedline([plotName, '.eps']);

