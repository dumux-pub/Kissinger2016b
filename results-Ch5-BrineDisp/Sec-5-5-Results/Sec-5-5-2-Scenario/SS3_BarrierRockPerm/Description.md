# Simulations for the injection and post-injection period for the 1p2c model for different scenarios with permeabilities of the Upper Buntsandstein barrier.

Each folder contains an scenario-specific input file and an output file (out.log) with the results. The simulations use the grid obtained from the initialization period of the reference model as an initial condition. On this grid the initialized primary variables are stored.

**Files:**

- **plot_flux_barrier.m** Plot script for Fig. 5.18.

- **reference** The reference scenario has a permeability of 1e-18 m2 for the Upper Buntsandstein barrier.

- **bar1e-17** This scenario has a permeability of 1e-17 m^2 for the Upper Buntsandstein barrier.

- **bar1e-19** This scenario has a permeability of 1e-19 m^2 for the Upper Buntsandstein barrier.

- **bar1e-20** This scenario has a permeability of 1e-20 m^2 for the Upper Buntsandstein barrier.