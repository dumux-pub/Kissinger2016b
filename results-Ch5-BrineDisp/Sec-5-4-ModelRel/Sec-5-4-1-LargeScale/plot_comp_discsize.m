%Plot script for Fig. 5.9. Get flux through the fault zone into the top aquifer 
%calculate the fluxes use the function outputReader for reading the
%log-files. 
% - Loop over the the log-files to be evaluated 
% - divide the flux by the injection rate to make the flux dimensionless

clear all;

plotName = 'Fig-5-9-large-scale-disc';

%define the indices for result and search lists
timeIdx = 1;
fluxFaultIdx = 3;
fluxRupelIdx = 5;
fluxHolesIdx = 4;
fluxTotalIdx = 2;

eps = 1e4;
tinit = 2; % [s]
injectionRate = 10.8777; % [kg/s]
%array of logfiles to read
logFileNameN{1} = 'D150_R100_N/out.log';
legendArray{1} = 'D150-R100';
logFileNameN{2} = 'D300_R100_N/out.log';
legendArray{2} = 'D300-R100';
logFileNameN{3} = 'D450_R100_N/out.log';
legendArray{3} = 'D450-R100';
legendArray{4} = 'Analytical';
logFileNameD{1} = 'D150_R100_D/out.log';
logFileNameD{2} = 'D300_R100_D/out.log';
logFileNameD{3} = 'D450_R100_D/out.log';
titleStr{1} = 'Neumann';
titleStr{2} = 'Dirichlet';

%the list of strings to be searched in the log file
strList{timeIdx} = 'Time:';
strList{fluxFaultIdx} = 'Total Flux around salt wall:';
strList{fluxRupelIdx} = 'Total Flux Across Rupel:';
strList{fluxHolesIdx} = 'Total Flux Across Holes Rupel:';
strList{fluxTotalIdx} = 'Total Flux into Ter Quar:';

%calculate the fluxes use the function outputReader for reading the
%log-files
for logIter = 1:length(logFileNameN)
    output = outputReader(strList, logFileNameN{logIter});
    tInitIdx = max(find(abs(output{timeIdx} - tinit < eps)));
    time{logIter} = (output{timeIdx}(tInitIdx:length(output{timeIdx})) - tinit)/365/24/3600;
    flux{logIter}{fluxTotalIdx} = (output{fluxTotalIdx}(tInitIdx:length(output{fluxTotalIdx})))*(-1)/injectionRate;
    flux{logIter}{fluxHolesIdx} = (output{fluxHolesIdx}(tInitIdx:length(output{fluxHolesIdx})))*(-1)/injectionRate;
    flux{logIter}{fluxFaultIdx} = (output{fluxFaultIdx}(tInitIdx:length(output{fluxFaultIdx})))*(-1)/injectionRate;
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements found for search patterns');
    end
end

for logIter = 1:length(logFileNameD)
    output = outputReader(strList, logFileNameD{logIter});
    tInitIdx = max(find(abs(output{timeIdx} - tinit < eps)));
    timeD{logIter} = (output{timeIdx}(tInitIdx:length(output{timeIdx})) - tinit)/365/24/3600;
    fluxD{logIter}{fluxTotalIdx} = (output{fluxTotalIdx}(tInitIdx:length(output{fluxTotalIdx})))*(-1)/injectionRate;
    fluxD{logIter}{fluxHolesIdx} = (output{fluxHolesIdx}(tInitIdx:length(output{fluxHolesIdx})))*(-1)/injectionRate;
    fluxD{logIter}{fluxFaultIdx} = (output{fluxFaultIdx}(tInitIdx:length(output{fluxFaultIdx})))*(-1)/injectionRate;
    if(length(output{1}) ~= length(output{2}))
        error('Different number of elements found for search patterns');
    end
end


% Get analytical solution from .fig 
openfig('analyticalN.fig', 'invisible');
data=get(gca,'Children');
xAnalyticalN=get(data,'XData'); %get the x data
yAnalyticalN=get(data,'YData'); %get the y data

% Get analytical solution from .fig 
openfig('analyticalD.fig', 'invisible');
data=get(gca,'Children');
xAnalyticalD=get(data,'XData'); %get the x data
yAnalyticalD=get(data,'YData'); %get the y data

%%%%%%%%%%%%%%%%%%%%%%
%Figure
timePlot = 100;
strInterpreter = 'latex';
iFontSize = 9;
set(gcf, 'Units', 'centimeters');
Fig = figure(...
    'PaperUnits','centimeters', ...
     'PaperPosition',[0 , 0, 16, 16]); 
%specify color vector
co = [0 0 1;
      0 0.5 0;
      1 0 0];
  
  for subPlotIter = 1:length(titleStr)
      subplot(1,2,subPlotIter);
      if(subPlotIter == 1)
          for logIter = 1:length(logFileNameN)+1
              if(logIter == length(logFileNameN)+1)
                  fluxplot = plot(xAnalyticalN, yAnalyticalN, 'k-'); %, 'Color', co(logIter,:));
              else
                  fluxplot = plot(time{logIter}(time{logIter}<=timePlot), flux{logIter}{fluxTotalIdx}(time{logIter}<=timePlot),...
                      'Color', co(logIter,:));
              end
              set(fluxplot, 'LineWidth',1.0);
              hold on;
          end
          title(titleStr{subPlotIter},'FontSize', iFontSize);
          h_leg = legend(legendArray,'Location', 'NorthWest');
            set(h_leg,'FontSize', iFontSize);
      end
      
      if(subPlotIter == 2)
          for logIter = 1:length(logFileNameD)+1
              if(logIter == length(logFileNameD)+1)
                  fluxplot = plot(xAnalyticalD, yAnalyticalD, 'k-'); %, 'Color', co(logIter,:));
              else
                  fluxplot = plot(timeD{logIter}(timeD{logIter}<=timePlot), fluxD{logIter}{fluxTotalIdx}(timeD{logIter}<=timePlot),...
                      'Color', co(logIter,:));
              end
              set(fluxplot, 'LineWidth',1.0);
              hold on;
          end
          title(titleStr{subPlotIter},'FontSize', iFontSize);
          %           set(h_title,'Interpreter',strInterpreter)
      end
      ylabel('Mass flow / injection rate [-]', 'FontSize', iFontSize);
      xlabel('Time [years]', 'FontSize', iFontSize);
      %Set the font size of the axis ticks
      set(gca,'FontSize',iFontSize)
      axis([0,100,0,1])
      axis square;
      grid on;
  end

  saveas(Fig,plotName,'epsc')

