# Comparison to Zeidouni-Method: plot the simulation results against the analytical solution for the Dirichlet and Neumann case for different domain extension radii and discretization lengths

To compile model for the two-layer geometry change solling.cc: `#define GEOMETRY 2` and compile (MODELTYPE stays at default value 1).

**Files:**

- **plot_comp_discsize.m** Plot script for Fig. 5.9

- **plot_comp_domainsize.m** Plot script for Fig. 5.8

- **Faultleakage_final_twolayer_reference_alex.m** Analytical solution (Zeidouni-Method) to create reference results for the Dirichlet and Neumann case

- **analyticalD.fig** Results of the Zeidouni-Method for the Dirichlet case.

- **analyticalN.fig** Results of the Zeidouni-Method for the Neumann case.

- **DXXX-RXXX_Y** Simulation folders. DXXX stands for the discretization length, RXXX stands for the domain radius, and Y is either D or N, i.e. Dirchlet case or Neumann case. Dumux input files (solling_twolayer.input) are given in each folder as well as output files of each simulation (out.log).
