#############################################################
# Configuration file for solling1p2cni
#############################################################

#############################################################
# General Parameters
#############################################################
[TimeManager]
DtInitial = 1 # [s] initialization length
TEnd = 3.1536e9 # [s] 100 years

[Grid]
File = ../../../../grid-Ch5-BrineDisp/D450_R100_D.dgf 

#############################################################
# Problem parameters
#############################################################
[Problem]
Name = solling_twolayer				# [-] the name of the output files
InitializationOnly = false # specifiy whether the simulation should stop after the initialization period
InitializationPressureOnly = false # stop after the pressure initialization (first episode) and write dgf with results
ReadFromDGFPressureInitialization = false # Read the results of the pressure initialization from a dgf

# Episode managment
SaltInitDT = 1 # [s] initial time step for salt initialization episode
SaltEpsLength = 1 # [s] 1 second
InjectionEpsLength = 1.5768e9 # [s] 50 years
InjectionInitDT = 1e6 # [s] ca. 10 days, initial time step of the injection period
InjectionMaxDT = 6.3072e7 # [s] 2 years
PostInjectionEpsLength = 1.5768e9 # [s] 50 years
PostInjectionInitDT = 1e6 # [s] ca. 10 days, initial time step of the post-injection period
PostInjectionMaxDT = 6.3072e7 # [s] 2 years

# Output
EpisodeOutput = true # Write output at the end of every episode
TimeStepOutput = 10000 # Write output at every nth time step
TimeStepRestart = 50 # Write a restart file at the end of every nth time st
DebugOutput = false # Write certain vtu output only if this value is true

# Initialization/Salt transport
MassReductionFactor = 1e-6 # porosity reduction factor for the pressure initialization period
EnableSaltTransport = false # if true the model is initalised with a linear salt gradient
XlNaClSoluteGrad = 1.5e-4 # [kgSalt/kgWater/m] gradient in z-direction for salt transport. This is actually a molality but it should be a mass fraction.
XlNaClSoluteMax = 0.3589 # [kgSalt/ kgWater] maximum solubility of salt at 20°C and 1 bar Source: http://en.wikipedia.org/wiki/Solubility_table
XlNaClSoluteMin = 25e-6 # [kgSalt/ kgWater] freshwater salt molality 25 mg/kg
InitSaltTerQuar = false # also ter quar vertices will get an inital salt concentration

#stationarity bounds, determine when a stationary state has been reached and an the initialization episodes are terminated
StatDiffPress = 1e7 # [Pa]
StatDiffSaltMoleFrac = 1e7 # [molSalt/molBrine] 

# Boundary conditions
PressureTOR = 1e5 # Atmospheric pressure at top of reservoir
GWRegenerationRate = 0.0 # [kg/s/sm] groundwater regeneration rate, corresponds to 100 mm/year --> 100[mm/year]*1000[kg/qm]/1000[mm/m]/3600[s/h]/24[h/d]/365[d/year]
Closed = true # close all side boundaries
ApplyGeothermalGradient = false # set a temperature gradient 0.03K/m
DirichletAtTop = false # set dirichlet at the top boundary

# Injection management
InjectionRate = 10.8777 # [kg/s] As in Scholz 2013 Table 4.2
UseCO2VolumeEquivalentInjRate = false # states whether the injection rate should be converted to a CO2 equivalent rate

#Measurement location coordinates for box model (nodal values)
InjCoor = 0 0 125     
DeltaSphere = 5. 

#Measurement point coordinates for box model (nodal values)
M1 = -2400 0 125
M2 = -5000 0 125

#DFM-specifc parameter for assigning the weight of matrix for the transport equation if a fracture is present
TransportEqMatrixWeight = 0

#Injection location coordinates for cell centered model (element centers)
#XInjCoor = -150 #725779 #7.26e+05 #725556.91
#YInjCoor = 150 #5.97192e+06	# 5.98e+06 #5971762.74	      
#DeltaXY = 10 # the delta for looking for a cell in x and y direction
#ZInjCoord = 12.5 #1651.31
#DeltaZ = 5.

#Measurement point coordinates for cell centered model (element centers)
#M1x = -2350 
#M1y = 150
#M1z = 12.5 

#M2x = -4850
#M2y = 0
#M2z = 12.5

#############################################################					
# Spatial Parameters
#############################################################
[SpatialParams]
Swr = 0.2		# [-] residual saturation wetting phase
Snr = 0.05 		# [-] residual saturation non-wetting phase
BrooksCoreyPe = 0	# [Pa] entry pressure for Brooks Corey law
BrooksCoreyLambda = 2.0	# [-] lambda for Brooks Corey law
Compressibility = 4.5e-10 # [1/Pa] compressibility of the solid phase
InitialPermeability = 1e-20 # Use this permeability for all values below during the initialization period
FractureWidth = 100 # [m] width of the faultzone, double the fracture width here since the fracture is located on a boundary face
Dz = 25 # [m] vertical cell length 
XCoorFault = -5000 # x coordinate of the fault zone


#Permeability, Porosity values as well as bottom depths for the different layers
PermQuar = 1e-13 # [sm]
PermRC = 1e-25 # [sm]
PermSol = 1e-13 # [sm]
FracturePermeability = 1e-12 # [sm]

PorQuar = 0.2 # [-]
PorRC = 0.001 # [-]
PorSol = 0.2 # [-]
FracturePorosity = 0.01 # [-]

BotQuar = 50 # [m]
BotRC = 100 # [m]
BotSol = 150 # [m]



####################################################################
# Fluid system
####################################################################
[FluidSystem]
NTemperature = 100		# [-] number of tabularization entries
NPressure = 400			# [-] number of tabularization entries
PressureLow = 1e5		# [Pa] low end for tabularization of fluid properties
PressureHigh = 4e7		# [Pa] high end for tabularization of fluid properties
TemperatureLow = 280 	# [K] low end for tabularization of fluid properties
TemperatureHigh = 420 	# [K] high end for tabularization of fluid properties

# Parameters for simplified EOS
UseSimpleRelations = true # use constant values for fluid compressibility and viscosity
Viscosity = 1e-3 # [Pa s] dynamic viscosity of water
CompressibilityFluid = 4.5e-10 # [1/Pa] compressibility of water
ReferenceDensity = 1000.0 # [kg/m3] reference density at reference pressure
ReferencePressure = 1e5 # [Pa] reference pressure
DiffusionCoefficient = 1.587e-9 # [m2/s] molecular diffusion coefficient

#######################################################################
# VTK
#####################################################################
[Vtk]
AddVelocity = true

[Implicit]
EnablePartialReassemble = true

[Newton]
MaxRelativeShift = 1e-4
ProceedWithoutConvergence = false
EnableChop = false
ScaleLinearSystem = false
WriteConvergence = false

[LinearSolver]
Verbosity = 1
MaxIterations = 500

