# Saltpool experiment: plot the simulation results against the experimental results

**Files:**

- **plotSaltPool.m** Plot script for Fig. 5.11

- **saltpool_10_data.csv** Experimental results for the Saltpool experiment with the high salinity of 0.1

- **saltpool_1_data.csv** Experimental results for the Saltpool experiment with the low salinity of 0.01

- **CXX-DXXX** Simulation folders. CXX stands for either C10 or C1 which is the high and the low salinity case respectively.
DXXX stands for the number of elements, i.e. 50^3, 75^3, 100^3 or 125^3, used in the grid convergence study.
Dumux input files (Saltpool.input) are given in each folder as well as grid files (Saltpool.dgf) and output files of each simulation (out.log).
