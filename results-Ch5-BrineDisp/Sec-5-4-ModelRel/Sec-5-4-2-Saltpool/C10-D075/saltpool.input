###############################################################
# Parameter file for tutorial_coupled.
# Everything behind a '#' is a comment.
# Type "./tutorial_coupled --help" for more information.
###############################################################

###############################################################
# Mandatory arguments
###############################################################


# Values according to Diersch and Kolditz, 2002 Table 5 (Saltpool Benchmark)

[TimeManager]
TEnd = 9000 # duration of the simulation [s]
DtInitial = 1e-4 # initial time step size [s]
MaxTimeStepSize = 30 # [s]
 
[Grid]
File = saltpool.dgf

[Problem]
Name = saltpool # name of the problem
InitialSaltWaterHeight = 0.06 # [m] height of the initial salt column
#Rate = 1.89e-6 # [m3/s] volumetric in- and outflow rate case: 0.01
Rate = 1.83e-6 # [m3/s] volumetric in- and outflow rate case: 0.1
#InitialSaltMassFraction = 0.01 # [-] mass salt / mass brine case: 0.01
InitialSaltMassFraction = 0.1 # [-] mass salt / mass brine case: 0.1
DirichletPressure = 1e5 # [Pa] pressure at left, front, upper corner

[FluidSystem]
ReferenceDensity = 1000 # [kg/m3] density of water
#Alpha = 0.76 # [-] solute expansion coefficient  case: 0.01
Alpha = 0.735 # [-] solute expansion coefficient  case: 0.1
ReferenceViscosity = 1.0e-3 # [Pa s] reference dynamic viscosity of water
DiffusionCoefficient = 1e-9 # [m2/s] molecular diffusion coefficient

[SpatialParams]
Permeability = 9.77e-10 # [m2] Permeability
Porosity = 0.372 # [-] effective porosity
LongDispersion = 1.2e-3 # [m] longitudinal dispersivity
TransDispersion = 1.2e-4 # [m] transverse dispersivity
  
[Implicit]
EnablePartialReassemble = true

[LinearSolver]
#MaxIterations = 500
#Verbosity = 2


###############################################################
# Simulation restart
#
# DuMux simulations can be restarted from *.drs files
# Set Restart to the value of a specific file, 
# e.g.:  'Restart = 27184.1' for the restart file
# name_time=27184.1_rank=0.drs
# Please comment in the two lines below, if restart is desired.
###############################################################
# [TimeManager]
# Restart = ... 
