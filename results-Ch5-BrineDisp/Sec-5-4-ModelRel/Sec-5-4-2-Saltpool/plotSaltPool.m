% Plot Saltpool problem for 0.01 and 0.1 salinity cases with different discretization lengths
% Plot 1: 0.01 case
% Plot 2: 0.1 case

clear all;
plotName{1} = 'Fig-5-11c-saltpool1';
plotName{2} = 'Fig-5-11d-saltpool10';
% plotName2 = 'ComparePR_timestepsize';
% plotName3 = 'ComparePR_massfraction';
% plotName4 = 'ComparePR_RePerTimeStep';

%Read experimental data from csv

saltpool{1} = csvread('saltpool_1_data.csv');
saltpool{2} = csvread('saltpool_10_data.csv');

%array of logfiles to read
logFileName{1,1} = 'C1-D050/out.log';
% logFileName{1,2} = 'C0.01-D075/out.log';
logFileName{1,2} = 'C1-D100/out.log';
logFileName{1,3} = 'C1-D125/out.log';
logFileName{2,1} = 'C10-D050/out.log';
legendStr{1} = 'D 50$^3$';
% logFileName{2,2} = 'C0.1-D075/out.log';
%legendStr{2} = 'D 75';
logFileName{2,2} = 'C10-D100/out.log';
legendStr{2} = 'D 100$^3$';
logFileName{2,3} = 'C10-D125/out.log';
legendStr{3} = 'D 125$^3$';
legendStr{4} = 'Experiment';


%strList
strList{1} = 'Time:';
strList{2} = 'Salt MassFrac at Outflow: ';

%title string
titleStr{1} = 'Saltpool case $X^S$ = 0.01';
titleStr{2} = 'Saltpool case $X^S$ = 0.1';


%%%%%%%%%%%%%%%%%%%%%%
% general figure setting
strInterpreter = 'latex';
strFontName = 'Times';
iFontSize = 12;
numberFontSize = 12;
strFontUnit = 'pixels';
iResolution = 150;

%colors
co = [0 0 1;
      0 0.5 0;
      1 0 0;
      0.5 0 0.5;
      0 0 0];
  
%symbols  
sy{1} = '-';
sy{2} = '--';
sy{3} = ':';
sy{4} = '-.';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Fig = figure;
figure(Fig);
set(gcf, 'Units', 'centimeters');

for plotIter = 1:min(size(logFileName))
    for logIter = 1:length(logFileName)+1
        if(logIter == length(logFileName)+1)
            h_plot(logIter) = plot(saltpool{plotIter}(:,1)*60, saltpool{plotIter}(:,2), 'kx-');
            set(h_plot(logIter), 'LineWidth',1);
            hold on;
            continue;
        end
        if(isempty(logFileName{plotIter,logIter}))
            continue;
        end
        %Read output files
        outputSalt{logIter} = outputReader(strList, logFileName{plotIter,logIter});
        %Plot
        h_plot(logIter) = plot(outputSalt{logIter}{1}, outputSalt{logIter}{2}*100, sy{logIter}, 'color', co(logIter,:));
        set(h_plot(logIter), 'LineWidth',1);
        hold on;
    end
    hold off;
    title(titleStr{plotIter}, 'Interpreter','latex');
    if(plotIter == 1)
        h_leg = legend(legendStr, 'Location', 'NorthEast');
        set(h_leg,'Interpreter','latex');
    end  
    ylabel('Salinity [%]');
    xlabel('Time [s]');
    axis([0,9000,0,1e-1])
    axis square;

set(Fig,'position',[0 0, 8, 8])
set(Fig,'PaperPositionMode','Auto')
saveas(Fig,plotName{plotIter},'epsc')
%This function fixes the issue of badly exported dots in the line style
fix_dottedline([plotName{plotIter}, '.eps']);

end
