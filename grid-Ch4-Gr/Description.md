# Grid used in Chapter 4

Radial symmetric grid (piece of cake) used for the simulations in Chapter 4.

**Files:**

- **pieceofcake.dgf** dgf file used for the simulations in Chapter 4. Grids like this can be created using the Matlab-script **pieceofcake.m** in **matlab-functions/dgfGridFunctions**

