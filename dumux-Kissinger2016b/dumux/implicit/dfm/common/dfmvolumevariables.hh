/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase discrete fracture-matrix model.
 */
#ifndef DUMUX_MODELS_DFM_VOLUME_VARIABLES_HH
#define DUMUX_MODELS_DFM_VOLUME_VARIABLES_HH

#include <dune/common/fvector.hh>
#include "dfmproperties.hh"

namespace Dumux
{
/*!
 * \ingroup TwoPDFMModel
 * \ingroup ImplicitVolumeVariables
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the two-phase discrete fracture-matrix model.
 */
template <class TypeTag>
class DFMVolumeVariables : public GET_PROP_TYPE(TypeTag, MatrixVolumeVariables)
{
    typedef typename GET_PROP_TYPE(TypeTag, MatrixVolumeVariables) ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename FVElementGeometry::SubControlVolumeFace SCVFace;
    typedef typename FVElementGeometry::FractureScv FractureScv;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) GridType;
    typedef typename GridType::ctype DT;

    enum {
            dim = GridView::dimension,
            dimWorld = GridView::dimensionworld
    };
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef typename Dune::ReferenceElements<DT, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<DT, dim> ReferenceElement;

public:
    /*!
     * \copydoc ImplicitVolumeVariables::update
     */
    void update(const PrimaryVariables &priVars,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                int scvIdx,
                bool isOldSol)
    {
        ParentType::update(priVars,
                           problem,
                           element,
                           fvGeometry,
                           scvIdx,
                           isOldSol);

        //update the fracture geometry
        fvGeometry.updateFracture(element, problem);
        fracScv_ = fvGeometry.fracScv[scvIdx];
        fracturePorosity_ = problem.spatialParams().meanFracturePorosity(element, fvGeometry, fractureScv());
    }

    /*!
     * \brief Returns the fracture scv object.
     */
    const FractureScv &fractureScv() const
    {
        return fracScv_;
    }

    /*!
     * \brief Returns the average porosity within the fracture.
     */
    Scalar fracturePorosity() const
    {
        return  fracturePorosity_;
    }

    /*!
     * \brief Returns the effective mobility of a given phase within
     *        the control volume.
     *
     * \param phaseIdx The phase index
     */
    Scalar fractureMobility(int phaseIdx) const
    {
        return this->mobility(phaseIdx);
    }

protected:

    FractureScv fracScv_;
    Scalar fracturePorosity_;

private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};
} // end namespace

#endif // DUMUX_MODELS_2PDFM_VOLUME_VARIABLES_HH
