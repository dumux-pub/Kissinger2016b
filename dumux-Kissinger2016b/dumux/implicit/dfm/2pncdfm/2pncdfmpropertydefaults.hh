// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup TwoNCDFMPModel
 * \file
 *
 * \brief Defines some default values for the properties of the the
 *        two phase n component discrete fracture fully implicit model.
 */

#ifndef DUMUX_TWOPNC_DFM_PROPERTY_DEFAULTS_HH
#define DUMUX_TWOPNC_DFM_PROPERTY_DEFAULTS_HH

#include <dumux/implicit/2pnc/2pncproperties.hh>
#include <dumux/implicit/2pnc/2pncvolumevariables.hh>
#include <dumux/implicit/2pnc/2pncfluxvariables.hh>
#include <dumux/implicit/dfm/common/implicitdfmspatialparams.hh>
#include "2pncdfmlocalresidual.hh"
#include "2pncdfmmodel.hh"
#include "2pncdfmproperties.hh"
#include "2pncdfmvolumevariables.hh"



namespace Dumux
{
// \{
namespace Properties
{
//! define the model
SET_TYPE_PROP(TwoPNCDFM, Model, TwoPNCDFMModel<TypeTag>);

//! Use the 2pnc local residual function for the 2p model
SET_TYPE_PROP(TwoPNCDFM, LocalResidual, TwoPNCDFMLocalResidual<TypeTag>);

//! Use the 2pnc local residual function for the 2pnc model
SET_TYPE_PROP(TwoPNCDFM, VolumeVariables, TwoPNCDFMVolumeVariables<TypeTag>);

//////////////////////////////////////////////////////////////////
// Property values for matrix model required for the general discrete fracture model
//////////////////////////////////////////////////////////////////

//! set matrix FluxVariables
SET_TYPE_PROP(TwoPNCDFM, MatrixFluxVariables, TwoPNCFluxVariables<TypeTag>);

//! set matrix VolumeVariables
SET_TYPE_PROP(TwoPNCDFM, MatrixVolumeVariables, TwoPNCVolumeVariables<TypeTag>);

}

}
#endif

