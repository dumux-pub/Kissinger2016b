// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Element-wise calculation the local Jacobian for the single-phase,
 *        two-component discrete fracture model in the fully implicit scheme.
 */

#ifndef DUMUX_TWOP_NC_DFM_LOCAL_RESIDUAL_HH
#define DUMUX_TWOP_NC_DFM_LOCAL_RESIDUAL_HH

#include <dumux/implicit/dfm/2pncdfm/2pncdfmproperties.hh>
#include <dumux/implicit/2pnc/2pnclocalresidual.hh>

namespace Dumux
{
/*!
 *
 * \ingroup TwoPNCDFMModel
 * \ingroup ImplicitLocalResidual
 * \brief Calculate the local Jacobian for the two-phase,
 *        n-component model in the fully implicit scheme.
 *
 *  This class is used to fill the gaps in BoxLocalResidual for the 1p2c flow and transport.
 */
template<class TypeTag>
class TwoPNCDFMLocalResidual : public TwoPNCLocalResidual<TypeTag>
{
protected:
    typedef TwoPNCLocalResidual<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, LocalResidual) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    enum { dim = GridView::dimension };
    enum { dimWorld = GridView::dimensionworld };
    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum
    {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        numComponents = GET_PROP_VALUE(TypeTag, NumComponents),

        replaceCompEqIdx = GET_PROP_VALUE(TypeTag, ReplaceCompEqIdx),

        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,

        conti0EqIdx = Indices::conti0EqIdx,
    };

public:
    /*!
     * \brief Constructor. Sets the upwind weight.
     */
    TwoPNCDFMLocalResidual()
    {
        // retrieve the upwind weight for the mass conservation equations. Use the value
        // specified via the property system as default, and overwrite
        // it by the run-time parameter from the Dune::ParameterTree
        upwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);
//        transportEqMatrixWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, TransportEqMatrixWeight);
//        transportEqFractureWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, TransportEqFractureWeight);
    };

    /*!
     * \brief Evaluate the amount of all conservation quantities
     *        (e.g. phase mass) within a finite volume.
     *
     *        \param storage The mass of the component within the sub-control volume
     *        \param scvIdx The index of the considered face of the sub-control volume
     *        \param usePrevSol Evaluate function with solution of current or previous time step
     */
    void computeStorage(PrimaryVariables &storage, const int scvIdx, const bool usePrevSol) const
    {

        // if flag usePrevSol is set, the solution from the previous
        // time step is used, otherwise the current solution is
        // used. The secondary variables are used accordingly.  This
        // is required to compute the derivative of the storage term
        // using the implicit euler method.
        const ElementVolumeVariables &elemVolVars = usePrevSol ? this->prevVolVars_() : this->curVolVars_();
        const VolumeVariables &volVars = elemVolVars[scvIdx];

        storage = 0;
        ///////////////////////////////////////////////////////////////////////
        PrimaryVariables storageFracture;
        PrimaryVariables storageMatrix;
        storageFracture = 0.0;
        storageMatrix = 0.0;
        //        const GlobalPosition &globalPos = geometry.corner(scvIdx);

        if (volVars.fractureScv().isFractureScv)
        {
            for (unsigned int phaseIdx = 0; phaseIdx<2; phaseIdx++)
            {
                for (unsigned int compIdx = 0; compIdx < numComponents; ++compIdx) //H2O, Air, Salt
                {
                    int eqIdx = conti0EqIdx + compIdx;
                    if (replaceCompEqIdx != eqIdx)
                    {
                        storageFracture[eqIdx] += volVars.molarDensity(phaseIdx)
                                * volVars.fracturePorosity()
                                * volVars.fractureScv().fractureVolFrac
                                * volVars.fluidState().moleFraction(phaseIdx, compIdx)
                                * volVars.saturationFracture(phaseIdx);

                        storageMatrix[eqIdx] += volVars.molarDensity(phaseIdx)
                                * volVars.porosity()
                                * volVars.fractureScv().matrixVolFrac
                                * volVars.fluidState().moleFraction(phaseIdx, compIdx)
//                                * dsm_dsf
                                * volVars.saturationMatrix(phaseIdx);
                    }
                    else
                    {
                        storageFracture[replaceCompEqIdx] += volVars.molarDensity(phaseIdx)
                                * volVars.fracturePorosity()
                                * volVars.fractureScv().fractureVolFrac
                                * volVars.saturationFracture(phaseIdx);

                        storageMatrix[replaceCompEqIdx] += volVars.molarDensity(phaseIdx)
                                * volVars.saturation(phaseIdx)
                                * volVars.porosity();
                    }
                }
            }
        }
        else
        {
            for (unsigned int phaseIdx = 0; phaseIdx < 2; phaseIdx++)
            {
                for (unsigned int compIdx = 0; compIdx < numComponents; ++compIdx) //H2O, Air, Salt
                {
                    int eqIdx = conti0EqIdx + compIdx;
                    if (replaceCompEqIdx != eqIdx)
                    {
                        storageFracture[eqIdx] += 0.0;
                        storageMatrix[eqIdx] += volVars.molarDensity(phaseIdx)
                                                * volVars.porosity()
                                                * volVars.fluidState().moleFraction(phaseIdx, compIdx)
                                                * volVars.saturation(phaseIdx);
                    }
                    else
                    {
                        storageFracture[eqIdx] += 0.0;
                        storageMatrix[eqIdx] += volVars.molarDensity(phaseIdx)
                                                * volVars.porosity()
                                                * volVars.saturation(phaseIdx);
                    }

                }
            }
        }

        //Hack: Manipulate the storage term for the salt component, if we are dealing with a fracture
        if(volVars.fractureScv().isFractureScv)
        {
            storageMatrix[FluidSystem::NaClIdx] *= GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.TransportEqMatrixWeight);
            storageFracture[FluidSystem::NaClIdx] *= GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.TransportEqFractureWeight);
        }

        //add fracture and matrix storage
        storage = storageFracture  + storageMatrix;
    }

    /*!
     * \brief Evaluate the advective mass flux of all components over
     *        a face of a sub-control volume.
     *
     * \param flux The advective flux over the sub-control-volume face for each component
     * \param fluxVars The flux variables at the current scv
     */
    void computeAdvectiveFlux(PrimaryVariables &flux, const FluxVariables &fluxVars) const
    {
        ////////
        // advective fluxes of all components in all phases
        ////////
        //First calculate the matrix flux from the parent
        ParentType::computeAdvectiveFlux(flux, fluxVars);
        // data attached to upstream and the downstream vertices
        // of the current phase
        //Calculate the fracture advective flux
        if(fluxVars.fractureFace().isFractureScvf)
        {
            for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
            {
                // data attached to upstream and the downstream vertices
                // of the current phase
                const VolumeVariables &upFracture =
                        this->curVolVars_(fluxVars.upstreamFractureIdx(phaseIdx));
                const VolumeVariables &dnFracture =
                        this->curVolVars_(fluxVars.downstreamFractureIdx(phaseIdx));

                for (unsigned int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                    // add advective flux of current component in current
                    // phase
                    unsigned int eqIdx = conti0EqIdx + compIdx;
                    if (eqIdx != replaceCompEqIdx)
                    {
                        flux[eqIdx] +=
                        fluxVars.fractureVolumeFlux(phaseIdx)
                        * ( upwindWeight_ // upstream vertex
                           *  (upFracture.molarDensity(phaseIdx)
                           *   upFracture.fluidState().moleFraction(phaseIdx, compIdx))
                           + (1 - upwindWeight_) // downstream vertex
                           * (dnFracture.molarDensity(phaseIdx)
                           *  dnFracture.fluidState().moleFraction(phaseIdx, compIdx)));

                        Valgrind::CheckDefined(fluxVars.fractureVolumeFlux(phaseIdx));
                        Valgrind::CheckDefined(upFracture.fluidState().molarDensity(phaseIdx));
                        Valgrind::CheckDefined(dnFracture.fluidState().molarDensity(phaseIdx));
                        Valgrind::CheckDefined(upFracture.fluidState().moleFraction(phaseIdx, compIdx));
                        Valgrind::CheckDefined(dnFracture.fluidState().moleFraction(phaseIdx, compIdx));
                    }
                    else
                    {
                        flux[eqIdx] +=
                        fluxVars.fractureVolumeFlux(phaseIdx)
                            * ( upwindWeight_ // upstream vertex
                               *  (upFracture.molarDensity(phaseIdx))
                               + (1 - upwindWeight_) // downstream vertex
                               * (dnFracture.molarDensity(phaseIdx)));

                        Valgrind::CheckDefined(fluxVars.fractureVolumeFlux(phaseIdx));
                        Valgrind::CheckDefined(upFracture.molarDensity(phaseIdx));
                        Valgrind::CheckDefined(dnFracture.molarDensity(phaseIdx));
                    }
                }
            }
        }
    }

    Implementation *asImp_()
    { return static_cast<Implementation *> (this); }
    const Implementation *asImp_() const
    { return static_cast<const Implementation *> (this); }

private:
    Scalar upwindWeight_;
};

}

#endif
