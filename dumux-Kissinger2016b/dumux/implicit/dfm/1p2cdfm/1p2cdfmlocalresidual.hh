// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Element-wise calculation the local Jacobian for the single-phase,
 *        two-component discrete fracture model in the fully implicit scheme.
 */

#ifndef DUMUX_ONEP_TWOC_DFM_LOCAL_RESIDUAL_HH
#define DUMUX_ONEP_TWOC_DFM_LOCAL_RESIDUAL_HH

#include <dumux/implicit/dfm/1p2cdfm/1p2cdfmproperties.hh>
#include <dumux/implicit/1p2c/1p2clocalresidual.hh>

namespace Dumux
{
/*!
 *
 * \ingroup OnePTwoCDFMModel
 * \ingroup ImplicitLocalResidual
 * \brief Calculate the local Jacobian for the single-phase,
 *        two-component model in the fully implicit scheme.
 *
 *  This class is used to fill the gaps in BoxLocalResidual for the 1p2c flow and transport.
 */
template<class TypeTag>
class OnePTwoCDFMLocalResidual : public OnePTwoCLocalResidual<TypeTag>
{
protected:
    typedef OnePTwoCLocalResidual<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, LocalResidual) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    enum { dim = GridView::dimension };
    enum { dimWorld = GridView::dimensionworld };
    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        //phase index
        phaseIdx = Indices::phaseIdx,
        transportCompIdx = Indices::transportCompIdx
    };
    // indices of the equations
    enum {
        conti0EqIdx = Indices::conti0EqIdx,
        transportEqIdx = Indices::transportEqIdx
    };

    //! property that defines whether mole or mass fractions are used
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);

public:
    /*!
     * \brief Constructor. Sets the upwind weight.
     */
    OnePTwoCDFMLocalResidual()
    {
        // retrieve the upwind weight for the mass conservation equations. Use the value
        // specified via the property system as default, and overwrite
        // it by the run-time parameter from the Dune::ParameterTree
        upwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);
        transportEqMatrixWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, TransportEqMatrixWeight);
        transportEqFractureWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, TransportEqFractureWeight);
    };

    /*!
     * \brief Evaluate the amount of all conservation quantities
     *        (e.g. phase mass) within a finite volume.
     *
     *        \param storage The mass of the component within the sub-control volume
     *        \param scvIdx The index of the considered face of the sub-control volume
     *        \param usePrevSol Evaluate function with solution of current or previous time step
     */
    void computeStorage(PrimaryVariables &storage, const int scvIdx, const bool usePrevSol) const
    {

        // if flag usePrevSol is set, the solution from the previous
        // time step is used, otherwise the current solution is
        // used. The secondary variables are used accordingly.  This
        // is required to compute the derivative of the storage term
        // using the implicit euler method.
        const ElementVolumeVariables &elemVolVars = usePrevSol ? this->prevVolVars_() : this->curVolVars_();
        const VolumeVariables &volVars = elemVolVars[scvIdx];

        storage = 0;
        if(!useMoles) //mass-fraction formulation
        {
            //Matrix storage
            // storage term of continuity equation - massfractions
            // all matrix storage terms are multiplied with the volume fraction of the matrix
            // which 1-fractureVolFrac if the fracture volume is smaller than the matrix volume
            // and zero if the fracture volume is larger than the matrix volume
            // additionally the storage of the transport equation is manipulated by the transportEqMatrixWeight_ factor
            // which can be used to reduce the matrix storage of the transport equation allowing for faster
            // transport velocities
            storage[conti0EqIdx] += volVars.density()*volVars.porosity()*volVars.fractureScv().matrixVolFrac;
            //storage term of the transport equation - massfractions
            if(volVars.fractureScv().isFractureScv)
                storage[transportEqIdx] +=
                        volVars.density()*volVars.massFraction(transportCompIdx)*volVars.porosity()
                        *volVars.fractureScv().matrixVolFrac
                        *transportEqMatrixWeight_;
            else
                storage[transportEqIdx] +=
                        volVars.density()*volVars.massFraction(transportCompIdx)*volVars.porosity()
                        *volVars.fractureScv().matrixVolFrac;

            //Fracture storage
            // storage term of continuity equation - massfractions
            // dfm specific implementation: the fracture storage term is multiplied by the fracture volume fraction
            // fracture volume fraction = volume fracture / volume scv
            // the fracture volume fraction is obtained from the fracScv.fractureVolFrac() function
            // the fracture volume is a virtial volume that means it is added to the scv.
            // transportEqFractureWeight_ is similar to the transportEqMatrixWeight_ factor only for the fracture
            if(volVars.fractureScv().isFractureScv)
            {
                storage[conti0EqIdx] +=
                        volVars.density()*volVars.fracturePorosity()*volVars.fractureScv().fractureVolFrac;
                //storage term of the transport equation - massfractions

                storage[transportEqIdx] +=
                        volVars.density()*volVars.massFraction(transportCompIdx)*volVars.fracturePorosity()*
                        volVars.fractureScv().fractureVolFrac*transportEqFractureWeight_;
            }

        }
        else //mole-fraction formulation
        {
            // storage term of continuity equation- molefractions
            //careful: molarDensity changes with moleFrac!
            // all matrix storage terms are multiplied with the volume fraction of the matrix
            // which 1-fractureVolFrac if the fracture volume is smaller than the matrix volume
            // and zero if the fracture volume is larger than the matrix volume
            // additionally the storage of the transport equation is manipulated by the transportEqMatrixWeight_ factor
            // which can be used to reduce the matrix storage of the transport equation allowing for faster
            // transport velocities
            storage[conti0EqIdx] += volVars.molarDensity()*volVars.porosity()
                    *volVars.fractureScv().matrixVolFrac;
            // storage term of the transport equation - molefractions
            if(volVars.fractureScv().isFractureScv)
                storage[transportEqIdx] +=
                        volVars.molarDensity()*volVars.moleFraction(transportCompIdx)*volVars.porosity()
                        *volVars.fractureScv().matrixVolFrac
                        *transportEqMatrixWeight_;
            else
                storage[transportEqIdx] +=
                        volVars.molarDensity()*volVars.moleFraction(transportCompIdx)*volVars.porosity()
                        *volVars.fractureScv().matrixVolFrac;

            //Fracture storage
            // storage term of continuity equation - massfractions
            // dfm specific implementation: the fracture storage term is multiplied by the fracture volume fraction
            // fracture volume fraction = volume fracture / volume scv
            // the fracture volume fraction is obtained from the fracScv.fractureVolFrac() function
            // the fracture volume is a virtial volume that means it is added to the scv.
            // transportEqFractureWeight_ is similar to the transportEqMatrixWeight_ factor only for the fracture
            if(volVars.fractureScv().isFractureScv)
            {
                storage[conti0EqIdx] += volVars.molarDensity()*volVars.porosity()*
                        volVars.fractureScv().fractureVolFrac;
                // storage term of the transport equation - molefractions
                storage[transportEqIdx] +=
                        volVars.molarDensity()*volVars.moleFraction(transportCompIdx)*
                        volVars.porosity()*volVars.fractureScv().fractureVolFrac
                        *transportEqFractureWeight_;
            }
        }
    }

    /*!
     * \brief Evaluate the advective mass flux of all components over
     *        a face of a sub-control volume.
     *
     * \param flux The advective flux over the sub-control-volume face for each component
     * \param fluxVars The flux variables at the current scv
     */
    void computeAdvectiveFlux(PrimaryVariables &flux, const FluxVariables &fluxVars) const
    {
        ////////
        // advective fluxes of all components in all phases
        ////////
        //First calculate the matrix flux from the parent
        ParentType::computeAdvectiveFlux(flux, fluxVars);
        // data attached to upstream and the downstream vertices
        // of the current phase

        //Calculate the fracture advective flux
        if(fluxVars.fractureFace().isFractureScvf)
        {
            const VolumeVariables &up =
                    this->curVolVars_(fluxVars.upstreamFractureIdx(/*phaseIdx=0*/ 0));
            const VolumeVariables &dn =
                    this->curVolVars_(fluxVars.downstreamFractureIdx(/*phaseIdx=0*/ 0));

            if(!useMoles) //mass-fraction formulation
            {
                // total mass flux - massfraction
                //fractureVolumeFlux(/*phaseIdx=*/ 0) is the Darcy velocity multiplied with the normal vector and the mobility, calculated in 1p2cfluxvariables.hh
                flux[conti0EqIdx] +=
                        fluxVars.fractureVolumeFlux(/*phaseIdx=*/ 0) *
                        ((     upwindWeight_)*up.density()
                                +
                                ((1 - upwindWeight_)*dn.density()));

                // advective flux of the second component - massfraction
                flux[transportEqIdx] +=
                        fluxVars.fractureVolumeFlux(/*phaseIdx=*/ 0) *
                        ((    upwindWeight_)*up.density() * up.massFraction(transportCompIdx)
                                +
                                (1 - upwindWeight_)*dn.density()*dn.massFraction(transportCompIdx));
            }
            else //mole-fraction formulation
            {
                // total mass flux - molefraction
                //fractureVolumeFlux(/*phaseIdx=*/ 0) is the Darcy velocity multiplied with the normal vector and the mobility, calculated in 1p2cfluxvariables.hh
                flux[conti0EqIdx] +=
                        fluxVars.fractureVolumeFlux(/*phaseIdx=*/ 0) *
                        ((     upwindWeight_)*up.molarDensity()
                                +
                                ((1 - upwindWeight_)*dn.molarDensity()));

                // advective flux of the second component -molefraction
                flux[transportEqIdx] +=
                        fluxVars.fractureVolumeFlux(/*phaseIdx=*/ 0) *
                        ((    upwindWeight_)*up.molarDensity() * up.moleFraction(transportCompIdx)
                                +
                                (1 - upwindWeight_)*dn.molarDensity() * dn.moleFraction(transportCompIdx));
            }
        }
    }

    Implementation *asImp_()
    { return static_cast<Implementation *> (this); }
    const Implementation *asImp_() const
    { return static_cast<const Implementation *> (this); }

private:
    Scalar upwindWeight_;
    Scalar transportEqMatrixWeight_;
    Scalar transportEqFractureWeight_;
};

}

#endif
