// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup OnePTwoCModel
 * \file
 *
 * \brief Defines some default values for the properties of the the
 *        single-phase, two-component fully implicit model.
 */

#ifndef DUMUX_1P2C_DFM_PROPERTY_DEFAULTS_HH
#define DUMUX_1P2C_DFM_PROPERTY_DEFAULTS_HH

#include <dumux/implicit/1p2c/1p2cproperties.hh>
#include "1p2cdfmlocalresidual.hh"
#include "1p2cdfmmodel.hh"
#include "1p2cdfmproperties.hh"



namespace Dumux
{
// \{
namespace Properties
{
//! define the model
SET_TYPE_PROP(OnePTwoCDFM, Model, OnePTwoCDFMModel<TypeTag>);

//! Use the 1p2c local residual function for the 1p2c model
SET_TYPE_PROP(OnePTwoCDFM, LocalResidual, OnePTwoCDFMLocalResidual<TypeTag>);

//! Define the factor to be multiplied with the matrix storage term of transport equation
SET_SCALAR_PROP(OnePTwoCDFM, ProblemTransportEqMatrixWeight, 1.0);

//! Define the factor to be multiplied with the fracture storage term of transport equation
SET_SCALAR_PROP(OnePTwoCDFM, ProblemTransportEqFractureWeight, 1.0);

//////////////////////////////////////////////////////////////////
// Property values for matrix model required for the general discrete fracture model
//////////////////////////////////////////////////////////////////

//! set matrix FluxVariables
SET_TYPE_PROP(OnePTwoCDFM, MatrixFluxVariables, OnePTwoCFluxVariables<TypeTag>);

//! set matrix VolumeVariables
SET_TYPE_PROP(OnePTwoCDFM, MatrixVolumeVariables, OnePTwoCVolumeVariables<TypeTag>);

}

}
#endif

