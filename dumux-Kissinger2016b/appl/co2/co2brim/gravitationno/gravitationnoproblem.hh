// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief CO2 injection into a radially symmetric domain.
 */

#ifndef DUMUX_GRAVITATION_NO_PROBLEM_HH
#define DUMUX_GRAVITATION_NO_PROBLEM_HH

#if HAVE_ALUGRID
#include <dune/grid/alugrid/2d/alugrid.hh>
#elif HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#else
#warning ALUGrid is necessary for this test.
#endif

#include <dune/grid/yaspgrid.hh>
//#include <dune/grid/sgrid.hh>

#include <dumux/material/components/co2.hh>
#include <dumux/material/components/co2tablereader.hh>
#include <dumux/implicit/2p/2pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/material/fluidsystems/brineco2fluidsystem.hh>
#include <dumux/common/parameters.hh>
#include "gravitationnospatialparams.hh"
#include <cmath>
#include <dumux/material/components/brine.hh>
#include <dumux/material/components/h2o.hh>
#include "gravitationnoco2tables.hh"

namespace Dumux
{

template <class TypeTag>
class GravitationNoProblem;

//////////
// Specify the properties for the Gravitation Number problem
//////////
namespace Properties
{
NEW_TYPE_TAG(GravitationNoProblem, INHERITS_FROM(TwoP, GravitationNoSpatialParams));
NEW_TYPE_TAG(GravitationNoBoxProblem, INHERITS_FROM(BoxModel, GravitationNoProblem));

// Set the grid type
SET_TYPE_PROP(GravitationNoProblem, Grid, Dune::ALUGrid<3, 3, Dune::cube, Dune::conforming>);

// Set the problem property
SET_TYPE_PROP(GravitationNoProblem, Problem, Dumux::GravitationNoProblem<TypeTag>);

SET_TYPE_PROP(GravitationNoProblem, CO2Table, Dumux::GravitationNoCO2Tables::CO2Tables);

SET_TYPE_PROP(GravitationNoProblem, FluidSystem, Dumux::BrineCO2FluidSystem<TypeTag>);

//// Enable partial reassembly of the jacobian matrix?
SET_BOOL_PROP(GravitationNoProblem, ImplicitEnablePartialReassemble, false);

// Enable reuse of jacobian matrices?
SET_BOOL_PROP(GravitationNoProblem, ImplicitEnableJacobianRecycling, false);

// Write the solutions of individual newton iterations?
SET_BOOL_PROP(GravitationNoProblem, NewtonWriteConvergence, false);

// Use forward differences instead of central differences
SET_INT_PROP(GravitationNoProblem, ImplicitNumericDifferenceMethod, +1);

// Enable gravity
SET_BOOL_PROP(GravitationNoProblem, ProblemEnableGravity, true);
}//end namespace properties

/*!
 * \brief CO2 injection into a radially symmetric domain.
 *
 * All coordinates can be converted into zylinder coordinates using the function "convertToPolarCoordinates_()".
 * The CO2 is injected until the CO2 plume reaches a spill point at a distance of 1km from the injection well.
 * Once the CO2 is near the spill point (warning point), the time step size is reduced to ensure no CO2 crosses
 * the spill point.
 *
 */
template <class TypeTag >
class GravitationNoProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;



    enum {

        // primary variable indices
        pwIdx = Indices::pwIdx,
        SnIdx = Indices::snIdx,

        // equation indices
        contiNEqIdx = Indices::contiNEqIdx,

        // phase indices
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,


        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Dumux::H2O<Scalar> H2O;
    typedef typename Dumux::Brine<Scalar, H2O> Brine;
    typedef typename GET_PROP_TYPE(TypeTag, CO2Table) CO2Table;
    typedef typename Dumux::CO2<Scalar, CO2Table> CO2;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

public:
    /*!
     * \brief The constructor
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    GravitationNoProblem(TimeManager &timeManager,
                const GridView &gridView)
        : ParentType(timeManager, gridView)
    {


        //Read the parameter file
        try
        {
//            nTemperature_       = GET_RUNTIME_PARAM(TypeTag, int, FluidSystem.NTemperature);
//            nPressure_          = GET_RUNTIME_PARAM(TypeTag, int, FluidSystem.NPressure);
//            pressureLow_        = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.PressureLow);
//            pressureHigh_       = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.PressureHigh);
//            temperatureLow_     = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.TemperatureLow);
//            temperatureHigh_    = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.TemperatureHigh);
//            depthTOR_           = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.DepthTOR);
            pressureAtTOR_      = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PressureAtTOR);
            name_               = GET_RUNTIME_PARAM(TypeTag, std::string, Problem.Name);
            temperature_        = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.Temperature);
//            radiusMax_          = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.RadiusMax);
            radiusWell_         = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.RadiusWell);
            radiusSpillPoint_   = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.RadiusSpillPoint);
            radiusWarningPoint_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.RadiusWarningPoint);
            alphaMax_           = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.AlphaMax);
            thickness_          = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Thickness);


        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }
        std::cout<<"bBoxMax = "<<this->bBoxMax()[dim-1]<<std::endl;
        thicknessCapRock_ = (this->bBoxMax()[dim-1] - thickness_)/2;
        this->spatialParams().setThicknessCapRock(thicknessCapRock_);
        depthBOR_ = thickness_ + thicknessCapRock_;
        nTemperature_ = 10;
        nPressure_ = 100;
        pressureLow_ = pressureAtTOR_ - 0.3*pressureAtTOR_;
        pressureHigh_ = 2*pressureAtTOR_;
        temperatureLow_ = temperature_ - temperature_ *0.1;
        temperatureHigh_ = temperature_ + temperature_ *0.1;

        FluidSystem::init(/*Tmin=*/temperatureLow_,
                          /*Tmax=*/temperatureHigh_,
                          /*nT=*/nTemperature_,
                          /*pmin=*/pressureLow_,
                          /*pmax=*/pressureHigh_,
                          /*np=*/nPressure_);
//                          GET_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Salinity));


        //Get the global indices of all nodes located on the spill point surface
        std::cout<<"Salinity= "<<GET_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Salinity)<<std::endl;
        //check the maximum radius
        radiusMax_ = this->bBoxMax()[0];
        std::cout<<"Maximum Radius= "<<radiusMax_<<std::endl;
        FVElementGeometry fvElemGeom;
        VolumeVariables volVars;
        int scvIdx;
        int sizeSP = 0;
        int sizeWP = 0;
        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator elemEndIt = this->gridView().template end<0>();
        for (; eIt != elemEndIt; ++eIt)
        {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {
                fvElemGeom.update(this->gridView(), *eIt);

                int numVerts = eIt->template count<dim> ();

                for (scvIdx = 0; scvIdx < numVerts; ++scvIdx)
                {
                    const GlobalPosition &globalPos = fvElemGeom.subContVol[scvIdx].global;
                    int globalIdx = this->vertexMapper().map(*eIt, scvIdx, dim);

                    //Check if node lies on spill point area
                    if (onSpillPointArea_(globalPos))
                    {
                        sizeSP = sizeSP + 1;
                        globalIdxSP_.resize(sizeSP);
                        globalIdxSP_[sizeSP-1] = globalIdx; //index of node on spill point area
                        //                    std::cout<<"Global Index: "<<globalIdx_[size]<<std::endl;
                        //                    std::cout<<"X Coordinate: "<<globalPos[0]<<std::endl;
                    }
                    if (onWarningPointArea_(globalPos))
                    {
                        sizeWP = sizeWP + 1;
                        globalIdxWP_.resize(sizeWP);
                        globalIdxWP_[sizeWP-1] = globalIdx;
                        //                    std::cout<<"Global Index: "<<globalIdxWP_[sizeWP-1]<<std::endl;
                        //                    std::cout<<"X Coordinate: "<<globalPos[0]<<std::endl;
                    }

                }
            }
        }
        std::cout<<"No of points on warning point area: "<<sizeWP<<" Rank: "<<this->gridView().comm().rank()<<std::endl;
        std::cout<<"No of points on spill point area: "<<sizeSP<<" Rank: "<<this->gridView().comm().rank()<<std::endl;
        //Set the end bool to false
        end_ = 0;
        timeStepReduction_ = 0;
        eps_ = 1e-6;
        densityW_ = Brine::liquidDensity(temperature_, pressureAtTOR_);
        std::cout<<"Initial Brine Density at TOR: "<<densityW_<<std::endl;
        Scalar viscosityBrineTOR = Brine::liquidViscosity(temperature_, pressureAtTOR_);
        std::cout<<"Initial Brine Viscosity at TOR: "<<viscosityBrineTOR<<std::endl;
        Scalar densityCO2TOR = CO2::liquidDensity(temperature_, pressureAtTOR_);
        std::cout<<"Initial CO2 Density at TOR: "<<densityCO2TOR<<std::endl;
        Scalar viscosityCO2TOR = CO2::gasViscosity(temperature_, pressureAtTOR_);
        std::cout<<"Initial CO2 Viscosity at TOR: "<<viscosityCO2TOR<<std::endl;
//        this->timeManager().startNextEpisode(365*24*3600);
    }

    void episodeEnd()
            {
                // Start new episode if episode is over and assign new boundary conditions
//                this->timeManager().startNextEpisode(365*24*3600);
//                this->timeManager().setTimeStepSize(this->timeManager().timeStepSize());
            }
    bool shouldWriteOutput() const
    {
        return
            (this->timeManager().timeStepIndex() == 0) ||
//            this->timeManager().episodeWillBeOver() ||
            this->timeManager().willBeFinished() ||
            end_ == 1;
    }

    bool shouldWriteRestartFile() const
    {
        return false;

    }

    void addOutputVtkFields()
        {
        typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;

         // get the number of degrees of freedom
         unsigned numDofs = this->model().numDofs();
         unsigned numElements = this->gridView().size(0);

         //create required scalar fields
         ScalarField *viscosityCO2 = this->resultWriter().allocateManagedBuffer(numDofs);
         ScalarField *viscosityBrine = this->resultWriter().allocateManagedBuffer(numDofs);
         ScalarField *boxVolume = this->resultWriter().allocateManagedBuffer(numDofs);
         (*boxVolume) = 0;


         //Fill the scalar fields with values
         ScalarField *rank = this->resultWriter().allocateManagedBuffer(numElements);

         FVElementGeometry fvGeometry;
         VolumeVariables volVars;

         ElementIterator eIt = this->gridView().template begin<0>();
         ElementIterator eEndIt = this->gridView().template end<0>();
         for (; eIt != eEndIt; ++eIt)
         {
             int idx = this->elementMapper().map(*eIt);
             (*rank)[idx] = this->gridView().comm().rank();
             fvGeometry.update(this->gridView(), *eIt);


             for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
             {
                 int globalIdx = this->model().dofMapper().map(*eIt, scvIdx, dofCodim);
                 volVars.update(this->model().curSol()[globalIdx],
                                *this,
                                *eIt,
                                fvGeometry,
                                scvIdx,
                                false);
                 (*viscosityCO2)[globalIdx] += volVars.fluidState().viscosity(SnIdx);
                 (*viscosityBrine)[globalIdx] += volVars.fluidState().viscosity(pwIdx);
                 (*boxVolume)[globalIdx] += fvGeometry.subContVol[scvIdx].volume;
             }

         }

         //pass the scalar fields to the vtkwriter

         this->resultWriter().attachDofData(*viscosityCO2, "viscosityCO2", isBox);
         this->resultWriter().attachDofData(*viscosityBrine, "viscosityBrine", isBox);
         this->resultWriter().attachDofData(*boxVolume, "boxVolume", isBox);
        }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }

    /*!
     * \brief Called directly after the time integration.
     */

    void preTimeStep()
    {
        std::vector<int>::iterator it;
        if(timeStepReduction_ == 0)
        {

            //loop over all nodes in the globalIdxWP_ vector and check if the saturation in any of them is greater zero
            for (it = globalIdxWP_.begin() ; it < globalIdxWP_.end(); it++)
            {
                //            std::cout<<"Size: "<<globalIdx_.size()<<std::endl;
                PrimaryVariables priVars;
                int globalIdx = *it;
                //            std::cout<<"GlobalIdx = "<<globalIdx<<std::endl;
                priVars = this->model().curSol()[globalIdx];
                if(priVars[SnIdx] > 1e-5 && timeStepReduction_ == false)
                {
                    std::cout<<"Gas phase appears Sn = "<<priVars[SnIdx]<<" at warning point, reducing the time step."<<std::endl;
//                    this->timeManager().setTimeStepSize(86400.0); //Time step of one day
                    timeStepReduction_ = 1;
                }
            }
        }

        //If one processor has timeStepReduction_ == 1, all processes get that value
        if (this->gridView().comm().size() > 1)
            timeStepReduction_ = this->gridView().comm().max(timeStepReduction_);

        if(timeStepReduction_ == 1)
        {
            this->timeManager().setTimeStepSize(86400.0);//Time step of one day
        }

    }
    /*!
     * \brief Called directly after the time integration.
     */
    void postTimeStep()
    {
        // Calculate storage terms
        PrimaryVariables storage;
        this->model().globalStorage(storage);

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0)
        {
            std::cout<<"Storage: " << storage << std::endl;
        }

        std::vector<int>::iterator it;
        if(timeStepReduction_ == 1)
        {
            //loop over all nodes in the globalIdxSP_ vector and check if the saturation in any of them is greater zero
            for (it = globalIdxSP_.begin() ; it < globalIdxSP_.end(); it++)
            {
                //            std::cout<<"Size: "<<globalIdx_.size()<<std::endl;
                PrimaryVariables priVars;
                int globalIdx = *it;
                //            std::cout<<"GlobalIdx = "<<globalIdx<<std::endl;
                priVars = this->model().curSol()[globalIdx];
                if(priVars[SnIdx] > 1e-5)
                {
                    std::cout<<"Gas phase appears Sn = "<<priVars[SnIdx]<<" at spill point"<<std::endl;
                    end_ = 1;

                }
            }
        }

        //If one processor has end_ == 1, all processes get that value
        if (this->gridView().comm().size() > 1)
            end_ = this->gridView().comm().max(end_);

        //Write the output only once
        if(end_ == 1)
        {

            this->timeManager().setEndTime(this->timeManager().time());

        }

    }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a uniform temperature of 10 degrees Celsius.
     */
    Scalar temperatureAtPos(GlobalPosition globalPos) const
    { return temperature_; };


    void sourceAtPos(PrimaryVariables &values,
                const GlobalPosition &globalPos) const
    {
        values = 0;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position of the center of the finite volume
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {
//        if (onInjectionBoundary_(globalPos) || onBackBoundary_(globalPos)) {
        if (onBackBoundary_(globalPos)) {
            values.setAllDirichlet();
        }
        else {
            values.setAllNeumann();
        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        initialValues_(values, globalPos);


    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations [kg / (m^2 *s )]
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void neumannAtPos(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        values = 0.0;
        if (onInjectionBoundary_(globalPos))
        {
            Scalar massRate = 3.171; //kgCO2/s 3.171 -> 0.1 MT/a, 3.171
//            Scalar area = (thickness_-1.)*2*3.14*radiusWell_; //area well in m2
            Scalar area = (thickness_)*2*3.14*radiusWell_; //area well in m2
            values[SnIdx] = -massRate/area; //kg/s/sqm CO2
        }


    }
    // \}

    /*!
     * \name Volume terms
     */
    // \{


    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param globalPos The center of the finite volume which ought to be set.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        initialValues_(values, globalPos);

    }
    // \}

private:

    bool onSideBoundary_(const GlobalPosition &globalPos) const
    {
        Scalar angle = convertToPolarCoordinates_(globalPos)[1];
        return angle < eps_ || angle > alphaMax_ + eps_;
    }

    bool onTopBottomBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[dim-1] < eps_ || globalPos[dim-1] > this->bboxMax[dim-1] - eps_ ;
    }

    bool onInjectionBoundary_(const GlobalPosition &globalPos) const
    {
        Scalar radius = convertToPolarCoordinates_(globalPos)[0];
        return radius < radiusWell_ + eps_ && globalPos[dim-1] > thicknessCapRock_ && globalPos[dim-1] < thickness_ + thicknessCapRock_;
    }

    bool onBackBoundary_(const GlobalPosition &globalPos) const
    {
        Scalar radius = convertToPolarCoordinates_(globalPos)[0];
        return radius > radiusMax_ - eps_;
    }

    bool onSpillPointArea_(const GlobalPosition &globalPos) const
    {
        Scalar radius = convertToPolarCoordinates_(globalPos)[0];
        return radius > radiusSpillPoint_ - 1.0 && radius < radiusSpillPoint_ + 1.0;
    }

    bool onWarningPointArea_(const GlobalPosition &globalPos) const
    {
        Scalar radius = convertToPolarCoordinates_(globalPos)[0];
        return radius > radiusWarningPoint_ + radiusWell_ - 3.0 && radius < radiusWarningPoint_ + radiusWell_ + 3.0;
    }

    //converts GlobalPosition(x,y,z) to GlobalPosition(r, alpha, z)
    GlobalPosition convertToPolarCoordinates_(const GlobalPosition &globalPos) const
    {
        GlobalPosition polarPos;
        polarPos[0] = sqrt(pow(globalPos[0],2) + pow(globalPos[1],2)); //radius
        polarPos[1] = acos(globalPos[0]/polarPos[0]); //angle
        polarPos[2] = globalPos[2]; //z-coordinate
        return polarPos;
    }

    void initialValues_(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        // depthTOR = 0.
        Scalar depthFromTOR = depthBOR_ - globalPos[dim-1];

        // hydrostatic pressure
        values[pwIdx] = pressureAtTOR_ - densityW_*this->gravity()[dim-1]*depthFromTOR;
        values[SnIdx] = 0.0;
    }


    int nTemperature_;
    int nPressure_;
    Scalar pressureLow_;
    Scalar pressureHigh_;
    Scalar temperatureLow_;
    Scalar temperatureHigh_;
    Scalar radiusMax_;
    Scalar radiusWell_;
    Scalar radiusSpillPoint_;
    Scalar radiusWarningPoint_;
    Scalar alphaMax_;
    Scalar densityW_;
    Scalar temperature_;
    Scalar depthBOR_;
    Scalar pressureAtTOR_;
//    Scalar depthTOR_;
    Scalar thicknessCapRock_;
    Scalar thickness_;
    Scalar eps_;
    std::string name_;
    std::vector<int> globalIdxSP_; //Spill points
    std::vector<int> globalIdxWP_; //Warning points
    int end_;
    bool timeStepReduction_;
};
} //end namespace

#endif
