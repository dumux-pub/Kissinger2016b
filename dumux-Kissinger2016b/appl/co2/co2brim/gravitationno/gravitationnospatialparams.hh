// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The spatial parameters for the GravitationNoProblem which uses the
 *        twophase box model
 */
#ifndef DUMUX_GRAVITATION_NO_SPATIAL_PARAMS_HH
#define DUMUX_GRAVITATION_NO_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/implicitspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedlinearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class GravitationNoSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(GravitationNoSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(GravitationNoSpatialParams, SpatialParams, Dumux::GravitationNoSpatialParams<TypeTag>);

// Set the material Law
SET_PROP(GravitationNoSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedBrooksCorey<Scalar> EffectiveLaw;
//    typedef RegularizedLinearMaterial<Scalar> EffectiveLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffectiveLaw> type;
};
}
/*!
 * \brief The spatial parameters for the GravitationNoProblem which uses the
 *        two-phase box model
 */
template<class TypeTag>
class GravitationNoSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

public:
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    GravitationNoSpatialParams(const GridView& gridView)
        : ParentType(gridView)
    {
        try
        {
            thickness_          = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Thickness);
//            thicknessCapRock_   = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.ThicknessCapRock);
            reservoirPorosity_  = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.ReservoirPorosity);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }

        // residual saturations

        materialParamsReservoir_.setSwr(0.3);
        materialParamsReservoir_.setSnr(0.0);

        // parameters for the Brooks and Corey law
        // alpha and n
        materialParamsReservoir_.setPe(1e4);
        materialParamsReservoir_.setLambda(2.0);

        //linear parameters
//        materialParamsReservoir_.setEntryPc(1e4);
//        materialParamsReservoir_.setMaxPc(1e7);

        // residual saturations

        materialParamsBarrier_.setSwr(0.6);
        materialParamsBarrier_.setSnr(0.0);

        // parameters for the Brooks and Corey law
        // alpha and n
        materialParamsBarrier_.setPe(1e6);
        materialParamsBarrier_.setLambda(2.0);

        //linear parameters
//        materialParamsBarrier_.setEntryPc(1e6);
//        materialParamsBarrier_.setMaxPc(1e7);



        barrierPorosity_ = 0.01;
        barrierK_ = 1e-19;
        reservoirK_ = 1e-13;
    }


    void setThicknessCapRock(const Scalar thicknessCapRock)
    {
        thicknessCapRock_ = thicknessCapRock;
        std::cout<<"thickness caprock = "<<thicknessCapRock_<<std::endl;

    }
    /*!
     * \brief Intrinsic permability
     *
     * \param element The current element
     * \param fvElemGeom The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     * \return Intrinsic permeability
     */
    Scalar intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvElemGeom,
                                 int scvIdx) const
    {
        const GlobalPosition &globalPos = fvElemGeom.subContVol[scvIdx].global;
        if (isReservoir_(globalPos))
            return reservoirK_;
        return barrierK_;
    }

    /*!
     * \brief Porosity
     *
     * \param element The current element
     * \param fvElemGeom The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     * \return Porosity
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx) const
    {
        const GlobalPosition &globalPos = fvElemGeom.subContVol[scvIdx].global;
        if (isReservoir_(globalPos))
            return reservoirPorosity_;
        return barrierPorosity_;
    }

    /*!
     * \brief Function for defining the parameters needed by constitutive relationships (kr-Sw, pc-Sw, etc.).
     *
     * \param element The current element
     * \param fvElemGeom The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     * \return the material parameters object
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvElemGeom,
                                                int scvIdx) const
    {
//        const GlobalPosition &globalPos = fvElemGeom.subContVol[scvIdx].global;
        const GlobalPosition &globalPos = fvElemGeom.subContVol[scvIdx].global;
        if (isReservoir_(globalPos))
            return materialParamsReservoir_;
        return materialParamsBarrier_;
    }


private:
    bool isReservoir_(const GlobalPosition &globalPos) const
    {
        if (globalPos[dim-1] < thickness_ + thicknessCapRock_  + 0.5 && globalPos[dim-1] > thicknessCapRock_ - 0.5)
            return true;
        else
            return false;
    }




    Scalar reservoirK_;
    Scalar barrierK_;
    Scalar reservoirPorosity_;
    Scalar barrierPorosity_;
    Scalar thickness_;
    Scalar thicknessCapRock_;


    MaterialLawParams materialParamsReservoir_;
    MaterialLawParams materialParamsBarrier_;
};

} // end namespace
#endif

