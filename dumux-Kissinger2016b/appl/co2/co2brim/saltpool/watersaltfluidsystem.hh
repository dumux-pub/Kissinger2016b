// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2011 by Andreas Lauser                                    *
 *   Copyright (C) 2010 by Bernd Flemisch                                    *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A fluid system with one phase and two components
 *        (interstitial fluid and TRAIL, a therapeutic agent for
 *        cancer therapy).
 */
#ifndef DUMUX_WATER_SALT_FLUID_SYSTEM_HH
#define DUMUX_WATER_SALT_FLUID_SYSTEM_HH

#include <dune/common/exceptions.hh>

#include <dumux/material/fluidsystems/basefluidsystem.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/brine_varSalinity.hh>
#include <dumux/material/components/tabulatedcomponent.hh>
#include <assert.h>

#include <dumux/common/propertysystem.hh>
#include <dumux/common/basicproperties.hh>


namespace Dumux
{
namespace FluidSystems
{

/*!
 * \ingroup Fluidsystems
 *
 * \brief A fluid system with one phase and two components
 *        (interstitial fluid and TRAIL, a therapeutic agent for
 *        cancer therapy).
 *
 * A fluid system with one phase and two components representing an
 * interstitial fluid that contains therapeutic agent (TRAIL). This is
 * used in conjunction the 1p2c model.
 */
template <class Scalar>
class WaterSalt
: public BaseFluidSystem<Scalar, WaterSalt<Scalar> >
{

public:
    typedef WaterSalt<Scalar> ThisType;
    typedef BaseFluidSystem<Scalar, ThisType> Base;
    typedef Dumux::H2O<Scalar> H2O_IAPWS;
    typedef Dumux::TabulatedComponent<Scalar, H2O_IAPWS> H2O;



    /****************************************
     * Fluid phase related static parameters
     ****************************************/

//    typedef H2O_IAPWS H2O;
    typedef Dumux::BrineVarSalinity<Scalar, H2O> Brine;
    //! Number of phases in the fluid system
    static constexpr int numPhases = 2;

    //! Index of the liquid phase
   // static constexpr int lPhaseIdx = 1;
    static constexpr int wPhaseIdx = 0; // index of the water phase
    static constexpr int nPhaseIdx = 1; // index of the air phase
    
    static const int wCompIdx = 0;
    static const int nCompIdx = 1;

    /*!
     * \brief Return the human readable name of a fluid phase
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static const char *phaseName(int phaseIdx)
    {
//        static const char *name[] = {
//            "l"
//        };
//
//        assert(0 <= phaseIdx && phaseIdx < numPhases);
//        return name[phaseIdx];
        switch (phaseIdx) {
        case wPhaseIdx: return "liquid";
        case nPhaseIdx: return "gas";
        };
        DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
    }

    /*!
     * \brief Return whether a phase is liquid
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isLiquid(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
  			return phaseIdx != nPhaseIdx; 
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be an ideal mixture.
     *
     * We define an ideal mixture as a fluid phase where the fugacity
     * coefficients of all components times the pressure of the phase
     * are indepent on the fluid composition. This assumtion is true
     * if Henry's law and Rault's law apply. If you are unsure what
     * this function should return, it is safe to return false. The
     * only damage done will be (slightly) increased computation times
     * in some cases.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isIdealMixture(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        // we assume Henry's and Rault's laws for the water phase and
        // and no interaction between gas molecules of different
        // components, so all phases are ideal mixtures!
        return true;
    }

    /*!
     * \brief Returns true if and only if a fluid phase is assumed to
     *        be compressible.
     *
     * Compressible means that the partial derivative of the density
     * to the fluid pressure is always larger than zero.
     *
     * \param phaseIdx The index of the fluid phase to consider
     */
    static bool isCompressible(int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        return true;
    }

    /****************************************
     * Component related static parameters
     ****************************************/

    //! Number of components in the fluid system
    static constexpr int numComponents = 2;

    static constexpr int H2OIdx = 0;
    static constexpr int SaltIdx = 1;




    /*!
     * \brief Return the human readable name of a component
     *
     * \param compIdx The index of the component to consider
     */
    static const char *componentName(int compIdx)
    {
        static const char *name[] = {
            "Water",
            "Salt"
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return name[compIdx];
    }

    /*!
     * \brief Return the molar mass of a component in [kg/mol].
     *
     * \param compIdx The index of the component to consider
     */
    static Scalar molarMass(int compIdx)
    {
        static const Scalar M[] = {
            18e-3, // [kg/mol],
            58.44e-3, // [kg/mol]
        };

        assert(0 <= compIdx && compIdx < numComponents);
        return M[compIdx];
    }

    /****************************************
     * thermodynamic relations
     ****************************************/

    /*!
     * \brief Initialize the fluid system's static parameters generically
     *
     * If a tabulated H2O component is used, we do our best to create
     * tables that always work.
     */
    static void init()
    {
//        init(/*startTemp=*/273.15, /*endTemp=*/623.15, /*tempSteps=*/100,
//                /*startPressure=*/1e4, /*endPressure=*/40e6, /*pressureSteps=*/200);
    }

    static void init(Scalar startTemp, Scalar endTemp, int tempSteps,
            Scalar startPressure, Scalar endPressure, int pressureSteps)
    {
//        if(H2O::isTabulated)
//        {
//            std::cout << "Initializing tables for the pure-water properties.\n";
//            H2O::init(startTemp, endTemp, tempSteps,
//                    startPressure, endPressure, pressureSteps);
//        }
        // set the salinity of brine
        //            BrineRawComponent::salinity = salinity;
        //
        //            if(Brine::isTabulated)
        //            {
        //                std::cout << "Initializing tables for the brine fluid properties.\n";
        //                Brine::init(startTemp, endTemp, tempSteps,
        //                                      startPressure, endPressure, pressureSteps);
        //            }
    }


    /*!
     * \brief Return the phase density [kg/m^3].
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    using Base::density;
    template <class FluidState>
    static Scalar density(const FluidState &fluidState,
                          int phaseIdx)
    {
//        assert(0 <= phaseIdx && phaseIdx < numPhases);
//
//        Scalar temperature = fluidState.temperature(phaseIdx);
//        Scalar pressure = fluidState.pressure(phaseIdx);
//        Scalar massFracSalt = fluidState.massFraction(phaseIdx, SaltIdx);
//     //   return 1000;
//
//     if (phaseIdx == nPhaseIdx)
//     {
//     return 9999;
//     }
//	 else
//       return Brine::liquidDensity(temperature, pressure, massFracSalt);
//
        DUNE_THROW(Dune::NotImplemented, "density in parent type of fluidsystem.");

    }

    /*!
     * \brief Calculate the fugacity coefficient [Pa] of an individual
     *        component in a fluid phase
     *
     * The fugacity coefficient \f$\phi_\kappa\f$ is connected to the
     * fugacity \f$f_\kappa\f$ and the component's molarity
     * \f$x_\kappa\f$ by means of the relation
     *
     * \f[ f_\kappa = \phi_\kappa * x_{\kappa} \f]
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     */
    using Base::fugacityCoefficient;
    template <class FluidState>
    static Scalar fugacityCoefficient(const FluidState &fluidState,
                                      int phaseIdx,
                                      int compIdx)
    {
        assert(0 <= phaseIdx  && phaseIdx < numPhases);
        assert(0 <= compIdx  && compIdx < numComponents);
        if (phaseIdx == wPhaseIdx) {
                  if (compIdx == H2OIdx)
                      return 1000;
                  else
                  return 0.5;
              }
        else

        return 1.0;
    }

    /*!
     * \brief Return the dynamic viscosity of a phase [Pa s].
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    using Base::viscosity;
    template <class FluidState>
    static Scalar viscosity(const FluidState &fluidState,
                            int phaseIdx)
    {
//        assert(0 <= phaseIdx && phaseIdx < numPhases);
//        Scalar temperature = fluidState.temperature(phaseIdx);
//        Scalar pressure = fluidState.pressure(phaseIdx);
//        Scalar massFracSalt = fluidState.massFraction(phaseIdx, SaltIdx);
//
//        if (phaseIdx == nPhaseIdx)
//        {
//        return 9999;
//        }
//        else
//          return Brine::liquidViscosity(temperature, pressure, massFracSalt);
        DUNE_THROW(Dune::NotImplemented, "viscosity in parent type of fluidsystem.");
    }

    /*!
     * \brief Calculate the molecular diffusion coefficient for a
     *        component in a fluid phase [mol^2 * s / (kg*m^3)]
     *
     * Molecular diffusion of a compoent \f$\kappa\f$ is caused by a
     * gradient of the chemical potential and follows the law
     *
     * \f[ J = - D \mathbf{grad} mu_\kappa \f]
     *
     * where \f$\mu_\kappa\f$ is the component's chemical potential,
     * \f$D\f$ is the diffusion coefficient and \f$J\f$ is the
     * diffusive flux. \f$mu_\kappa\f$ is connected to the component's
     * fugacity \f$f_\kappa\f$ by the relation
     *
     * \f[ \mu_\kappa = R T_\alpha \mathrm{ln} \frac{f_\kappa}{p_\alpha} \f]
     *
     * where \f$p_\alpha\f$ and \f$T_\alpha\f$ are the fluid phase'
     * pressure and temperature.
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIdx The index of the component to consider
     */
    using Base::diffusionCoefficient;
    template <class FluidState>
    static Scalar diffusionCoefficient(const FluidState &fluidState,
                                       int phaseIdx,
                                       int compIdx)
    {
        // TODO!
        DUNE_THROW(Dune::NotImplemented, "Diffusion coefficients");
    }


    /*!
     * \brief Given a phase's composition, temperature and pressure,
     *        return the binary diffusion coefficient for components
     *        \f$i\f$ and \f$j\f$ in this phase.
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     * \param compIIdx The index of the first component to consider
     * \param compJIdx The index of the second component to consider
     */
    using Base::binaryDiffusionCoefficient;
    template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState &fluidState,
                                             int phaseIdx,
                                             int compIIdx,
                                             int compJIdx)

    {
        DUNE_THROW(Dune::NotImplemented, "binaryDiffusionCoefficient in parent type of fluidsystem.");
    }

    /*!
     * \brief Given a phase's composition, temperature, pressure and
     *        density, calculate its specific enthalpy [J/kg].
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx  for which phase to give back the heat capacity
     */
    using Base::enthalpy;
    template <class FluidState>
    static Scalar enthalpy(const FluidState &fluidState,
                                 int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        Scalar temperature = fluidState.temperature(phaseIdx);
        Scalar pressure = fluidState.pressure(phaseIdx);
        Scalar massFracSalt = fluidState.massFraction(phaseIdx, SaltIdx);


        if (phaseIdx == nPhaseIdx)
        {
        return 9999;
        }
        else
          return Brine::liquidEnthalpy(temperature, pressure, massFracSalt);

//        DUNE_THROW(Dune::NotImplemented, "Enthalpies");
    }

    /*!
     * \brief Thermal conductivity of a fluid phase [W/(m^2 K/m)].
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx  for which phase to give back the heat capacity
     */
    using Base::thermalConductivity;
    template <class FluidState>
    static Scalar thermalConductivity(const FluidState &fluidState,
                                      int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        if (phaseIdx == wPhaseIdx)
            return  0.6; // conductivity of water[W / (m K ) ]

        else
            return 9999;
//            DUNE_THROW(Dune::NotImplemented, "Thermal conductivities.");
    }

    /*!
     * \brief Specific isobaric heat capacity of a fluid phase.
     *        \f$\mathrm{[J/kg]}\f$.
     *
     * \param fluidState An abitrary fluid state
     * \param phaseIdx The index of the fluid phase to consider
     */
    using Base::heatCapacity;
    template <class FluidState>
    static Scalar heatCapacity(const FluidState &fluidState,
                               int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < numPhases);
        if(phaseIdx == wPhaseIdx)
            return H2O::liquidHeatCapacity(fluidState.temperature(phaseIdx),
                                           fluidState.pressure(phaseIdx));
        else
            return 9999;
//            DUNE_THROW(Dune::NotImplemented, "Heat capacities.");
    }

private:




   };






/*!
 * \brief A pure single-phase fluid system.
 *
 * This is an adapter to use Dumux::WaterSaltFluidSystem<TypeTag>, as is
 * done with most other classes in Dumux and all template parameters
 * are usually defined in the property system anyhow.
 */
template<class TypeTag>
class WaterSaltFluidSystem: public FluidSystems::WaterSalt<typename GET_PROP_TYPE(TypeTag, Scalar)>
{
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Scalar)) Scalar;
    typedef typename FluidSystems::WaterSalt<Scalar> ParentType;
    typedef WaterSaltFluidSystem<TypeTag> ThisType;


public:

    typedef Dumux::NullParameterCache ParameterCache;

    //Density calculation according to Diersch and Kolditz, 2002 Eq. 20
    //in [kg/m3]
    template <class FluidState>
    static Scalar density(const FluidState &fluidState,  const ParameterCache &paramCache, int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < ParentType::numPhases);
        Scalar alpha = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, Alpha);
        Scalar referenceDensity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, ReferenceDensity);
        Scalar massFractionSalt = fluidState.massFraction(phaseIdx, ParentType::SaltIdx);
        Scalar density = referenceDensity*(1 + alpha*massFractionSalt);
        return density;
    }

    //Overload the density function without the FluidState as an argument for calling this function in the problem
    static Scalar density(const Scalar massFractionSalt)
    {
        Scalar alpha = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, Alpha);
        Scalar referenceDensity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, ReferenceDensity);
        Scalar density = referenceDensity*(1 + alpha*massFractionSalt);
        return density;
    }

    //Viscosity calculation according to Diersch and Kolditz, 2002 Table 5 variable fluid viscosity
    //in [Pa s]
    template <class FluidState>
    static Scalar viscosity(const FluidState &fluidState,  const ParameterCache &paramCache, int phaseIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < ParentType::numPhases);
        Scalar referenceViscosity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, ReferenceViscosity);
        Scalar massFractionSalt = fluidState.massFraction(phaseIdx, ParentType::SaltIdx);
        Scalar viscosity = referenceViscosity*(1 + 1.85*massFractionSalt - 4.1*massFractionSalt*massFractionSalt + 44.5*massFractionSalt*massFractionSalt*massFractionSalt);
        return viscosity;
    }

    //Get the diffusion coefficient from the parameter file
    //in [m2/s]
    template <class FluidState>
    static Scalar binaryDiffusionCoefficient(const FluidState &fluidState, const ParameterCache &paramCache,
                                                int phaseIdx,
                                                int compIIdx,
                                                int compJIdx)
    {
        assert(0 <= phaseIdx && phaseIdx < ParentType::numPhases);
        Scalar binaryDiffusionCoefficient = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FluidSystem, DiffusionCoefficient);
        return binaryDiffusionCoefficient;
    }
};
} // end namepace FluidSystem
} // end namepace Dumux

#endif
