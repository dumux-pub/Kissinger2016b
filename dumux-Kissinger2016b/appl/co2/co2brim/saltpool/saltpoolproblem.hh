// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief Definition of a problem, for the 1p2c problem:
 * Component transport of nitrogen dissolved in the water phase.
 */
#ifndef DUMUX_SALTPOOL_PROBLEM_HH
#define DUMUX_SALTPOOL_PROBLEM_HH

#include <dune/grid/io/file/dgfparser/dgfyasp.hh>

#include <dumux/implicit/1p2c/1p2cmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/linear/amgbackend.hh>

#include "watersaltfluidsystem.hh"
#include "saltpoolspatialparams.hh"

//Linear solvers
#include <dumux/linear/amgbackend.hh>
#include <dumux/linear/seqsolverbackend.hh>



#define NONISOTHERMAL 0 

namespace Dumux
{

template <class TypeTag>
class SaltPoolProblem;

namespace Properties
{
#if NONISOTHERMAL
NEW_TYPE_TAG(SaltPoolProblem, INHERITS_FROM(OnePTwoCNI));
NEW_TYPE_TAG(SaltPoolBoxProblem, INHERITS_FROM(BoxModel, SaltPoolProblem));
NEW_TYPE_TAG(SaltPoolCCProblem, INHERITS_FROM(CCModel, SaltPoolProblem));
#else
NEW_TYPE_TAG(SaltPoolProblem, INHERITS_FROM(OnePTwoC));
NEW_TYPE_TAG(SaltPoolBoxProblem, INHERITS_FROM(BoxModel, SaltPoolProblem));
NEW_TYPE_TAG(SaltPoolCCProblem, INHERITS_FROM(CCModel, SaltPoolProblem));
#endif

// Set the grid type
SET_PROP(SaltPoolProblem, Grid)
{
    typedef Dune::YaspGrid<3> type;

};
// Set the problem property
SET_PROP(SaltPoolProblem, Problem)
{
    typedef Dumux::SaltPoolProblem<TypeTag> type;
};

// Set fluid configuration
SET_PROP(SaltPoolProblem, FluidSystem)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::FluidSystems::WaterSaltFluidSystem<TypeTag> type;
};

// Set the spatial parameters
SET_TYPE_PROP(SaltPoolProblem,
              SpatialParams,
              Dumux::SaltPoolSpatialParams<TypeTag>);

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(SaltPoolProblem, UseMoles, false);

// Enable velocity output
SET_BOOL_PROP(SaltPoolProblem, VtkAddVelocity, true);

// Disable gravity
SET_BOOL_PROP(SaltPoolProblem, ProblemEnableGravity, true);

// Linear solver settings
SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::AMGBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::ILUnBiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::SORBiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::SSORBiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::GSBiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::JacBiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::ILUnCGBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::SORCGBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::SSORCGBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::GSCGBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::JacCGBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::SSORRestartedGMResBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::ILU0BiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::ILU0CGBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::ILU0RestartedGMResBackend<TypeTag> );
//SET_TYPE_PROP(SaltPoolProblem, LinearSolver, Dumux::ILUnRestartedGMResBackend<TypeTag> );
}


/*!
 * \ingroup OnePTwoCModel
 * \ingroup ImplicitTestProblems
 *
 * \brief Definition of a problem, for the 1p2c problem:
 * Nitrogen is dissolved in the water phase and
 * is transported with the water flow from the left side to the right.
 *
 * The model domain is 1 m times 1 m with a discretization length of 0.05 m
 * and homogeneous soil properties (\f$ \mathrm{K=10e-10, \Phi=0.4, \tau=0.28}\f$).
 * Initially the domain is filled with pure water.
 *
 * At the left side, a Dirichlet condition defines a nitrogen mole fraction
 * of 0.3 mol/mol.
 * The water phase flows from the left side to the right due to the applied pressure
 * gradient of 1e5 Pa/m. The nitrogen is transported with the water flow
 * and leaves the domain at the right boundary
 * where an outflow boundary condition is applied.
 * 
 * The model is able to use either mole or mass fractions. The property useMoles can be set to either true or false in the
 * problem file. Make sure that the according units are used in the problem setup. The default setting for useMoles is true.
 *
 * This problem uses the \ref OnePTwoCModel model.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box1p2c -parameterFile ./test_box1p2c.input</tt> or 
 * <tt>./test_cc1p2c -parameterFile ./test_cc1p2c.input</tt>
 */
template <class TypeTag>
class SaltPoolProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // world dimension
        dimWorld = GridView::dimensionworld,
        dim = GridView::dimension
    };
    enum {
        // indices of the primary variables
        pressureIdx = Indices::pressureIdx,
        massOrMoleFracIdx = Indices::massOrMoleFracIdx,
#if NONISOTHERMAL
        temperatureIdx = Indices::temperatureIdx
#endif
    };
    enum {
        // index of the transport equation
        conti0EqIdx = Indices::conti0EqIdx,
        transportEqIdx = Indices::transportEqIdx,
#if NONISOTHERMAL
        energyEqIdx = Indices::energyEqIdx
#endif
    };

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::Intersection Intersection;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    //! property that defines whether mole or mass fractions are used
        static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);

public:
    SaltPoolProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
        , eps_(1e-6)
    {
        //initialize fluid system
        FluidSystem::init();

        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        initialSaltWaterHeight_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InitialSaltWaterHeight);
        rate_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Rate);
        initialSaltMassFraction_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, InitialSaltMassFraction);
        dirichletPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, DirichletPressure);

        //get the bounding box
        upperRightX_ =  this->bBoxMax()[0];
        upperRightY_ =  this->bBoxMax()[1];
        upperRightZ_ =  this->bBoxMax()[dimWorld-1];

        //stating in the console whether mole or mass fractions are used
        if(!useMoles)
        {
        	std::cout<<"problem uses mass-fractions"<<std::endl;
        }
        else
        {
        	std::cout<<"problem uses mole-fractions"<<std::endl;
        }
        episodeLength_ = 4500;
        this->timeManager().startNextEpisode(episodeLength_);
    }

    /*!
     * \name Problem parameters
     */
    // \{
    bool shouldWriteOutput()
    {
      return (this->timeManager().timeStepIndex() == 0) ||
             this->timeManager().episodeWillBeOver() ||
             this->timeManager().willBeFinished();
    }
    
    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     *
     * The default behaviour is to write one restart file every 5 time
     * steps. This file is intented to be overwritten by the
     * implementation.
     */
    bool shouldWriteRestartFile() const
    {
        return false;
    }

        void episodeEnd()
            {
                // Start new episode if episode is over and assign new boundary conditions
                this->timeManager().startNextEpisode(episodeLength_);
                this->timeManager().setTimeStepSize(this->timeManager().timeStepSize());
            }
    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

    void postTimeStep()
    {
        // Calculate storage terms
        PrimaryVariables storage;

        this->model().globalStorage(storage);
        Scalar saltMassFrac = -1;
        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator elemEndIt = this->gridView().template end<0>();
        for (; eIt != elemEndIt; ++eIt)
        {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {
                FVElementGeometry fvGeometry;
                fvGeometry.update(this->gridView(), *eIt);

                int numVerts = eIt->template count<dim> ();

                for (int scvIdx = 0; scvIdx < numVerts; ++scvIdx)
                {
                    const GlobalPosition &globalPos = fvGeometry.subContVol[scvIdx].global;

                    if(globalPos[0] > upperRightX_ - eps_ && globalPos[1] > upperRightY_ - eps_ &&
                            globalPos[dimWorld -1] > upperRightZ_ - eps_)
                    {
                        //get the right dof index (vertex or element index)
                        int dofIdxGlobal;
                        if(isBox)
                            dofIdxGlobal = this->model().dofMapper().map(*eIt, scvIdx, dofCodim);
                        else
                            dofIdxGlobal = this->model().dofMapper().map(*eIt, /*scvIdx*/0, dofCodim);

                        saltMassFrac = this->model().curSol()[dofIdxGlobal][massOrMoleFracIdx];
                    }
                }
            }
        }
 

        Scalar time = this->timeManager().time();
        Scalar dt = this->timeManager().timeStepSize();

        //make sure to take the maximum saltMassFrac of all processes.
        if (this->gridView().comm().size() > 1)
            saltMassFrac = this->gridView().comm().max(saltMassFrac);
        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0)
        {               // std::setprecision(10);
            std::cout<<"Time: "<<time+dt <<";"<< " Time step size: "<< dt <<";"<<std::endl;
            std::cout<<"Storage: "<< storage <<";"<< std::endl;
	    std::cout<<"Salt MassFrac at Outflow: "<<saltMassFrac<<";"<<std::endl;
        }
    }

#if !NONISOTHERMAL
    /*!
     * \brief Returns the temperature within the domain [K].
     *
     * This problem assumes a temperature of 20 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 20; } // in [K]

#endif

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position for which the bc type should be evaluated
     */
    void boundaryTypesAtPos(BoundaryTypes &values, 
                            const GlobalPosition &globalPos) const
    {
        values.setAllNeumann();
      
        //Inflow upper left, front corner
        if(globalPos[dimWorld-1] > this->bBoxMax()[dimWorld-1] - eps_ &&
                globalPos[0] < eps_ && globalPos[1] < eps_)
        {
            values.setAllDirichlet();
//            std::cout<<"Global Position Dirichlet: "<<globalPos<<std::endl;
        }

    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The position for which the bc type should be evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        //Inflow upper left, front upper corner with constant pressure and zero salt mass fraction
        values[pressureIdx] = dirichletPressure_;
        values[massOrMoleFracIdx] = 0.0;
    }


  /*!
      * \brief Evaluate the boundary conditions for a neumann
      *        boundary segment.
      *
      * This is the method for the case where the Neumann condition is
      * potentially solution dependent and requires some quantities that
      * are specific to the fully-implicit method.
      *
      * \param values The neumann values for the conservation equations in units of
      *                 \f$ [ \textnormal{unit of conserved quantity} / (m^2 \cdot s )] \f$
      * \param element The finite element
      * \param fvGeometry The finite-volume geometry
      * \param intersection The intersection between element and boundary
      * \param scvIdx The local subcontrolvolume index
      * \param boundaryFaceIdx The index of the boundary face
      * \param elemVolVars All volume variables for the element
      *
      * For this method, the \a values parameter stores the mass flux
      * in normal direction of each phase. Negative values mean influx.
      */
     void solDependentNeumann(PrimaryVariables &values,
                       const Element &element,
                       const FVElementGeometry &fvGeometry,
                       const Intersection &is,
                       const int scvIdx,
                       const int boundaryFaceIdx,
                       const ElementVolumeVariables &elemVolVars) const



  /*   {
         values = 0;
         //Get the position of the integration point at the boundary
         GlobalPosition globalPos = fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;
         Scalar distanceVertIP = std::sqrt(is.geometry().volume())/4;

         //Outflow
         if(globalPos[dimWorld-1] > this->bBoxMax()[dimWorld-1] - eps_ &&
                 globalPos[0] > this->bBoxMax()[0] - distanceVertIP - eps_ &&
                 globalPos[1] > this->bBoxMax()[1] - distanceVertIP - eps_)
         {
             Scalar area = fvGeometry.boundaryFace[boundaryFaceIdx].area;
             Scalar rho = elemVolVars[scvIdx].density();
             Scalar massFracSalt = elemVolVars[scvIdx].massFraction(massOrMoleFracIdx);
             values[conti0EqIdx] = rho*rate_/area;
             values[transportEqIdx] =  rho*rate_/area*massFracSalt;
//             std::cout<<"Global Position IP: "<<globalPos<<std::endl;
         }
     }
*/
   {
         values = 0;
         //Get the position of the integration point at the boundary
         GlobalPosition globalPos = fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;
         Scalar distanceVertIP;
         if(isBox)
             distanceVertIP = std::sqrt(is.geometry().volume())/4;
         else
             distanceVertIP = std::sqrt(is.geometry().volume())/2;

         //Outflow
         if(globalPos[dimWorld-1] > this->bBoxMax()[dimWorld-1] - eps_ &&
                 globalPos[0] > this->bBoxMax()[0] - distanceVertIP - eps_ &&
                 globalPos[1] > this->bBoxMax()[1] - distanceVertIP - eps_)
         {
             Scalar area = fvGeometry.boundaryFace[boundaryFaceIdx].area;
             Scalar rho = elemVolVars[scvIdx].density();
             Scalar massFracSalt = elemVolVars[scvIdx].massFraction(massOrMoleFracIdx);
             values[conti0EqIdx] = rho*rate_/area;
             values[transportEqIdx] =  rho*rate_/area*massFracSalt;
//             std::cout<<"Global Position IP: "<<globalPos<<std::endl;
         }
     }
    
   

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a priVars parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^3*s) or kg/(m^3*s))
     */
    void sourceAtPos(PrimaryVariables &priVars,
                     const GlobalPosition &globalPos) const
    {
        priVars = Scalar(0.0);
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }

    // \}

private:
    // the internal method for the initial condition
    void initial_(PrimaryVariables &priVars,
                  const GlobalPosition &globalPos) const
    {
        Scalar rhoBrine = FluidSystem::density(initialSaltMassFraction_);
        Scalar rhoFreshWater = FluidSystem::density(0);
        Scalar zMax = this->bBoxMax()[dimWorld-1];
        Scalar brineHeight;
        Scalar freshWaterHeight;
        if((zMax - globalPos[dimWorld-1]) < (zMax - initialSaltWaterHeight_))
        {
            freshWaterHeight = zMax - globalPos[dimWorld-1];
            brineHeight = 0.0;
        }
        else
        {
            freshWaterHeight = zMax - initialSaltWaterHeight_;
            brineHeight = initialSaltWaterHeight_ - globalPos[dimWorld-1];
        }
        priVars[pressureIdx] = dirichletPressure_ + 9.81*(rhoFreshWater*freshWaterHeight + rhoBrine*brineHeight); // initial condition for the pressure

        if((zMax - globalPos[dimWorld-1]) < (zMax - initialSaltWaterHeight_))
            priVars[massOrMoleFracIdx] = 0.0;  // initial condition for the fresh water
        else
            priVars[massOrMoleFracIdx] = initialSaltMassFraction_;  // initial condition for the fresh water
#if NONISOTHERMAL
        priVars[temperatureIdx] = 290.;
#endif
    }

    const Scalar eps_;
    //Input parameters declaration
    std::string name_;
    Scalar initialSaltWaterHeight_;
    Scalar rate_;
    Scalar initialSaltMassFraction_;
    Scalar dirichletPressure_;
    Scalar episodeLength_;
    Scalar upperRightX_;
    Scalar upperRightY_;
    Scalar upperRightZ_;
};

} //end namespace
#endif
