// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outlfow problem.
 */
#ifndef DUMUX_1P2CNI_OUTFLOW_SPATIAL_PARAMS_HH
#define DUMUX_1P2CNI_OUTFLOW_SPATIAL_PARAMS_HH

#if MODELTYPE == 1
#include <dumux/implicit/dfm/common/implicitdfmspatialparams1p.hh>
#else /*MODELTYPE == 2*/
#include <dumux/implicit/dfm/common/implicitdfmspatialparams.hh>
#endif
//#include <dumux/material/spatialparams/implicitspatialparams1p.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dune/common/float_cmp.hh>

#include "../helperclasses/vertidxtoelemneighbormapper.hh"

namespace Dumux
{

//forward declaration
template<class TypeTag>
class SollingSpatialParams;

namespace Properties
{

#if MODELTYPE != 1
// The spatial parameters TypeTag
NEW_TYPE_TAG(SollingSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(SollingSpatialParams, SpatialParams, Dumux::SollingSpatialParams<TypeTag>);
// Set the material Law
SET_PROP(SollingSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffMaterialLaw> type;
};

#endif
}
/*!
 * \ingroup OnePTwoCBoxModel
 * \ingroup ImplicitTestProblems
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outflow problem.
 */
template<class TypeTag>
#if MODELTYPE == 1
class SollingSpatialParams : public ImplicitDFMSpatialParamsOneP<TypeTag>
#else
class SollingSpatialParams : public ImplicitDFMSpatialParams<TypeTag>
#endif
{
    // Parent Type
#if MODELTYPE == 1
    typedef ImplicitDFMSpatialParamsOneP<TypeTag> ParentType;
#else
    typedef ImplicitDFMSpatialParams<TypeTag> ParentType;
#endif

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef Dune::GridPtr<Grid> GridPointer;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;

    typedef typename GridView:: template Codim<0>::Iterator ElementIterator;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;


    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld,
        pressureIdx = Indices::pressureIdx,
    };
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;
  //  typedef Dune::ReservoirPropertyCapillary<3> ReservoirProperties;
    typedef typename GET_PROP(TypeTag, ParameterTree) Params;

    typedef std::vector<Scalar> PermeabilityType;
    typedef std::vector<Scalar> PorosityType;
#if MODELTYPE == 2
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;
#endif
    typedef Dumux::VertIdxToElemNeighborMapper<GridView> VertIdxToElemNeighborMapper;
    typedef typename VertIdxToElemNeighborMapper::NeighborElementSeedsIterator NeighborElementSeedsIterator;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    //typedef LinearMaterial<Scalar> EffMaterialLaw;
public:

    enum {
        //Layer indices
        quarIdx=0,
        terIdx=1,
        rcIdx=2,
        oliIdx=3,
        obsIdx=4,
        mbsIdx=5,
        solIdx=6,
        umbsIdx=7,
        ubsIdx=8,
        zecIdx=9,
        kreideIdx=10,
        tzIdx=11,
        quarTopIdx = 12,

        //Flux line indices
        noFluxIdx=0,
        rupelFluxIdx=1,
    };

    enum {
        //indices of element parameters specified in dgf file
        outerDomainEIdx = 0
    };

    SollingSpatialParams(const GridView &gridView, const Problem &problem)
    : ParentType(gridView, problem), gridView_(gridView), vertexElementMapper_(gridView)
    {
        try
        {
            Swr_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Swr);
            Snr_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Snr);
            brooksCoreyPe_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BrooksCoreyPe);
            brooksCoreyLambda_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BrooksCoreyLambda);
            compressibility_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Compressibility);
            longDispersion_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.LongDispersion);
            transDispersion_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.TransDispersion);
            initialPermeability_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.InitialPermeability);
            arrayPerm_[quarTopIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermQuarTop);
            arrayPerm_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermQuar);
            arrayPerm_[terIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermTer);
            arrayPerm_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermRC);
            arrayPerm_[oliIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermOli);
            arrayPerm_[obsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermOBS);
            arrayPerm_[mbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermMBS);
            arrayPerm_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermSol);
            arrayPerm_[umbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermUMBS);
            arrayPerm_[ubsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermUBS);
            arrayPerm_[zecIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermZec);
            arrayPerm_[kreideIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermKreide);
            arrayPerm_[tzIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermTZ);
            arrayPor_[quarTopIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorQuarTop);
            arrayPor_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorQuar);
            arrayPor_[terIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorTer);
            arrayPor_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorRC);
            arrayPor_[oliIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorOli);
            arrayPor_[obsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorOBS);
            arrayPor_[mbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorMBS);
            arrayPor_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorSol);
            arrayPor_[umbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorUMBS);
            arrayPor_[ubsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorUBS);
            arrayPor_[zecIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorZec);
            arrayPor_[kreideIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorKreide);
            arrayPor_[tzIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorTZ);
            arrayBotLayer_[quarTopIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotQuarTop);
            arrayBotLayer_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotQuar);
            arrayBotLayer_[terIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotTer);
            arrayBotLayer_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotRC);
            arrayBotLayer_[oliIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotOli);
            arrayBotLayer_[obsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotOBS);
            arrayBotLayer_[mbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotMBS);
            arrayBotLayer_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotSol);
            arrayBotLayer_[umbsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotUMBS);
            arrayBotLayer_[ubsIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotUBS);
            arrayBotLayer_[zecIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotZec);
            arrayBotLayer_[kreideIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotKreide);
            tzWidth_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.TzWidth);
            hCellWidth_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.HCellWidth);
            //get the mass reduction factor
            massReductionFactor_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.MassReductionFactor);
            fractureOff_ = GET_RUNTIME_PARAM(TypeTag, bool, SpatialParams.FractureOff);

            //Position and radius holes Rupel:
            holesRupelClosed_ = GET_RUNTIME_PARAM(TypeTag, bool, SpatialParams.HolesRupelClosed);
            hole1_ = GET_RUNTIME_PARAM(TypeTag, GlobalPosition, SpatialParams.Hole1);
            hole1Radius_ =  GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Hole1Radius);
            hole2_ = GET_RUNTIME_PARAM(TypeTag, GlobalPosition, SpatialParams.Hole2);
            hole2Radius_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Hole2Radius);
            hole3_ = GET_RUNTIME_PARAM(TypeTag, GlobalPosition, SpatialParams.Hole3);
            hole3Radius_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Hole3Radius);
            hole3Thickness_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Hole3Thickness);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown while reading in parameters in the spatial parameters file!\n";
            exit(1);
        }
        //initialize eps_
        eps_ = 1e-6;

        //initialize fracture faces by calling private method
        initFracFaces_();

        //initialize permeability and porosity vectors by calling private method
        initPermPor_();

        //initialize salt boundary by calling private method
        initSaltVertex_();

        //initialize salt boundary by calling private method
        initVertexTerQuar_();

#if MODELTYPE == 2
        //parameters for 2pdfm model required by the 2pdfm volume variables
        swrf_    = 0.00;
        swrm_    = 0.00;
        SnrF_    = 0.00;
        SnrM_    = 0.00;
        pdf_     = 1000; //2.5*1e4;
        pdm_     = 2000; //2.5*1e4;
        lambdaF_ = 2.0;
        lambdaM_ = 2.0;
        // residual saturations
        materialParams_.setSwr(Swr_);
        materialParams_.setSnr(Snr_);

        // parameters for the Brooks-Corey law
        materialParams_.setPe(brooksCoreyPe_);
        materialParams_.setLambda(brooksCoreyLambda_);
#endif
    }

    ~SollingSpatialParams()
    {}

    /*!
     * \brief Update the spatial parameters with the flow solution
     *        after a timestep.
     *
     * \param globalSolution the global solution vector
     */
//    void update(const SolutionVector &globalSolution)
//    {
//    };

    /*!
     * \brief Define the intrinsic permeability \f$\mathrm{[m^2]}\f$.
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    Scalar intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvElemGeom,
                                 int scvIdx) const
    {
    	if(this->problem().timeManager().episodeIndex() == Problem::initPressureEpsIdx &&
    			Dune::FloatCmp::lt<Scalar>(perm_[this->problem().elementMapper().index(element)], initialPermeability_))
        {
            return initialPermeability_; //high permeability for creating hydrostatic conditions
        }
        return perm_[this->problem().elementMapper().index(element)];
    }

    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx) const
    {
        return 0;
    }

    /*!
     * \brief Define the porosity \f$\mathrm{[-]}\f$.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx,
                    Scalar pressure) const
    {

        int dofIdxGlobal = this->problem().model().dofMapper().subIndex(element, scvIdx, dofCodim);
        int eIdxGlobal = gridView_.indexSet().index(element);

        //effective porosity calculation according to Schäfer et al. 2011
        Scalar refPorosity = porosity_[eIdxGlobal];
        if(this->problem().timeManager().episodeIndex() == Problem::initPressureEpsIdx)
        {
            refPorosity *= massReductionFactor_;
            return refPorosity;
        }
        //        Scalar compressibilty = 4.5e-10; // 1/Pa
        Scalar refPressure = this->problem().initialPVMap_.find(dofIdxGlobal)->second[pressureIdx];
        Scalar X = compressibility_*(pressure - refPressure);
        Scalar effPorosity = refPorosity*(1 + X + std::pow(X,2)/2);
        return effPorosity;
    }

#if MODELTYPE == 2
    /*!
     * \brief Function for defining the parameters needed by constitutive relationships (kr-sw, pc-sw, etc.).
     *
     * \param element The current element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume.
     * \return the material parameters object of the Fracture
     */
    const MaterialLawParams& fractureMaterialLawParams(const Element &element,
                                                    const FVElementGeometry &fvGeometry,
                                                    int scvIdx) const
    {
        // be picky if called for non-fracture vertices
        assert(fvGeometry.fracScv[scvIdx].isFractureScv);

        return materialParams_;
    }

    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvElemGeom,
                                                int scvIdx) const
    {
        return materialParams_;
    }
#endif

    /*!
     * \brief Define the dispersivity.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
    GlobalPosition dispersivity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
        GlobalPosition dispersivity;
        dispersivity[0]= longDispersion_;
        dispersivity[1]= transDispersion_;
        dispersivity[2]= transDispersion_;
        return dispersivity;
    }

    bool useTwoPointGradient(const Element &element,
                             const int vertexI,
                             const int vertexJ) const
    {
        return false;
    }

    /*!
      * \brief Define the fracture porosity \f$\mathrm{[-]}\f$.
      *
      * \param element The finite element
      * \param fvGeometry The finite volume geometry
      * \param localFaceIdx The local index of the face
      */
    Scalar fracturePorosity(const Element &element,
                            const FVElementGeometry &fvGeometry,
                            const int localFaceIdx) const
    {
        //Return pressure independent porosity in initialization period
        if(this->problem().timeManager().episodeIndex() == 1)
        {
            return arrayPor_[tzIdx]*massReductionFactor_;
        }
        return arrayPor_[tzIdx];
    }

     /*!
      * \brief Function for defining the intrinsic (absolute) fracture permeability.
      *
      * \param element The current element
      * \param fvGeometry The current finite volume geometry of the element
      * \param localFaceIdx The index of the face.
      * \return the intrinsic fracture permeability
      */
     Scalar fracturePermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 const int localFaceIdx) const
     {
         if(this->problem().timeManager().episodeIndex() == Problem::initPressureEpsIdx &&
                 Dune::FloatCmp::lt<Scalar>(arrayPerm_[tzIdx], initialPermeability_))
         {
             return initialPermeability_; //high permeability for creating hydrostatic conditions
         }
         return arrayPerm_[tzIdx];
     }

     /*!
      * \brief Function for defining the fracture width.
      *
      * \param element The current element
      * \param fvGeometry The current finite volume geometry of the element
      * \param localFaceIdx The index of the face.
      * \return the fracture width
      */
     Scalar fractureWidth(const Element &element,
                          const FVElementGeometry &fvGeometry,
                          const int localFaceIdx) const
     {
         return tzWidth_;
     }

    /*!
     * \brief Returns the heat capacity \f$[J/m^3 K]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
//    Scalar heatCapacity(const Element &element,
//                        const FVElementGeometry &fvGeometry,
//                        const int scvIdx) const
//    {
//        return
//            790 // specific heat capacity of granite [J / (kg K)]
//            * 2700 // density of granite [kg/m^3]
//            * (1 - porosity(element, fvGeometry, scvIdx));
//    }

    /*!
     * \brief Returns the thermal conductivity \f$[W/m^2]\f$ of the porous material.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
//    Scalar thermalConductivitySolid(const Element &element,
//                                    const FVElementGeometry &fvGeometry,
//                                    const int scvIdx) const
//    {
//        return lambdaSolid_;
//    }

    //Check if the vertex is at the transition zone, i.e. having both zec and other layer properties
    bool saltVertex(const int vIdxGlobal) const
    {
        return saltVertex_[vIdxGlobal];
    }

    //Find the element belonging to a river
    bool isRiver(const Element &element, const int scvIdx) const
    {
        return false;
    }

    //Find the element belonging to a river
    bool isRiver(const Vertex &vertex) const
    {
        return false;
    }

    //Check if the vertex has ter or quar layer neighbors
    bool terQuarVertex(const int vIdxGlobal) const
    {
        return terQuarVertex_[vIdxGlobal];
    }

    int findElementRowIdx(const Element &element) const
    {
        return 0;
    }

    //Find the elements belonging to the flux line
    int findFluxLineIdx(const Element &element) const
    {
        GlobalPosition globalPos = element.geometry().center();
        if(globalPos[dim-1] < arrayBotLayer_[rcIdx] && globalPos[dim-1] > arrayBotLayer_[terIdx])
            return rupelFluxIdx;
        else
            return noFluxIdx;
    }

    //Find the layer index from the grid file
    int findLayerIdx(const Element &element) const
    {
        GlobalPosition globalPos = element.geometry().center();
        //return the index of the layer so that it fits to the array starting with 0
        int layerIdx = initialLayerIdx(element);

        //Return layer index if we are in the outer domain
        if(this->problem().eInOuterDomain(element))
            return layerIdx;
        //Return zec
        if(layerIdx != rcIdx && globalPos[0] < hCellWidth_)
            return zecIdx;
        else if(layerIdx == rcIdx || layerIdx == oliIdx)
        {
            //Check if element center is within
            if(
                (
                    (
                        this->problem().globalPosInSphere(globalPos, hole1_, hole1Radius_)
                        || this->problem().globalPosInSphere(globalPos, hole2_, hole2Radius_)
                    )
                    && !holesRupelClosed_
                )
                ||
                (
                    this->problem().globalPosInSphere(globalPos, hole3_, hole3Radius_/2.0)
                    && globalPos[0] < hCellWidth_ + hole3Thickness_
                )
                ||
                (
                    layerIdx == oliIdx
                    && globalPos[0] < hCellWidth_ + hole3Thickness_
                )
              )
                return kreideIdx;
        }
        else
            return layerIdx;
        return layerIdx;
    }

#if MODELTYPE == 2
    //parameters for 2pdfm model required by the 2pdfm volume variables
    Scalar swrf_;
    Scalar swrm_;
    Scalar SnrF_;
    Scalar SnrM_;
    Scalar lambdaF_;
    Scalar lambdaM_;
    Scalar pdf_;
    Scalar pdm_;
#endif

private:

    //Initialize the fracture faces
    //Loop over all intersections and mark the fracture intersections with true by calling the setFractureFace() function in the
    //ParentType
    void initFracFaces_()
    {
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Initializing fracture faces."<<std::endl;
        }
        if(fractureOff_)
        {
            if(gridView_.comm().rank() == 0)
            {
                std::cout<<"Fracture turned off."<<std::endl;
            }
            return;
        }
        //Loop over all elements mark elements with interface zec/other
        ElementIterator eIt = gridView_.template begin<0>();
        const ElementIterator eEndIt = gridView_.template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            bool eInOuterDomain = this->problem().eInOuterDomain(*eIt);
            //Check if the layer idx is zec
            if(findLayerIdx(*eIt) == zecIdx && !eInOuterDomain)
            {
                //Loop over intersections
                IntersectionIterator isEndIt = gridView_.iend(*eIt);
                for (IntersectionIterator isIt = gridView_.ibegin(*eIt); isIt != isEndIt; ++isIt)
                {
                    //Check if element has neighbor
                    if(isIt->neighbor())
                    {
                        ElementPointer outsideEPtr(isIt->outside());
                        bool eOutsideInOuterDomain = this->problem().eInOuterDomain(*outsideEPtr);
                        int layerIdx = findLayerIdx(*outsideEPtr);
                        //Check if outside element is not zec and not ubs, which is a
                        //sufficient condition for a fracture face
                        if(layerIdx != zecIdx && layerIdx != ubsIdx && !eOutsideInOuterDomain)
                        {
                            //Get the local index of the face and call the ParentType to mark the face
                            int localFaceIdx = isIt->indexInInside();
                            ParentType::setFractureFace(*eIt, localFaceIdx, true);
                        }
                    }
                }
            }
        }
    }

    //Initialize the permeability and porosity.
    //Loop over all elements and obtain the permeability and porosity values from the gridPtr.
    //Store the values in the perm_ and porosity_ vectors.
    //Do some checks at the end.
    void initPermPor_()
    {
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Initializing permeabilities and porosities."<<std::endl;
        }
        int numElems = gridView_.size(0);
        perm_.resize(numElems);
        porosity_.resize(numElems);
        ElementIterator eIt = gridView_.template begin<0>();
        const ElementIterator eEndIt = gridView_.template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            int eIdxGlobal = gridView_.indexSet().index(*eIt);
            int layerIdx = findLayerIdx(*eIt);
            perm_[eIdxGlobal] = arrayPerm_[layerIdx];
            porosity_[eIdxGlobal] = arrayPor_[layerIdx];
        }

        //Check maximum and minimum values in perm_/porosity_ vectors
        Scalar maxPor = 0;
        Scalar minPor = 0;
        minPor = *std::min_element(porosity_.begin(), porosity_.end());
        maxPor = *std::max_element(porosity_.begin(), porosity_.end());

        Scalar maxPerm = 0;
        Scalar minPerm = 0;
        minPerm = *std::min_element(perm_.begin(), perm_.end());
        maxPerm = *std::max_element(perm_.begin(), perm_.end());

        //Compare over different processes
        if (gridView_.comm().size() > 1)
        {
            minPor = gridView_.comm().min(minPor);
            maxPor = gridView_.comm().min(maxPor);
            maxPerm = gridView_.comm().min(maxPerm);
            minPerm = gridView_.comm().min(minPerm);
        }
        //Output only for rank 0
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Minimum Perm: "<<minPerm<<" Maximum Perm: "<<maxPerm<<std::endl;
            std::cout<<"Minimum Porosity: "<<minPor<<" Maximum Porosity: "<<maxPor<<std::endl;
        }
    }

    //Check if the vertex is in the salt. Vertices at the border between salt and another layer can be assigned as salt vertices
    // depending on the property "depletedSaltBorder" (internal function)
    void initSaltVertex_()
    {
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Initializing salt vertices."<<std::endl;
        }
        int numVertex = gridView_.size(dim);
        saltVertex_.resize(numVertex, false);

        VertexIterator vIt = gridView_.template begin<dim>();
        VertexIterator vEndIt = gridView_.template end<dim>();
        for(; vIt != vEndIt; ++vIt)
        {
            int vIdxGlobal = this->problem().vertexMapper().index(*vIt);

            //Get the iterator over the neighbor elements
            NeighborElementSeedsIterator nESIt = vertexElementMapper_.vertexElementsBegin(vIdxGlobal);
            NeighborElementSeedsIterator nESItEnd = vertexElementMapper_.vertexElementsEnd(vIdxGlobal);
            bool hasSalt = false;
            bool hasNoSalt = false;
            for(; nESIt != nESItEnd; ++nESIt)
            {
                ElementPointer ePtr = this->gridView_.grid().entityPointer(*nESIt);
                int layerIdx = findLayerIdx(*ePtr);
                if(layerIdx == zecIdx)
                    hasSalt = true;
                else
                    hasNoSalt = true;
            }
            //vertices within the salt layer
            if(hasSalt && !hasNoSalt)
                saltVertex_[vIdxGlobal] = true;
            //outside of salt layer or on the salt border
            else
                saltVertex_[vIdxGlobal] = false;

        }
    }

    //Check if the vertex has ter or quar layer neighbors (internal function)
    void initVertexTerQuar_()
    {
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Initializing ter or quar vertices."<<std::endl;
        }
        int numVertex = gridView_.size(dim);
        terQuarVertex_.resize(numVertex, false);

        VertexIterator vIt = gridView_.template begin<dim>();
        VertexIterator vEndIt = gridView_.template end<dim>();
        for(; vIt != vEndIt; ++vIt)
        {
#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
            int vIdxGlobal = this->problem().vertexMapper().index(*vIt);
#else
            int vIdxGlobal = this->problem().vertexMapper().map(*vIt);
#endif
            //Get the iterator over the neighbor elements
            NeighborElementSeedsIterator nESIt = vertexElementMapper_.vertexElementsBegin(vIdxGlobal);
            NeighborElementSeedsIterator nESItEnd = vertexElementMapper_.vertexElementsEnd(vIdxGlobal);
            bool hasTerQuar = false;
            for(; nESIt != nESItEnd; ++nESIt)
            {
                ElementPointer ePtr = this->gridView_.grid().entityPointer(*nESIt);
                int layerIdx = findLayerIdx(*ePtr);
                if(layerIdx == terIdx || layerIdx == quarIdx || layerIdx == quarTopIdx)
                    hasTerQuar = true;

            }
            if(hasTerQuar)
                terQuarVertex_[vIdxGlobal] = true;
        }
    }

    //return the layer index according to the layer bottom (internal function)
    int initialLayerIdx(const Element &element) const
    {
        GlobalPosition globalPos = element.geometry().center();
        if(globalPos[dim-1] < arrayBotLayer_[quarTopIdx])
            return quarTopIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[quarIdx] && globalPos[dim-1] > arrayBotLayer_[quarTopIdx])
            return quarIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[terIdx] && globalPos[dim-1] > arrayBotLayer_[quarIdx])
            return terIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[rcIdx] && globalPos[dim-1] > arrayBotLayer_[terIdx])
            return rcIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[oliIdx] && globalPos[dim-1] > arrayBotLayer_[rcIdx])
            return oliIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[kreideIdx] && globalPos[dim-1] > arrayBotLayer_[oliIdx])
            return kreideIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[obsIdx] && globalPos[dim-1] > arrayBotLayer_[kreideIdx])
            return obsIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[mbsIdx] && globalPos[dim-1] > arrayBotLayer_[obsIdx])
            return mbsIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[solIdx] && globalPos[dim-1] > arrayBotLayer_[mbsIdx])
            return solIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[umbsIdx] && globalPos[dim-1] > arrayBotLayer_[solIdx])
            return umbsIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[ubsIdx] && globalPos[dim-1] > arrayBotLayer_[umbsIdx])
            return ubsIdx;
        else
            return zecIdx;
    }

public:
    // ReservoirProperties
    static constexpr int numLayer_ = 13;
    Scalar arrayTopLayer_[numLayer_];
    Scalar topOfReservoir_;
    Scalar tzWidth_;
    Scalar initialPermeability_;
    Scalar arrayPerm_[numLayer_];
    Scalar arrayPor_[numLayer_];
    Scalar arrayBotLayer_[numLayer_];

private:

    Scalar eps_;

    Scalar hCellWidth_;
    //Vectors storing permeability and porosity values at every element for faster access
    PermeabilityType perm_;
    PorosityType porosity_;
    Scalar longDispersion_;
    Scalar transDispersion_;

   // ReservoirProperties* resProps_;
    const GridView gridView_;
    GridPointer *gridPtr_;

    //Two phase parameters
#if MODELTYPE == 2
    MaterialLawParams materialParams_;
#endif
    Scalar Swr_;
    Scalar Snr_;
    Scalar brooksCoreyPe_;
    Scalar brooksCoreyLambda_;
    Scalar compressibility_;

    //Mass reduction factor for multiplication with porosity
    Scalar massReductionFactor_;

    std::vector<bool> riverDof_;
    std::vector<bool> terQuarVertex_;
    std::vector<bool> saltVertex_;
    bool fractureOff_;
    // Mapper for assigning neighbor elements (value) to global vertex indices
    VertIdxToElemNeighborMapper vertexElementMapper_;

    //Coordinates of Rupel holes
    bool holesRupelClosed_;
    GlobalPosition hole1_;
    Scalar hole1Radius_;
    GlobalPosition hole2_;
    Scalar hole2Radius_;
    GlobalPosition hole3_;
    Scalar hole3Radius_;
    Scalar hole3Thickness_;

};

}

#endif
