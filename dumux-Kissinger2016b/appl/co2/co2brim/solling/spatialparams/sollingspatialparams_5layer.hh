// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outlfow problem.
 */
#ifndef DUMUX_1P2CNI_OUTFLOW_SPATIAL_PARAMS_HH
#define DUMUX_1P2CNI_OUTFLOW_SPATIAL_PARAMS_HH

#if SPATIALPARAMS == 1
#include <dumux/material/spatialparams/implicitspatialparams1p.hh>
#elif SPATIALPARAMS == 2
#include <dumux/material/spatialparams/implicitspatialparams.hh>
#else
#include <dumux/material/spatialparams/fvspatialparams.hh>
#endif
//#include <dumux/material/spatialparams/implicitspatialparams1p.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class SollingSpatialParams;

namespace Properties
{

#if SPATIALPARAMS != 1
// Set the material Law
NEW_TYPE_TAG(SollingSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(SollingSpatialParams, SpatialParams, Dumux::SollingSpatialParams<TypeTag>);

SET_PROP(SollingSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffMaterialLaw> type;
};

#endif
}
/*!
 * \ingroup OnePTwoCBoxModel
 * \ingroup ImplicitTestProblems
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outflow problem.
 */
template<class TypeTag>
#if SPATIALPARAMS == 1
class SollingSpatialParams : public ImplicitSpatialParamsOneP<TypeTag>
#elif SPATIALPARAMS == 2
class SollingSpatialParams : public ImplicitSpatialParams<TypeTag>
#else
class SollingSpatialParams : public FVSpatialParams<TypeTag>
#endif
{
    // Parent Type
#if SPATIALPARAMS == 1
    typedef ImplicitSpatialParamsOneP<TypeTag> ParentType;
#elif SPATIALPARAMS == 2
    typedef ImplicitSpatialParams<TypeTag> ParentType;
#else
    typedef FVSpatialParams<TypeTag> ParentType;
#endif

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef Dune::GridPtr<Grid> GridPointer;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GridView:: template Codim<0>::Iterator ElementIterator;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld,
        pressureIdx = Indices::pressureIdx
    };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;
  //  typedef Dune::ReservoirPropertyCapillary<3> ReservoirProperties;
    typedef typename GET_PROP(TypeTag, ParameterTree) Params;

    typedef std::vector<FieldMatrix> PermeabilityType;
#if SPATIALPARAMS != 1
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;
#endif

//    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    //typedef LinearMaterial<Scalar> EffMaterialLaw;
public:

    SollingSpatialParams(const GridView &gridView)
        : ParentType(gridView), gridView_(gridView)
    {

        try
        {
            topOfReservoir_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.TopOfReservoir);
            kzByKx_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.KzByKx);
            Swr_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Swr);
            Snr_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Snr);
            brooksCoreyPe_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BrooksCoreyPe);
            compressibility_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Compressibility);
            brooksCoreyLambda_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BrooksCoreyLambda);
            arrayPerm_[0] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermKreide);
            arrayPerm_[1] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermOBS);
            arrayPerm_[2] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermMBS);
            arrayPerm_[3] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermSol);
            arrayPerm_[4] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermUBS);
            arrayPerm_[5] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermZec);
            arrayPor_[0] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorKreide);
            arrayPor_[1] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorOBS);
            arrayPor_[2] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorMBS);
            arrayPor_[3] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorSol);
            arrayPor_[4] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorUBS);
            arrayPor_[5] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorZec);

            //get the initialization period
            initializationPeriod_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InitializationPeriod);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }

        //initial permeability
        initialPermeability_ = Scalar(0.0); //initialise the permeability tensor
        for(int i=0; i<dimWorld; ++i)
        {
            //High permeability for initialization
            initialPermeability_[i][i]  = 1e-9;
        }


#if SPATIALPARAMS != 1
        // residual saturations
        materialParams_.setSwr(Swr_);
        materialParams_.setSnr(Snr_);

        // parameters for the Brooks-Corey law
        materialParams_.setPe(brooksCoreyPe_);
        materialParams_.setLambda(brooksCoreyLambda_);
#endif
    }

    ~SollingSpatialParams()
    {}

    void setParams(GridPointer *gridPtr, Problem *problemPtr)
    {
        gridPtr_ = gridPtr;
//        modelPtr_ = model;
        problemPtr_ = problemPtr;
        int numElems = gridView_.size(0);

        permloc_.resize(numElems);
        porosity_.resize(numElems);
        ElementIterator elemIt = gridView_.template begin<0>();
        const ElementIterator elemEndIt = gridView_.template end<0>();
        for (; elemIt != elemEndIt; ++elemIt)
        {
            int elemIdx = gridView_.indexSet().index(*elemIt);
            int layerIdx = findLayerIdx(*elemIt);
            if(layerIdx < 0)
                layerIdx=0;

            porosity_[elemIdx] = arrayPor_[layerIdx];

            if (porosity_[elemIdx]<=1e-4)
            { porosity_[elemIdx]=1e-4;}


            permloc_[elemIdx] = Scalar(0.0); //initialise the permeability tensor
            for(int i=0; i<dimWorld; ++i)
            {
                permloc_[elemIdx][i][i] = arrayPerm_[layerIdx];
                //If permloc is smaller than the smallest permeability defined in arrayPerm than assign the smallest arrayPerm to permloc
                permloc_[elemIdx][i][i] = permloc_[elemIdx][i][i] < 1e-20 ? 1e-20 : permloc_[elemIdx][i][i];

            }
            permloc_[elemIdx][dimWorld-1][dimWorld-1]=permloc_[elemIdx][dimWorld-1][dimWorld-1]*kzByKx_;
        }

        Scalar maxPor = 0;
        Scalar minPor = 0;
        minPor = *std::min_element(porosity_.begin(), porosity_.end());
        maxPor = *std::max_element(porosity_.begin(), porosity_.end());


        Scalar maxPerm = 0.;
        Scalar minPerm = 1e10;

        for (auto & element : permloc_)
        {
            for(int i=0; i<dim; ++i)
            {
                maxPerm = element[i][i] > maxPerm ? element[i][i] : maxPerm;
                minPerm = element[i][i] < minPerm ? element[i][i] : minPerm;
            }
        }



        if (gridView_.comm().size() > 1)
        {
            minPor = gridView_.comm().min(minPor);
            maxPor = gridView_.comm().min(maxPor);
            maxPerm = gridView_.comm().min(maxPerm);
            minPerm = gridView_.comm().min(minPerm);
        }

        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Minimum Perm: "<<minPerm<<" Maximum Perm: "<<maxPerm<<std::endl;
            std::cout<<"Minimum Porosity: "<<minPor<<" Maximum Porosity: "<<maxPor<<std::endl;
        }
    }

    /*!
     * \brief Update the spatial parameters with the flow solution
     *        after a timestep.
     *
     * \param globalSolution the global solution vector
     */
//    void update(const SolutionVector &globalSolution)
//    {
//    };

    /*!
     * \brief Define the intrinsic permeability \f$\mathrm{[m^2]}\f$.
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
#if SPATIALPARAMS != 3
    const FieldMatrix& intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvElemGeom,
                                 int scvIdx) const
#else
    const FieldMatrix& intrinsicPermeability(const Element &element) const
#endif
    {
      if(problemPtr_->timeManager().time() < initializationPeriod_)
      {
          return initialPermeability_; //high permeability for creating hydrostatic conditions
      }

      int elemIdx = gridView_.indexSet().index(element);
      return permloc_[elemIdx];


    }

    /*!
     * \brief Define the porosity \f$\mathrm{[-]}\f$.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
#if SPATIALPARAMS != 3
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx) const
#else
    Scalar porosity(const Element &element) const
#endif
    {
        int elementIdx = gridView_.indexSet().index(element);
        int vertexIdx = problemPtr_->vertexMapper().map(element, scvIdx, dim);
        GlobalPosition globalPos = fvElemGeom.subContVol[scvIdx].global;
        PrimaryVariables initialValues(0);
        problemPtr_->initial_(initialValues, globalPos);

        //effective porosity calculation according to Schäfer et al. 2011
        Scalar refPorosity_ = porosity_[elementIdx];
//        Scalar compressibilty = 4.5e-10; // 1/Pa
        Scalar refPressure = initialValues[pressureIdx];
        Scalar curPressure = problemPtr_->model().curSol()[vertexIdx][pressureIdx];
        Scalar X = compressibility_*(curPressure - refPressure);
        Scalar effPorosity = refPorosity_*(1 + X + std::pow(X,2)/2);
        //return resProps_->porosity(index);
        return effPorosity;
    }


    // return the parameter object for the Brooks-Corey material law which depends on the position
#if SPATIALPARAMS == 2
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvElemGeom,
                                                int scvIdx) const
#elif SPATIALPARAMS == 3
    const MaterialLawParams& materialLawParams(const Element &element) const
#else
#endif
#if SPATIALPARAMS != 1
    {
        return materialParams_;
    }
#endif

    /*!
     * \brief Define the dispersivity.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
    Scalar dispersivity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
        return 0;
    }

    bool useTwoPointGradient(const Element &element,
                             const int vertexI,
                             const int vertexJ) const
    {
        return false;
    }

    /*!
     * \brief Returns the heat capacity \f$[J/m^3 K]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
//    Scalar heatCapacity(const Element &element,
//                        const FVElementGeometry &fvGeometry,
//                        const int scvIdx) const
//    {
//        return
//            790 // specific heat capacity of granite [J / (kg K)]
//            * 2700 // density of granite [kg/m^3]
//            * (1 - porosity(element, fvGeometry, scvIdx));
//    }

    /*!
     * \brief Returns the thermal conductivity \f$[W/m^2]\f$ of the porous material.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
//    Scalar thermalConductivitySolid(const Element &element,
//                                    const FVElementGeometry &fvGeometry,
//                                    const int scvIdx) const
//    {
//        return lambdaSolid_;
//    }

    int findLayerIdx(Element &element) const
    {
        //return the index of the layer so that it fits to the array strating with 0
        return (int)((*gridPtr_).parameters(element)[1] + 0.5) - 1;
    }


private:
    static constexpr int numLayer_ = 6;
    Scalar arrayTopLayer_[numLayer_];
    Scalar topOfReservoir_;

    PermeabilityType permloc_;
      PermeabilityType permloc_high_;
      std::vector<double> porosity_;
    mutable FieldMatrix initialPermeability_;

   // ReservoirProperties* resProps_;
    const GridView gridView_;
    Scalar kzByKx_;
    GridPointer *gridPtr_;

    Scalar arrayPerm_[numLayer_];
    Scalar arrayPor_[numLayer_];

    //Two phase parameters
#if SPATIALPARAMS != 1
    MaterialLawParams materialParams_;
#endif
    Scalar Swr_;
    Scalar Snr_;
    Scalar brooksCoreyPe_;
    Scalar brooksCoreyLambda_;
    Scalar compressibility_;

    //Copy of the model
//    Model *modelPtr_;

    // problem pointer for obtaining the solution
    Problem *problemPtr_;

    //Initialization period
    Scalar initializationPeriod_;
};

}

#endif
