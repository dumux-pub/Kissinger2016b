// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outlfow problem.
 */
#ifndef DUMUX_1P2CNI_OUTFLOW_SPATIAL_PARAMS_HH
#define DUMUX_1P2CNI_OUTFLOW_SPATIAL_PARAMS_HH

#if MODELTYPE == 1
#include <dumux/implicit/dfm/common/implicitdfmspatialparams1p.hh>
#else
#include <dumux/implicit/dfm/common/implicitdfmspatialparams2p.hh>
#endif
//#include <dumux/material/spatialparams/implicitspatialparams1p.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dune/common/float_cmp.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class SollingSpatialParams;

namespace Properties
{

#if MODELTYPE != 1
// The spatial parameters TypeTag
NEW_TYPE_TAG(SollingSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(SollingSpatialParams, SpatialParams, Dumux::SollingSpatialParams<TypeTag>);
// Set the material Law
SET_PROP(SollingSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffMaterialLaw> type;
};

#endif
}
/*!
 * \ingroup OnePTwoCBoxModel
 * \ingroup ImplicitTestProblems
 *
 * \brief Definition of the spatial parameters for the 1p2c
 *        outflow problem.
 */
template<class TypeTag>
#if MODELTYPE == 1
class SollingSpatialParams : public ImplicitDFMSpatialParamsOneP<TypeTag>
#else
class SollingSpatialParams : public ImplicitDFMSpatialParamsTwoP<TypeTag>
#endif
{
    // Parent Type
#if MODELTYPE == 1
    typedef  ImplicitDFMSpatialParamsOneP<TypeTag> ParentType;
#else
    typedef  ImplicitDFMSpatialParamsTwoP<TypeTag> ParentType;
#endif

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef Dune::GridPtr<Grid> GridPointer;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename Grid::ctype CoordScalar;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;

    typedef typename GridView:: template Codim<0>::Iterator ElementIterator;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;


    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld,
        pressureIdx = Indices::pressureIdx,
    };
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldMatrix<Scalar,dim,dim> FieldMatrix;
  //  typedef Dune::ReservoirPropertyCapillary<3> ReservoirProperties;
    typedef typename GET_PROP(TypeTag, ParameterTree) Params;

    typedef std::vector<Scalar> PermeabilityType;
#if MODELTYPE != 1
    //get the material law from the property system
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;
#endif

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    //typedef LinearMaterial<Scalar> EffMaterialLaw;
public:

    enum {
        //Layer indices
        quarIdx=0,
        rcIdx=1,
        solIdx=2,

        //Flux line indices
        noFluxIdx=0,
        rupelFluxIdx=1,
    };

    enum {
        //indices of element parameters specified in dgf file
        outerDomainEIdx = 0
    };


    SollingSpatialParams(const GridView &gridView, const Problem &problem)
        : ParentType(gridView, problem), gridView_(gridView)
    {

        Scalar initialPermeability(0);
        try
        {
            Swr_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Swr);
            Snr_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Snr);
            brooksCoreyPe_= GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BrooksCoreyPe);
            brooksCoreyLambda_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BrooksCoreyLambda);
            compressibility_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Compressibility);
            initialPermeability_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.InitialPermeability);
            arrayPerm_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermQuar);
            arrayPerm_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermRC);
            arrayPerm_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PermSol);
            arrayPor_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorQuar);
            arrayPor_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorRC);
            arrayPor_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.PorSol);
            arrayBotLayer_[quarIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotQuar);
            arrayBotLayer_[rcIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotRC);
            arrayBotLayer_[solIdx] = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.BotSol);
            dz_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.Dz);

            //fracture parameters
            fracPermeability_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.FracturePermeability);
            fracPorosity_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.FracturePorosity);
            fracWidth_ = GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.FractureWidth);

            xCoorFault_ =  GET_RUNTIME_PARAM(TypeTag, Scalar, SpatialParams.XCoorFault);

            //get the mass reduction factor
            massReductionFactor_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.MassReductionFactor);
            deltaSphere_          = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.DeltaSphere);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown while reading in parameters in the spatial parameters file!\n";
            exit(1);
        }

        //initialize eps_
        eps_ = 1e-6;

        //initialize fracture faces by calling private method
        initFracFaces_();

        //initialize permeability and porosity vectors by calling private method
        initPermPor_();

#if MODELTYPE != 1
        // residual saturations
        materialParams_.setSwr(Swr_);
        materialParams_.setSnr(Snr_);

        // parameters for the Brooks-Corey law
        materialParams_.setPe(brooksCoreyPe_);
        materialParams_.setLambda(brooksCoreyLambda_);
#endif
    }

    ~SollingSpatialParams()
    {}

    /*!
     * \brief Update the spatial parameters with the flow solution
     *        after a timestep.
     *
     * \param globalSolution the global solution vector
     */
//    void update(const SolutionVector &globalSolution)
//    {
//    };

    /*!
     * \brief Define the intrinsic permeability \f$\mathrm{[m^2]}\f$.
     *
     * \param element The current finite element
     * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
     */
    Scalar intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvElemGeom,
                                 int scvIdx) const
    {
        if(this->problem().timeManager().episodeIndex() == Problem::initPressureEpsIdx &&
                Dune::FloatCmp::lt<Scalar>(perm_[this->problem().elementMapper().index(element)], initialPermeability_))
        {
            return initialPermeability_; //high permeability for creating hydrostatic conditions
        }
        return perm_[this->problem().elementMapper().index(element)];
    }

    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx) const
    {
        return 0;
    }

    /*!
     * \brief Define the porosity \f$\mathrm{[-]}\f$.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvElemGeom,
                    int scvIdx,
                    Scalar pressure) const
    {
//        if(this->problem().timeManager().time() < initializationPeriod_)
//        {
//            return initializationPeriodPor_;
//        }

        int dofIdxGlobal = this->problem().model().dofMapper().subIndex(element, scvIdx, dofCodim);
        int elementIdx = gridView_.indexSet().index(element);

        //effective porosity calculation according to Schäfer et al. 2011
        Scalar refPorosity = porosity_[elementIdx];
        if(this->problem().timeManager().episodeIndex() == 1)
        {
            refPorosity *= massReductionFactor_;
            return refPorosity;
        }
//        Scalar compressibilty = 4.5e-10; // 1/Pa
        Scalar refPressure = this->problem().initialPVMap_.find(dofIdxGlobal)->second[pressureIdx];
        Scalar X = compressibility_*(pressure - refPressure);
        Scalar effPorosity = refPorosity*(1 + X + std::pow(X,2)/2);
        return effPorosity;
    }


    // return the parameter object for the Brooks-Corey material law which depends on the position
#if MODELTYPE == 2
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvElemGeom,
                                                int scvIdx) const
    {
        return materialParams_;
    }
#endif

    /*!
     * \brief Define the dispersivity.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     */
    Scalar dispersivity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
        return 0;
    }

    bool useTwoPointGradient(const Element &element,
                             const int vertexI,
                             const int vertexJ) const
    {
        return false;
    }

    /*!
      * \brief Define the fracture porosity \f$\mathrm{[-]}\f$.
      *
      * \param element The finite element
      * \param fvGeometry The finite volume geometry
      * \param localFaceIdx The local index of the face
      */
    Scalar fracturePorosity(const Element &element,
                            const FVElementGeometry &fvGeometry,
                            const int localFaceIdx) const
    {
        return fracPorosity_;
    }

     /*!
      * \brief Function for defining the intrinsic (absolute) fracture permeability.
      *
      * \param element The current element
      * \param fvGeometry The current finite volume geometry of the element
      * \param localFaceIdx The index of the face.
      * \return the intrinsic fracture permeability
      */
     Scalar fracturePermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 const int localFaceIdx) const
     {
         if(Dune::FloatCmp::lt<Scalar>(this->problem().timeManager().time(), initializationPeriod_) &&
                 Dune::FloatCmp::lt<Scalar>(fracPermeability_, initialPermeability_))
         {
             return initialPermeability_; //high permeability for creating hydrostatic conditions
         }
         return fracPermeability_;
     }

     /*!
      * \brief Function for defining the fracture width.
      *
      * \param element The current element
      * \param fvGeometry The current finite volume geometry of the element
      * \param localFaceIdx The index of the face.
      * \return the fracture width
      */
     Scalar fractureWidth(const Element &element,
                          const FVElementGeometry &fvGeometry,
                          const int localFaceIdx) const
     {
         return fracWidth_;
     }

     //Check if the vertex is at the transition zone, i.e. having both zec and other layer properties
     bool saltVertex(const int vIdxGlobal) const
     {
         return false;
     }

    //Find the element belonging to a river
    bool isRiver(const Element &element, const int scvIdx) const
    {
        return false;
    }

    //Find the element belonging to a river
    bool isRiver(const Vertex &vertex) const
    {
        return false;
    }

    //Check if the vertex has ter or quar layer neighbors
    bool terQuarVertex(const int vIdxGlobal) const
    {
        return false;
    }

    int findElementRowIdx(const Element &element) const
    {
        return 0;
    }

    //Find the elements belonging to the flux line
    int findFluxLineIdx(const Element &element) const
    {
        GlobalPosition globalPos = element.geometry().center();
        if(globalPos[dim-1] < arrayBotLayer_[rcIdx] && globalPos[dim-1] > arrayBotLayer_[quarIdx] + dz_)
            return rupelFluxIdx;
        else
            return noFluxIdx;

    }

    //Find the layer index from the grid file
    int findLayerIdx(const Element &element) const
    {
        GlobalPosition globalPos = element.geometry().center();
        if(globalPos[0] < xCoorFault_ - eps_ || globalPos[1] < 0 - eps_)
            return rcIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[quarIdx])
            return quarIdx;
        else if(globalPos[dim-1] < arrayBotLayer_[rcIdx] && globalPos[dim-1] > arrayBotLayer_[quarIdx])
            return rcIdx;
        else
            return solIdx;
    }


private:

    //Initialize the fracture faces
    //Loop over all intersections and mark the fracture intersections with true by calling the setFractureFace() function in the
    //ParentType
    void initFracFaces_()
    {
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Initializing fracture faces."<<std::endl;
        }
        //Loop over all elements mark elements with interface zec/other
        ElementIterator eIt = gridView_.template begin<0>();
        const ElementIterator eEndIt = gridView_.template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            if(this->problem().eInOuterDomain(*eIt))
                continue;
            //Loop over intersections
            IntersectionIterator isEndIt = gridView_.iend(*eIt);
            for (IntersectionIterator isIt = gridView_.ibegin(*eIt); isIt != isEndIt; ++isIt)
            {
                GlobalPosition globalPos = isIt->geometry().center();
                GlobalPosition sphereCenter(globalPos);
                sphereCenter[0] = xCoorFault_;

                if(this->problem().globalPosInSphere(globalPos, sphereCenter, deltaSphere_))
                {
                    //Get the local index of the face and call the ParentType to mark the face
                    int localFaceIdx = isIt->indexInInside();
                    ParentType::setFractureFace(*eIt, localFaceIdx, true);
                }
            }
        }
    }


    //Initialize the permeability and porosity.
    //Loop over all elements and obtain the permeability and porosity values from the gridPtr.
    //Store the values in the perm_ and porosity_ vectors.
    //Do some checks at the end.
    void initPermPor_()
    {
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Initializing permeabilities and porosities."<<std::endl;
        }
        int numElems = gridView_.size(0);
        perm_.resize(numElems);
        porosity_.resize(numElems);
        ElementIterator eIt = gridView_.template begin<0>();
        const ElementIterator eEndIt = gridView_.template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            int eIdxGlobal = gridView_.indexSet().index(*eIt);
            int layerIdx = findLayerIdx(*eIt);
            perm_[eIdxGlobal] = arrayPerm_[layerIdx];
            porosity_[eIdxGlobal] = arrayPor_[layerIdx];
        }

        //Check maximum and minimum values in perm_/porosity_ vectors
        Scalar maxPor = 0;
        Scalar minPor = 0;
        minPor = *std::min_element(porosity_.begin(), porosity_.end());
        maxPor = *std::max_element(porosity_.begin(), porosity_.end());

        Scalar maxPerm = 0;
        Scalar minPerm = 0;
        minPerm = *std::min_element(perm_.begin(), perm_.end());
        maxPerm = *std::max_element(perm_.begin(), perm_.end());

        //Compare over different processes
        if (gridView_.comm().size() > 1)
        {
            minPor = gridView_.comm().min(minPor);
            maxPor = gridView_.comm().min(maxPor);
            maxPerm = gridView_.comm().min(maxPerm);
            minPerm = gridView_.comm().min(minPerm);
        }
        //Output only for rank 0
        if(gridView_.comm().rank() == 0)
        {
            std::cout<<"Minimum Perm: "<<minPerm<<" Maximum Perm: "<<maxPerm<<std::endl;
            std::cout<<"Minimum Porosity: "<<minPor<<" Maximum Porosity: "<<maxPor<<std::endl;
        }
    }


    static constexpr int numLayer_ = 4;
    Scalar arrayBotLayer_[numLayer_];
    Scalar saltDomeWidth_;

    std::vector<Scalar> perm_;
    std::vector<Scalar> porosity_;
    Scalar tzWidth_;
    Scalar dz_;
    Scalar initialPermeability_;

   // ReservoirProperties* resProps_;
    const GridView gridView_;
    Scalar kzByKx_;
    GridPointer *gridPtr_;

    Scalar arrayPerm_[numLayer_];
    Scalar arrayPor_[numLayer_];

    //Two phase parameters
#if MODELTYPE != 1
    MaterialLawParams materialParams_;
#endif
    Scalar Swr_;
    Scalar Snr_;
    Scalar brooksCoreyPe_;
    Scalar brooksCoreyLambda_;
    Scalar compressibility_;

    //Initialization period
    Scalar initializationPeriod_;

    //Mass reduction factor for multiplication with porosity
    Scalar massReductionFactor_;

    Scalar fracPermeability_; //fracture permeability
    Scalar fracPorosity_; // fracture porosity
    Scalar fracWidth_; // fracture width

    //x coordinate fault
    Scalar xCoorFault_;

    Scalar deltaSphere_;

    Scalar eps_;
};

}

#endif
