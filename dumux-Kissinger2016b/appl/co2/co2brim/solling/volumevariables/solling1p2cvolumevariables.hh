// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Quantities required by the single-phase, two-component box
 *        model defined on a vertex.
 */
#ifndef DUMUX_SOLLING1P2C_VOLUME_VARIABLES_HH
#define DUMUX_SOLLING1P2C_VOLUME_VARIABLES_HH

#include <dumux/implicit/1p2c/1p2cvolumevariables.hh>
#include <dumux/material/fluidstates/compositionalfluidstate.hh>

#include "dumux/implicit/1p2c/1p2cproperties.hh"

namespace Dumux
{

/*!
 * \ingroup OnePTwoCModel
 * \ingroup ImplicitVolumeVariables
 * \brief Contains the quantities which are constant within a
 *        finite volume in the single-phase, two-component model.
 */
template <class TypeTag>
class Solling1p2cVolumeVariables : public OnePTwoCVolumeVariables<TypeTag>
{
    typedef OnePTwoCVolumeVariables<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        phaseIdx = Indices::phaseIdx,
        phaseCompIdx = Indices::phaseCompIdx,
        transportCompIdx = Indices::transportCompIdx
    };
    //indices of primary variables
    enum{
        pressureIdx = Indices::pressureIdx,
        massOrMoleFracIdx = Indices::massOrMoleFracIdx
    };

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity Element;
    enum { dim = GridView::dimension };
    enum { dimWorld = GridView::dimensionworld };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar,dim> DimVector;
    typedef Dune::FieldVector<Scalar,dimWorld> GlobalPosition;

public:


    /*!
     * \copydoc ImplicitVolumeVariables::update
     */
    void update(const PrimaryVariables &priVars,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int scvIdx,
                const bool isOldSol)
    {   //Update the volume variables
        ParentType::update(priVars, problem, element, fvGeometry, scvIdx, isOldSol);
        //Update the porosity using either old or new solution
        ParentType::porosity_ = problem.spatialParams().porosity(element, fvGeometry, scvIdx, this->pressure());
    }
};

}// end namespace

#endif
