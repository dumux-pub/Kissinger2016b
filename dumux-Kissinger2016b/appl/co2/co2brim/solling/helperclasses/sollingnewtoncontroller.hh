// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Reference implementation of a controller class for the Newton solver.
 *
 * Usually this controller should be sufficient.
 */
#ifndef DUMUX_SOLLING_NEWTON_CONTROLLER_HH
#define DUMUX_SOLLING_NEWTON_CONTROLLER_HH

#include <dune/common/float_cmp.hh>
#include <dumux/common/exceptions.hh>
#include <dumux/common/propertysystem.hh>
#include <dumux/nonlinear/newtoncontroller.hh>

namespace Dumux
{

template <class TypeTag>
class NewtonController;

template <class TypeTag>
class NewtonConvergenceWriter;

namespace Properties
{
//! Should Newton method proceed if the linear solver does not converge
NEW_PROP_TAG(NewtonProceedWithoutConvergence);
//! truncate the newton update for the first few Newton iterations of a time step
NEW_PROP_TAG(NewtonEnableChop);
//!Scale the linear system by the inverse of its (block-)diagonal entries if property is true
NEW_PROP_TAG(NewtonScaleLinearSystem);
//!Use local line search in case the maximum shift increases from last iter to current iter
//NEW_PROP_TAG(NewtonUseLocalLineSearch);

// set default values
SET_BOOL_PROP(NewtonMethod, NewtonProceedWithoutConvergence, false);
SET_BOOL_PROP(NewtonMethod, NewtonEnableChop, false);
SET_BOOL_PROP(NewtonMethod, NewtonScaleLinearSystem, false);
//SET_BOOL_PROP(NewtonMethod, NewtonUseLocalLineSearch, false);
} //namespace properties

/*!
 * \brief A Solling specific controller for the newton solver, which knows
 *       'physically meaningful' solution.
 */
template <class TypeTag>
class SollingNewtonChop
{
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    enum{
        // primary variable indices
        pressureIdx = Indices::pressureIdx,
#if MODELTYPE == 2
        NaClIdx = FluidSystem::NaClIdx,
#else
        NaClIdx = Indices::massOrMoleFracIdx,
#endif
    };

public:
    static void chop(SolutionVector &uCurrentIter,
                     const SolutionVector &uLastIter)
    {
        int countPressureChop = 0;
        int countMoleFracChop = 0;
        for (unsigned int i = 0; i < uLastIter.size(); ++i) {

            bool chopPressure =  pressureChop_(uCurrentIter[i][pressureIdx], uLastIter[i][pressureIdx]);
            bool chopMoleFrac = moleFracChop_(uCurrentIter[i][NaClIdx], uLastIter[i][NaClIdx]);
            if(chopPressure)
                ++countPressureChop;
            if(chopMoleFrac)
                ++countMoleFracChop;
        }
        std::cout<<"No. of pressure chops= "<<countPressureChop<< "No. of mole frac chops= "<<countMoleFracChop<<std::endl;
    };

private:
    static bool clampValue_(Scalar &val,
                            const Scalar minVal,
                            const Scalar maxVal)
    {
        Scalar oldVal = val;
        val = std::max(minVal, std::min(val, maxVal));
        return Dune::FloatCmp::ne<Scalar>(val, oldVal);
    };

    static bool pressureChop_(Scalar &val,
                              const Scalar oldVal)
    {
        const Scalar maxDelta = std::max(oldVal/4.0, 10e3);
        bool chop = clampValue_(val, oldVal - maxDelta, oldVal + maxDelta);
        val = std::max(0.0, val); // don't allow negative pressures
        return chop;
    }

    static bool saturationChop_(Scalar &val,
                                const Scalar oldVal)
    {
        const Scalar maxDelta = 0.25;
        bool chop = clampValue_(val, oldVal - maxDelta, oldVal + maxDelta);
//        clampValue_(val, -0.001, 1.001);
        val = std::max(0.0, std::min(val, 1.0)); // do not allow saturations negative or greater one
        return chop;
    }

    static bool moleFracChop_(Scalar &val,
                              const Scalar oldVal)
    {
        // no component mole fraction can change by more than 20% per iteration
        const Scalar maxDelta = 0.20;
        bool chop = clampValue_(val, oldVal - maxDelta, oldVal + maxDelta);
        val = std::max(0.0, std::min(val, 1.0)); // do not allow saturations negative or greater one
        return chop;
//        clampValue_(val, -0.001, 1.001);
    }

};

/*!
 * \ingroup Newton
 * \brief A reference implementation of a Newton controller specific
 *        for the box scheme.
 *
 * If you want to specialize only some methods but are happy with the
 * defaults of the reference controller, derive your controller from
 * this class and simply overload the required methods.
 */
template <class TypeTag>
class SollingNewtonController : public NewtonController<TypeTag>
{

    typedef NewtonController<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, NewtonController) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, JacobianMatrix) JacobianMatrix;
    typedef SollingNewtonChop<TypeTag> NewtonChop;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;

    enum {
            numEq = GET_PROP_VALUE(TypeTag, NumEq),
            dim = GridView::dimension
    };

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

public:
    /*!
     * \brief Constructor
     */
public:
    SollingNewtonController(const Problem &problem)
        : ParentType(problem)
    {
        proceedWithoutConvergence_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Newton, ProceedWithoutConvergence);
        enableChop_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Newton, EnableChop);
        enableScaleLinear_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Newton, ScaleLinearSystem);
//        useLocalLineSearch_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Newton, UseLocalLineSearch);
    }

    /*!
     * \brief Update the current solution with a delta vector.
     *
     * The error estimates required for the newtonConverged() and
     * newtonProceed() methods should be updated inside this method.
     *
     * Different update strategies, such as line search and chopped
     * updates can be implemented. The default behavior is just to
     * subtract deltaU from uLastIter, i.e.
     * \f[ u^{k+1} = u^k - \Delta u^k \f]
     *
     * \param uCurrentIter The solution vector after the current iteration
     * \param uLastIter The solution vector after the last iteration
     * \param deltaU The delta as calculated from solving the linear
     *               system of equations. This parameter also stores
     *               the updated solution.
     */
    void newtonUpdate(SolutionVector &uCurrentIter,
                      const SolutionVector &uLastIter,
                      const SolutionVector &deltaU)
    {
        ParentType::newtonUpdate(uCurrentIter, uLastIter, deltaU);

        //Perform chop if property EnableChop is true
        if (enableChop_) {
            // put crash barriers along the update path at the
            // beginning...
            NewtonChop::chop(uCurrentIter, uLastIter);
        }

        //Perform global line search if the relative shift has increased from the last
        //Newton step
        if (!this->useLineSearch_ &&
                Dune::FloatCmp::lt<Scalar>(this->lastShift_, this->shift_) &&
                this->numSteps_ != 0 &&
                Dune::FloatCmp::lt<Scalar>(this->shiftTolerance_, this->shift_))
        {
            this->lineSearchUpdate_(uCurrentIter, uLastIter, deltaU);
        }
    }

    /*!
     * \brief Solve the linear system of equations \f$\mathbf{A}x - b = 0\f$.
     *
     * Throws Dumux::NumericalProblem if the linear solver didn't
     * converge.
     *
     * \param A The matrix of the linear system of equations
     * \param x The vector which solves the linear system
     * \param b The right hand side of the linear system
     */
    void newtonSolveLinear(JacobianMatrix &A,
                           SolutionVector &x,
                           SolutionVector &b)
    {
        try {
            if (this->numSteps_ == 0)
            {
                Scalar norm2 = b.two_norm2();
                if (this->gridView_().comm().size() > 1)
                    norm2 = this->gridView_().comm().sum(norm2);

                this->initialResidual_ = std::sqrt(norm2);
            }

            //Scale the linear system by the inverse of
            //* its (block-)diagonal entries if property is true
            if(enableScaleLinear_)
                scaleLinearSystem_(A, b);

            //Call the solver
            int converged = this->linearSolver_.solve(A, x, b);

            // make sure all processes converged
            int convergedRemote = converged;
            if (this->gridView_().comm().size() > 1)
                convergedRemote = this->gridView_().comm().min(converged);

            if (!converged) {
                if(proceedWithoutConvergence_)
                    std::cout<<"Linear solver did not converge, continuing anyway"<<std::endl;
                else
                    DUNE_THROW(NumericalProblem,
                           "Linear solver did not converge");
            }
            else if (!convergedRemote) {
                if(proceedWithoutConvergence_)
                    std::cout<<"Linear solver did not converge on a remote process, continuing anyway"<<std::endl;
                else
                    DUNE_THROW(NumericalProblem,
                           "Linear solver did not converge on a remote process");
            }
        }
        catch (Dune::MatrixBlockError e) {
            // make sure all processes converged
            int converged = 0;
            if (this->gridView_().comm().size() > 1)
                converged = this->gridView_().comm().min(converged);

            NumericalProblem p;
            std::string msg;
            std::ostringstream ms(msg);
            ms << e.what() << "M=" << A[e.r][e.c];
            p.message(ms.str());
            throw p;
        }
        catch (const Dune::Exception &e) {
            // make sure all processes converged
            int converged = 0;
            if (this->gridView_().comm().size() > 1)
                converged = this->gridView_().comm().min(converged);

            NumericalProblem p;
            p.message(e.what());
            throw p;
        }
    }

private:


//    void localLineSearchUpdate_(SolutionVector &uCurrentIter,
//                           const SolutionVector &uLastIter,
//                           const SolutionVector &deltaU)
//    {
//        SolutionVector curRes(uLastIter);
//        Scalar curRed = this->method().model().globalResidual(curRes, uCurrentIter);
//        SolutionVector lastRes(uLastIter);
//        Scalar lastRed = this->method().model().globalResidual(lastRes, uCurrentIter);
//
//
//        //Loop over all dof
//        auto entityIter  = this->method().problem().gridView().template begin< dofCodim >();
//        const auto entityIterEnd  = this->method().problem().gridView().template end< dofCodim >();
//        for(; entityIter !=  entityIterEnd; ++entityIter)
//        {
//#if DUNE_VERSION_NEWER(DUNE_COMMON, 2, 4)
//                            int dofIdxGlobal = this->method().model().dofMapper().index(*entityIter);
//#else
//                            int dofIdxGlobal = this->method().model().dofMapper().map(*entityIter);
//#endif
//            //allow only interior entities
//            //ToDo add functionality for border entities, requires overlap
//            if(entityIter->partitionType() != Dune::InteriorEntity)
//                continue;
//
//            //Loop over equations
//            for(unsigned int eqIdx = 0; eqIdx < numEq; ++eqIdx)
//            {
//                Scalar lambda = 0.5;
//                int iterNum = 0;
//                PrimaryVariables lastIterPV = uLastIter[dofIdxGlobal];
//                //Do local line search if
//                //i) the last residual is smaller than the current one and
//                //ii) the realtive shift in primary varibales is larger than the shift tolerance
//                //iii) we have not exceeded the allowed number of iterations
//                while( Dune::FloatCmp::le<Scalar>(lastRes[dofIdxGlobal][eqIdx], curRes[dofIdxGlobal][eqIdx])
//                        && Dune::FloatCmp::lt<Scalar>(this->shiftTolerance_, this->method().model().relativeShiftAtDof(uCurrentIter[dofIdxGlobal], lastIterPV))
//                        && iterNum < 10 )
//                {
//                    iterNum += 1;
//                    uCurrentIter[dofIdxGlobal][eqIdx] = deltaU[dofIdxGlobal][eqIdx];
//                    uCurrentIter[dofIdxGlobal][eqIdx] *= -lambda;
//                    uCurrentIter[dofIdxGlobal][eqIdx] +=  uLastIter[dofIdxGlobal][eqIdx];
//                    curRes[dofIdxGlobal][eqIdx] = this->method().model().dofResidual(*entityIter, eqIdx, uCurrentIter[dofIdxGlobal][eqIdx]);
//                    lambda /= 2.0;
//                }
//            }
//        }
//        curRed = this->method().model().globalResidual(curRes, uCurrentIter);
//        curRed /= this->initialResidual_;
//        this->endIterMsg() << ", residual reduction " << this->lastReduction_ << "->"  << curRed <<std::endl;
//           return;
//
////       Scalar lambda = 1.0;
////       SolutionVector tmp(uLastIter);
////
////       while (true) {
////           uCurrentIter = deltaU;
////           uCurrentIter *= -lambda;
////           uCurrentIter += uLastIter;
////
////           // calculate the residual of the current solution
////           reduction_ = this->method().model().globalResidual(tmp, uCurrentIter);
////           reduction_ /= initialResidual_;
////
////           if (reduction_ < lastReduction_ || lambda <= 0.125) {
////               this->endIterMsg() << ", residual reduction " << lastReduction_ << "->"  << reduction_ << "@lambda=" << lambda;
////               return;
////           }
////
////           // try with a smaller update
////           lambda /= 2.0;
////       }
//    }

    /*!
     * \brief Scale the linear system by the inverse of
     * its (block-)diagonal entries.
     *
     * \param matrix the matrix to scale
     * \param rhs the right hand side vector to scale
     */

    void scaleLinearSystem_(JacobianMatrix& matrix, SolutionVector& rhs)
    {
        typename JacobianMatrix::RowIterator row = matrix.begin();
        for(; row != matrix.end(); ++row)
        {
            typedef typename JacobianMatrix::size_type size_type;
            size_type rowIdx = row.index();

            typedef typename JacobianMatrix::block_type MatrixBlock;
            MatrixBlock diagonal = matrix[rowIdx][rowIdx];
            diagonal.invert();

            typedef typename SolutionVector::block_type VectorBlock;
            const VectorBlock b = rhs[rowIdx];
            diagonal.mv(b, rhs[rowIdx]);

            typename JacobianMatrix::ColIterator col = row->begin();
            for (; col != row->end(); ++col)
                col->leftmultiply(diagonal);
        }
    }

    bool proceedWithoutConvergence_;
    bool enableChop_;
    bool enableScaleLinear_;
//    bool useLocalLineSearch_;
};
} // namespace Dumux

#endif
