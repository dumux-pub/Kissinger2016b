#ifndef DUNE_GENERIC_DGFEXTENDER_HH
#define DUNE_GENERIC_DGFEXTENDER_HH

/** \file 
 *  \brief write a GridView to a DGF file
 *  \author Martin Nolte
 */

#include <fstream>
#include <vector>

#include <dune/common/float_cmp.hh>
#include <dune/grid/common/grid.hh>
#include <dune/grid/io/file/vtk/boundaryiterators.hh>
#include <dune/common/float_cmp.hh>
//#include <dumux/common/basicproperties.hh>
#include "dgfextender.hh"


namespace Dumux
{

template <class TypeTag>
class GenericDGFExtender;

namespace Properties
{
//NEW_PROP_TAG(DGFExtender); //! The type of the dgfextender
//NEW_TYPE_TAG(GenericDGFExtender, INHERITS_FROM(NumericModel));
//SET_TYPE_PROP(GenericDGFExtender,
//              DGFExtender,
//              Dumux::GenericDGFExtender<TypeTag>);
}

  /** \class DGFExtender
   *  \ingroup DuneGridFormatParser
   *  \brief write a GridView to a DGF file
   *
   *  The DGFExtender allows create a DGF file from a given GridView. It allows
   *  for the easy creation of file format converters.
   *
   *  \tparam  GV  GridView to write in DGF format
   */
  template< class TypeTag >
  class GenericDGFExtender : public DGFExtender<TypeTag>
  {
    typedef GenericDGFExtender< TypeTag > This;
    typedef Dumux::DGFExtender< TypeTag > ParentType;

  public:
    /** \brief type of grid view */
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    /** \brief type of underlying hierarchical grid */
    typedef typename GridView::Grid Grid;
    typedef Dune::GridPtr<Grid> GridPointer;

    /** \brief dimension of the grid */
    static const int dim = GridView::dimension;
    static const int dimGrid = GridView::dimension;

  private:
    typedef typename GET_PROP_TYPE(TypeTag, DGFExtender) Implementation;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GridView::template Codim<0>::Entity Element;
	typedef typename GridView::Intersection Intersection;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim< 0 >::Iterator ElementIterator;
    typedef typename GridView::template Codim< dimGrid >::Iterator VertexIterator;
    typedef Dune::VTK::BoundaryIterator<GridView> BoundaryIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::ctype CoordScalar;
    typedef typename Dune::ReferenceElements<CoordScalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<CoordScalar, dim> ReferenceElement;
    typedef typename IndexSet::IndexType Index;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dimGrid> GlobalPosition;

  public:

    /** \brief constructor
     *
     *  \param[in]  gridView  grid view to operate on
     */
    GenericDGFExtender ( const GridView &gridView, GridPointer &gridPtr )
    : ParentType(gridView, gridPtr)
      {
        botQuar_ = GET_RUNTIME_PARAM(TypeTag, Scalar, GridExtender.BotQuar);
        dirichlet_ = GET_RUNTIME_PARAM(TypeTag, bool, GridExtender.Dirichlet);
      }

    //Check whether the boundary intersection is marked for extension
    bool extendIs(const Element element, const Intersection is) const
    {
        if(!is.boundary())
            DUNE_THROW( Dune::GridError, "Interior intersection marked for extension!" );


        //No elements on the top and bottom boundaries
        int intersectionIdx = is.indexInInside();
        if(intersectionIdx == ParentType::topIdx || intersectionIdx == ParentType::bottomIdx
                || intersectionIdx == ParentType::leftIdx || intersectionIdx == ParentType::frontIdx)
            return false;

        if(dirichlet_)
        {
            GlobalPosition globalPos = is.geometry().center();
            if(globalPos[dim-1] < botQuar_)
                return false;
        }

        return true;
    }

    //Calculate center coordinates, default center of gravity
    void calculateCenterCoor()
    {
        ParentType::centerCoor_ = 1e10;
        BoundaryIterator bIsItEnd(ParentType::gridView_, true);
        for(BoundaryIterator bIsIt(ParentType::gridView_); bIsIt != bIsItEnd; ++bIsIt)
        {
            GlobalPosition globalPos = bIsIt->geometry().center();
            ParentType::centerCoor_[0] = std::min(globalPos[0], ParentType::centerCoor_[0]);
            ParentType::centerCoor_[1] = std::min(globalPos[1], ParentType::centerCoor_[1]);
            ParentType::centerCoor_[dim-1] = std::min(globalPos[dim-1], ParentType::centerCoor_[dim-1]);
        }
        std::cout<<"Finished calculating center of domain: "<<ParentType::centerCoor_<<std::endl;
    }

  protected:
    Implementation &asImp_()
    {
        assert(static_cast<Implementation*>(this) != 0);
        return *static_cast<Implementation*>(this);
    }

    const Implementation &asImp_() const
    {
        assert(static_cast<const Implementation*>(this) != 0);
        return *static_cast<const Implementation*>(this);
    }
  private:

    Scalar botQuar_;
    Scalar dirichlet_;
  };
}

#endif // #ifndef DUNE_DGFWRITER_HH
