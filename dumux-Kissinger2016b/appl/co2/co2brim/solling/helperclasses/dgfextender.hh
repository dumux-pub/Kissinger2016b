#ifndef DUNE_DGFEXTENDER_HH
#define DUNE_DGFEXTENDER_HH

/** \file 
 *  \brief write a GridView to a DGF file
 *  \author Martin Nolte
 */

#include <fstream>
#include <vector>

#include <dune/common/float_cmp.hh>
#include <dune/grid/common/grid.hh>
#include <dune/grid/io/file/vtk/boundaryiterators.hh>
#include <dune/common/float_cmp.hh>
#include <dumux/common/basicproperties.hh>

namespace Dumux
{

template <class TypeTag>
class DGFExtender;

namespace Properties
{
NEW_PROP_TAG(DGFExtender); //! The type of the dgfextender
//SET_TYPE_PROP(NumericModel,
//              DGFExtender,
//              Dumux::DGFExtender<TypeTag>);
SET_TYPE_PROP(NumericModel,
              GridView,
              typename GET_PROP_TYPE(TypeTag, Grid)::LeafGridView);
}

  /** \class DGFExtender
   *  \ingroup DuneGridFormatParser
   *  \brief write a GridView to a DGF file
   *
   *  The DGFExtender allows create a DGF file from a given GridView. It allows
   *  for the easy creation of file format converters.
   *
   *  \tparam  GV  GridView to write in DGF format
   */
  template< class TypeTag >
  class DGFExtender
  {
    typedef DGFExtender< TypeTag > This;

  public:
    /** \brief type of grid view */
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    /** \brief type of underlying hierarchical grid */
    typedef typename GridView::Grid Grid;
    typedef Dune::GridPtr<Grid> GridPointer;

    /** \brief dimension of the grid */
    static const int dim = GridView::dimension;
    static const int dimGrid = GridView::dimension;

  private:
    typedef typename GET_PROP_TYPE(TypeTag, DGFExtender) Implementation;
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
	typedef typename GridView::Intersection Intersection;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim<dim>::EntityPointer VertexPointer;
    typedef typename GridView::template Codim< 0 >::Iterator ElementIterator;
    typedef typename GridView::template Codim< dimGrid >::Iterator VertexIterator;
    typedef Dune::VTK::BoundaryIterator<GridView> BoundaryIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::ctype CoordScalar;
    typedef typename Dune::ReferenceElements<CoordScalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<CoordScalar, dim> ReferenceElement;
    typedef typename IndexSet::IndexType Index;
    typedef double Scalar;
    typedef Dune::FieldVector<Scalar, dimGrid> GlobalPosition;

  public:

    enum{
        leftIdx = 0,
        rightIdx = 1,
        frontIdx = 2,
        rearIdx = 3,
        bottomIdx = 4,
        topIdx = 5,
    };
    /** \brief constructor
     *
     *  \param[in]  gridView  grid view to operate on
     */
    DGFExtender ( const GridView &gridView, GridPointer &gridPtr )
    : gridView_( gridView )
    {
        gridPtr_ = &gridPtr;
        int numVertices = gridView.size(/*codim faces*/ dim);
        vertexWritten_.resize(numVertices, false);
        numExtElem_ = GET_RUNTIME_PARAM(TypeTag, int, GridExtender.NumExtElem);
        outerRadius_ = GET_RUNTIME_PARAM(TypeTag, Scalar, GridExtender.OuterRadius);
        power_ = GET_RUNTIME_PARAM(TypeTag, Scalar, GridExtender.Power);
        asImp_().calculateCenterCoor();
    }

    GlobalPosition getPosOfExtVertex(const Element &element, const Intersection &is, const int scvIdx, Scalar extVertexNo) const
    {
        ++extVertexNo; //increase by 1
        GlobalPosition posCenterDomain(centerCoor_);
        GlobalPosition posBouVertex = element.geometry().corner(scvIdx);
        posCenterDomain[dimGrid-1] = posBouVertex[dimGrid-1];
        GlobalPosition posExtVertex(posBouVertex);
        GlobalPosition normal = posBouVertex - posCenterDomain;
        Scalar innerRadius = normal.two_norm();
        if(Dune::FloatCmp::le<Scalar>(outerRadius_, innerRadius))
            DUNE_THROW( Dune::GridError, "Outer radius smaller than inner radius!" );
        normal /= normal.two_norm();
        Scalar factor = spacer(innerRadius, outerRadius_, extVertexNo, power_);
        for( int coorIter = 0; coorIter < dimGrid; ++coorIter)
        {
            if(coorIter == dimGrid-1)
                break;
            posExtVertex[coorIter] = posCenterDomain[coorIter] + factor*normal[coorIter];
        }
        if(hasSameCoordinates(posBouVertex, posExtVertex))
            DUNE_THROW( Dune::GridError, "Boundary vertex and external vertex coordinates are the same!" );
        return posExtVertex;
    }

    Scalar spacer(const Scalar innerRadius, const Scalar outerRadius, const Scalar extVertexNo, const Scalar power) const
    {
        Scalar rate = std::pow(Scalar(numExtElem_), -power)*std::log(outerRadius/innerRadius);
        Scalar factor = innerRadius*std::exp(std::pow(extVertexNo, power)*rate);
        return factor;
    }

    //Check whether the boundary intersection is marked for extension
    bool extendIs(const Element element, const Intersection is) const
    {
        if(!is.boundary())
            DUNE_THROW( Dune::GridError, "Interior intersection marked for extension!" );

        int intersectionIdx = is.indexInInside();
        if(intersectionIdx == topIdx || intersectionIdx == bottomIdx)
            return false;

        return true;
    }

    //Get the number of external elements
    //Get the number of external elements
    unsigned int getNumberExtElements(const Element element) const
    {
        return numExtElem_;
    }

    /** \brief write the GridView into a std::ostreamm
     *
     *  \param  gridout  std::ostream to write the grid to
     */
    void write ( std::ostream &gridout/*,
					    const std::vector<double>& porosity, 
					    const std::vector<double>& permeability,
        				    const std::vector<double>& river*/) const;

    /** \brief write the GridView to a file
     *
     *  \param[in] fileName  name of the write to write the grid to
     */
    void write ( const std::string &fileName/*,
					    const std::vector<double>& porosity, 
					    const std::vector<double>& permeability,
					    const std::vector<double>& river */) const;

    //Calculate center coordinates, default center of gravity
    void calculateCenterCoor()
    {
        std::cout<<"Calculating center of domain"<<std::endl;
        Scalar totalVolume = 0.0;
        GlobalPosition vectorSum(0.0);
        const  ElementIterator end = gridView_.template end< 0 >();
        for( ElementIterator eIt = gridView_.template begin< 0 >(); eIt != end; ++eIt )
        {
            Scalar eVolume = eIt->geometry().volume();
            GlobalPosition eCenter = eIt->geometry().center();
            totalVolume += eVolume;
            eCenter *= eVolume;
            vectorSum += eCenter;
        }
        vectorSum /= totalVolume;
        centerCoor_ = vectorSum;
        std::cout<<"Finished calculating center of domain: "<<centerCoor_<<std::endl;
    }

    //get parameters for elements
    void getEParameters(std::vector<Scalar> &eParam, const Element &element, bool extensionE) const
    {
        eParam = (*gridPtr_).template parameters<Element>(element);
        if(extensionE)
            eParam.push_back(1.0);
        else
            eParam.push_back(0.0);
    }

    //get parameters for vertices
    void getVParameters(std::vector<Scalar> &vParam, const Vertex &vertex, bool extensionV) const
    {
        vParam = (*gridPtr_).template parameters<Vertex>(vertex);
    }

  protected:
    Implementation &asImp_()
    {
        assert(static_cast<Implementation*>(this) != 0);
        return *static_cast<Implementation*>(this);
    }

    const Implementation &asImp_() const
    {
        assert(static_cast<const Implementation*>(this) != 0);
        return *static_cast<const Implementation*>(this);
    }

    //Check whether two coordinates vectors are equal
    bool hasSameCoordinates( const GlobalPosition &globalPos1, const GlobalPosition &globalPos2) const
    {
        if(Dune::FloatCmp::eq<Scalar>(globalPos1[0], globalPos2[0])
                && Dune::FloatCmp::eq<Scalar>(globalPos1[1], globalPos2[1])
                && Dune::FloatCmp::eq<Scalar>(globalPos1[dimGrid-1], globalPos2[dimGrid-1]))
            return true;
        return false;
    }

    //return whether the vertex was already wirtten into the dgf file
    bool getVertexWritten_(const Index vIdxGlobal) const
    {
        return vertexWritten_[vIdxGlobal];
    }

    //return whether the vertex was already wirtten into the dgf file
    void setVertexWritten_(const Index vIdxGlobal) const
    {
        vertexWritten_[vIdxGlobal] = true;
    }

    GridView gridView_;
    GridPointer *gridPtr_;
    mutable std::vector<bool> vertexWritten_;
    unsigned int numExtElem_;
    Scalar outerRadius_;
    Scalar power_;
    GlobalPosition centerCoor_;
  };



  template< class TypeTag >
  inline void DGFExtender< TypeTag >::write ( std::ostream &gridout/*,
					    const std::vector<double>& porosity, 
					    const std::vector<double>& permeability,
					    const std::vector<double>& river*/) const
  {
    // set the stream to full double precision
    gridout.setf( std::ios_base::scientific, std::ios_base::floatfield );
    gridout.precision( 16 );
    
    const IndexSet &indexSet = gridView_.indexSet();
    //Map key = vIdxGlobal, Map value vIdxActiveDomain (no salt vertices)
    std::map<int, int> activeDomainVertexMap_;
    std::map<Index, std::vector<Index>> vertexExtensionMap;
   
    // write DGF header
    gridout << "DGF" << std::endl;
    int nOfParametersVert = (*gridPtr_).nofParameters(/*codim*/dim);
    // write all vertices into the "vertex" block
    gridout << std::endl << "VERTEX" << std::endl;
    if(nOfParametersVert > 0)
        gridout << "parameters "<<nOfParametersVert << std::endl;
    std::cout<<"Adding "<<nOfParametersVert<<" parameters to vertices."<<std::endl;
    Index vertexCount = 0;
    const VertexIterator vend = gridView_.template end< dimGrid >();
    std::cout<<"Writing original vertices to dgf"<<std::endl;
    for( VertexIterator vit = gridView_.template begin< dimGrid >(); vit != vend; ++vit )
    {
        int vIdxGlobal = indexSet.template index<dimGrid>( *vit );
        activeDomainVertexMap_[vIdxGlobal] = vertexCount++;
        gridout << vit->geometry().corner( 0 )[0] << " " << vit->geometry().corner( 0 )[1] << " " << vit->geometry().corner( 0 )[2];

        //Write vertex parameters
        std::vector<Scalar> vParam;
        asImp_().getVParameters(vParam, *vit, false);
        if(vParam.size() >= 1)
        {
            for( size_t i = 0; i < vParam.size(); ++i )
                gridout << " " << vParam[ i ];
        }
        gridout << std::endl;
    }
    std::cout<<"Writing extension vertices to dgf"<<std::endl;

    //Brackets so that BoundaryIterators get destroyed
    {
        //loop over boundary intersections
        BoundaryIterator isIt(gridView_);
        BoundaryIterator isItEnd(gridView_, true);
        for(; isIt != isItEnd; ++isIt)
        {
            ElementPointer ePtr = isIt->inside();
            if(asImp_().extendIs(*ePtr, *isIt))
            {
                Dune::GeometryType geomType = ePtr->geometry().type();
                const ReferenceElement &referenceElement = ReferenceElements::general(geomType);
                int intersectionIdx = isIt->indexInInside();
                int numVerticesOfIntersection = referenceElement.size(intersectionIdx, /*codim intersection*/1, /*codim vertice*/ dim);
                for (int vertInIntersection = 0; vertInIntersection < numVerticesOfIntersection; vertInIntersection++)
                {
                    int vIdxInElement = referenceElement.subEntity(intersectionIdx, 1, vertInIntersection, dim);
                    Index vIdxGlobal =  indexSet.template subIndex<0>(*ePtr, vIdxInElement, dim);
                    VertexPointer vPtr = *ePtr.template subEntity<dim>(vIdxInElement);
                    if(!getVertexWritten_(vIdxGlobal))
                    {
                        std::vector<Index> extVIdx;
                        extVIdx.push_back(activeDomainVertexMap_.find(vIdxGlobal)->second);
                        unsigned int numExtElem = asImp_().getNumberExtElements(*ePtr);
                        for(int extVertexIter = 0; extVertexIter < numExtElem; ++extVertexIter)
                        {
                            GlobalPosition globalPos = asImp_().getPosOfExtVertex(*ePtr, *isIt, vIdxInElement, extVertexIter);
                            gridout << globalPos[0] << " "
                                    << globalPos[1] << " "
                                    << globalPos[2];

                            //Write vertex parameters
                            std::vector<Scalar> vParam;
                            asImp_().getVParameters(vParam, *vPtr, true);
                            if(vParam.size() >= 1)
                            {
                                for( size_t i = 0; i < vParam.size(); ++i )
                                    gridout << " " << vParam[ i ];
                            }
                            gridout << std::endl;
                            //Resize vector and place vertex count at the end
                            extVIdx.push_back(vertexCount++);
                        }
                        vertexExtensionMap[vIdxGlobal] = extVIdx;
                        setVertexWritten_(vIdxGlobal);
                    }
                }
            }
        }
    }
    gridout << "#" << std::endl;
    std::cout<<"Wrote "<<vertexCount<<" vertices to dgf. Original no of vertices "<<indexSet.size( dimGrid )<<"."<<std::endl;
    std::cout<<"Writing elements to dgf"<<std::endl;
    int nOfParametersElem = (*gridPtr_).nofParameters(/*codim*/0);
    //Add one parameter for domain specification
    ++nOfParametersElem;
    std::cout<<"Adding "<<nOfParametersElem<<" parameters to elements."<<std::endl;
    // write all cubes to the "cube" block
    gridout << std::endl << "CUBE" << std::endl;
    gridout << "parameters "<<nOfParametersElem << std::endl;
    int count = 0;
    const  ElementIterator end = gridView_.template end< 0 >();
    for( ElementIterator eIt = gridView_.template begin< 0 >(); eIt != end; ++eIt )
    {
      std::vector< Index > vertices( 1 << dimGrid );
      for( size_t i = 0; i < vertices.size(); ++i )
        vertices[ i ] = activeDomainVertexMap_.find(indexSet.subIndex( *eIt, i, dimGrid ))->second;
      
      gridout << vertices[ 0 ];
      for( size_t i = 1; i < vertices.size(); ++i )
        gridout << " " << vertices[ i ];

      std::vector<Scalar> eParam;
      asImp_().getEParameters(eParam, *eIt, false);
      if(eParam.size() >= 1)
      {
          for( size_t i = 0; i < eParam.size(); ++i )
              gridout << " " << eParam[ i ];
      }
      gridout << std::endl;
      count++;
    }
    std::cout<<"Writing extension elements to dgf"<<std::endl;

    //Brackets so that BoundaryIterators get destroyed
    {
        //loop over boundary intersections
        BoundaryIterator isIt(gridView_);
        BoundaryIterator isItEnd(gridView_, true);
        for(; isIt != isItEnd; ++isIt)
        {
            ElementPointer ePtr = isIt->inside();
            if(asImp_().extendIs(*ePtr, *isIt))
            {
                int intersectionIdx = isIt->indexInInside();
                std::vector<Index> vertices( 1 << dimGrid );
                //Loop over extension elements and write them to dgf-file
                unsigned int numExtElem = asImp_().getNumberExtElements(*ePtr);
                for (int extElemIter = 0; extElemIter < numExtElem; ++extElemIter)
                {
                    if(intersectionIdx == leftIdx)
                    {
                        vertices[0] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 0, dimGrid ))->second[extElemIter + 1];
                        vertices[1] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 0, dimGrid ))->second[extElemIter];
                        vertices[2] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 2, dimGrid ))->second[extElemIter + 1];
                        vertices[3] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 2, dimGrid ))->second[extElemIter];
                        vertices[4] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 4, dimGrid ))->second[extElemIter + 1];
                        vertices[5] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 4, dimGrid ))->second[extElemIter];
                        vertices[6] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 6, dimGrid ))->second[extElemIter + 1];
                        vertices[7] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 6, dimGrid ))->second[extElemIter];
                    }
                    else if(intersectionIdx == rightIdx)
                    {
                        vertices[0] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 1, dimGrid ))->second[extElemIter];
                        vertices[1] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 1, dimGrid ))->second[extElemIter + 1];
                        vertices[2] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 3, dimGrid ))->second[extElemIter];
                        vertices[3] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 3, dimGrid ))->second[extElemIter + 1];
                        vertices[4] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 5, dimGrid ))->second[extElemIter];
                        vertices[5] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 5, dimGrid ))->second[extElemIter + 1];
                        vertices[6] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 7, dimGrid ))->second[extElemIter];
                        vertices[7] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 7, dimGrid ))->second[extElemIter + 1];
                    }
                    else if(intersectionIdx == frontIdx)
                    {
                        vertices[0] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 0, dimGrid ))->second[extElemIter + 1];
                        vertices[1] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 1, dimGrid ))->second[extElemIter + 1];
                        vertices[2] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 0, dimGrid ))->second[extElemIter];
                        vertices[3] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 1, dimGrid ))->second[extElemIter];
                        vertices[4] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 4, dimGrid ))->second[extElemIter + 1];
                        vertices[5] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 5, dimGrid ))->second[extElemIter + 1];
                        vertices[6] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 4, dimGrid ))->second[extElemIter];
                        vertices[7] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 5, dimGrid ))->second[extElemIter];
                    }
                    else if(intersectionIdx == rearIdx)
                    {
                        vertices[0] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 2, dimGrid ))->second[extElemIter];
                        vertices[1] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 3, dimGrid ))->second[extElemIter];
                        vertices[2] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 2, dimGrid ))->second[extElemIter + 1];
                        vertices[3] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 3, dimGrid ))->second[extElemIter + 1];
                        vertices[4] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 6, dimGrid ))->second[extElemIter];
                        vertices[5] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 7, dimGrid ))->second[extElemIter];
                        vertices[6] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 6, dimGrid ))->second[extElemIter + 1];
                        vertices[7] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 7, dimGrid ))->second[extElemIter + 1];
                    }
                    else if(intersectionIdx == bottomIdx)
                    {
                        vertices[0] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 0, dimGrid ))->second[extElemIter + 1];
                        vertices[1] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 1, dimGrid ))->second[extElemIter + 1];
                        vertices[2] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 2, dimGrid ))->second[extElemIter + 1];
                        vertices[3] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 3, dimGrid ))->second[extElemIter + 1];
                        vertices[4] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 0, dimGrid ))->second[extElemIter];
                        vertices[5] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 1, dimGrid ))->second[extElemIter];
                        vertices[6] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 2, dimGrid ))->second[extElemIter];
                        vertices[7] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 3, dimGrid ))->second[extElemIter];
                    }
                    else if(intersectionIdx == topIdx)
                    {
                        vertices[0] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 4, dimGrid ))->second[extElemIter];
                        vertices[1] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 5, dimGrid ))->second[extElemIter];
                        vertices[2] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 6, dimGrid ))->second[extElemIter];
                        vertices[3] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 7, dimGrid ))->second[extElemIter];
                        vertices[4] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 4, dimGrid ))->second[extElemIter + 1];
                        vertices[5] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 5, dimGrid ))->second[extElemIter + 1];
                        vertices[6] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 6, dimGrid ))->second[extElemIter + 1];
                        vertices[7] = vertexExtensionMap.find(indexSet.subIndex( *ePtr, 7, dimGrid ))->second[extElemIter + 1];
                    }
                    else
                    {
                        DUNE_THROW(Dune::GridError, "Wrong local intersection index");
                    }
                    gridout << vertices[ 0 ];
                    for( size_t i = 1; i < vertices.size(); ++i )
                        gridout << " " << vertices[ i ];

                    std::vector<Scalar> eParam;
                    asImp_().getEParameters(eParam, *ePtr, true);
                    if(eParam.size() >= 1)
                    {
                        for( size_t i = 0; i < eParam.size(); ++i )
                            gridout << " " << eParam[ i ];
                    }
                    gridout << std::endl;
                    count++;
                }
            }
        }
    }
    gridout << "#" << std::endl;
    std::cout<<"Wrote "<<count<<" elements to dgf. Original no of elements "<<indexSet.size( 0 )<<"."<<std::endl;
    // write the name into the "gridparameter" block
    gridout << std::endl << "BOUNDARYDOMAIN" << std::endl;
    gridout <<"default 1"<<std::endl;
    gridout << "#" << std::endl;
  }


  template< class TypeTag >
  inline void DGFExtender< TypeTag >::write ( const std::string &fileName/*,
					    const std::vector<double>& porosity,
					    const std::vector<double>& permeability,
					    const std::vector<double>& river*/) const
  {
    std::ofstream gridout( fileName.c_str() );
    write( gridout/*, porosity, permeability, river */);
    std::cout<<"Created dgf file"<<std::endl;
  }

}

#endif // #ifndef DUNE_DGFWRITER_HH
