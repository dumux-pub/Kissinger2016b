#ifndef DUNE_DGFWRITER_HH
#define DUNE_DGFWRITER_HH

/** \file 
 *  \brief write a GridView to a DGF file
 *  \author Martin Nolte
 */

#include <fstream>
#include <vector>

#include <dune/grid/common/grid.hh>
#include <dune/common/float_cmp.hh>
#include "../helperclasses/vertidxtoelemneighbormapper.hh"

namespace Dumux
{

  /** \class DGFWriterParam
   *  \ingroup DuneGridFormatParser
   *  \brief write a GridView to a DGF file
   *
   *  The DGFWriterParam allows create a DGF file from a given GridView. It allows
   *  for the easy creation of file format converters.
   *
   *  \tparam  GV  GridView to write in DGF format
   */
  template< class TypeTag  >
  class DGFWriterParam
  {
    typedef DGFWriterParam< TypeTag > This;

  public:
    /** \brief type of grid view */
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    /** \brief type of underlying hierarchical grid */
    typedef typename GridView::Grid Grid;
    typedef Dune::GridPtr<Grid> GridPointer;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;

    /** \brief dimension of the grid */
    static const int dimGrid = GridView::dimension;

  private:
    typedef typename GridView::IndexSet IndexSet;
    typedef typename GridView::template Codim<0>::Entity Element;
	typedef typename GridView::Intersection Intersection;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim< 0 >::Iterator ElementIterator;
    typedef typename GridView::template Codim< dimGrid >::Iterator VertexIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename SolutionVector::block_type VectorBlock;
//    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;


    typedef typename IndexSet::IndexType Index;

    typedef Dune::ReferenceElement< typename Grid::ctype, dimGrid > RefElement;
    typedef Dune::ReferenceElements< typename Grid::ctype, dimGrid > RefElements;

  public:

    /** \brief constructor
     *
     *  \param[in]  gridView  grid view to operate on
     */
    DGFWriterParam ( const GridView &gridView, const SolutionVector &sol )
    : gridView_( gridView ), sol_(sol)
    {}

    /** \brief write the GridView into a std::ostreamm
     *
     *  \param  gridout  std::ostream to write the grid to
     */
    void write ( std::ostream &gridout/*,
					    const std::vector<Scalar>& porosity,
					    const std::vector<Scalar>& permeability,
        				    const std::vector<Scalar>& river*/) const;

    /** \brief write the GridView to a file
     *
     *  \param[in] fileName  name of the write to write the grid to
     */
    void write ( const std::string &fileName/*,
					    const std::vector<Scalar>& porosity,
					    const std::vector<Scalar>& permeability,
					    const std::vector<Scalar>& river */) const;

  private:

    const GridView gridView_;
    const SolutionVector sol_;
  };


  template< class TypeTag >
  inline void DGFWriterParam< TypeTag >::write ( std::ostream &gridout/*,
					    const std::vector<Scalar>& porosity,
					    const std::vector<Scalar>& permeability,
					    const std::vector<Scalar>& river*/) const
  {
    // set the stream to full Scalar precision
    gridout.setf( std::ios_base::scientific, std::ios_base::floatfield );
    gridout.precision( 16 );
    
    const IndexSet &indexSet = gridView_.indexSet();
    std::vector< Index > vertexIndex( indexSet.size( dimGrid ) );
    // write DGF header
    gridout << "DGF" << std::endl;
    int numPV = GET_PROP_VALUE(TypeTag, NumEq);
    // write all vertices into the "vertex" block
    gridout << std::endl << "VERTEX" << std::endl;
    gridout << "parameters "<<numPV << std::endl;
    Index vertexCount = 0;
    const VertexIterator vend = gridView_.template end< dimGrid >();
    std::cout<<"Writing vertices to dgf"<<std::endl;
    for( VertexIterator vit = gridView_.template begin< dimGrid >(); vit != vend; ++vit )
    {
        int vIdxGlobal = indexSet.template index<dimGrid>( *vit );

        vertexIndex[vIdxGlobal] = vertexCount++;
        //      vertexIndex[ indexSet.index( *vit ) ] = vertexCount++;
        gridout << vit->geometry().corner( 0 )[0] << " " << vit->geometry().corner( 0 )[1] << " " << vit->geometry().corner( 0 )[2];

        //Write the solution behind each vertex
        const VectorBlock vertexSol = sol_[vIdxGlobal];
        for( size_t i = 0; i < vertexSol.size(); ++i )
            gridout << " " << vertexSol[ i ];

        gridout << std::endl;
    }
    gridout << "#" << std::endl;
    if( vertexCount != indexSet.size( dimGrid ) )
      DUNE_THROW( Dune::GridError, "Index set reports wrong number of vertices." );

    std::cout<<"Writing elements to dgf"<<std::endl;
    // write all cubes to the "cube" block
    gridout << std::endl << "CUBE" << std::endl;
    gridout << "parameters 4" << std::endl;
    int count = 0;
    const  ElementIterator end = gridView_.template end< 0 >();
    for( ElementIterator eIt = gridView_.template begin< 0 >(); eIt != end; ++eIt )
    {
      std::vector<Scalar> eParam = GridCreator::template parameters<Element>(*eIt);
      if( !eIt->type().isCube())
        continue;

      std::vector< Index > vertices( 1 << dimGrid );
      for( size_t i = 0; i < vertices.size(); ++i )
        vertices[ i ] = vertexIndex[indexSet.subIndex( *eIt, i, dimGrid )];
      
      gridout << vertices[ 0 ];
      for( size_t i = 1; i < vertices.size(); ++i )
        gridout << " " << vertices[ i ];


      for( size_t i = 0; i < eParam.size(); ++i )
          gridout << " " << eParam[ i ];

      gridout << std::endl;

      count++;
    }
    gridout << "#" << std::endl;

    std::cout<<"Writing boundary segments to dgf"<<std::endl;
    // write all boundaries to the "boundarysegments" block
    gridout << std::endl << "BOUNDARYSEGMENTS" << std::endl;
    for( ElementIterator eIt = gridView_.template begin< 0 >(); eIt != end; ++eIt )
    {
      if( !eIt->hasBoundaryIntersections() )
        continue;

      const RefElement &refElement = RefElements::general( eIt->type() );

      IntersectionIterator isIt = gridView_.ibegin(*eIt);
      const IntersectionIterator isItEnd = gridView_.iend(*eIt);
      for(; isIt != isItEnd; ++isIt )
      {
        if( !isIt->boundary() )
          continue;

        const int boundaryId = isIt->boundaryId();
        if( boundaryId <= 0 )
        {
          std::cerr << "Warning: Ignoring nonpositive boundary id: "
                    << boundaryId << "." << std::endl;
          continue;
        }

        const int faceNumber = isIt->indexInInside();
        const unsigned int faceSize = refElement.size( faceNumber, 1, dimGrid );
        std::vector< Index > vertices( faceSize );
        for( unsigned int vIdxOnFace = 0; vIdxOnFace < faceSize; ++vIdxOnFace )
        {
          const int vIdxOnE = refElement.subEntity( faceNumber, 1, vIdxOnFace, dimGrid );
          vertices[ vIdxOnFace ] = vertexIndex[ indexSet.subIndex( *eIt, vIdxOnE, dimGrid ) ];
        }
        gridout << boundaryId << "   " << vertices[ 0 ];
        for( unsigned int i = 1; i < faceSize; ++i )
          gridout << " " << vertices[ i ];
        gridout << std::endl;
      }
    }
    gridout << "#" << std::endl;

    // write the name into the "gridparameter" block
    gridout << std::endl << "BOUNDARYDOMAIN" << std::endl;
    gridout <<"default 1"<<std::endl;
    gridout << "#" << std::endl;
  }

  template< class TypeTag >
  inline void DGFWriterParam< TypeTag >::write ( const std::string &fileName/*,
					    const std::vector<Scalar>& porosity,
					    const std::vector<Scalar>& permeability,
					    const std::vector<Scalar>& river*/) const
  {
    std::ofstream gridout( fileName.c_str() );
    write( gridout/*, porosity, permeability, river */);
    std::cout<<"Created dgf file"<<std::endl;
  }

}

#endif // #ifndef DUNE_DGFWRITER_HH
