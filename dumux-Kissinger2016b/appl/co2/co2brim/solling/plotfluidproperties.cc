// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Simple test for the gnuplot interface, which plots a parabola
 */

#include "config.h"
#include <vector>
#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/components/h2o.hh>
#include "fluidsystems/brine.hh"
#include <appl/co2/snohvit/snohvit_co2tables.hh>
#include <dumux/material/components/co2.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

using namespace std;
////////////////////////
// the main function
////////////////////////
int main()
{
    //fluid types
    typedef Dumux::Brine<double, Dumux::H2O<double>> Brine;
    typedef Dumux::SnohvitCO2Tables::CO2Tables CO2Table;
    typedef Dumux::CO2<double, CO2Table> CO2;
    typedef Dumux::RegularizedBrooksCorey<double> EffMaterialLaw;
    typedef Dumux::EffToAbsLaw<EffMaterialLaw> EffToAbsLaw;
    typedef typename EffToAbsLaw::Params MaterialLawParams;

    unsigned int numIntervals = 100;
    double yMin = 1e100;
    double yMax = -1e100;
    vector<double> temp(numIntervals + 1);

    //brine density and viscosity over temperature for salinities 0.0, 0.1, 0.2, 0.29
    vector<double> T(numIntervals + 1);
    vector<double> salinity = {0.0, 0.1, 0.2, 0.29};
    vector<string> rhoBStr (salinity.size(), "rhoB_");
    vector<string> visBStr (salinity.size(), "visB_");
    vector<vector<double>> rhoB(salinity.size(), temp);
    vector<vector<double>> visB(salinity.size(), temp);
    double TMin = 283.15;
    double TMax = 383.15;
    double TRange = TMax - TMin;
    double rhoBMin = yMin;
    double rhoBMax = yMax;
    double visBMin = yMin;
    double visBMax = yMax;
    double pressureB = 1e7;

    //CO2 density and viscosity over pressure for temperature 303 K (30°C), 317 (45°), 333 (60°), 347 (75°)
    vector<double> p(numIntervals + 1);
    vector<double> temperatureC = {303, 317, 333, 347};
    vector<string> rhoCStr (temperatureC.size(), "rhoC_");
    vector<string> visCStr (temperatureC.size(), "visC_");
    vector<vector<double>> rhoC(temperatureC.size(), temp);
    vector<vector<double>> visC(temperatureC.size(), temp);
    double pMin = 1e6;
    double pMax = 3e7;
    double pRange = pMax - pMin;
    double rhoCMin = yMin;
    double rhoCMax = yMax;
    double visCMin = yMin;
    double visCMax = yMax;

    //Calculation of Capillary Pressure-Saturation and relative permeability
    vector<double> Sw(numIntervals + 1);
    vector<double> pc(numIntervals + 1);
    vector<double> krw(numIntervals + 1);
    vector<double> krn(numIntervals + 1);
    MaterialLawParams params;
    // residual saturations
    params.setSwr(0.3);
    params.setSnr(0);

    // parameters for the Brooks-Corey law
    params.setPe(1e4);
    params.setLambda(2.0);
    double SwMin = params.swr();
    double SwMinPlot = 0.0;
    double SwMax = 1.0;
    double SwRange = SwMax - SwMin;
    double pcMin = yMin;
    double pcMax = yMax;
    double krMin = 0.0;
    double krMax = 1.0;

    string pcSw = "lambda2pe1e4";
    string krwSw = "krw";
    string krnSw = "krn";

    for (unsigned salVecIter=0; salVecIter<salinity.size(); salVecIter++)
    {
        //convert components of salinity vector into a vector of strings
        rhoBStr[salVecIter].append(static_cast<ostringstream*>( &(ostringstream() << salinity[salVecIter]) )->str());
        visBStr[salVecIter].append(static_cast<ostringstream*>( &(ostringstream() << salinity[salVecIter]) )->str());

        //get values from component functions
        for (int i = 0; i <= numIntervals; i++)
        {
            //calculation for brine
            T[i] = TMin + TRange * double(i) /double(numIntervals);
            rhoB[salVecIter][i] = Brine::liquidDensity(T[i], pressureB, salinity[salVecIter]);
            visB[salVecIter][i] = Brine::liquidViscosity(T[i], pressureB, salinity[salVecIter]);
            rhoBMin = std::min(rhoBMin, rhoB[salVecIter][i]);
            rhoBMax = std::max(rhoBMax, rhoB[salVecIter][i]);
            visBMin = std::min(visBMin, visB[salVecIter][i]);
            visBMax = std::max(visBMax, visB[salVecIter][i]);
        }
    }

    //calculation for CO2
    for (unsigned tempVecIter=0; tempVecIter<temperatureC.size(); tempVecIter++)
    {
        //convert components of temperatureC vector into a vector of strings
        rhoCStr[tempVecIter].append(static_cast<ostringstream*>( &(ostringstream() << temperatureC[tempVecIter]) )->str());
        visCStr[tempVecIter].append(static_cast<ostringstream*>( &(ostringstream() << temperatureC[tempVecIter]) )->str());

        //get values from component functions
        for (int i = 0; i <= numIntervals; i++)
        {
            p[i] = pMin + pRange * double(i) /double(numIntervals);
            rhoC[tempVecIter][i] = CO2::liquidDensity(temperatureC[tempVecIter], p[i]);
            visC[tempVecIter][i] = CO2::liquidViscosity(temperatureC[tempVecIter], p[i]);
            rhoCMin = std::min(rhoCMin, rhoC[tempVecIter][i]);
            rhoCMax = std::max(rhoCMax, rhoC[tempVecIter][i]);
            visCMin = std::min(visCMin, visC[tempVecIter][i]);
            visCMax = std::max(visCMax, visC[tempVecIter][i]);
        }
    }

    //get values from material law
    for (int i = 0; i <= numIntervals; i++)
    {
        Sw[i] = SwMin + SwRange * double(i) /double(numIntervals);
        pc[i] = EffToAbsLaw::pc(params, Sw[i]);
        krw[i] = EffToAbsLaw::krw(params, Sw[i]);
        krn[i] = EffToAbsLaw::krn(params, Sw[i]);
        pcMin = min(pcMin, pc[i]);
        pcMax = max(pcMax, pc[i]);
    }

    //brine density over temperature for different salinities
    {
        Dumux::GnuplotInterface<double> gnuplot(false);
        gnuplot.setXRange(TMin, TMax);
        gnuplot.setYRange(rhoBMin, rhoBMax);
        gnuplot.setXlabel("Temperature [K]");
        gnuplot.setYlabel("Brine density [kg/m3]");
        gnuplot.setDatafileSeparator(',');
        for (unsigned salVecIter=0; salVecIter<salinity.size(); salVecIter++)
        {
            gnuplot.addDataSetToPlot(T, rhoB[salVecIter], rhoBStr[salVecIter]);
        }
        gnuplot.plot("rhoB");
    }

    //brine density over temperature for different salinities
    {
        Dumux::GnuplotInterface<double> gnuplot(false);
        gnuplot.setXRange(TMin, TMax);
        gnuplot.setYRange(visBMin, visBMax);
        gnuplot.setXlabel("Temperature [K]");
        gnuplot.setYlabel("Brine viscosity [Pa s]");
        gnuplot.setDatafileSeparator(',');
        for (unsigned salVecIter=0; salVecIter<salinity.size(); salVecIter++)
        {
            gnuplot.addDataSetToPlot(T, visB[salVecIter], visBStr[salVecIter]);
        }
        gnuplot.plot("visB");
    }

    //CO2 density over pressure for differen temperatures
    {
        Dumux::GnuplotInterface<double> gnuplot(false);
        gnuplot.setXRange(pMin, pMax);
        gnuplot.setYRange(rhoCMin, rhoCMax);
        gnuplot.setXlabel("Pressure [Pa]");
        gnuplot.setYlabel("CO2 density [Pa s]");
        gnuplot.setDatafileSeparator(',');
        for (unsigned tempVecIter=0; tempVecIter<temperatureC.size(); tempVecIter++)
        {
            gnuplot.addDataSetToPlot(p, rhoC[tempVecIter], rhoCStr[tempVecIter]);
        }
        gnuplot.plot("rhoC");
    }

    //CO2 density over pressure for differen temperatures
    {
        Dumux::GnuplotInterface<double> gnuplot(false);
        gnuplot.setXRange(pMin, pMax);
        gnuplot.setYRange(visCMin, visCMax);
        gnuplot.setXlabel("Pressure [Pa]");
        gnuplot.setYlabel("CO2 viscosity [Pa s]");
        gnuplot.setDatafileSeparator(',');
        for (unsigned tempVecIter=0; tempVecIter<temperatureC.size(); tempVecIter++)
        {
            gnuplot.addDataSetToPlot(p, visC[tempVecIter], visCStr[tempVecIter]);
        }
        gnuplot.plot("visC");
    }

    //Capillary pressure saturation curve
    {
        Dumux::GnuplotInterface<double> gnuplot(false);
        gnuplot.setXRange(SwMinPlot, SwMax);
        gnuplot.setYRange(pcMin, pcMax);
        gnuplot.setXlabel("Sw");
        gnuplot.setYlabel("pc [Pa]");
        gnuplot.setDatafileSeparator(',');
        gnuplot.addDataSetToPlot(Sw, pc, pcSw);
        gnuplot.plot("pcSw");
    }

    //Relative permeability curves for wetting and non-wetting fluid
    {
        Dumux::GnuplotInterface<double> gnuplot(false);
        gnuplot.setXRange(SwMinPlot, SwMax);
        gnuplot.setYRange(krMin, krMax);
        gnuplot.setXlabel("Sw");
        gnuplot.setYlabel("kr [Pa]");
        gnuplot.setDatafileSeparator(',');
        gnuplot.addDataSetToPlot(Sw, krw, krwSw);
        gnuplot.addDataSetToPlot(Sw, krn, krnSw);
        gnuplot.plot("krSw");
    }


    exit(0);
}
