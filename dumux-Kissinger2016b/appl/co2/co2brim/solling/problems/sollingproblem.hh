// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Definition of a virtual injection into the snohvit CO2 storage reservoir close
 * to a chimney structure (for dgf format).
 *
 *
 * This file contains the boundary and initial conditions of the snohvit-chimney scenario
 */
#ifndef DUMUX_SOLLING_PROBLEM_HH
#define DUMUX_SOLLING_PROBLEM_HH

#if HAVE_ALUGRID
#include <dune/grid/alugrid/2d/alugrid.hh>
#elif HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#else
#warning ALUGrid is necessary for this test.
#endif

#include <dune/common/float_cmp.hh>
#include <dune/grid/io/file/vtk/boundaryiterators.hh>
#include <dumux/implicit/dfm/common/implicitdfmporousmediaproblem.hh>
#include <dumux/implicit/box/intersectiontovertexbc.hh>
#include <appl/co2/snohvit/snohvit_co2tables.hh>
#include <dumux/io/dgfgridcreator.hh>
#include <dumux/material/components/co2.hh>
#include <dumux/material/components/co2tablereader.hh>
#include <dumux/material/components/brinevarsalinity.hh>
#include <dumux/material/components/h2o.hh>

#if MODELTYPE == 2
#include "../models/solling2pncmodel.hh"
#include "../volumevariables/solling2pncvolumevariables.hh"
#include "../fluidsystems/h2oco2naclfluidsystem.hh"
#else //MODELTYPE == 1
#include "../models/solling1p2cnimodel.hh"
#include "../volumevariables/solling1p2cvolumevariables.hh"
#include "../fluidsystems/watersaltfluidsystem.hh"
#endif

#include "../helperclasses/dgfwriterresult.hh"
#include "../helperclasses/sollingnewtoncontroller.hh"
#include "../helperclasses/vertidxtoelemneighbormapper.hh"

//Linear solver backends
//#include <dumux/linear/amgbackend.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/linear/boxlinearsolver.hh>
#include "../helperclasses/amgbackend.hh"


//Define if non-isothermal
#define NONISOTHERMAL 0



//#include "../spatialparams/sollingspatialparams_5layer.hh"
//#include "../spatialparams/sollingspatialparams_alllayer.hh"
#if GEOMETRY == 1
#include "../spatialparams/sollingspatialparams_generic.hh"
#elif GEOMETRY == 2
#include "../spatialparams/sollingspatialparams_twolayer.hh"
#else
#include "../spatialparams/sollingspatialparams_complex.hh"
#endif
//#include "../spatialparams/testspatialparams.hh"
//#include "../spatialparams/sollingspatialparams1p.hh"
//#include <test/implicit/1p2cni/1p2coutflowspatialparams.hh>

namespace Dumux
{

template <class TypeTag>
class SollingProblem;

//////////
// Specify the properties for the lens problem
//////////
namespace Properties
{
NEW_PROP_TAG(CO2Table);
//SET_TYPE_PROP(SollingSpatialParams, ParentSpatialParams, Dumux::SollingSpatialParamsOneP);

#if MODELTYPE == 2
NEW_TYPE_TAG(SollingProblem, INHERITS_FROM(BoxTwoPNCDFM, SollingSpatialParams));
#else
NEW_TYPE_TAG(SollingProblem, INHERITS_FROM(BoxOnePTwoCDFM));
#endif
//Set the model to be used
#if MODELTYPE == 2
SET_TYPE_PROP(SollingProblem, Model, Dumux::SollingTwoPNCModel<TypeTag>);
#else
SET_TYPE_PROP(SollingProblem, Model, Dumux::SollingOnePTwoCNIModel<TypeTag>);
#endif
// Set the grid type
SET_TYPE_PROP(SollingProblem, Grid, Dune::ALUGrid<3, 3, Dune::cube, Dune::conforming>);

#if MODELTYPE == 1
////Set the newton controller
SET_TYPE_PROP(SollingProblem, NewtonController, SollingNewtonController<TypeTag>);
#endif

// Set the problem property
SET_TYPE_PROP(SollingProblem, Problem, Dumux::SollingProblem<TypeTag>);

// Set the CO2 table to be used; in this case not the the default table
SET_TYPE_PROP(SollingProblem, CO2Table, Dumux::SnohvitCO2Tables::CO2Tables);

// set the fluid system
SET_PROP(SollingProblem, FluidSystem)
{
    //private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
#if MODELTYPE == 2
    typedef Dumux::H2OCO2NaClFluidSystem<TypeTag> type;
#else
    typedef Dumux::FluidSystems::WaterSaltFluidSystem<TypeTag> type;
#endif
    //
};

// Set the spatial parameters
SET_TYPE_PROP(SollingProblem,
        SpatialParams,
        Dumux::SollingSpatialParams<TypeTag>);

// Set the volume variables property to allow for compressible solid phase
#if MODELTYPE == 2
SET_TYPE_PROP(SollingProblem,
        MatrixVolumeVariables,
        Dumux::Solling2pncVolumeVariables<TypeTag>);
#else
SET_TYPE_PROP(SollingProblem,
        MatrixVolumeVariables,
        Dumux::Solling1p2cVolumeVariables<TypeTag>);
#endif

//              Dumux::OnePTwoCOutflowSpatialParams<TypeTag>);

//SET_PROP(SollingProblem, FluidSystem)
//{ private:
//    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
//public:
//    typedef Dumux::FluidSystems::H2ON2LiquidPhase<Scalar, false> type;
//};


// Set the spatial parameters
//SET_TYPE_PROP(SollingProblem,
//              SpatialParams,
//              Dumux::SollingSpatialParams<TypeTag>);

//! the CO2 Model and VolumeVariables properties
//SET_TYPE_PROP(SollingProblem, Model, SnohvitTwoPModel<TypeTag>);

//// Set the CO2 table to be used; in this case not the the default table
//SET_TYPE_PROP(SollingProblem, CO2Table, Dumux::SnohvitCO2Tables::CO2Tables);
// Set the salinity mass fraction of the brine in the reservoir
//SET_SCALAR_PROP(SollingProblem, ProblemSalinity, 1e-1);

// Enable partial reassembly of the jacobian matrix?
//SET_BOOL_PROP(SollingProblem, ImplicitEnablePartialReassemble, false);

// Enable reuse of jacobian matrices?
//SET_BOOL_PROP(SollingProblem, ImplicitEnableJacobianRecycling, false);

// Write the solutions of individual newton iterations?
//SET_BOOL_PROP(SollingProblem, NewtonWriteConvergence, false);

// Use forward differences instead of central differences
//SET_INT_PROP(SollingProblem, ImplicitNumericDifferenceMethod, +1);

// Linear solver settings
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::AMGBackend<TypeTag> );
SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::ILUnBiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::SORBiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::SSORBiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::GSBiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::JacBiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::ILUnCGBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::SORCGBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::SSORCGBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::GSCGBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::JacCGBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::SSORRestartedGMResBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::ILU0BiCGSTABBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::ILU0CGBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::ILU0RestartedGMResBackend<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::ILUnRestartedGMResBackend<TypeTag> );

//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::BoxBiCGStabILU0Solver<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::BoxBiCGStabSORSolver<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::BoxBiCGStabSSORSolver<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::BoxBiCGStabJacSolver<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::BoxBiCGStabGSSolver<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::BoxCGILU0Solver<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::BoxCGSORSolver<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::BoxCGSSORSolver<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::BoxCGJacSolver<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::BoxCGGSSolver<TypeTag> );
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::BoxCGGSSolver<TypeTag> );

//AMGModified:
//SET_TYPE_PROP(SollingProblem, LinearSolver, Dumux::AMGBackendModified<TypeTag> );

// Enable gravity
SET_BOOL_PROP(SollingProblem, ProblemEnableGravity, true);

#if MODELTYPE == 1
// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(SollingProblem, UseMoles, true);
#endif
// Define whether output should be ascii or binary format
SET_PROP(SollingProblem, VtkMultiWriter)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef typename Dumux::VtkMultiWriter<GridView, Dune::VTK::appendedraw> type;
};


}

template <class TypeTag >
class SollingProblem : public ImplicitDFMPorousMediaProblem<TypeTag>
{
    typedef SollingProblem<TypeTag> ThisType;
    typedef ImplicitDFMPorousMediaProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef Dune::GridPtr<Grid> GridPointer;
    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag,CO2Table) CO2Table;
    typedef Dumux::CO2<Scalar, CO2Table> CO2;
    typedef Dumux::H2O<Scalar> H2O;
    typedef Dumux::BrineVarSalinity<Scalar, H2O> Brine;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;

    enum {
        numEq = GET_PROP_VALUE(TypeTag, NumEq),

        // primary variable indices
        pressureIdx = Indices::pressureIdx,
        H2OIdx = FluidSystem::H2OIdx,
#if MODELTYPE == 2
        CO2Idx = FluidSystem::CO2Idx,
        NaClIdx = FluidSystem::NaClIdx,
#else
        NaClIdx = Indices::massOrMoleFracIdx,
#endif

        // equation indices
        contiWEqIdx = Indices::conti0EqIdx,
#if MODELTYPE == 2
        contiNEqIdx = Indices::contiNEqIdx,
        NaClEqIdx =  FluidSystem::NaClIdx,
#else
        NaClEqIdx = Indices::transportEqIdx,
#endif

        //non isothermal indices
#if NONISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
        energyEqIdx = Indices::energyEqIdx,
#endif

        //phase indices
#if MODELTYPE == 2
        wPhaseIdx = Indices::wPhaseIdx,
#else
        wPhaseIdx = Indices::phaseIdx,
#endif

#if MODELTYPE == 2
        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,
#endif
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::Traits::template Codim<0>::EntityPointer ElementPointer;
    typedef typename GridView::template Codim<dim>::EntityPointer VertexPointer;
    typedef typename Element::Geometry Geometry;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<1>::Iterator FaceIterator;
    typedef typename GridView::template Codim<dim-1>::Iterator EdgeIterator;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;
    typedef typename GridView::Intersection Intersection;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef Dune::VTK::BoundaryIterator<GridView> BoundaryIterator;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename FVElementGeometry::FractureScv FractureScv;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GridView::Grid::ctype CoordScalar;
    typedef typename Dune::ReferenceElements<CoordScalar, dim> ReferenceElements;
    typedef typename Dune::ReferenceElement<CoordScalar, dim> ReferenceElement;
    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;
    typedef Dumux::VertIdxToElemNeighborMapper<GridView> VertIdxToElemNeighborMapper;
    typedef typename VertIdxToElemNeighborMapper::NeighborElementSeedsIterator NeighborElementSeedsIterator;
    typedef Dune::FieldVector<Scalar, dim> LocalPosition;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar,dimWorld,dimWorld> FieldMatrix;
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };
#if MODELTYPE == 1
    enum { useMoles = GET_PROP_VALUE(TypeTag, UseMoles) };
#endif
    enum { numPhases = GET_PROP_VALUE(TypeTag, NumPhases)};
    typedef typename GET_PROP_TYPE(TypeTag, FluxVector) FluxVector;

public:

    enum
    {
        //episode indices
        initPressureEpsIdx = 1,
        initSaltEpsIdx = 2,
        injectionEpsIdx = 3,
        postInjectionEpsIdx = 4,

        //boundary type index
        riverBoundary = 0,
        rechargeBoundary = 1,
        lateralBoundary = 2,
        saltBoundary = 3,
        outerDomainBoundary = 4,
        outerDomainLateralBoundary = 5
    };

    /*!
     * \brief The constructor
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     * \param lensLowerLeft Global position of the lenses lower left corner
     * \param lensUpperRight Global position of the lenses upper right corner
     */
    SollingProblem(TimeManager &timeManager,
            const GridView &gridView)
    : ParentType(timeManager, gridView), vertexElementMapper_(this->gridView()),
      /* only needed for 2pnc model*/ useInterfaceCondition_(true)
    {
        // Gravity vector definition, z-origin at top (most shallow point)
        gravity_ = 0.;
        gravity_[dim-1] = 9.81;
        topOfReservoir_ = this->bBoxMin()[dimWorld-1];

        Scalar XlNaClSoluteGrad;
        Scalar XlNaClSoluteMax;
        Scalar XlNaClSoluteMin;

        //The initial solution has to written to the initialPVMap_ in the first call of preTimeStep()
        initializePVMap_ = true;

        //Set the stationary solution bool to false
        stationary_ = false;

        try
        {
            name_               = GET_RUNTIME_PARAM(TypeTag, std::string, Problem.Name);
            initializationPressureOnly_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.InitializationPressureOnly);
            initializationOnly_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.InitializationOnly);
            readFromDGFPressureInitialization_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.ReadFromDGFPressureInitialization);

            //Episode mangement
            saltInitDT_ =  GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.SaltInitDT);
            saltEpsLength_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.SaltEpsLength);
            injectionEpsLength_    = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InjectionEpsLength);
            injectionInitDT_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InjectionInitDT);
            injectionMaxDT_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InjectionMaxDT);
            postInjectionEpsLength_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PostInjectionEpsLength);
            postInjectionInitDT_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PostInjectionInitDT);
            postInjectionMaxDT_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PostInjectionMaxDT);

            //Output
            episodeOutput_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.EpisodeOutput);
            timeStepOutput_ = GET_RUNTIME_PARAM(TypeTag, int, Problem.TimeStepOutput);
            timeStepRestart_ =  GET_RUNTIME_PARAM(TypeTag, int, Problem.TimeStepRestart);
            debugOutput_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.DebugOutput);

            // Fluidsystem initialization
            nTemperature_       = GET_RUNTIME_PARAM(TypeTag, int, FluidSystem.NTemperature);
            nPressure_          = GET_RUNTIME_PARAM(TypeTag, int, FluidSystem.NPressure);
            pressureLow_        = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.PressureLow);
            pressureHigh_       = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.PressureHigh);
            temperatureLow_     = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.TemperatureLow);
            temperatureHigh_    = GET_RUNTIME_PARAM(TypeTag, Scalar, FluidSystem.TemperatureHigh);

            // Injection settings
            injectionRate_        = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InjectionRate);
            injCoord_ = GET_RUNTIME_PARAM(TypeTag, GlobalPosition, Problem.InjCoor);
            deltaSphere_		  = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.DeltaSphere);
            useCO2VolumeEquivalentInjRate_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.UseCO2VolumeEquivalentInjRate);

            //Measurement Coordinates
            m1_ =  GET_RUNTIME_PARAM(TypeTag, GlobalPosition, Problem.M1);
            m2_ = GET_RUNTIME_PARAM(TypeTag, GlobalPosition, Problem.M2);
#if GEOMETRY !=2
            dCoorX_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.DCoorX);
#endif
            //boundary conditions
            pressureTOR_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PressureTOR);
            gWRegenerationRate_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.GWRegenerationRate);
            closed_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.Closed);
            dirichletAtTop_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.DirichletAtTop);

            //initial conidtions
            massReductionFactor_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.MassReductionFactor);
            applyGeothermalGradient_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.ApplyGeothermalGradient);
            enableSaltTransport_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.EnableSaltTransport);
            XlNaClSoluteGrad = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.XlNaClSoluteGrad);
            XlNaClSoluteMax = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.XlNaClSoluteMax);
            XlNaClSoluteMin = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.XlNaClSoluteMin);
            initSaltTerQuar_ = GET_RUNTIME_PARAM(TypeTag, bool, Problem.InitSaltTerQuar);
            //stationarity bounds, determine when a stationary state has been reached
            statDiffPress_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.StatDiffPress);
            statDiffSaltMoleFrac_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.StatDiffSaltMoleFrac);

        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown while reading in parameters in the problem file!\n";
            exit(1);
        }
        int rowIdx[] = {3,6};
        calcFlowRowIdx_.resize(2);
        for(int i = 0; i < 2; i++)
        {
            calcFlowRowIdx_[i] = rowIdx[i];
        }
        eps_ = 1e-6;

        // initialize the tables of the fluid system
        FluidSystem::init(/*Tmin=*/temperatureLow_,
                /*Tmax=*/temperatureHigh_,
                /*nT=*/nTemperature_,
                /*pmin=*/pressureLow_,
                /*pmax=*/pressureHigh_,
                /*np=*/nPressure_);

        //initialize the maximum salt mole fraction [mol Salt/mol Brine] out of the maximum solutal salt mass fraction [kg Salt/kg Water]
        xlNaClmax_ = calculateMoleFracFromMolality_(XlNaClSoluteMax);
        //initialize the minimum salt mole fraction [mol Salt/mol Brine] out of the maximum solutal salt mass fraction [kg Salt/kg Water]
        xlNaClmin_ = calculateMoleFracFromMolality_(XlNaClSoluteMin);
        //initialize the maximum salt mole fraction grad [mol Salt/mol Brine/m] out of the NaCl solutal mass fraction grad [kg Salt/kg Water/m]
        xlNaClGrad_ = calculateMoleFracFromMolality_(XlNaClSoluteGrad);

        if(this->gridView().comm().rank() == 0)
        {
            std::cout<<"Max Mole Fraction NaCl: "<<xlNaClmax_<<std::endl;
            std::cout<<"Min Mole Fraction NaCl: "<<xlNaClmin_<<std::endl;
            std::cout<<"Mole Fraction NaCl Grad: "<<xlNaClGrad_<<std::endl;
        }
        //Find global index of injection cells and box volumes
        int injNoNodes = 0;
        injVolume_ = 0.;
        Scalar minElementVolume = std::numeric_limits<typename GET_PROP_TYPE(TypeTag,Scalar)>::max();
        GlobalPosition minElementCenter(0);
        int noElementRC = 0;
        depthSalt_ = 0;

        FVElementGeometry fvGeometry;
        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator elemEndIt = this->gridView().template end<0>();
        PrimaryVariables initialValues(0);
        gridHasOuterDomain_ = false;
        unsigned numE = this->gridView().size(0);
        outerDomainE_.resize(numE, false);;

        for (; eIt != elemEndIt; ++eIt)
        {
            unsigned eIdx = this->elementMapper().map(*eIt);
            //Check if there is an outer domain
            if(GridCreator::template nofParameters<Element>(*eIt)-1 == SpatialParams::outerDomainEIdx)
            {
                gridHasOuterDomain_ = true;
                outerDomainE_[eIdx] = eInOuterDomain(*eIt);
            }
            fvGeometry.update(this->gridView(), *eIt);
            GlobalPosition elementCenter = eIt->geometry().center();
            minElementVolume = std::min(minElementVolume, eIt->geometry().volume());
            minElementCenter = minElementVolume == eIt->geometry().volume() ? elementCenter : minElementCenter;
            //                int numVerts = eIt->template count<dim> ();

            //calculate a mean depth of the rc layer, this will serve as an indicator for starting the salt
            //linear gradient
            if(this->spatialParams().findLayerIdx(*eIt) == SpatialParams::rcIdx)
            {
                ++noElementRC;
                depthSalt_ += elementCenter[dimWorld-1];
            }

            //iterate over all degrees of freedom (i.e. nodes or element centers)
            for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
            {
                GlobalPosition globalPos(0);

                //the dof mapper will return either global vertice index or global element index depending
                //on dofcodim and the scvIdx. Using the dofMapper from the model is not possible here, since model.init()
                //function has not been called yet (gets called in timeManager.init())
                int dofIdxGlobal = -1;
                if(GET_PROP_VALUE(TypeTag, ImplicitIsBox))
                {
                    dofIdxGlobal = this->vertexMapper().map(*eIt, scvIdx, dim);
                    globalPos = eIt->geometry().corner(scvIdx);
                }

                else
                {
                    dofIdxGlobal = this->elementMapper().map(*eIt);
                    globalPos = eIt->geometry().center();
                }

                //initialize the fluxCalculationElement_ vector
                fluxLayerBase_.resize(this->elementMapper().index(*eIt)+1);
                if(this->spatialParams().findFluxLineIdx(*eIt) == 1)
                    fluxLayerBase_[this->elementMapper().index(*eIt)] = 1;
                else
                    fluxLayerBase_[this->elementMapper().index(*eIt)] = -largeNumber_;

                fluxLayer_.resize(this->elementMapper().index(*eIt)+1);
                if(this->spatialParams().findFluxLineIdx(*eIt) == 1)
                    fluxLayer_[this->elementMapper().index(*eIt)] = 1;
                else
                    fluxLayer_[this->elementMapper().index(*eIt)] = -largeNumber_;

                //initialize primary variable map
                //value above zero, gets properly initialized in preTimeStep()
                initialPVMap_[dofIdxGlobal]= 1e5;


                if(eIt->partitionType() == Dune::InteriorEntity)
                {

                    if(globalPosInSphere(globalPos, injCoord_, deltaSphere_))
                    {
                        injNoNodes += 1;
                        injVolume_ += fvGeometry.subContVol[scvIdx].volume;
                        std::cout<<"Found injection point"<<" dofIdxGlobal: "<<dofIdxGlobal
                                <<" Layer Index: "<<this->spatialParams().findLayerIdx(*eIt)<<
                                " GlobalPosition: "<<globalPos<<std::endl;
                    }

                    //Measurement point 1
                    if(globalPosInSphere(globalPos, m1_, deltaSphere_))
                    {
                        std::cout<<"Found measurementpoint 1"<<" dofIdxGlobal: "<<dofIdxGlobal
                                <<" Layer Index: "<<this->spatialParams().findLayerIdx(*eIt)<<
                                " GlobalPosition: "<<globalPos<<std::endl;
                    }

                    //Measurement point 2
                    if(globalPosInSphere(globalPos, m2_, deltaSphere_))
                    {
                        std::cout<<"Found measurementpoint 2"<<" dofIdxGlobal: "<<dofIdxGlobal
                                <<" Layer Index: "<<this->spatialParams().findLayerIdx(*eIt)<<
                                " GlobalPosition: "<<globalPos<<std::endl;
                    }
                }
            }
        }
        //calculate a mean depth of the rc layer, this will serve as an indicator for starting the salt
        //linear gradient
        if(this->gridView().comm().size() > 1)
        {
            noElementRC = this->gridView().comm().sum(noElementRC);
            depthSalt_ = this->gridView().comm().sum(depthSalt_);
        }
        depthSalt_ = depthSalt_/noElementRC;

        if(this->gridView().comm().rank() == 0)
        {
            std::cout<<"Starting salt gradient at depth: "<<depthSalt_<<std::endl;
            if(gridHasOuterDomain_)
                std::cout<<"Grid has outer domain."<<std::endl;
            else
                std::cout<<"Grid has no outer domain."<<std::endl;
        }

        //iterate over all element faces
        FaceIterator faceIt = this->gridView().template begin<1>();
        FaceIterator faceEndIt = this->gridView().template end<1>();
        GlobalPosition minFaceElementCenter(0);
        Scalar minFaceArea = std::numeric_limits<typename GET_PROP_TYPE(TypeTag,Scalar)>::max();
        for(; faceIt != faceEndIt; ++faceIt)
        {
            minFaceArea = std::min(minFaceArea, faceIt->geometry().volume());
            //            minFaceElementCenter = minFaceArea == faceIt->geometry().volume() ? faceIt->geometry().center() : minFaceElementCenter;
        }

        //iterate over all element edges
        EdgeIterator edgeIt = this->gridView().template begin<dim-1>();
        EdgeIterator edgeEndIt = this->gridView().template end<dim-1>();
        GlobalPosition minEdgeElementCenter(0);
        Scalar minEdgeLength = std::numeric_limits<typename GET_PROP_TYPE(TypeTag,Scalar)>::max();
        for(; edgeIt != edgeEndIt; ++edgeIt)
        {
            minEdgeLength = std::min(minEdgeLength, edgeIt->geometry().volume());
            //            minEdgeElementCenter = minEdgeLength == edgeIt->geometry().volume() ? edgeIt->geometry().center() : minEdgeElementCenter;
        }

        try
        {
            std::cout<<"Setting boundary conditions."<<std::endl;
            //Specify the vertex boundary condition
            // bc hierachy
            // 1. vertices containing inner and outer domain are outerDomainBoundary no matter what
            // 2. outerDomainLateralBoundary
            // 3. outerDomainBoundary
            // 4. lateral boundary
            // 5. river boundary
            // 6. rechargeBoundary
            // 7. saltBoundary
            BoundaryIterator bIsItEnd(this->gridView(), true);
            for(BoundaryIterator bIsIt(this->gridView()); bIsIt != bIsItEnd; ++bIsIt)
            {
                ElementPointer ePtr = bIsIt->inside();
                int intersectionIdx = bIsIt->indexInInside();
                //Loop over vertices on intersection
                Dune::GeometryType geomType = ePtr->geometry().type();
                const ReferenceElement &referenceElement = ReferenceElements::general(geomType);
                int numVerticesOfIntersection = referenceElement.size(intersectionIdx, /*codim intersection*/1, /*codim vertice*/ dim);
                for (int vertInIntersection = 0; vertInIntersection < numVerticesOfIntersection; vertInIntersection++)
                {
                    //Get the local vertex index with respect to the element from the local vertex index with respect to the interesection
                    int vIdxInElement = referenceElement.subEntity(intersectionIdx, /*codim intersection*/1, vertInIntersection, /*codim vertice*/dim);
                    //Get the global vertex index
                    int vIdxGlobal = this->vertexMapper().subIndex(*ePtr, vIdxInElement, dofCodim);
                    //Check if the global vertex was already written on the map, if yes skip the vertex
                    //This algorithm is called for every vertex once
                    std::map<int, int>::iterator it = boundaryTypesVertex_.find(vIdxGlobal);
                    if(it == boundaryTypesVertex_.end())
                    {
                        VertexPointer vPtr = *ePtr.template subEntity<dim>(vIdxInElement);
                        bool hasRiverBoundary = this->spatialParams().isRiver(*vPtr);
                        //Get the iterator over the neighbor elements
                        NeighborElementSeedsIterator nESIt = vertexElementMapper_.vertexElementsBegin(vIdxGlobal);
                        NeighborElementSeedsIterator nESItEnd = vertexElementMapper_.vertexElementsEnd(vIdxGlobal);
                        bool hasouterDomainLateralBoundary = false;
                        bool hasouterDomainBoundary = false;
                        bool hasLateralBoundary = false;
                        bool hasRechargeBoundary = false;
                        bool hasSaltBoundary = false;

                        //Loop over elements which contain the vertex
                        for(; nESIt != nESItEnd; ++nESIt)
                        {
                            ElementPointer ePtrVertexNeighbor = this->gridView().grid().entityPointer(*nESIt);
                            //Loop over intersections
                            IntersectionIterator isEndIt = this->gridView().iend(*ePtrVertexNeighbor);
                            for (IntersectionIterator isIt = this->gridView().ibegin(*ePtrVertexNeighbor); isIt != isEndIt; ++isIt)
                            {
                                //Check if the intersection is on the boundary
                                if(!isIt->boundary())
                                    continue;
                                //Check if the vertex is in the intersection
                                if(vIdxGlobalOnIntersection(vIdxGlobal, *isIt, *ePtrVertexNeighbor))
                                {
                                    int boundaryId = getBoundaryId(*isIt);
                                    if(boundaryId == outerDomainLateralBoundary)
                                        hasouterDomainLateralBoundary = true;
                                    else if(boundaryId == outerDomainBoundary)
                                        hasouterDomainBoundary = true;
                                    else if(boundaryId == lateralBoundary)
                                        hasLateralBoundary = true;
                                    else if(boundaryId == rechargeBoundary)
                                        hasRechargeBoundary = true;
                                    else if(boundaryId == saltBoundary)
                                        hasSaltBoundary = true;
                                }
                            }
                        }
                        if(hasouterDomainLateralBoundary)
                            boundaryTypesVertex_[vIdxGlobal] = outerDomainLateralBoundary;
                        else if(hasouterDomainBoundary)
                            boundaryTypesVertex_[vIdxGlobal] = outerDomainBoundary;
                        else if(hasLateralBoundary)
                            boundaryTypesVertex_[vIdxGlobal] = lateralBoundary;
                        else if(hasRiverBoundary)
                            boundaryTypesVertex_[vIdxGlobal] = riverBoundary;
                        else if(hasRechargeBoundary)
                            boundaryTypesVertex_[vIdxGlobal] = rechargeBoundary;
                        else if(hasSaltBoundary)
                            boundaryTypesVertex_[vIdxGlobal] = saltBoundary;
                        else
                            DUNE_THROW(Dune::NotImplemented, "No or invalid boundary id for boundary intersection!");
                    }
                }
            }
            std::cout<<"Finished setting boundary conditions."<<std::endl;
        }
        catch(...)
        {
            std::cerr << "Unknown exception thrown while setting boundary conditions!\n";
            exit(1);
        }

        if (this->gridView().comm().size() > 1)
        {
            injVolume_ = this->gridView().comm().sum(injVolume_);
            injNoNodes = this->gridView().comm().sum(injNoNodes);
            minFaceArea = this->gridView().comm().min(minFaceArea);
            minEdgeLength = this->gridView().comm().min(minEdgeLength);
        }

        if(this->gridView().comm().rank() == 0)
        {
            std::cout<<"No of scv for injection: "<<injNoNodes<<std::endl;
            std::cout<<"Injection Cell Volume: "<<injVolume_<<std::endl;
            std::cout<<"Minimal Element Volume: "<<minElementVolume<<" Element Center Coor: "<<minElementCenter<<std::endl;
            std::cout<<"Minimal Face Area: "<<minFaceArea<<" Element Center Coor: "<<minFaceElementCenter<<std::endl;
            std::cout<<"Minimal Edge Length: "<<minEdgeLength<<" Element Center Coor: "<<minEdgeElementCenter<<std::endl;
        }

        //Start the initialization of the hydrostatic pressure only if the episode index is zero (for restart it may be different)
        if(this->timeManager().episodeIndex() == 0)
        {
            this->timeManager().startNextEpisode(
                    std::numeric_limits<typename GET_PROP_TYPE(TypeTag,Scalar)>::max());
            if( this->gridView().comm().rank() == 0 )
                std::cout<<"Episode index is set to: "<<this->timeManager().episodeIndex()<<std::endl;
        }
    }

    void addOutputVtkFields()
    {
        typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;
        typedef Dune::BlockVector<Dune::FieldVector<double, dimWorld> > VectorField;
        typedef Dune::BlockVector<Dune::FieldVector<int, 1> > IntField;
        // get the number of degrees of freedom
        unsigned numDofs = this->model().numDofs();
        unsigned numElements = this->gridView().size(0);
        //create required scalar fields
        ScalarField *Kxx = this->resultWriter().allocateManagedBuffer(numElements);
        ScalarField *cellPorosity = this->resultWriter().allocateManagedBuffer(numElements);
        ScalarField *boxVolume = this->resultWriter().allocateManagedBuffer(numDofs);
        VectorField *cellCenter = this->resultWriter().template allocateManagedBuffer<double, dimWorld>(numElements);
        ScalarField *fluxLayerBase = this->resultWriter().allocateManagedBuffer(numElements);
        ScalarField *fluxLayer = this->resultWriter().allocateManagedBuffer(numElements);
        IntField *fluxLine = this->resultWriter().template allocateManagedBuffer<int>(numElements);
        ScalarField *elementVolume = this->resultWriter().allocateManagedBuffer(numElements);
        IntField *layerIdx = this->resultWriter().template allocateManagedBuffer<int>(numElements);
        ScalarField *deltaPw = this->resultWriter().allocateManagedBuffer(numDofs);
        ScalarField *deltaXlNaCl = this->resultWriter().allocateManagedBuffer(numDofs);
        ScalarField *temperature = this->resultWriter().allocateManagedBuffer(numDofs);
        IntField *river = this->resultWriter().template allocateManagedBuffer<int>(numDofs);
        ScalarField *pwInstatIdx = this->resultWriter().template allocateManagedBuffer<Scalar>(numDofs);
        ScalarField *trInstatIdx = this->resultWriter().template allocateManagedBuffer<Scalar>(numDofs);
        IntField *domainType = this->resultWriter().template allocateManagedBuffer<int>(numElements);

        //fracture specific output
        IntField *fractureVertex = this->resultWriter().template allocateManagedBuffer<int>(numDofs);
        ScalarField *fracturePorosity = this->resultWriter().allocateManagedBuffer(numDofs);

        //Will only be written if debugOutput_ == true
        IntField *rowIdx = this->resultWriter().template allocateManagedBuffer<int>(numElements);
        IntField *topBoundaryId = this->resultWriter().template allocateManagedBuffer<int>(numElements);
        IntField *lateralBoundaryId = this->resultWriter().template allocateManagedBuffer<int>(numElements);
        IntField *saltBoundaryId = this->resultWriter().template allocateManagedBuffer<int>(numElements);
        IntField *outerDomainBoundaryId = this->resultWriter().template allocateManagedBuffer<int>(numElements);
        IntField *outerDomainLateralBoundaryId = this->resultWriter().template allocateManagedBuffer<int>(numElements);
        IntField *conform = this->resultWriter().template allocateManagedBuffer<int>(numElements);
        IntField *boundaryType = this->resultWriter().template allocateManagedBuffer<int>(numDofs);
        ScalarField *fractureVolume = this->resultWriter().allocateManagedBuffer(numDofs);
        ScalarField *matrixVolume = this->resultWriter().allocateManagedBuffer(numDofs);
        ScalarField *fractureArea = this->resultWriter().allocateManagedBuffer(numDofs);
        IntField *fracScvNum = this->resultWriter().template allocateManagedBuffer<int>(numDofs);

        //initialize values
        (*boxVolume) = 0.0;
        (*conform) = 1;
        (*fractureVertex) = 0;
        (*fractureVolume) = 0.0;
        (*matrixVolume) = 0.0;
        (*fractureArea) = 0.0;
        (*fracturePorosity) = 0.0;
        (*fracScvNum) = 0;
        (*topBoundaryId) = 0;
        (*lateralBoundaryId) = 0;
        (*saltBoundaryId) = 0;
        (*outerDomainBoundaryId) = 0;
        (*outerDomainLateralBoundaryId) = 0;
        (*boundaryType) = 0;

        //Fill the scalar fields with values
        ScalarField *rank = this->resultWriter().allocateManagedBuffer(numElements);

        FVElementGeometry fvGeometry;
        VolumeVariables volVars;
        PrimaryVariables initialValues(0);
        GlobalPosition globalPos(0);
        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator eEndIt = this->gridView().template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {
                int eIdx = this->elementMapper().index(*eIt);
                (*rank)[eIdx] = this->gridView().comm().rank();
                fvGeometry.update(this->gridView(), *eIt);
                fvGeometry.updateFracture(*eIt, *this);
                const Geometry& geometry = eIt->geometry();
                Dune::GeometryType geomType = geometry.type();
                const ReferenceElement &referenceElement = ReferenceElements::general(geomType);
                ElementVolumeVariables elemVolVars;
                elemVolVars.update(*this,
                        *eIt,
                        fvGeometry,
                        false /* oldSol? */);

                (*domainType)[eIdx] =  eInOuterDomain(eIdx);
                if(debugOutput_)
                {
                    (*rowIdx)[eIdx] = this->spatialParams().findElementRowIdx(*eIt);
                }

                for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                {
                    int dofIdxGlobal = this->model().dofMapper().subIndex(*eIt, scvIdx, dofCodim);

                    //update the volvars with the initial solution from initialPVMap_
                    VolumeVariables volVarsInitial;
                    volVarsInitial.update(initialPVMap_.find(dofIdxGlobal)->second,
                         *this,
                         *eIt,
                         fvGeometry,
                         scvIdx,
                         false /* oldSol? */);

                    globalPos = eIt->geometry().corner(scvIdx);
                    (*boxVolume)[dofIdxGlobal] += fvGeometry.subContVol[scvIdx].volume;
                    (*deltaPw)[dofIdxGlobal] = elemVolVars[scvIdx].fluidState().pressure(wPhaseIdx)
                                                                                    - volVarsInitial.fluidState().pressure(wPhaseIdx);
                    (*deltaXlNaCl)[dofIdxGlobal] = elemVolVars[scvIdx].fluidState().massFraction(wPhaseIdx, NaClIdx)
                                                                                    - volVarsInitial.fluidState().massFraction(wPhaseIdx, NaClIdx);
                    (*pwInstatIdx)[dofIdxGlobal] =  std::abs(this->model().curSol()[dofIdxGlobal][pressureIdx]
                                                                                                  - this->model().prevSol()[dofIdxGlobal][pressureIdx]);
                    (*trInstatIdx)[dofIdxGlobal] =  std::abs(this->model().curSol()[dofIdxGlobal][NaClIdx]
                                                                                                  - this->model().prevSol()[dofIdxGlobal][NaClIdx]);
                    (*temperature)[dofIdxGlobal] = temperatureAtPos(globalPos);
                    (*river)[dofIdxGlobal] = this->spatialParams().isRiver(*eIt, scvIdx);

                    FractureScv fracScv = elemVolVars[scvIdx].fractureScv();
                    (*fracturePorosity)[dofIdxGlobal] += elemVolVars[scvIdx].fracturePorosity(); //for hexaeder
                    if(this->spatialParams().isFractureVert(*eIt, scvIdx))
                        (*fractureVertex)[dofIdxGlobal] = 1;
                    if(debugOutput_)
                    {
                        (*fractureVolume)[dofIdxGlobal] += fracScv.fractureVolume;
                        (*matrixVolume)[dofIdxGlobal] += fracScv.matrixVolFrac*fvGeometry.subContVol[scvIdx].volume;
                        //Check if the global vertex is on the map
                        std::map<int, int>::iterator it = boundaryTypesVertex_.find(dofIdxGlobal);
                        if(it != boundaryTypesVertex_.end())
                        {
                            (*boundaryType)[dofIdxGlobal] = boundaryTypesVertex_.find(dofIdxGlobal)->second;
                        }
                        if(fracScv.hasFractureVolume)
                        {
                            (*fracScvNum)[dofIdxGlobal] += 1;
                        }
                    }
                }

                 if(debugOutput_)
                 {
                     for (int scvfIdx=0; scvfIdx<referenceElement.size(/* codim = edges*/dim-1); scvfIdx++)
                     {
                         FluxVariables fluxVars(*this,
                                 *eIt,
                                 fvGeometry,
                                 scvfIdx,
                                 elemVolVars,
                                 false);
                         int dofIdxGlobali = this->model().dofMapper().subIndex(*eIt, fluxVars.face().i, dofCodim);
                         int dofIdxGlobalj = this->model().dofMapper().subIndex(*eIt, fluxVars.face().j, dofCodim);
                         (*fractureArea)[dofIdxGlobali] += fluxVars.fractureFace().area;
                         (*fractureArea)[dofIdxGlobalj] += fluxVars.fractureFace().area;
                     }
                 }

                (*Kxx)[eIdx] = this->spatialParams().intrinsicPermeability(*eIt, fvGeometry, /*element data*/ 0);
                (*elementVolume)[eIdx] = eIt->geometry().volume();
                (*layerIdx)[eIdx] = this->spatialParams().findLayerIdx(*eIt);
                int dofIdxGlobal = this->model().dofMapper().subIndex(*eIt, 0, dofCodim);
                (*cellPorosity)[eIdx] = this->spatialParams().porosity(*eIt, fvGeometry, /*element data*/ 0, this->model().curSol()[dofIdxGlobal][pressureIdx]);

                (*cellCenter)[eIdx] = eIt->geometry().center();
                (*fluxLayerBase)[eIdx] = fluxLayerBase_[eIdx];
                (*fluxLayer)[eIdx] = fluxLayer_[eIdx];
                (*fluxLine)[eIdx] = this->spatialParams().findFluxLineIdx(*eIt);

                if(debugOutput_)
                {
                    //Loop over all intersections of the grid and return true if they are conforming
                    IntersectionIterator isIt = this->gridView().ibegin(*eIt);
                    IntersectionIterator isItEnd = this->gridView().iend(*eIt);
                    for(; isIt != isItEnd; ++isIt)
                    {

                        if(!isIt->conforming())
                            conform[eIdx] = 0;
                        if(isIt->boundary())
                        {
                            int boundaryId = getBoundaryId(*isIt);
                            if(boundaryId == rechargeBoundary)
                                (*topBoundaryId)[eIdx] = 1;
                            else if(boundaryId == lateralBoundary)
                                (*lateralBoundaryId)[eIdx] = 1;
                            else if(boundaryId == saltBoundary)
                                (*saltBoundaryId)[eIdx] = 1;
                            else if (boundaryId == outerDomainBoundary)
                                (*outerDomainBoundaryId)[eIdx] = 1;
                            else if (boundaryId == outerDomainLateralBoundary)
                                (*outerDomainLateralBoundaryId)[eIdx] = 1;
                            else
                                DUNE_THROW(Dune::NotImplemented, "No or invalid boundary id for boundary intersection!");
                        }
                    }
                }
            }
        }

        //divide the fracture porosity by the number of scv per cv
        for (int dofIdx = 0; dofIdx < numDofs; dofIdx++)
        {
            if((*fracScvNum)[dofIdx] > 0)
                (*fracturePorosity)[dofIdx] /= (*fracScvNum)[dofIdx];
        }

        //pass the scalar fields to the vtkwriter
        this->resultWriter().attachDofData(*Kxx, "Kxx", false); //element data
        this->resultWriter().attachDofData(*cellPorosity, "cellwisePorosity", false); //element data
        this->resultWriter().attachDofData(*layerIdx, "layer_index", false); //element data
        this->resultWriter().attachDofData(*boxVolume, "bV", isBox);
        this->resultWriter().attachDofData(*cellCenter, "cellCenter", false, dimWorld); //element data
        this->resultWriter().attachDofData(*elementVolume, "elementVolume", false);
        this->resultWriter().attachDofData(*fluxLayerBase, "fluxLayerBase", false);
        this->resultWriter().attachDofData(*fluxLayer, "fluxLayer", false);
        this->resultWriter().attachDofData(*fluxLine, "fluxLine", false);
        this->resultWriter().attachDofData(*deltaPw, "deltaPw", isBox);
        this->resultWriter().attachDofData(*deltaXlNaCl, "deltaXlNaCl", isBox);
        this->resultWriter().attachDofData(*temperature, "T", isBox);
        this->resultWriter().attachDofData(*river, "river", isBox);
        this->resultWriter().attachDofData(*pwInstatIdx, "pwInstatIdx", isBox);
        this->resultWriter().attachDofData(*trInstatIdx, "trInstatIdx", isBox);
        this->resultWriter().attachDofData(*domainType, "DomainType", false);

        //fracture
        this->resultWriter().attachDofData(*fracturePorosity, "FracturePorosity", isBox);
        this->resultWriter().attachDofData(*fractureVertex, "FractureVertex", isBox);

        if(debugOutput_)
        {
            this->resultWriter().attachDofData(*rowIdx, "RowIndex", false);
            this->resultWriter().attachDofData(*topBoundaryId, "topBoundary", false);
            this->resultWriter().attachDofData(*lateralBoundaryId, "lateralBoundary", false);
            this->resultWriter().attachDofData(*saltBoundaryId, "saltBoundary", false);
            this->resultWriter().attachDofData(*outerDomainBoundaryId, "outerDomainBoundary", false);
            this->resultWriter().attachDofData(*outerDomainLateralBoundaryId, "outerDomainLateralBoundary", false);
            this->resultWriter().attachDofData(*conform, "conform", false);
            this->resultWriter().attachDofData(*boundaryType, "BoundaryType", isBox);
            this->resultWriter().attachDofData(*fractureVolume, "FractureVolume", isBox);
            this->resultWriter().attachDofData(*matrixVolume, "MatrixVolume", isBox);
            this->resultWriter().attachDofData(*fractureArea, "FractureArea", isBox);
            this->resultWriter().attachDofData(*fracScvNum, "fracScvNum", isBox);

        }
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns true if the current solution should be written to
     *        disk (i.e. as a VTK file)
     *
     * The default behaviour is to write out every the solution for
     * very time step. This file is intented to be overwritten by the
     * implementation.
     */
    bool shouldWriteOutput() const
    {
        if(this->timeManager().episodeWillBeOver() && episodeOutput_)
            return true;
        else if(this->timeManager().timeStepIndex() % timeStepOutput_ == 0)
            return true;
        else
        {
            //If the time is lesser than zero, we are dealing with the initial solution
            return  Dune::FloatCmp::lt<Scalar>(this->timeManager().time(), 0.) ||
                    this->timeManager().willBeFinished();
        }
    }

    /*!
     * \brief Returns true if a restart file should be written to
     *        disk.
     *
     * The default behaviour is to write one restart file every 5 time
     * steps. This file is intented to be overwritten by the
     * implementation.
     */
    bool shouldWriteRestartFile() const
    {
        return (this->timeManager().episodeIsOver()) || this->timeManager().willBeFinished()
                || (this->timeManager().timeStepIndex() % timeStepRestart_ == 0);
    }

    /*!
     * \brief Called directly after the time integration.
     */
    void postTimeStep()
    {
        // Calculate storage terms
        PrimaryVariables storage;
        this->model().globalStorage(storage);
        double time = this->timeManager().time();
        double dt = this->timeManager().timeStepSize();

        Scalar pInjection(0), pM1(0), pM2(0);
        //Calculate the flux
        ElementIterator eIt = this->gridView().template begin<0>();
        ElementIterator elemEndIt = this->gridView().template end<0>();
        FluxVector fluxAcrossFault(0);
        FluxVector fluxIntoTer(0);
        FluxVector fluxAcrossRupel(0);
        FluxVector fluxAcrossHolesRupel(0);
        FluxVector fluxAcrossLateralBou(0);
        FluxVector zero(0);
        std::vector<FluxVector> fluxQuar;
        fluxQuar.resize(calcFlowRowIdx_.size(), zero);

        //Maximum change between old an new time step
        Scalar maxDiffPres = -std::numeric_limits<typename GET_PROP_TYPE(TypeTag,Scalar)>::max(); //initialize with small number
        GlobalPosition maxDiffPresGlobalPos(0);
        Scalar maxDiffSaltMoleFrac = -std::numeric_limits<typename GET_PROP_TYPE(TypeTag,Scalar)>::max(); //initialize with small number
        GlobalPosition maxDiffSaltMoleFracGlobalPos(0);

        //check if element is on lateral boundary
        //loop over boundary intersections
        BoundaryIterator bIsIt(this->gridView());
        BoundaryIterator bIsItEnd(this->gridView(), true);
        for(; bIsIt != bIsItEnd; ++bIsIt)
        {
            if(getBoundaryId(*bIsIt) == lateralBoundary)
            {
                ElementPointer ePtr = bIsIt->inside();
                FluxVector flux(0);
                //Calculate the flux across the boundary intersection
                this->model().calculateFluxAccrossIntersection(flux, *ePtr, *bIsIt);
                fluxAcrossLateralBou += flux;
            }
        }

        //Calculate fluxes over flux line
        for (; eIt != elemEndIt; ++eIt)
        {
            //Calculate maximum change between old and new time step to determine stationarity
            FVElementGeometry fvElemGeom;
            fvElemGeom.update(this->gridView(), *eIt);
            for (int scvIdx = 0; scvIdx < fvElemGeom.numScv; ++scvIdx)
            {
                GlobalPosition globalPos(0);
                int dofIdxGlobal = this->model().dofMapper().subIndex(*eIt, scvIdx, dofCodim);
                if(isBox)
                    globalPos = eIt->geometry().corner(scvIdx);
                else
                    globalPos = eIt->geometry().center();

                Scalar diffPres = std::abs(this->model().curSol()[dofIdxGlobal][pressureIdx] - this->model().prevSol()[dofIdxGlobal][pressureIdx]);
                maxDiffPres = std::max(diffPres, maxDiffPres);
                maxDiffPresGlobalPos = maxDiffPres == diffPres ? globalPos : maxDiffPresGlobalPos;

                Scalar diffSaltMassFrac = std::abs(this->model().curSol()[dofIdxGlobal][NaClIdx] - this->model().prevSol()[dofIdxGlobal][NaClIdx]);
                maxDiffSaltMoleFrac = std::max(diffSaltMassFrac, maxDiffSaltMoleFrac);
                maxDiffSaltMoleFracGlobalPos = maxDiffSaltMoleFrac == diffSaltMassFrac ? globalPos : maxDiffSaltMoleFracGlobalPos;

                if(globalPosInSphere(globalPos, injCoord_, deltaSphere_))
                    pInjection = this->model().curSol()[dofIdxGlobal][pressureIdx];

                if(globalPosInSphere(globalPos, m1_, deltaSphere_))
                    pM1 = this->model().curSol()[dofIdxGlobal][pressureIdx];

                if(globalPosInSphere(globalPos, m2_, deltaSphere_))
                    pM2 = this->model().curSol()[dofIdxGlobal][pressureIdx];
            }

            //check if element marked as a flux element
            if(this->spatialParams().findFluxLineIdx(*eIt) == SpatialParams::rupelFluxIdx)
            {
                FluxVector flux(0);
                Scalar zNormalComp = -std::numeric_limits<typename GET_PROP_TYPE(TypeTag,Scalar)>::max();
                IntersectionIterator isIt = this->gridView().ibegin(*eIt);
                IntersectionIterator isFluxPtr = isIt;
                IntersectionIterator isEndIt = this->gridView().iend(*eIt);
                for (; isIt != isEndIt; ++isIt)
                {
                    if (!isIt->neighbor())
                        continue;

                    //Use the intersection with the highest positive z value of its normal, i.e. normal pointing in vertical direction
                    zNormalComp = std::max(zNormalComp, isIt->centerUnitOuterNormal()[dimWorld-1]);
                    isFluxPtr = zNormalComp > isIt->centerUnitOuterNormal()[dimWorld-1] ? isFluxPtr : isIt;
                }
                //Calculate the flux across the intersection
                this->model().calculateFluxAccrossIntersection(flux, *eIt, *isFluxPtr);
                //If the flux element is not part of the rupel layer it must be the fault zone
                if(this->spatialParams().findLayerIdx(*eIt) != SpatialParams::rcIdx)
                {
                    fluxAcrossFault += flux;
                }

                //Sum up the flux across the rupel
                if(this->spatialParams().findLayerIdx(*eIt) == SpatialParams::rcIdx)
                {
                    fluxAcrossRupel += flux;
                }
                //only sum the fluxes over holes in the rupel for the complex model
                GlobalPosition globalPos = eIt->geometry().center();
                //Sum up the flux across the holes in the rupel which are situated on a coordinate smaller than 730851.6875 in x direction
#if GEOMETRY == 0
                if(globalPos[0] < dCoorX_ && this->spatialParams().findLayerIdx(*eIt) != SpatialParams::rcIdx)
                {
                    fluxAcrossHolesRupel += flux;
                }
#elif GEOMETRY == 1
                if(globalPos[0] > dCoorX_ && this->spatialParams().findLayerIdx(*eIt) != SpatialParams::rcIdx)
                {
                    fluxAcrossHolesRupel += flux;
                }
#else
#endif

                //make the flux specific to the surface kg/s/m2
                fluxIntoTer += flux;
                flux /= isFluxPtr->geometry().volume();
                fluxLayer_[this->elementMapper().index(*eIt)] = flux[0];
            }

#if GEOMETRY != 2

            //Calculate flow into quar 1 and 2
            for(int rowIter = 0; rowIter < calcFlowRowIdx_.size(); rowIter++)
            {
                if(this->spatialParams().findElementRowIdx(*eIt) == calcFlowRowIdx_[rowIter])
                {
                    FluxVector flux(0);
                    IntersectionIterator isIt = this->gridView().ibegin(*eIt);
                    IntersectionIterator isEndIt = this->gridView().iend(*eIt);
                    for (; isIt != isEndIt; ++isIt)
                    {
                        if (!isIt->neighbor())
                            continue;

                        //Check if the outside element has a lower row index in which case it is situataed above. If so add to flux
                        ElementPointer outsideEPtr(isIt->outside());
                        if(this->spatialParams().findElementRowIdx(*outsideEPtr) == calcFlowRowIdx_[rowIter] - 1)
                        {
                            //Calculate the flux across the intersection
                            this->model().calculateFluxAccrossIntersection(flux, *eIt, *isIt);
                            fluxQuar[rowIter] -= flux;
                            flux = 0.0;
                        }
                    }
                }
            }
#endif
        }

        //subtract the flux accross the holes in the Rupel from flux accross fault
        //so that we get only the flux across the fault
        fluxAcrossFault -= fluxAcrossHolesRupel;

        if (this->gridView().comm().size() > 1)
        {
            //sum the flux and storage over processes if parallel
            fluxAcrossFault = this->gridView().comm().sum(fluxAcrossFault);
            fluxAcrossRupel = this->gridView().comm().sum(fluxAcrossRupel);
            fluxAcrossHolesRupel = this->gridView().comm().sum(fluxAcrossHolesRupel);
            fluxIntoTer = this->gridView().comm().sum(fluxIntoTer);
            fluxAcrossLateralBou = this->gridView().comm().sum(fluxAcrossLateralBou);
            fluxQuar[0] = this->gridView().comm().sum(fluxQuar[0]);
            fluxQuar[1] = this->gridView().comm().sum(fluxQuar[1]);
            maxDiffPres = this->gridView().comm().max(maxDiffPres);
            maxDiffSaltMoleFrac = this->gridView().comm().max(maxDiffSaltMoleFrac);
            pInjection = this->gridView().comm().max(pInjection);
            pM1 = this->gridView().comm().max(pM1);
            pM2 = this->gridView().comm().max(pM2);
        }

        if (this->gridView().comm().rank() == 0)
        {
            //Write output
            // store default precision of cout
            std::streamsize ss = std::cout.precision();
            //set the stream to full double precision
            std::cout.precision( 16 );

            std::cout<<"Time [s]: "<<time+dt <<";"<<std::endl;
            std::cout<<"Time step size [s]: "<< dt <<";"<<std::endl;
            std::cout<<"Storage PV "<<contiWEqIdx<<" [mol Brine]: "<< storage[contiWEqIdx] <<";"<<std::endl;
#if MODELTYPE == 2
            std::cout<<"Storage PV "<<contiNEqIdx<<" [mol CO2]: "<< storage[contiNEqIdx] <<";"<<std::endl;
#endif
            std::cout<<"Storage PV"<<NaClEqIdx<<" [mol NaCl]: "<< storage[NaClEqIdx] <<";"<<std::endl;
            //Write the maximum change between old an new time step
            std::cout<<"Maximum difference between last and current time step PV"<<pressureIdx<<" [Pa]: "<<maxDiffPres<<";"<<std::endl;
            std::cout<<"Located at PV"<<pressureIdx<<": "<<maxDiffPresGlobalPos<<";"<<std::endl;
            std::cout<<"Maximum difference between last and current time step PV"<<NaClIdx<<" [-]: "<<maxDiffSaltMoleFrac<<";"<<std::endl;
            std::cout<<"Located at PV"<<NaClIdx<<": "<<maxDiffSaltMoleFracGlobalPos<<";"<<std::endl;

            //Write the pressure at the measurement points
            std::cout<<"Injection Pw [Pa]: "<<pInjection<<";"<<std::endl;
            std::cout<<"Measurement Point 1 Pw [Pa]: "<<pM1<<";"<<std::endl;
            std::cout<<"Measurement Point 2 Pw [Pa]: "<<pM2<<";"<<std::endl;

            //Write the flux
            std::cout<<"Total mass flow around salt wall [kg/s]: "<<fluxAcrossFault[contiWEqIdx]<<";"<<std::endl;
            std::cout<<"Salt flow around salt wall [kgNaCl/s]: "<<fluxAcrossFault[NaClEqIdx]<<";"<<std::endl;
            std::cout<<"Volume flow around salt wall [m3/s]: "<<fluxAcrossFault[numEq]<<";"<<std::endl;
            std::cout<<"Total mass flow across Rupel [kg/s]: "<<fluxAcrossRupel[contiWEqIdx]<<";"<<std::endl;
            std::cout<<"Salt flow across Rupel [kgNaCl/s]: "<<fluxAcrossRupel[NaClEqIdx]<<";"<<std::endl;
            std::cout<<"Volume flow across Rupel [m3/s]: "<<fluxAcrossRupel[numEq]<<";"<<std::endl;
            std::cout<<"Total mass flow across windows Rupel [kg/s]: "<<fluxAcrossHolesRupel[contiWEqIdx]<<";"<<std::endl;
            std::cout<<"Salt flow across windows Rupel [kgNaCl/s]: "<<fluxAcrossHolesRupel[NaClEqIdx]<<";"<<std::endl;
            std::cout<<"Volume flow across windows Rupel [m3/s]: "<<fluxAcrossHolesRupel[numEq]<<";"<<std::endl;
            std::cout<<"Total mass flow into target aquifers [kg/s]: "<<fluxIntoTer[contiWEqIdx]<<";"<<std::endl;
            std::cout<<"Salt flow into target aquifers [kgNaCl/s]: "<<fluxIntoTer[NaClEqIdx]<<";"<<std::endl;
            std::cout<<"Volume flow into target aquifers [m3/s]: "<<fluxIntoTer[numEq]<<";"<<std::endl;
            std::cout<<"Total mass flow across lateral boundary [kg/s]: "<<fluxAcrossLateralBou[contiWEqIdx]<<";"<<std::endl;
            std::cout<<"Salt flow across lateral boundary [kgNaCl/s]: "<<fluxAcrossLateralBou[NaClEqIdx]<<";"<<std::endl;
            std::cout<<"Volume flow across lateral boundary [m3/s]: "<<fluxAcrossLateralBou[numEq]<<";"<<std::endl;
            std::cout<<"Total mass flow into Quar2 [kg/s]: "<<fluxQuar[1][contiWEqIdx]<<";"<<std::endl;
            std::cout<<"Salt flow into Quar2 [kgNaCl/s]: "<<fluxQuar[1][NaClEqIdx]<<";"<<std::endl;
            std::cout<<"Volume flow into Quar2 [m3/s]: "<<fluxQuar[1][numEq]<<";"<<std::endl;
            std::cout<<"Total mass flow into Quar1 [kg/s]: "<<fluxQuar[0][contiWEqIdx]<<";"<<std::endl;
            std::cout<<"Salt flow into Quar1 [kgNaCl/s]: "<<fluxQuar[0][NaClEqIdx]<<";"<<std::endl;
            std::cout<<"Volume flow into Quar1 [m3/s]: "<<fluxQuar[0][numEq]<<";"<<std::endl;

            //set precision of cout to default again
            std::cout.precision (ss);
        }

        //Mark the solution as stationary
        stationary_ = false;
        if(maxDiffPres < statDiffPress_ && maxDiffSaltMoleFrac < statDiffSaltMoleFrac_)
            stationary_ = true;
    }

    /*!
     * \brief Called directly before the time integration.
     */

    void preTimeStep()
    {
        //Set the end time always to max
        this->timeManager().setEndTime(std::numeric_limits<typename GET_PROP_TYPE(TypeTag,Scalar)>::max());

        //for the first time step or restart write the initial solution to the initialPVMap_
        //this is only meaningful for restart if we are before or have just completed the
        //initialization period
        if(initializePVMap_)
        {

            //Assign the natural flux accross the flux layer after the initialization period
            fluxLayerBase_ = fluxLayer_;

            //For determining the volume equivalent injection rate
            Scalar injDensityCO2(1);
            Scalar injDensityBrine(1);
            GlobalPosition globalPos(0);
            FVElementGeometry fvGeometry;
            ElementIterator eIt = this->gridView().template begin<0>();
            ElementIterator elemEndIt = this->gridView().template end<0>();

            for (; eIt != elemEndIt; ++eIt)
            {
                FVElementGeometry fvGeometry;
                fvGeometry.update(this->gridView(), *eIt);

                for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                {
                    if(GET_PROP_VALUE(TypeTag, ImplicitIsBox))
                    {
                        globalPos = eIt->geometry().corner(scvIdx);
                    }

                    else
                    {
                        globalPos = eIt->geometry().center();
                    }

                    int dofIdxGlobal = this->model().dofMapper().subIndex(*eIt, scvIdx, dofCodim);
                    //store the pressure after initialization for each dof, this is needed for the boundary conditions (lateral boundaries)
                    //and the porosity calculation (reference pressure)
                    initialPVMap_[dofIdxGlobal] = this->model().curSol()[dofIdxGlobal];

                    //Determine the brine and CO2 density at the injection dof
                    if(eIt->partitionType() == Dune::InteriorEntity && useCO2VolumeEquivalentInjRate_)
                    {
                        FVElementGeometry fvGeometry;
                        fvGeometry.update(this->gridView(), *eIt);
                        VolumeVariables volVars;
                        volVars.update(initialPVMap_[dofIdxGlobal],
                                *this,
                                *eIt,
                                fvGeometry,
                                scvIdx,
                                false /* oldSol? */);

                        if(globalPosInSphere(globalPos, injCoord_, deltaSphere_))
                        {
                            injDensityCO2 = CO2::gasDensity(volVars.temperature(), volVars.fluidState().pressure(wPhaseIdx));
                            injDensityBrine = volVars.fluidState().density(wPhaseIdx);
                        }
                    }
                }
            }
            //adjust CO2 injection rate to brine
            if(useCO2VolumeEquivalentInjRate_)
            {
#if MODELTYPE == 1
                injectionRate_ =  GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InjectionRate);
                injectionRate_ *= injDensityBrine/injDensityCO2;
#endif
                std::cout<<"Volume equivalent brine injection rate = "<<injectionRate_<<std::endl;
                std::cout<<"CO2 density = "<<injDensityCO2<<std::endl;
                std::cout<<"Brine density = "<<injDensityBrine<<std::endl;
            }
            //Set the flag to false again after initializing
            initializePVMap_ = false;
        }

        //Set the episode specific max time step sizes
        if(this->timeManager().episodeIndex() > initSaltEpsIdx)
        {
            if(this->timeManager().episodeIndex() == injectionEpsIdx)
            {
                if(this->timeManager().timeStepSize() > injectionMaxDT_)
                    this->timeManager().setTimeStepSize(injectionMaxDT_);
            }
            else
            {
                if(this->timeManager().timeStepSize() > postInjectionMaxDT_)
                    this->timeManager().setTimeStepSize(postInjectionMaxDT_);
            }
        }
    }

    /*!
     * \brief Called after post time step, to check if the epidose should
     * be terminated
     *
     * If the problem is stationary, terminate the episode, if we are in
     * the initialization period
     */
    bool episodeIsOver()
    {
        if(this->timeManager().episodeIndex() < injectionEpsIdx)
            return stationary_;

        return false;
    }

    void episodeEnd()
    {
        // Start the salt initialization period, again set the episode length to a very large value
        // or finish the simulation if initializationPressureOnly_ is true
        if(this->timeManager().episodeIndex() < initSaltEpsIdx)
        {
            if(initializationPressureOnly_)
            {
                this->timeManager().setFinished(true);
                if (this->gridView().comm().rank() == 0)
                {
                    std::cout<<"Writing result to dgf."<<std::endl;
                }
                writeResults2DGF_();
                if (this->gridView().comm().rank() == 0)
                {
                    std::cout<<"Initialization took: "<<this->timeManager().time()<<std::endl;
                    std::cout<<"Simulation terminates now. initializationPressureOnly_ = "
                            <<initializationPressureOnly_<<std::endl;
                }
            }
            else
            {
                this->timeManager().startNextEpisode(saltEpsLength_);
                this->timeManager().setTimeStepSize(saltInitDT_);
                initializePVMap_ = true;
                if (this->gridView().comm().rank() == 0)
                {
                    std::cout<<"Episode index is set to: "<<this->timeManager().episodeIndex()<<std::endl;
                }
            }
        }
        // Start the injection period after the initialization periods
        else if(this->timeManager().episodeIndex() == initSaltEpsIdx)
        {
            if(initializationOnly_)
            {
                this->timeManager().setFinished(true);
                std::cout<<"Writing result to dgf."<<std::endl;
                writeResults2DGF_();
                if (this->gridView().comm().rank() == 0)
                {
                    std::cout<<"Initialization took: "<<this->timeManager().time()<<std::endl;
                    std::cout<<"Simulation terminates now. initializationOnly_ = "
                            <<initializationOnly_<<std::endl;
                }
            }
            else
            {
                this->timeManager().startNextEpisode(injectionEpsLength_);
                this->timeManager().setTimeStepSize(injectionInitDT_);
                initializePVMap_ = true;
                if (this->gridView().comm().rank() == 0)
                {
                    std::cout<<"Assigning Dirichlet boundary conditions for side boundaries "
                            "from the solution after the initialization period"<<std::endl;
                    std::cout<<"Episode index is set to: "<<this->timeManager().episodeIndex()<<std::endl;
                    std::cout<<"Initialization took: "<<this->timeManager().time()<<";"<<std::endl;
                    std::cout<<"Injection starts now."<<std::endl;
                }
            }
        }
        // Start post injection episode for the rest of the time with the length of tEnd - current time
        else if(this->timeManager().episodeIndex() == injectionEpsIdx)
        {
            this->timeManager().startNextEpisode(postInjectionEpsLength_);
            this->timeManager().setTimeStepSize(postInjectionInitDT_);
            if (this->gridView().comm().rank() == 0)
            {
                std::cout<<"Episode index is set to: "<<this->timeManager().episodeIndex()<<std::endl;
                std::cout<<"Injection has stopped."<<std::endl;
            }
        }
        // Terminate the simulation after the post injection episode
        else
        {
            this->timeManager().startNextEpisode();
            if (this->gridView().comm().rank() == 0)
            {
                std::cout<<"Post injection period is over."<<std::endl;
            }
            this->timeManager().setFinished(true);
        }
    }


    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    {
        return name_;
    }

#if !NONISOTHERMAL
    /*!
     * \brief Returns the temperature within the domain.
     *
     *
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        Scalar temperature;
        //Set temperature using either the geothermal gradient or the temperature at the CO2 injection point
        if(applyGeothermalGradient_)
            temperature = 273.15 + 8. + 0.03*(globalPos[dimWorld-1]);
        else
            temperature = 273.15 + 8. + 0.03*(injCoord_[dimWorld-1]);

        if(temperature < 273.15 + 8.)
            temperature = 273.15 + 8.;

        return temperature;
    }
#endif

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param values The source and sink values for the conservation equations in units of
     *                 \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scvIdx The local subcontrolvolume index
     * \param elemVolVars All volume variables for the element
     *
     * For this method, the \a values parameter stores the rate mass
     * generated or annihilate per volume unit. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void solDependentSource(PrimaryVariables &values,
            const Element &element,
            const FVElementGeometry &fvGeometry,
            const int scvIdx,
            const ElementVolumeVariables &elemVolVars) const
    {
        values = 0;

        // injection source term
        if(this->timeManager().episodeIndex() == injectionEpsIdx)
        {
            GlobalPosition globalPos(0);
            if(isBox)
            {
                globalPos = element.geometry().corner(scvIdx);
                //                    globalIdx = this->vertexMapper().map(element, scvIdx, dofCodim);
            }
            else
            {
                globalPos = element.geometry().center();
                //                    globalIdx = this->elementMapper().map(element, scvIdx, dofCodim);
            }

            if(globalPosInSphere(globalPos, injCoord_, deltaSphere_))
            {
                //Injection rate in mol/s/m^3
#if MODELTYPE == 2
                values[contiWEqIdx] = 0.0;
                values[contiNEqIdx] = injectionRate_/injVolume_/FluidSystem::molarMass(CO2Idx);
                values[NaClEqIdx] = 0.0;
#else
                values[contiWEqIdx] = injectionRate_
                        /elemVolVars[scvIdx].fluidState().averageMolarMass(wPhaseIdx)
                        /injVolume_;
                values[NaClEqIdx] = injectionRate_
                        /elemVolVars[scvIdx].fluidState().averageMolarMass(wPhaseIdx)
                        *elemVolVars[scvIdx].fluidState().moleFraction(wPhaseIdx, NaClIdx)
                        /injVolume_;
#endif
            }
        }
#if MODELTYPE == 1
        // set the salt source
        int dofIdxGlobal = this->model().dofMapper().subIndex(element, scvIdx, dofCodim);
        //Set the salt concentration constant during the initialization, in order to obtain the
        //right pressures for the initial concentration
        if(this->timeManager().episodeIndex() == initPressureEpsIdx || !enableSaltTransport_)
        {
            //Obtain initial values from initialPVMap_ (faster) rather then initial(...)
            values[NaClEqIdx] = largeNumber_*(elemVolVars[scvIdx].fluidState().moleFraction(wPhaseIdx, NaClIdx)
                    - initialPVMap_.find(dofIdxGlobal)->second[NaClIdx]);
        }
        else
        {
            int vIdxglobal = this->vertexMapper().subIndex(element, scvIdx, dofCodim);
            bool saltVertex = this->spatialParams().saltVertex(vIdxglobal);
            if(saltVertex)
            {
                values[NaClEqIdx] = largeNumber_*(elemVolVars[scvIdx].fluidState().moleFraction(wPhaseIdx, NaClIdx)
                        - initialPVMap_.find(dofIdxGlobal)->second[NaClIdx]);
            }
        }
#endif
    }

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param vertex The vertex on the boundary for which the
     *               conditions needs to be specified
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        int vIdxGlobal = this->vertexMapper().index(vertex);
        int boundaryType = boundaryTypesVertex_.find(vIdxGlobal)->second;

        if(boundaryType == riverBoundary)
            values.setAllDirichlet();
#if GEOMETRY!=2
        else if(boundaryType == rechargeBoundary
                && GET_RUNTIME_PARAM(TypeTag, bool, Problem.DirichletAtTop))
#else
        else if(boundaryType == rechargeBoundary
                && (GET_RUNTIME_PARAM(TypeTag, bool, Problem.DirichletAtTop) || this->timeManager().episodeIndex() <= 2))
#endif
            values.setAllDirichlet();
        //Lateral outer domain boundaries Dirichlet only after initialization
        else if (this->timeManager().episodeIndex() >= injectionEpsIdx
                && boundaryType == outerDomainLateralBoundary)
            values.setAllDirichlet();
        //Lateral boundaries Dirichlet only after initialization and if not closed
        else if(this->timeManager().episodeIndex() >= injectionEpsIdx
                && boundaryType == lateralBoundary && !closed_)
            values.setAllDirichlet();
        //Constant salt concentration at salt boundary (bottom)
        else if(boundaryType == saltBoundary)
            values.setDirichlet(NaClIdx);
        //All other boundarie no flow or recharge
        else
            values.setAllNeumann();
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex representing the "half volume on the boundary"
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values,
            const Vertex &vertex) const
    {
        values = 0.0;
        int vIdxGlobal = this->model().dofMapper().map(vertex);
        //specify pressure for top of domain
        values[pressureIdx] = initialPVMap_.find(vIdxGlobal)->second[pressureIdx];
        values[NaClIdx] = initialPVMap_.find(vIdxGlobal)->second[NaClIdx];
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * This is the method for the case where the Neumann condition is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param values The neumann values for the conservation equations in units of
     *                 \f$ [ \textnormal{unit of conserved quantity} / (m^2 \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param intersection The intersection between element and boundary
     * \param scvIdx The local subcontrolvolume index
     * \param boundaryFaceIdx The index of the boundary face
     * \param elemVolVars All volume variables for the element
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void solDependentNeumann(PrimaryVariables &values,
            const Element &element,
            const FVElementGeometry &fvGeometry,
            const Intersection &is,
            const int scvIdx,
            const int boundaryFaceIdx,
            const ElementVolumeVariables &elemVolVars) const
    {
        values = 0.0;
        const GlobalPosition globalPos = fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;
        int vIdxGlobal = this->vertexMapper().subIndex(element, scvIdx, dofCodim);
        int boundaryType = boundaryTypesVertex_.find(vIdxGlobal)->second;

        //Top boundary condition: Recharge
        if(boundaryType == rechargeBoundary)
        {
            values[pressureIdx] = -gWRegenerationRate_/FluidSystem::molarMass(H2OIdx); //[mol/sqm/s]

            //For the 1p1c case the groundwater regernation rate should have the same constant salinity as the whole system
            if(!enableSaltTransport_)
            {
                values[pressureIdx] = -gWRegenerationRate_/elemVolVars[scvIdx].fluidState().averageMolarMass(wPhaseIdx); //[mol/sqm/s]
                values[NaClIdx] = -gWRegenerationRate_
                        /elemVolVars[scvIdx].fluidState().averageMolarMass(wPhaseIdx)
                        *elemVolVars[scvIdx].fluidState().moleFraction(wPhaseIdx, NaClIdx);
            }
        }

#if(GEOMETRY == 2) //two layer system have no flow boundaries for xmin and ymin
        if(globalPos[0] < this->bBoxMin()[0] + eps_ || globalPos[1] < this->bBoxMin()[1] + eps_)
            values = 0;
#endif
        //Set Dirichlet conditions at the top during the initialization for the generic models
#if(GEOMETRY != 0)
        if(this->timeManager().episodeIndex() < injectionEpsIdx
                && boundaryType == rechargeBoundary)
        {
            int dofIdxGlobal = this->model().dofMapper().subIndex(element, scvIdx, dofCodim);
            values[pressureIdx] = elemVolVars[scvIdx].fluidState().pressure(wPhaseIdx) - initialPVMap_.find(dofIdxGlobal)->second[pressureIdx]; // [Pa]
        }
#endif

    }
    // \}


    void initial(PrimaryVariables &values,
            const Element &element,
            const FVElementGeometry &fvElemGeom,
            int scvIdx) const
    {
        values = 0.0;
        //Initialize the solution vector from the dgf.
        if(readFromDGFPressureInitialization_)
        {
            const VertexPointer vPtr = element.template subEntity<dim>(scvIdx);
            try
            {
                std::vector<Scalar> entityParam = GridCreator::template parameters<Vertex>(*vPtr);
                //initialization for 1p2c or 2p3c model
                if(GridCreator::template nofParameters<Vertex>(*vPtr) == numEq)
                {
                    for(int pvIdx = 0; pvIdx < numEq; ++pvIdx)
                    {
                        values[pvIdx] = entityParam[pvIdx];
                    }
                }
#if MODELTYPE == 2
                //This case is only relevant for the 2p3c model which is initialized from a 1p2c model
                else if (GridCreator::template nofParameters<Vertex>(*vPtr) == numEq -1)
                {
                    for(int pvIdx = 0; pvIdx < numEq; ++pvIdx)
                    {
                        if(pvIdx == contiWEqIdx)
                            values[pvIdx] = entityParam[pvIdx];
                        else if(pvIdx == contiNEqIdx)
                            values[pvIdx] = 0.0;
                        else if(pvIdx == NaClEqIdx)
                            values[pvIdx] = entityParam[pvIdx-1];
                        else
                            DUNE_THROW(Dune::NotImplemented, "Wrong pvIdx!");
                    }
                }
#endif
                else
                    DUNE_THROW(Dune::NotImplemented, "No parameters in dgf-file!");
            }
            catch (Dune::NotImplemented &e) {
                std::cerr << e << ". Abort!\n";
                exit(1) ;
            }
            catch (...) {
                std::cerr << "Unknown exception thrown while initializing the solution vector from the given dgf!\n";
                exit(1);
            }
            return;
        }

        GlobalPosition globalPos;
        globalPos = 0;

        //Where does the primary variable live?
        if(isBox)
            globalPos = element.geometry().corner(scvIdx);
        else
            globalPos = element.geometry().center();

        // Set the initial pressure
        values[pressureIdx] = pressureTOR_ + 1000*this->gravity()[dimWorld-1]*(std::abs(this->bBoxMin()[dimWorld-1]) + globalPos[dimWorld-1]);

#if GEOMETRY == 0
        //Set the top boundary to atmosperic pressure for the complex model
        if(dofOnTopBoundary_(element, scvIdx))
            values[pressureIdx] = pressureTOR_;
#endif

#if GEOMETRY != 2
        //Set the initial salt concentration
        int vIdxGlobal = this->vertexMapper().subIndex(element, scvIdx, dofCodim);
        bool saltVertex = this->spatialParams().saltVertex(vIdxGlobal);
        //Check if ter or quar vertices should be initialized
        if(this->spatialParams().terQuarVertex(vIdxGlobal) && !initSaltTerQuar_ && !saltVertex)
            values[NaClEqIdx] = xlNaClmin_;
        else
        {
            values[NaClIdx] = (globalPos[dimWorld-1] - depthSalt_)*xlNaClGrad_;
            //Check if the scv is on the salt boundary or if it is a vertex located in the salt dome, if so assign maximum salt mass fraction
            if(dofOnSaltBoundary_(element, scvIdx) || saltVertex)
                values[NaClEqIdx] = xlNaClmax_;

            // Remove negative values and values over xlNaClmax_
            if(values[NaClEqIdx] < 0)
                values[NaClEqIdx] = xlNaClmin_;
            if(values[NaClEqIdx] > xlNaClmax_)
                values[NaClEqIdx] = xlNaClmax_;
        }
#endif
    }

#if MODELTYPE == 2
    /*!
     * \brief Return the initial phase state inside a control volume.
     */
    int initialPhasePresence(const Vertex &vert,
            int &globalIdx,
            const GlobalPosition &globalPos) const
    {
        return wPhaseOnly;
    }
#endif

    /*!
     * \brief Reference to the VertIdxToElemNeighborMapper.
     */
    const VertIdxToElemNeighborMapper &vertexElementMapper() const
    { return vertexElementMapper_; }

    // Overwrite the default gravity function for problem, gravity_ is set in the constructor
    const GlobalPosition& gravity() const
    {
        return gravity_;
    }

    // Check whether point is in the circular recharge region
    bool globalPosInSphere(const GlobalPosition &globalPos,
            const GlobalPosition &sphereCenter, const Scalar sphereRadius) const
    {
        GlobalPosition diffVec = globalPos - sphereCenter;
        Scalar radius = diffVec.two_norm();
        return Dune::FloatCmp::le<Scalar>(radius,sphereRadius);
    }

    int getBoundaryId(const Intersection &is) const
    {
        if(!is.boundary())
            return -1;
        const GlobalPosition normal = is.centerUnitOuterNormal();
        Scalar normalZComp, absNormalZComp;
        normalZComp = normal[dimWorld-1];
        absNormalZComp = std::fabs(normalZComp);
        ElementPointer ePtr = is.inside();

        if(eInOuterDomain(*ePtr))
        {
#if GEOMETRY != 2
            if(Dune::FloatCmp::le<Scalar>(absNormalZComp, 0.2))
                return outerDomainLateralBoundary;
#else
            //Check if we are on a symmetry boundary for the two layer model, if yes it
            // should not be assigned outerDomainLateralBoundary but lateralBoundary later
            if(Dune::FloatCmp::le<Scalar>(absNormalZComp, 0.2))
            {
                GlobalPosition globalPos = is.geometry().center();
                if(Dune::FloatCmp::gt<Scalar>(globalPos[0], this->bBoxMin()[0]) &&
                    Dune::FloatCmp::gt<Scalar>(globalPos[1], this->bBoxMin()[1]))
                    return outerDomainLateralBoundary;
                else
                    return lateralBoundary;
            }
#endif
            return outerDomainBoundary;
        }
        else if(Dune::FloatCmp::le<Scalar>(absNormalZComp, 0.2))
            return lateralBoundary;
        //Top boundary condition, dirichlet if dirichletAtTop_ is true
        else if(Dune::FloatCmp::gt<Scalar>(absNormalZComp, 0.2) && Dune::FloatCmp::lt<Scalar>(normalZComp, 0))
            return rechargeBoundary;
        else
            return saltBoundary;
    }

    bool eInOuterDomain(const int eIdx) const
    {
        return outerDomainE_[eIdx];
    }

    bool eInOuterDomain(const Element element) const
    {
        try{
            auto eParam = GridCreator::template parameters<Element>(element);
            //Check if element is in outer domain
            if(eParam.size()-1 != SpatialParams::outerDomainEIdx)
            {
                return false;
            }
            else
                return eParam[eParam.size()-1];
        }
        catch(...)
        {
            DUNE_THROW(Dune::NotImplemented, "Call to dgf parameters ended in tragedy");
        }
    }

    /*!
     * \brief Whether the interface condition is used.
     */
    bool useInterfaceCondition() const
    {
        return useInterfaceCondition_;
    }
private:


    /*!
      * \brief Check whether an scv lies on an intersection
      *
      * \param vIdxGlobal Global index of the vertex in the element
      * \param is Intersection object
      * \param element Element object which contains the scv and intersection
      */
    bool vIdxGlobalOnIntersection(int vIdxGlobal, const Intersection &is, const Element &element) const
    {
//        const Geometry& geometry = element.geometry();
        Dune::GeometryType geomType = element.geometry().type();
        const ReferenceElement &referenceElement = ReferenceElements::general(geomType);
        int intersectionIdx = is.indexInInside();
        int numVerticesOfIntersection = referenceElement.size(intersectionIdx, 1, dim);
        for (int vIdxInIntersection = 0; vIdxInIntersection < numVerticesOfIntersection; vIdxInIntersection++)
        {
            int vIdxInElement = referenceElement.subEntity(intersectionIdx, 1, vIdxInIntersection, dim);
            int vIdxGlobalInElement = this->vertexMapper().subIndex(element, vIdxInElement, dofCodim);
            if(vIdxGlobalInElement == vIdxGlobal)
                return true;
        }
        return false;
    }

    void writeResults2DGF_() const
    {
        Dumux::DGFWriterParam<TypeTag> dgfWriter(this->gridView(), this->model().curSol());
        if(this->timeManager().episodeIndex() == initPressureEpsIdx)
            dgfWriter.write("initialization_pressure.dgf");
        else if(this->timeManager().episodeIndex() == initSaltEpsIdx)
            dgfWriter.write("initialization_salt.dgf");
        else
            DUNE_THROW(Dune::NotImplemented, "Wrong episode index in function writeResults2DGF_(): "
                    <<this->timeManager().episodeIndex());

        std::cout<<"success!"<<std::endl;
    }

    Scalar calculateMoleFracFromMolality_(Scalar molalityNaCl)
    {

        //First calculate the mass fraction from the molality assuming standard temperature and pressure
        Scalar XlNaCl = molalityNaCl;
        Scalar XlNaClLastIter = 0;
        Scalar T = 293.15; // [K] -> 20°C
        Scalar p = 1e5; // [Pa] -> 1 bar
        Scalar densityW, densityB;

        while(std::abs(XlNaCl - XlNaClLastIter) > eps_)
        {
            XlNaClLastIter = XlNaCl;
            densityW = H2O::liquidDensity(T, p);
            densityB = Brine::liquidDensity(T, p, XlNaCl);
            XlNaCl = molalityNaCl*densityW/densityB;
        }
        //Calculate mole fraction from mass fraction assuming only water and salt as components of the brine phase
        Scalar XlH2O = 1.0 - XlNaCl;
        Scalar molarMassH2O = FluidSystem::molarMass(H2OIdx);
        Scalar molarMassNaCl = FluidSystem::molarMass(NaClIdx);
        Scalar molarMassBrine = molarMassH2O*molarMassNaCl/(XlNaCl*molarMassH2O + XlH2O*molarMassNaCl);
        Scalar xlNaCl = XlNaCl*molarMassBrine/molarMassNaCl;
        return xlNaCl;
    }

    bool dofOnSaltBoundary_(const Element &element, int scvIdx) const
    {
        IntersectionIterator isIt = this->gridView().ibegin(element);
        IntersectionIterator isEndIt = this->gridView().iend(element);
        for (; isIt != isEndIt; ++isIt)
        {
            //Check if intersection is not on salt boundary
            if(getBoundaryId(*isIt) != saltBoundary)
                continue;

            //box
            if(isBox)
            {
                if(this->model().scvIdxIsOnIntersection(scvIdx, *isIt, element))
                    return true;
            }
            //cc
            else
            {
                return true;
            }
        }
        return false;
    }

    bool dofOnTopBoundary_(const Element &element, int scvIdx) const
    {
        IntersectionIterator isIt = this->gridView().ibegin(element);
        IntersectionIterator isEndIt = this->gridView().iend(element);
        for (; isIt != isEndIt; ++isIt)
        {
            //Check if intersection is not on salt boundary
            if(getBoundaryId(*isIt) != rechargeBoundary)
                continue;

            //box
            if(isBox)
            {
                if(this->model().scvIdxIsOnIntersection(scvIdx, *isIt, element))
                    return true;
            }
            //cc
            else
            {
                return true;
            }
        }
        return false;
    }

    // General
    Scalar eps_;
    static constexpr Scalar largeNumber_ = 1e3;
    std::string name_ ;
    bool initializationPressureOnly_;
    bool initializationOnly_;
    bool readFromDGFPressureInitialization_;

    GlobalPosition gravity_;

    // Episode management
    Scalar saltInitDT_;
    Scalar saltEpsLength_;
    Scalar injectionEpsLength_;
    Scalar injectionInitDT_;
    Scalar injectionMaxDT_;
    Scalar postInjectionEpsLength_;
    Scalar postInjectionInitDT_;
    Scalar postInjectionMaxDT_;

    //Output
    bool episodeOutput_;
    int timeStepOutput_;
    int timeStepRestart_;
    bool debugOutput_;

    //stationarity bounds
    Scalar statDiffPress_;
    Scalar statDiffSaltMoleFrac_;
    bool stationary_;

    // FluidSystem initialization
    int nTemperature_;
    int nPressure_;
    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;

    // Coordinate boundaries of the injection well
    GlobalPosition injCoord_;
    Scalar deltaSphere_;

    //Measurement point vectors
    GlobalPosition m1_;
    GlobalPosition m2_;
    Scalar dCoorX_;

    // Total volume of injection cells
    Scalar injVolume_;
    Scalar injectionRate_;

    // Boundary conditions
public:
    //initial primary variables, is updated after initialization
    std::map<int, PrimaryVariables> initialPVMap_;
    bool initializePVMap_;
    // Boundary vertex map specifying boundary conditions for each vertex
    std::map<int, int> boundaryTypesVertex_;
    bool gridHasOuterDomain_;
    std::vector<bool> outerDomainE_;

private:
    Scalar pressureTOR_;
    Scalar topOfReservoir_;
    Scalar gWRegenerationRate_;
    bool closed_;
    bool dirichletAtTop_;
    bool useCO2VolumeEquivalentInjRate_;

    //initial conditions
    Scalar massReductionFactor_;
    bool enableSaltTransport_;
    bool initSaltTerQuar_;
    Scalar xlNaClGrad_;
    Scalar depthSalt_;
    Scalar xlNaClmax_;
    Scalar xlNaClmin_;
    Scalar xlNaClInj_;
    bool applyGeothermalGradient_;

    //flux calcultation elements 1 = true, 0 = false
    std::vector<Scalar> fluxLayerBase_;
    std::vector<Scalar> fluxLayer_;
    std::vector<int> calcFlowRowIdx_;

    // Mapper for assigning neighbor elements (value) to global vertex indices
    VertIdxToElemNeighborMapper vertexElementMapper_;
    //only needed for 2pncdfm
    bool useInterfaceCondition_;
};
} //end namespace

#endif
