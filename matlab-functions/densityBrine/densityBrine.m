%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%density calculation of brine according to Batzle and Wang 1992
%
% Input parameter:
% T = temperature in K
% p = pressure in Pa
% S = mass Fraction NaCl
% Output parameter:
% densityBrine_ in kg/m3

function densityBrine_ = densityBrine(T, p, S)

    T_C = T - 273.15;
    pMPa = p/1e6;
    densityW = density_water_batzle(T_C, pMPa);
    densityBrine_ = density_brine_batzle(T_C, pMPa, S, densityW);

end
