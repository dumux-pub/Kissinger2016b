%Calculates the density of water according to Batzle and Wang 1992

function [roh_w] = density_water_batzle(T,p)

roh_w=(1 + 1e-6*(-80*T - 3.3*T^2 + 0.00175*T^3 + 489*p - 2*T*p + 0.016*T^2*p - 1.3e-5*T^3*p - 0.333*p^2 - 0.002*T*p^2))*1000;

end