% Calculates the density of brine according to Batzle and Wang 1992

function [roh_b] = density_brine_batzle_false(T,p,Xsw,roh_w)

roh_b=roh_w + Xsw*(0.668+0.44*Xsw + 1.06e-6*(300*(p)-2400*(p)*Xsw + T*(80-3*T - 3300*Xsw - 13*(p) + 47*(p)*Xsw)))*1000;

end