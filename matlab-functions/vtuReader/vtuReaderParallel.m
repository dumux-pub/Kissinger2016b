% Function to create a mat file from vtu output for non-parallel simulations
% created by Dr. Sergey Oladyshkin
% modified by Lena Walter
% modified by Alex Kissinger
%
% Input Parameters:
% pathToPVD = path to the pvd file
% pathToOutputMatFile = path to '.mat' file
% variableNameList = List with variable strings to be searched in the vtu
% files
%
% Output Parameters:
% success = return 1 if successful else 0


%-------------------------------------------------------------------------------------------
%---------- Reading VTU's
%-------------------------------------------------------------------------------------------
% 

function [success] = vtuReaderParallel(pathToPVD, pathToOutputMatFile, variableNameList)

display(pathToPVD);
display(pathToOutputMatFile);
display(variableNameList);
%display(NP);
% Get the path from pathToPVD
path = fileparts(pathToPVD);
%Check wether the output name has '.mat' appended or not
if strcmp(pathToOutputMatFile(length(pathToOutputMatFile)-3:length(pathToOutputMatFile)) ,'.mat')
    output = pathToOutputMatFile;
else
    output = [pathToOutputMatFile '.mat'];
end

success = 0;
fid = fopen(pathToPVD, 'r');

%initialise number of processes
NP = 1;

if(fid ~= -1)
    
    timeStepIterator=0;
    processIterator=0;
    while feof(fid) == 0
        line = fgetl(fid);
        if (strfind(line,'DataSet')~=0)
            if(strfind(line,'DataSet  part=')~=0) %parallel
                processIterator = processIterator + 1;
                if(strfind(line,'DataSet  part="0"')~=0)
                    timeStepIterator=timeStepIterator+1;
                    processIterator = 1;
                    ref_Time(timeStepIterator)=str2num(line(strfind(line,'timestep="')+length('timestep="'):strfind(line,'" f')-1));
                    ref_SimulationOutputFileNames{processIterator, timeStepIterator}=line(strfind(line,'file="')+length('file="'):strfind(line,'"/>')-1);
                    
                else
                    ref_SimulationOutputFileNames{processIterator, timeStepIterator}=line(strfind(line,'file="')+length('file="'):strfind(line,'"/>')-1);
                    NP = max(processIterator, NP);
                    
                end
                
                % If a path has been specified in pathToPVD add this path to
                % the simulation output file name
                if(path)
                    ref_SimulationOutputFileNames{processIterator, timeStepIterator} = [path,'/',ref_SimulationOutputFileNames{processIterator,timeStepIterator}];
                end
            else %sequential
                timeStepIterator=timeStepIterator+1;
                ref_Time(timeStepIterator)=str2num(line(strfind(line,'timestep="')+length('timestep="'):strfind(line,'" f')-1));
                ref_SimulationOutputFileNames{timeStepIterator}=line(strfind(line,'file="')+length('file="'):strfind(line,'"/>')-1);
                % If a path has been specified in pathToPVD add this path to
                % the simulation output file name
                if(path)
                    ref_SimulationOutputFileNames{timeStepIterator} = [path,'/',ref_SimulationOutputFileNames{timeStepIterator}];
                end
            end
        end
        
    end
    %display(ref_SimulationOutputFileNames);
    display(NP);

else error('error opening: %s', pathToPVD);
end
fclose(fid);

%display(ref_SimulationOutputFileNames);

%--->Reading Number static data
ref_NumberOfPoints = 0;
ref_NumberOfCells = 0;
for processIterator=1:1:NP
    fid = fopen(strcat(ref_SimulationOutputFileNames{processIterator, 1}), 'rt');
    if(fid ~= -1)
        while feof(fid) == 0
            line = fgetl(fid);
            if (strfind(line,'NumberOfPoints=')~=0)
                ref_NumberOfPoints=ref_NumberOfPoints+str2num(line(strfind(line,'NumberOfPoints="')+length('NumberOfPoints="'):strfind(line,'">')-1));
                
            end
            if (strfind(line,'NumberOfCells=')~=0)
                ref_NumberOfCells=ref_NumberOfCells+str2num(line(strfind(line,'NumberOfCells="')+length('NumberOfCells="'):strfind(line,'" N')-1));
            end
            
        end

    else error('error opening:', ref_SimulationOutputFileNames{1});
    end
    fclose(fid);
end


% Create output file and save static variables
save(output, 'ref_Time', 'ref_SimulationOutputFileNames', 'ref_NumberOfCells', 'ref_NumberOfPoints');

%---> Reading from all Output Simulation Files or timespeps
for timeStepIterator=1:1:length(ref_SimulationOutputFileNames)
    
    PartNumberOfPoints = 0; %number of points for the current processor
    SumNumberOfPoints = 0;
    StartNumberOfPoints = 1;
    
    for processIterator=1:1:NP
        fid = fopen(strcat(ref_SimulationOutputFileNames{processIterator, timeStepIterator}), 'rt');
        
        %Check whether file exists
        if(fid ~= -1)
            
            % Loop over all lines until end of vtu file
            while feof(fid) == 0
                
                line = fgetl(fid);
                %for parallel runnings the number of nodes has to sum up
                %from all processors
                if (strfind(line,'NumberOfPoints=')~=0)
                 %   PartNumberOfPoints=str2num(line(strfind(line,'NumberOfPoints="')+length('NumberOfPoints="'):strfind(line,'">')-1));
                    SumNumberOfPoints=SumNumberOfPoints+str2num(line(strfind(line,'NumberOfPoints="')+length('NumberOfPoints="'):strfind(line,'">')-1));
                end
                %display(PartNumberOfPoints);
%                 display(SumNumberOfPoints);
%                 display(StartNumberOfPoints);
                % Loop over all variables in the variableNameList
                for  variableIterator = 1:length(variableNameList)
                    
                    
                    if (strfind(line,['Name="',variableNameList{variableIterator},'"'])~=0)
                        
%                         display(timeStepIterator);
%                         display(processIterator);
%                         display(line);
%                         display(StartNumberOfPoints);
%                         display(SumNumberOfPoints);
                        % check how many components the variable has, i.e.
                        % scalar or vector
                        numComponents = str2num(line(strfind(line,'NumberOfComponents="')+length('NumberOfComponents="') ...
                            :strfind(line,'" f')-1));
                        variableName = ['ref_', variableNameList{variableIterator}];
                        %Check if the parameter is a scalar or a vector and
                        %assign the right formatting for fscanf
                        component = '';
                        for componentIterator=1:numComponents
                            component = [component, '%g '];
                        end
%                         display(numComponents);
                        %remove regexpr '^' in variable variable name
                        variableName = regexprep(variableName,'\^','');
                        % for all time steps except the first get the
                        % variable from the .mat file and append the data
                        % for the new time step. Then update the .mat file
                        if(processIterator > 1 || timeStepIterator > 1)
                            load(output, eval('variableName'));
                            variableNameTemp = 'variableValue';
                            eval(sprintf('%s=%s;',variableNameTemp, eval('variableName')));
                        end
    
                        variableValue(timeStepIterator,1:numComponents,StartNumberOfPoints:SumNumberOfPoints) = fscanf(fid, component, [numComponents (SumNumberOfPoints-StartNumberOfPoints+1)]); 
                        
                        variableNameTemp2 = 'variableValue';
                        eval(sprintf('%s=%s;',variableName,variableNameTemp2));
                        save(output, eval('variableName'), '-append');
                        
                        clear variableValue;
                    end
                end
                if (strfind(line,'Name="Coordinates"')~=0)
                    numComponents = str2num(line(strfind(line,'NumberOfComponents="')+length('NumberOfComponents="') ...
                        :strfind(line,'" f')-1));
                    %Check if the parameter is a scalar or a vector and
                    %assign the right formatting for fscanf
                    component = '';
                    for componentIterator=1:numComponents
                        component = [component, '%g '];
                    end
%                     display(component);
                    ref_Coordinates(timeStepIterator,1:numComponents,StartNumberOfPoints:SumNumberOfPoints)=fscanf(fid, component, [numComponents (SumNumberOfPoints-StartNumberOfPoints+1)]);
                    save(output, 'ref_Coordinates', '-append');
                end
            end
            
            %throw error if vtu file does not exist
        else error('error opening:', ref_SimulationOutputFileNames{timeStepIterator});
            
        end
                        
        
        StartNumberOfPoints=SumNumberOfPoints+1;
        fclose(fid);
        fprintf('> > > >Succesfully read %s \n', ref_SimulationOutputFileNames{processIterator, timeStepIterator});

    end
end

fprintf('> > > >Created %s \n', output);
success = 1;


end







    




