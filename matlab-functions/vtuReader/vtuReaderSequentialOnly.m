% Function to create a mat file from vtu output for non-parallel simulations
% created by Dr. Sergey Oladyshkin
% modified by Lena Walter
% modified by Alex Kissinger
%
% Input Parameters:
% pathToPVD = path to the pvd file
% pathToOutputMatFile = path to '.mat' file
% variableNameList = List with variable strings to be searched in the vtu
% files
%
% Output Parameters:
% success = return 1 if successful else 0


%-------------------------------------------------------------------------------------------
%---------- Reading VTU's
%-------------------------------------------------------------------------------------------
% 

function [success] = vtuReader(pathToPVD, pathToOutputMatFile, variableNameList)

% Get the path from pathToPVD
path = fileparts(pathToPVD);
%Check wether the output name has '.mat' appended or not
if strcmp(pathToOutputMatFile(length(pathToOutputMatFile)-3:length(pathToOutputMatFile)) ,'.mat')
    output = pathToOutputMatFile;
else
    output = [pathToOutputMatFile '.mat'];
end

success = 0;
fid = fopen(pathToPVD, 'r');

if(fid ~= -1)
    
    timeStepIterator=0;
    while feof(fid) == 0
        line = fgetl(fid);
        if (strfind(line,'DataSet')~=0)
            timeStepIterator=timeStepIterator+1;
            ref_Time(timeStepIterator)=str2num(line(strfind(line,'timestep="')+length('timestep="'):strfind(line,'" f')-1));
            ref_SimulationOutputFileNames{timeStepIterator}=line(strfind(line,'file="')+length('file="'):strfind(line,'"/>')-1);
            % If a path has been specified in pathToPVD add this path to
            % the simulation output file name
            if(path)
                ref_SimulationOutputFileNames{timeStepIterator} = [path,'/',ref_SimulationOutputFileNames{timeStepIterator}];
            end
        end
    end
    %display(ref_SimulationOutputFileNames);
    fclose(fid);
else error('error opening: %s', pathToPVD);
end

%--->Reading Number static data
fid = fopen(strcat(ref_SimulationOutputFileNames{1}), 'rt');
if(fid ~= -1)
    while feof(fid) == 0
        line = fgetl(fid);
        if (strfind(line,'NumberOfPoints=')~=0)
            ref_NumberOfPoints=str2num(line(strfind(line,'NumberOfPoints="')+length('NumberOfPoints="'):strfind(line,'">')-1));
            
        end
        if (strfind(line,'NumberOfCells=')~=0)
            ref_NumberOfCells=str2num(line(strfind(line,'NumberOfCells="')+length('NumberOfCells="'):strfind(line,'" N')-1));
        end
        
    end
    fclose(fid);
else error('error opening:', ref_SimulationOutputFileNames{1});
end

% Create output file and save static variables
save(output, 'ref_Time', 'ref_SimulationOutputFileNames', 'ref_NumberOfCells', 'ref_NumberOfPoints');

%---> Reading from all Output Simulation Files or timespeps
for timeStepIterator=1:1:length(ref_SimulationOutputFileNames)
    fid = fopen(strcat(ref_SimulationOutputFileNames{timeStepIterator}), 'rt');
    
    %Check whether file exists
    if(fid ~= -1)

        % Loop over all lines until end of vtu file
        while feof(fid) == 0

            line = fgetl(fid);
            
            % Loop over all variables in the variableNameList
            for  variableIterator = 1:length(variableNameList)
                
                if (strfind(line,['Name="',variableNameList{variableIterator},'"'])~=0)

                    % check how many components the variable has, i.e.
                    % scalar or vector
                    numComponents = str2num(line(strfind(line,'NumberOfComponents="')+length('NumberOfComponents="') ...
                        :strfind(line,'" f')-1));
                       variableName = ['ref_', variableNameList{variableIterator}];
   
                       %remove regexpr '^' in variable variable name
                        variableName = regexprep(variableName,'\^','');
                        % for all time steps except the first get the
                        % variable from the .mat file and append the data
                        % for the new time step. Then update the .mat file
                        if(timeStepIterator > 1)
                            load(output, eval('variableName'));
                            variableNameTemp = 'variableValue';
                            eval(sprintf('%s=%s;',variableNameTemp, eval('variableName')));
                        end

                        variableValue(timeStepIterator,:) = fscanf(fid, '%g', [1 ref_NumberOfPoints*numComponents]);
                        
                        variableNameTemp2 = 'variableValue';
                        eval(sprintf('%s=%s;',variableName,variableNameTemp2));
                        save(output, eval('variableName'), '-append');
                    
                        clear variableValue; 
                end
            end
            if (strfind(line,'Name="Coordinates"')~=0)
                    numComponents = str2num(line(strfind(line,'NumberOfComponents="')+length('NumberOfComponents="') ...
                    :strfind(line,'" f')-1));
                ref_Coordinates(timeStepIterator,:)=fscanf(fid, '%g', [1 ref_NumberOfPoints*numComponents]);
                save(output, 'ref_Coordinates', '-append');
            end     
        end
        
    %throw error if vtu file does not exist
    else error('error opening:', ref_SimulationOutputFileNames{timeStepIterator});
    end
    fprintf('> > > >Succesfully read %s \n', ref_SimulationOutputFileNames{timeStepIterator});
end

fprintf('> > > >Created %s \n', output);
success = 1;


end







    




