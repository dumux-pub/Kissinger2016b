function [ viscosityBrine ] = viscosityBrineKestin( temperature, pressure, salinity )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
T_C = temperature - 273.15;
P_MPA = pressure/1e6;
A1 = 3.324e-2;
A2 = 3.624e-3;
A3 = -1.879e-4;
B1 = -3.96e-2;
B2 = 1.02e-2;
B3 = -7.02e-4;
A = A1*salinity + A2*salinity^2 + A3*salinity^3;
B = B1*salinity + B2*salinity^2 + B3*salinity^3;
C = 1.2378*(20 - T_C) - 1.303e-3*(20-T_C)^2 + 3.06e-6*(20-T_C)^3 + ...
    2.55e-8*(20-T_C)^4/(96 + T_C);
mS = 6.044 + 2.8e-3*T_C + 3.6e-5*T_C^2;
betaW = -1.297 + 5.74e-2*T_C - 6.97e-4*T_C^2 + 4.47e-6*T_C^3 - 1.05e-8*T_C^4;
betaS = 0.545 + 2.8e-3*T_C - betaW;
betaP = 2.5*(salinity/mS) - 2*(salinity/mS)^2 + 0.5*(salinity/mS)^3;

beta = betaS*betaP + betaW;
mu = 10^(A + 3.000867722 + C*(B+1));
viscosityBrine = mu*(1 + beta*P_MPA);

end

