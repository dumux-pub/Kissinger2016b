% Calculates the viscosity of brine according to Batzle and Wang 1992

function [visco_brine] = viscosityBrine(temperature, pressure, salinity)

        if(temperature <= 275.) %regularisation
            %temperature = 275; 
            error('Temperature below 275 Kelvin, brine viscosity calculation failed');
        end
        
        T_C = temperature - 273.15;

        A = (0.42*(salinity^0.8-0.17)^2 + 0.045)*T_C^0.8;
        visco_brine = 0.1 + 0.333*salinity + (1.65+91.9*salinity*salinity*salinity)*exp(-A);

        visco_brine = visco_brine/1000.0; % unit: Pa s 
        
end