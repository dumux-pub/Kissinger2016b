%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Write dgf output files for 2d and 3d for rectilinear grids
% structured grids
% created by Alex Kissinger 2011-2014
% pathToDGF - path for dgf file
% x - vector of radius coordinates
% y - vector of angles
% z - vector of z coordinates
% coordMatrix - (optional) coordinate matrix containing a list of nodes for
% creating deformed rectlinear grids or 2d surface grids in 3d space.
% Dimensions: coordMatrix(nodes, 2d or 3d coordinates) or 
% coordMatrix(2d or 3d coordinates, nodes)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function dgfWriter(pathToDGF, x, y, z, coordMatrix)

if(checkUnique(x) ~= 1 || checkUnique(y) ~= 1)
    error('Input vectors are not unique');
end

if(checkAscending(x) ~= 1 || checkAscending(y) ~= 1)
    error('Input vectors are not ascending');
end

if (length(z) > 1)
    if (checkUnique(z) ~= 1)
        error('Input vectors are not unique');
    end
    if (checkAscending(z) ~= 1)
        error('Input vectors are not ascending');
    end
end


if(nargin == 4 || nargin == 5) %check whether number of input parameters
    %is correct
    fid = fopen(pathToDGF,'w');
    fprintf(fid, 'DGF\n');
    fprintf(fid, 'VERTEX\n');
    
    %write the nodes for the not deformed case
    if(nargin == 4)
        counter = 1;
        nodeNo(1)=0;
        if(length(z) > 1) %3d case
            for l=1:length(z)
                for j=1:length(y)
                    for i=1:length(x)
                        nodeNo(counter) = counter-1;
                        counter = counter+1;
                        dx = x(i);
                        dy = y(j);
                        dz = z(l);
                        fprintf(fid, '%12.15f %12.15f %12.15f \n ', dx,dy,dz);
                    end
                end
            end
        else %2d case
            for j=1:length(y)
                for i=1:length(x)
                    nodeNo(counter) = counter-1;
                    counter = counter+1;
                    dx = x(i);
                    dy = y(j);
                    fprintf(fid, '%12.15f %12.15f \n ', dx,dy);
                end
            end
        end
        
    end
    
    %write the nodes for the deformed case or 2d grids in three dimensions
    if(nargin == 5)
        counter = 1;
        nodeNo(1)=0;
        dim = size(coordMatrix);
        if(min(size(coordMatrix)) == 3)
            for i=1:max(size(coordMatrix))
                nodeNo(counter) = counter-1;
                counter = counter+1;
                if(dim(1) > dim(2)) % Check for the greater dimension of the matrix which should be nodes
                    fprintf(fid, '%12.15f %12.15f %12.15f \n ', ...
                        coordMatrix(i,:));
                else
                    fprintf(fid, '%12.15f %12.15f %12.15f \n ', ...
                        coordMatrix(:,i));
                end
            end
        elseif(min(size(coordMatrix)) == 2)
            for i=1:max(size(coordMatrix))
                nodeNo(counter) = counter-1;
                counter = counter+1;
                if(dim(1) > dim(2)) % Check for the greater dimension of the matrix which should be nodes
                    fprintf(fid, '%12.15f %12.15f \n ', ...
                        coordMatrix(i,:));
                else
                    fprintf(fid, '%12.15f %12.15f \n ', ...
                        coordMatrix(:,i));
                end
            end
        else
            error('Invalid dimensions of coordinate matrix.');
        end
    end
    fprintf(fid, '#\n');
    
    
    % Write which nodes belong to which element
    fprintf(fid, 'CUBE\n');
    if(length(z) > 1) % 3d case
        counter = 0;
        for l=1:length(z) -1
            for j=1:length(y) -1
                for i=1:length(x) -1
                    counter = counter+1;
                    fprintf(fid, '%d %d %d %d %d %d %d %d\n', ...
                        nodeNo(counter), ...
                        nodeNo(counter+1), ...
                        nodeNo(counter+length(x)), ...
                        nodeNo(counter+length(x)+1), ...
                        nodeNo(counter+length(x)*length(y)), ...
                        nodeNo(counter+length(x)*length(y)+1), ...
                        nodeNo(counter+length(x)*length(y)+length(x)), ...
                        nodeNo(counter+length(x)*length(y)+length(x)+1));
                end
                counter=counter+1;
            end
            counter=counter+length(x);
        end
        
    else % 2d case
        counter = 0;
        for j=1:length(y) -1
            for i=1:length(x)-1
                counter = counter+1;
                fprintf(fid, '%d %d %d %d\n', ...
                    nodeNo(counter), ...
                    nodeNo(counter+1), ...
                    nodeNo(counter+length(x)), ...
                    nodeNo(counter+length(x)+1));
            end
            counter=counter+1;
        end
    end
    
    fprintf(fid, '#\n');
    % If a surface grid is used dim=2 and dimWorld=3 do not attach the
    % boundary domain part to the dgf otherwise the grid cannot be parsed.
    if(nargin == 5)
        if(~(min(size(coordMatrix)) == 3 && length(z) == 1))
            fprintf(fid, 'BOUNDARYDOMAIN\n');
            fprintf(fid, 'default 1\n');
            fprintf(fid, '#\n');
        end
    else
        fprintf(fid, 'BOUNDARYDOMAIN\n');
        fprintf(fid, 'default 1\n');
        fprintf(fid, '#\n');
    end
    
else
    error('Wrong argument number for dgfWriterNormal function: %s\n', nargin);
end

end