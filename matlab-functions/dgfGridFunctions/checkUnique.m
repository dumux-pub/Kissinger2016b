%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Check if a vector only has unique values returns 1 if this is the case
%otherwise 0
% created by Alex Kissinger 2011-2014
%vector = input vector to be checked

function [uniqueVar] = checkUnique(vector)

uniqueVector = unique(vector);
if(length(uniqueVector) == length(vector))
    uniqueVar = 1;
else
    uniqueVar = 0;
end
end