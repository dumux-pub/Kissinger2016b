%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function returns a vector with values increasing with a logarithmic
% factor
% created by Alex Kissinger 2011-2014
% dx = return value, vector of coordinates
% noOfElements = approximate number of elements in coordinate direction
% maxCoordinate = maximal value of the coordinate
% minCellSize = smallest cell size
% maxCellSize = largest cell size

function [dx] = logSpacer(noOfElements, maxCoordinate, minCellSize, maxCellSize, startValue)

    if (nargin ~= 5)
        error('Wrong argument number for dgfWriter function: %s\n', nargin);
    end
    length = maxCoordinate - startValue;
    u = (noOfElements)*log10(length-minCellSize)/log10(length-(noOfElements*minCellSize));
    logFactor =  log10(length-minCellSize)/u;
    dx(1) = startValue;
    dx(2) = startValue + minCellSize;
    i = 2;
    while dx(i) < maxCoordinate
        i = i+1;
        dx(i) = startValue + (i-1)*minCellSize + 10^((i-1)*logFactor);
        
        if dx(i)-dx(i-1) > maxCellSize
            dx(i) = dx(i-1) + maxCellSize;    
        end 
    end
    dx(i) = maxCoordinate;
end