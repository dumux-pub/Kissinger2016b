% Calculates the Gr number according to Kopp et al. 2008


function [gr] = grNumberConstVolFlux(densityBrine, densityCO2, Kv, viscoCO2, vCr)

    gr = (densityBrine - densityCO2)* 9.81* Kv/ (viscoCO2* vCr); % [-] 

end