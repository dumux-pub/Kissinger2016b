% Read output files at every time step and write them into a mat file
%
% Input Parameters:
% logFileName = The file name to read
%
% Output Parameters:
% output(timeIter, Value) = The vector containing the data read from logFileName
%                           time iter is the time step index.
%                           Value is:
%                           absolute time (1)
%                           time step size (2)
%                           assemble time (3)
%                           solve time (4)
%                           update time (5)

function [output] = readAssSolUp(logFileName)
        
%get the time for after each time-step
lineIter = 0;
timeIter = 0;
assSolUpIter = 0;
timeStr = 'Time: ';
assSolUpTimeStr = 'Assemble/solve/update time: ';
newtonStepStr = 'Newton iteration ';
numLastIter = 0;

fid = fopen(logFileName, 'r');
while feof(fid) == 0
    lineIter =lineIter+1;
    line = fgetl(fid);
        % find time
        if(strfind(line, timeStr)~=0)
            timeIter = timeIter + 1;
%             str = regexp(line,'(\d+)(\.+)*(\d+)*','match');
            temp = sscanf(line, 'Time: %f; Time step size: %f;');
            output(timeIter, 1) = temp(1);
            output(timeIter, 2) = temp(2);
            clear temp;
        end
        %find assemble/solve/update time
        if(strfind(line, assSolUpTimeStr)~=0)
            assSolUpIter = assSolUpIter + 1;
            temp = sscanf(line, [assSolUpTimeStr '%f(%f%%)/%f(%f%%)/%f(%f%%)']);
            output(assSolUpIter, 3) = temp(1);
            output(assSolUpIter, 4) = temp(3);
            output(assSolUpIter, 5) = temp(5);
            clear temp;
        end
        %find the number of newton steps per time step
        if(strfind(line, newtonStepStr)~=0)
            str = line(strfind(line, newtonStepStr) ...
                +length(newtonStepStr):strfind(line, 'done')-2);
            output(timeIter+1, 6) =  str2double(str);
        end        
end


    
if(output(length(output), 1) == 0)
    output = output(1:length(output)-1, :);
end
fprintf('> > > >Succesfully read %s \n', logFileName);

fclose(fid);


% 
% save(outputMatName, 'output');
% 
% fprintf('> > > >Succesfully wrote %s \n', outputMatName);


end



        
