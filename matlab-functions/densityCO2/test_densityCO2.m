% test the enthalpy CO2 function
clear all;
% Tmin = 283.15; %K
% Tmax = 363.15; %K
% dT = 10; %K
% T = Tmin:dT:Tmax; %K
T = 320.15; %K

Pmin =0; %Pa
Pmax = 2e7; %Pa
dP = 1000; %Pa
P = Pmin:dP:Pmax; %Pa


%for i=1:length(T)
    for j=1:length(P)
        densityCO2_(j) = densityCO2(T, P(j)/1e6); %P in MPa
    end
plot(P, densityCO2_, '-');