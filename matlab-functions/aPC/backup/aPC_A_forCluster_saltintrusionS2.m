% ========================================================================
% Arbitraty Integrative Probabilistic Collocation Method
% Author: Dr. Sergey Oladyshkin
% SRC SimTech,Universitaet Stuttgart, Pfaffenwaldring 61, 70569 Stuttgart
% E-mail: Sergey.Oladyshkin@iws.uni-stuttgart.de
% ========================================================================
% modified by Lena Walter
% changes for parallel runs

clear all;

fprintf('\n');
fprintf('______________________________________________________________________________________________________________\n');
fprintf('______________________________________________________________________________________________________________\n');
fprintf('\n> > > > > Arbitraty Integrative Probabilistic Collocation Method \n');
fprintf('\n> > > > > aPC.: massive stochastic model reduction \n');
fprintf('\n> > > > > Module A: Preparation for parallel Snapshot Simulations\n');
fprintf('\n');
tic


%-------------------------------------------------------------------------
%---------- Initialization -----------------------------------------------
%-------------------------------------------------------------------------


%---> Reading from Parameter file
% InterfaceFileName='saltintrusion.input';
% fprintf('> > > > > aPC: Reading from Parameter File: %s ... \n',InterfaceFileName);
% fid = fopen(InterfaceFileName, 'rt');
% i=0;
% while feof(fid) == 0
%    i=i+1;
%    line = fgetl(fid);
%    InputFileData{i}=strcat(line);      
% end
% fclose(fid);


%---> Reading the Simulation Management Data
%Reading of Input Propertie Keywords List
% for i=1:1:length(InputFileData)-1
%     if (findstr(InputFileData{i},'<InputKeyWordsList>')~=0) i_beg=i+1; end  
%     if (findstr(InputFileData{i},'</InputKeyWordsList>')~=0) i_end=i-1; end  
% end
% ii=0;
% for i=i_beg:1:i_end
%     ii=ii+1;
%     InputKeyWordsList{ii}=strtrim(InputFileData{i});    
% end
%Reading of Names for Time Manager File
% for i=1:1:length(InputFileData)-1
%     if (findstr(InputFileData{i},'<TimeManagerFileName>')~=0) 
%        TimeManagerFileName=strtrim(InputFileData{i+1});    
%     end  
% end




%---------- Arbitratry Polynomial Initialization for Uncertanties and Design Parameters
fprintf('> > > > > P.C.M.: Initialization of Uncertanties and Design Parameters ...\n');
N=2; %Number of Uncertanties and Design Parameters
d=3; %d-order polynomial
P=factorial(N+d)/(factorial(N)*factorial(d)); %Total number of terms

fileNameInput = 'thickn_poro_südostbrandenburg_1.csv';
fid = fopen(fileNameInput, 'r');
raw = textscan(fid, '%d %f %f', 'HeaderLines', 1, 'delimiter', ';');

%Data distribution for Gr principal simulations
 
%Vector reservoir thickness
 MC_Vector(1,:)=raw{2};  % random('logn',mu,sigma,1,1000); %('logn',mean,sigma,m,n) mxn matrix
%Vector porosity
 MC_Vector(2,:)=raw{3}/100;



%Computation of Arbitratry Polynomials 
for i=1:N
    PolynomialBasisFileName(i)=strcat({'PolynomialBasis_'},num2str(i),{'.mat'});    
    aPoly_Construction(MC_Vector(i,:), d, PolynomialBasisFileName{i});
end


%Initialization of Collocation points
for i=1:1:N
    load(PolynomialBasisFileName{i},'Roots')
    Cpoints(i,:)=Roots;
end
% 
% 
%---------- Digital set of Collocation points combination  / Work with N=2 !!!
%DigitalUniqueCombinations=allcomb(1:d+1,1:d+1,1:d+1,1:d+1,1:d+1);
for i=1:N
    m{i}=1:d+1;
end
DigitalUniqueCombinations=allcomb(m{:});% Works with all N
for i=1:1:length(DigitalUniqueCombinations) 
    DigitalPointsWeight(i)=sum(DigitalUniqueCombinations(i,:));                 
end
%Sorting of Posible Digital Points Weight
[SortDigitalPointsWeight, index_SDPW]=sort(DigitalPointsWeight);
SortDigitalUniqueCombinations=DigitalUniqueCombinations(index_SDPW,:);
%Ranking relatively mean
for j=1:N    
    temp(j,:)=abs(Cpoints(j,:)-mean(MC_Vector(j,:)));
end
[temp_sort, index_CP]=sort(temp,2);
for j=1:N    
    SortCpoints(j,:)=Cpoints(j,index_CP(j,:));
end
%Mapping of Digital Combination to Cpoint Combination
for i=1:1:length(SortDigitalUniqueCombinations) 
    for j=1:1:N
        SortUniqueCombinations(i,j)=SortCpoints(j,SortDigitalUniqueCombinations(i,j));
    end
end
CollocationPointsBase=SortUniqueCombinations(1:P,:);

%  for i=1:1:20
%  MatrixA(i,1)=10;
%  end
% CollocationPointsBase1(:,1)=CollocationPointsBase(:,1);
% CollocationPointsBase1(:,2)=CollocationPointsBase(:,2);
% CollocationPointsBase1(:,3)=CollocationPointsBase(:,3);

% 
%----------Choice of polynomial degree combinations
%Choose the combination of polynomials for each snapshot point. This is
%done by settins up all possible combinations of polynomials of the
%different variables. Only unique combinations are considered and these are
%sorted according to the sum of each combinations. The first P combinations
%(ie the ones with the samllest sum) are chosen.
PossibleDegree=0:1:length(Cpoints(1,:))-1;
PosibleDegree=0:1:length(Cpoints(1,:))-1;
for i=2:1:N
    PosibleDegree=[PosibleDegree,0:1:length(Cpoints(i,:))-1];    
end
UniqueDegreeCombinations=unique(nchoosek(PosibleDegree,N),'rows');
%Posible Degree Weight Computation
for i=1:1:length(UniqueDegreeCombinations) 
    DegreeWeight(i)=0;
    for j=1:1:N
        DegreeWeight(i)=DegreeWeight(i)+UniqueDegreeCombinations(i,j);
    end
end
%Sorting of Posible Degree Weight
[SortDegreeWeight, i]=sort(DegreeWeight);
SortDegreeCombinations=UniqueDegreeCombinations(i,:);
%Set of MultiDim Collocation Points 
PolynomialDegree=SortDegreeCombinations(1:P,:);



%Z Initialization: space-independet matrix P*P (of Hermite polynomes)
%eventuell hier wieder in log umrechnen?????
for i= 1:1:P;        
    for j= 1:1:P;   
        Z(i,j)=1;
        for ii=1:1:N; 
            load(PolynomialBasisFileName{ii},'Polynomial');
            Z(i,j)=Z(i,j)*polyval(Polynomial(1+PolynomialDegree(j,ii),length(Polynomial):-1:1),CollocationPointsBase(i,ii));            
        end        
    end
end


%clear MC_Vector;


%save('Parallel_test.mat');
%-------------------------------------------------------------------------------------------
%---------- I. P. C. M.: Snapshot Computation: Deterministic Computational Model Call ------
%-------------------------------------------------------------------------------------------
fprintf('\n> > > > > aPC: Snapshots Computation ...\n');
fprintf('\n . . . . . . . . . . . . . . . . . . . . . . . . . . .\n\n\n');
%Preparation for Parallel Run
for l=1:P;
   %---> Changing of Properties in Input File Data    
  % Test_Perm(l)=4.0929*10^(-11)*CollocationPointsBase(l,2)^3.6555.*(1+2*(0.5-betainv(cdf('Normal',CollocationPointsBase(l,1),0,1),3,3)));
        for i=1:1:length(InputFileData)-1            
            if (findstr(InputFileData{i},'Problem.pressuregradient =')~=0)       
                InputFileData{i}=num2str(CollocationPointsBase1(l,1));
                %InputFileData{i+1}=num2str(4.0929*10^(-11)*CollocationPointsBase(l,2)^3.6555.*(1+CollocationPointsBase(l,1)));
                %InputFileData{i+1}=num2str(4.0929*10^(-11)*CollocationPointsBase(l,2)^3.6555.*(1+2*(0.5-betainv(cdf('Normal',CollocationPointsBase(l,1),0,1),3,3))));        
                InputFileData(i)=strcat({'Problem.pressuregradient ='},InputFileData(i)); %Formating
      
            end
            if (findstr(InputFileData{i},'Problem.welldistance =')~=0)       
                 InputFileData{i}=num2str(CollocationPointsBase1(l,2));
                 InputFileData(i)=strcat({'Problem.welldistance ='},InputFileData(i)); %Formating
             end
            if (findstr(InputFileData{i},'Problem.pumpingrate =')~=0)       
                 InputFileData{i}=num2str(CollocationPointsBase1(l,3));
                 InputFileData(i)=strcat({'Problem.pumpingrate ='},InputFileData(i)); %Formating
             end
%             if (findstr(InputFileData{i},'<LeakageWellPermeability>')~=0)       
%                 InputFileData{i+1}=num2str(CollocationPointsBase(l,3));
%                 InputFileData(i+1)=strcat({'            '},InputFileData(i+1)); %Formating
%             end
            %if (findstr(InputFileData{i},'<InjectionWellRate>')~=0)       
            %    InputFileData{i+1}=num2str(CollocationPointsBase(l,4));
            %    InputFileData(i+1)=strcat({'            '},InputFileData(i+1)); %Formating
            %end
            %if (findstr(InputFileData{i},'<InjectionWellWindowSize>')~=0)       
            %    InputFileData{i+1}=num2str(CollocationPointsBase(l,5));
            %    InputFileData(i+1)=strcat({'            '},InputFileData(i+1)); %Formating
            %end                
        end 
        
        %Run in separated folders
      
        SnapshotDirName(l)=strcat({'./aPC_Snapshot_'},num2str(l));
        mkdir(SnapshotDirName{l});
        %copyfile(InterfaceFileName,SnapshotDirName{l});
        
        %---> Writing to Interface File
        %fid = fopen(InterfaceFileName,'w+');
        fid = fopen(strcat(SnapshotDirName{l},'/',InterfaceFileName),'w+');         
        for i=1:1:length(InputFileData)        
            fprintf(fid,'%s\n',InputFileData{i});      
        end
        fclose(fid);
        
        %Copy the Run File
       % copyfile('saltintrusion',SnapshotDirName{l});
        
end
%   
% % Parallel Run
% % for l=1:P;
% %     
% %         %----- Module-A
% %     
% %    
% %         %Dumux Simulation                
% %         cd (SnapshotDirName{l});
% % 
% %         %100 days:
% %         %eval(strcat('!','./test_2p ../grids/leakingwellcoarse.dgf 8640000 100')) ;
% %         %cd ..
% %         %30 days:
% %       %  eval(strcat('!','./test_2p ../grids/leakingwellcoarse.dgf 2592000 100')) ;
% %       %  eval(strcat('!',' env LD_LIBRARY_PATH=/opt/iws/openmpi-1.4.2/lib:/temp/lenaw/DUMUX/external/pardiso/lib ./test_2p ./grids/bisecteddomaincoordinates.dgf 1000 63072000 >logfile.log'));
% %         !env LD_LIBRARY_PATH=/opt/iws/openmpi-1.4.2/lib:/temp/lenaw/DUMUX/external/pardiso/lib ./test_2p ../grids/bisecteddomaincoordinates.dgf 1000 63072000 >logfile.log
% % 
% %         %Stochastic Output                              
% %         PCM_ParametersVector(:,l)=CollocationPointsBase(l,:);            
% % 
% %         
% %         fprintf('\n> > > > > aIPCM.: Computation of currant snapshot was successfully completed.\n');
% %         
% %         delete('test_2p');
% %         cd ..
% %                             
% % end
% figure(1)
% %sortMC1=sort(MC_Vector(1,:));
% %a1=1:1:length(MC_Vector(1,:))
% %plot(a1,sortMC1(1,:));
% hist(((MC_Vector(1,:))),100);
% 
% figure(2)
% % sortMC2=sort(MC_Vector(2,:));
% % a2=1:1:length(MC_Vector(2,:))
% % plot(a2,sortMC2(1,:));
% hist(MC_Vector(2,:),20);
% figure(3)
% hist((MC_Vector(3,:)),20);
% %set(gca,'XTick',[1e-17 1e-16 1e-15 1e-14 1e-13 1e-12])
% %set(gca, 'XScale', 'log')
% %axis([1e-17 1e-12 0 700]);
% save('aPC_parallel_saltintrusionS2.mat');
% fprintf('\n');
% fprintf('> > > > aPC: Snapshot calculation completed \n');
% toc
% fprintf('\n');
% fprintf('______________________________________________________________________________________________________________\n');
% fprintf('______________________________________________________________________________________________________________\n');
