#!/bin/bash
#Parametes:
EXE='gravitationno'
OUT_FILE='out.txt'
JOB_FILE='job.sh'
NO_NODES='1'
NO_CORES='1'


##############################################################
	   
echo '#!/bin/bash'>> $JOB_FILE
echo 'umask 022'>> $JOB_FILE
echo "./$EXE >> $OUT_FILE">> $JOB_FILE
echo 'exit 0'>> $JOB_FILE	   
chmod +x $JOB_FILE
qsub -lnodes=$NO_NODES:ppn=$NO_CORES $JOB_FILE

