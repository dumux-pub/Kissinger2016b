function [MomentsMatrix, CollocationPointsBase, PolynomialDegree] = createMatrixAndCollocationPoints(N, d, MC_Vector)

%Total number of terms
P=factorial(N+d)/(factorial(N)*factorial(d)); 
%Computation of Arbitratry Polynomials
for i=1:N
    PolynomialBasisFileName(i)=strcat({'PolynomialBasis_'},num2str(i),{'.mat'});
    aPoly_Construction(MC_Vector(i,:), d, PolynomialBasisFileName{i});
end


%Initialization of Collocation points
for i=1:1:N
    load(PolynomialBasisFileName{i},'Roots')
    Cpoints(i,:)=Roots;
end
%
%
%---------- Digital set of Collocation points combination  / Work with N=2 !!!
%DigitalUniqueCombinations=allcomb(1:d+1,1:d+1,1:d+1,1:d+1,1:d+1);
for i=1:N
    m{i}=1:d+1;
end
DigitalUniqueCombinations=allcomb(m{:});% Works with all N
for i=1:1:length(DigitalUniqueCombinations)
    DigitalPointsWeight(i)=sum(DigitalUniqueCombinations(i,:));
end
%Sorting of Posible Digital Points Weight
[SortDigitalPointsWeight, index_SDPW]=sort(DigitalPointsWeight);
SortDigitalUniqueCombinations=DigitalUniqueCombinations(index_SDPW,:);
%Ranking relatively mean
for j=1:N
    temp(j,:)=abs(Cpoints(j,:)-mean(MC_Vector(j,:)));
end
[temp_sort, index_CP]=sort(temp,2);
for j=1:N
    SortCpoints(j,:)=Cpoints(j,index_CP(j,:));
end
%Mapping of Digital Combination to Cpoint Combination
for i=1:1:length(SortDigitalUniqueCombinations)
    for j=1:1:N
        SortUniqueCombinations(i,j)=SortCpoints(j,SortDigitalUniqueCombinations(i,j));
    end
end
CollocationPointsBase=SortUniqueCombinations(1:P,:);

%----------Choice of polynomial degree combinations
%Choose the combination of polynomials for each snapshot point. This is
%done by settins up all possible combinations of polynomials of the
%different variables. Only unique combinations are considered and these are
%sorted according to the sum of each combinations. The first P combinations
%(ie the ones with the samllest sum) are chosen.
PossibleDegree=0:1:length(Cpoints(1,:))-1;
PosibleDegree=0:1:length(Cpoints(1,:))-1;
for i=2:1:N
    PosibleDegree=[PosibleDegree,0:1:length(Cpoints(i,:))-1];    
end
UniqueDegreeCombinations=unique(nchoosek(PosibleDegree,N),'rows');
%Posible Degree Weight Computation
for i=1:1:length(UniqueDegreeCombinations) 
    DegreeWeight(i)=0;
    for j=1:1:N
        DegreeWeight(i)=DegreeWeight(i)+UniqueDegreeCombinations(i,j);
    end
end
%Sorting of Posible Degree Weight
[SortDegreeWeight, i]=sort(DegreeWeight);
SortDegreeCombinations=UniqueDegreeCombinations(i,:);
%Set of MultiDim Collocation Points 
PolynomialDegree=SortDegreeCombinations(1:P,:);



%Z Initialization: space-independet matrix P*P (of Hermite polynomes)
%eventuell hier wieder in log umrechnen?????
for i= 1:1:P;        
    for j= 1:1:P;   
        MomentsMatrix(i,j)=1;
        for ii=1:1:N; 
            load(PolynomialBasisFileName{ii},'Polynomial');
            MomentsMatrix(i,j)=MomentsMatrix(i,j)*polyval(Polynomial(1+PolynomialDegree(j,ii),length(Polynomial):-1:1),CollocationPointsBase(i,ii));            
        end        
    end
end

end