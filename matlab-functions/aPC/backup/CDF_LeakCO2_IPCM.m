%Probabilistic Collocation Method Plot

clear all;
%load('aprxT_MC30days_TDfix1000.mat', 'MC_Vector');


cd SimulationCoarse_IPCM_d1N3_30days;
load('MC_Vector.mat', 'MC_Vector');

load('IPCM_B.mat');
load('aprxT_IPCM.mat');

time_id=50;
LFS=9; %Litle Font Size
BFS=10; %Big Font Size    
PCMMC=1; % 0- no PCM+MC computations
%index=6; %index for section plot
%index=442; %Index for Top Pressure plot


%---------- I. P. C. M.
fprintf('\n');
fprintf('______________________________________________________________________________________________________________\n');
fprintf('______________________________________________________________________________________________________________\n');
fprintf('\n> > > > > Arbitraty Integrative Probabilistic Collocation Method \n');
fprintf('\n> > > > > aIPCM.: massive stochastic model reduction \n');
fprintf('\n');
tic


%Loading of T-section Data:
for l=1:length(SnapshotDirName);       
    load(strcat(SnapshotDirName{l},'/','IPCM_Output_Uniform_MassFlux.mat'));    
    for i=1:1:length(IPCM_Output_Uniform_MassFlux) 
        IPCM_Output_Uniform_CO2Leakage(l,i)=100*IPCM_Output_Uniform_MassFlux(4,i)/8.87;
    end     
end
clear IPCM_Output_Uniform_MassFlux;


%---------- P. C. M.: Matrix Solver ZC(T)=Y(T)   
clear temp;
inv_Z=pinv(Z);
for j=1:1:TimeStep;
    for ii=1:1:P;
        temp(ii)=IPCM_Output_Uniform_CO2Leakage(ii,j);
    end
    %C_CO2Leakage(:,j) = inv_Z*temp';                        
    C_CO2Leakage(:,j) = Z\temp';                        
end

fprintf('---> Collocation Matrix Error is %5f pourcents', 100*abs(sum(abs(Z*C_CO2Leakage(:,time_id)))-sum(abs(temp)))); 
    
fprintf('\n');
fprintf('______________________________________________________________________________________________________________\n');

%load('Full_MC_Data.mat','MC_Vector');
%Correlation:
%load('aprxT_MC30days_TDfix1000.mat', 'MC_Vector');
%for l=1:length(MC_Vector);
%    ErrorX(l)=norminv(cdf('Beta',0.5-(MC_Vector(1,l)./(4.0929*10^(-11)*MC_Vector(2,l).^3.6555)-1)/2,3,3),0,1);
%end
%MC_Vector(1,:)=ErrorX(:);


%Polinomial MC+PCM Computation
MCsize=size(MC_Vector);
MCsize=MCsize(2);


%Pre Computation for d and N
for j=1:1:P;
    for ii=1:1:N;     
        load(PolynomialBasisFileName{ii},'Polynomial');
        MC_Poly(ii,j,:)=polyval(Polynomial(1+PolynomialDegree(j,ii),length(Polynomial):-1:1),MC_Vector(ii,:));       
    end
end

for i=1:1:length(TimeVector) 
    for l=1:MCsize;
       PCMMC_CO2Leakage(l,i)=0;
       for j=1:1:P;
            multi=1;
            for ii=1:1:N;          
                multi=multi*MC_Poly(ii,j,l);
            end             
            PCMMC_CO2Leakage(l,i)=PCMMC_CO2Leakage(l,i)+C_CO2Leakage(j,i)*multi;
       end
    end  
     
   %Computational Time
   fprintf('---> M.C. on Polynomial: ');
   disp([datestr(now) ' - ' num2str(round(100*i/(length(TimeVector)-1))) '% completed']);
end
%CO2 Leakage Check Out of Pysical Boards
for i=1:1:length(TimeVector) 
    for l=1:MCsize;
        if PCMMC_CO2Leakage(l,i)<0;
            PCMMC_CO2Leakage(l,i)=0;
        end
    end
end   

%------------------------------------------------------


figure(1);

%----- Numerical Computation CO2 Leakage CDF
for k = 1:1:MCsize;
    %num_MC_CDFofCO2Leakage(k)=(k-1)/(MCsize)+1/(2*MCsize);
    num_PCMMC_CDFofCO2Leakage(k)=(k-1)/(MCsize)+1/(2*MCsize);
end
sortPCMMC_CO2Leakage=sort(PCMMC_CO2Leakage(:,time_id));
%line1=plot(sortPCMMC_CO2Leakage,num_PCMMC_CDFofCO2Leakage,'.');
%set(line1, 'Color', [0. 0. 1.], 'LineWidth', 3);     
hold on;
line=normplot(sortPCMMC_CO2Leakage);
%line2=plot(sort(MC_CO2Leakage),num_MC_CDFofCO2Leakage,'--');
set(line, 'Color', [0. 0. 0.]+0.2, 'LineWidth', 2, 'LineStyle','-', 'Marker', 'none');

%normplot(sort(MC_CO2Leakage));
grid on;
set(gca,'FontSize',BFS)
title('CDF of CO_2 Leakage','FontWeight', 'bold');
set(gca,'FontSize',LFS)
xlabel('CO_2 Leakage, [%]');
ylabel('Probability');
set(gca,'Color',[1 1 1])
set(gcf,'Color',[1 1 1]);
set(gcf,'Position',[100 200 600 400]);

%xlim([0.001 0.799])

fprintf('______________________________________________________________________________________________________________\n');
fprintf('______________________________________________________________________________________________________________\n');
