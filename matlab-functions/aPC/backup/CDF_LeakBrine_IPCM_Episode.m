%Probabilistic Collocation Method Plot

clear all;
%load('Version2/IPCM_A_parallel_version2.mat', 'MC_Vector');


%cd SimulationCoarse_IPCM_d1N3_30days;
%load('MC_Vector.mat', 'MC_Vector');

load('FluxEpisode.mat');
%load('aprxT_IPCM.mat');

time_id=36;
LFS=14; %Litle Font Size
BFS=14; %Big Font Size    
PCMMC=1; % 0- no PCM+MC computations
%index=6; %index for section plot
%index=442; %Index for Top Pressure plot
TimeSteps=61;

%---------- I. P. C. M.
fprintf('\n');
fprintf('______________________________________________________________________________________________________________\n');
fprintf('______________________________________________________________________________________________________________\n');
fprintf('\n> > > > > Arbitraty Integrative Probabilistic Collocation Method \n');
fprintf('\n> > > > > aIPCM.: massive stochastic model reduction \n');
fprintf('\n');
tic
%  CollocationPointsBase(:,3)=log(CollocationPointsBase(:,3));
% 
% 
% %Z Initialization: space-independet matrix P*P (of Hermite polynomes)
% %eventuell hier wieder in log umrechnen?????
% for i= 1:1:P;        
%     for j= 1:1:P;   
%         Z(i,j)=1;
%         for ii=1:1:N; 
%             load(PolynomialBasisFileName{ii},'Polynomial');
%             Z(i,j)=Z(i,j)*polyval(Polynomial(1+PolynomialDegree(j,ii),length(Polynomial):-1:1),CollocationPointsBase(i,ii));            
%         end        
%     end
% end




%Loading of T-section Data:
for l=1:length(SnapshotDirName);       
      
load(strcat(SnapshotDirName{l},'/','IPCM_Output_EpisodeMassFlux23.mat'));

    IPCM_Output_EpisodeMassFlux23(1,:)=IPCM_Output_EpisodeMassFlux23(1,:)-IPCM_Output_EpisodeMassFlux23(1,11);     
   Brine_Discharge=zeros(1,51);
%     for i=11:1:61
%        j=j+1;
   Brine_Discharge(1,1:51)=IPCM_Output_EpisodeMassFlux23(1,11:61);
   %end
   %for i=1:1:length(Brine_Discharge) 
      for i=1:1:length(IPCM_Output_EpisodeMassFlux23) 
        IPCM_Output_BrineLeakage(l,i)=IPCM_Output_EpisodeMassFlux23(1,i);
       %IPCM_Output_BrineLeakage(l,i)=Brine_Discharge(1,i);
    end     
end
%clear IPCM_Output_MassFlux;
%TimeSteps=51;

%---------- P. C. M.: Matrix Solver ZC(T)=Y(T)   
clear temp;
inv_Z=pinv(Z);
for j=1:1:TimeSteps;
    for ii=1:1:P;
        temp(ii)=IPCM_Output_BrineLeakage(ii,j);
    end
    %C_CO2Leakage(:,j) = inv_Z*temp';                        
    C_BrineLeakage(:,j) = Z\temp';                        
end

%fprintf('---> Collocation Matrix Error is %5f pourcents', 100*abs(sum(abs(Z*C_CO2Leakage(:,time_id)))-sum(abs(temp)))); 
    
fprintf('\n');
fprintf('______________________________________________________________________________________________________________\n');

%load('Full_MC_Data.mat','MC_Vector');
%Correlation:
%load('aprxT_MC30days_TDfix1000.mat', 'MC_Vector');
%for l=1:length(MC_Vector);
%    ErrorX(l)=norminv(cdf('Beta',0.5-(MC_Vector(1,l)./(4.0929*10^(-11)*MC_Vector(2,l).^3.6555)-1)/2,3,3),0,1);
%end
%MC_Vector(1,:)=ErrorX(:);


%Polinomial MC+PCM Computation
MCsize=size(MC_Vector);
MCsize=MCsize(2);


%Pre Computation for d and N
for j=1:1:P;
    for ii=1:1:N;     
        load(PolynomialBasisFileName{ii},'Polynomial');
        MC_Poly(ii,j,:)=polyval(Polynomial(1+PolynomialDegree(j,ii),length(Polynomial):-1:1),MC_Vector(ii,:));       
    end
end

for i=1:1:TimeSteps
    for l=1:MCsize;
       PCMMC_BrineLeakage(l,i)=0;
       for j=1:1:P;
            multi=1;
            for ii=1:1:N;          
                multi=multi*MC_Poly(ii,j,l);
            end             
            PCMMC_BrineLeakage(l,i)=PCMMC_BrineLeakage(l,i)+C_BrineLeakage(j,i)*multi;
       end
    end  
     
   %Computational Time
   fprintf('---> M.C. on Polynomial: ');
   disp([datestr(now) ' - ' num2str(round(100*i/(TimeSteps-1))) '% completed']);
end
%CO2 Leakage Check Out of Pysical Boards
% for i=1:1:length(TimeVector) 
%     for l=1:MCsize;
%         if PCMMC_CO2Leakage(l,i)<0;
%             PCMMC_CO2Leakage(l,i)=0;
%         end
%     end
% end   

%------------------------------------------------------
save('CDF.mat');

figure(6);

%----- Numerical Computation CO2 Leakage CDF
for k = 1:1:MCsize;
    %num_MC_CDFofCO2Leakage(k)=(k-1)/(MCsize)+1/(2*MCsize);
    num_PCMMC_CDFofBrineLeakage(k)=(k-1)/(MCsize)+1/(2*MCsize);
end
sortPCMMC_BrineLeakage=sort(PCMMC_BrineLeakage(:,time_id));
% if PCMsection_Output_DataVector(i1,i2)<0
%          PCMsection_Output_DataVector(i1,i2)=0;
%      end
%if sortPCMMC_BrineLeakage(:,1) <0.0
%    sortPCMMC_BrineLeakage (:,1)=0.0;
%end
%for i=3:1:128
line1=plot(sortPCMMC_BrineLeakage,num_PCMMC_CDFofBrineLeakage,'.');
%end
%set(line1, 'Color', [0. 0. 1.], 'LineWidth', 3);     
hold on;
%line=normplot(PCMMC_BrineLeakage);
%line2=plot(sort(MC_CO2Leakage),num_MC_CDFofCO2Leakage,'--');
%set(line2, 'Color', [0. 0. 0.]+0.2, 'LineWidth', 2, 'LineStyle','-', 'Marker', 'none');
set(line1, 'Color', [1. 0. 0.], 'LineWidth', 3, 'LineStyle','-', 'Marker', 'none');
%normplot(sort(MC_CO2Leakage));
grid on;
set(gca,'FontSize',BFS)
title('CDF of Brine Infiltration','FontWeight', 'bold');
set(gca,'FontSize',LFS)
xlabel('Brine Discharge, [kg/s]');
ylabel('Probability [-]');
set(gca,'Color',[1 1 1])
set(gcf,'Color',[1 1 1]);
set(gcf,'Position',[100 200 600 400]);
%axis([0 1.7 0 1]);
%xlim([0.001 0.799])

fprintf('______________________________________________________________________________________________________________\n');
fprintf('______________________________________________________________________________________________________________\n');



%Only the good range of the MC_Vector
 c=1;
 for a=1:1:length(MC_Vector)
     if MC_Vector(1,a)>-13.6%-13.7489473947018
     MC_Vector1(1,c)=MC_Vector(1,a);
     c=c+1;
     end
 end

 c=1;
 for a=1:1:length(MC_Vector)
     if MC_Vector(3,a)<-11.8851673348828;
     MC_Vector1(3,c)=MC_Vector(1,a);
     c=c+1;
     end
 end
%Polinomial MC+PCM Computation
MCsize1=size(MC_Vector1);
MCsize1=MCsize1(2);

 MC_Vector1(2,:)=random('beta',1,3,1,97);



%Pre Computation for d and N
for j=1:1:P;
    for ii=1:1:N;     
        load(PolynomialBasisFileName{ii},'Polynomial');
        MC_Poly1(ii,j,:)=polyval(Polynomial(1+PolynomialDegree(j,ii),length(Polynomial):-1:1),MC_Vector1(ii,:));       
    end
end

for i=1:1:TimeSteps
    for l=1:MCsize1;
       PCMMC_BrineLeakage1(l,i)=0;
       for j=1:1:P;
            multi1=1;
            for ii=1:1:N;          
                multi1=multi1*MC_Poly1(ii,j,l);
            end             
            PCMMC_BrineLeakage1(l,i)=PCMMC_BrineLeakage(l,i)+C_BrineLeakage(j,i)*multi1;
       end
    end  
     
   %Computational Time
   fprintf('---> M.C. on Polynomial: ');
   disp([datestr(now) ' - ' num2str(round(100*i/(TimeSteps-1))) '% completed']);
end
 

%------------------------------------------------------

figure(7);

%----- Numerical Computation CO2 Leakage CDF
for k = 1:1:MCsize1;
    %num_MC_CDFofCO2Leakage(k)=(k-1)/(MCsize)+1/(2*MCsize);
    num_PCMMC_CDFofBrineLeakage1(k)=(k-1)/(MCsize1)+1/(2*MCsize1);
end
sortPCMMC_BrineLeakage1=sort(PCMMC_BrineLeakage1(:,time_id));
% if PCMsection_Output_DataVector(i1,i2)<0
%          PCMsection_Output_DataVector(i1,i2)=0;
%      end
if sortPCMMC_BrineLeakage1(:,1) <0.0
    sortPCMMC_BrineLeakage1 (:,1)=0.0;
end
%for i=3:1:128
line1=plot(sortPCMMC_BrineLeakage1,num_PCMMC_CDFofBrineLeakage1,'.');
%end
%set(line1, 'Color', [0. 0. 1.], 'LineWidth', 3);     
hold on;
%line=normplot(PCMMC_BrineLeakage);
%line2=plot(sort(MC_CO2Leakage),num_MC_CDFofCO2Leakage,'--');
%set(line2, 'Color', [0. 0. 0.]+0.2, 'LineWidth', 2, 'LineStyle','-', 'Marker', 'none');
set(line1, 'Color', [0. 0. 0.], 'LineWidth', 3, 'LineStyle','-', 'Marker', 'none');
%normplot(sort(MC_CO2Leakage));
grid on;
set(gca,'FontSize',BFS)
title('CDF of Brine Infiltration','FontWeight', 'bold');
set(gca,'FontSize',LFS)
xlabel('Brine Discharge, [kg/s]');
ylabel('Probability [-]');
set(gca,'Color',[1 1 1])
set(gcf,'Color',[1 1 1]);
set(gcf,'Position',[100 200 600 400]);
%axis([0 1.7 0 1]);
%xlim([0.001 0.799])



