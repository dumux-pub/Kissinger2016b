cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_1
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_2
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_3
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_4
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_5
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_6
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_7
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_8
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_9
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_10
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_11
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_12
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_13
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_14
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_15
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_16
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_17
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_18
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_19
cp brinedisplacement_2p/brinedisplacement_2p IPCM_Snapshot_20

cp -r brinedisplacement_2p/grids IPCM_Snapshot_1
cp -r brinedisplacement_2p/grids IPCM_Snapshot_2
cp -r brinedisplacement_2p/grids IPCM_Snapshot_3
cp -r brinedisplacement_2p/grids IPCM_Snapshot_4
cp -r brinedisplacement_2p/grids IPCM_Snapshot_5
cp -r brinedisplacement_2p/grids IPCM_Snapshot_6
cp -r brinedisplacement_2p/grids IPCM_Snapshot_7
cp -r brinedisplacement_2p/grids IPCM_Snapshot_8
cp -r brinedisplacement_2p/grids IPCM_Snapshot_9
cp -r brinedisplacement_2p/grids IPCM_Snapshot_10
cp -r brinedisplacement_2p/grids IPCM_Snapshot_11
cp -r brinedisplacement_2p/grids IPCM_Snapshot_12
cp -r brinedisplacement_2p/grids IPCM_Snapshot_13
cp -r brinedisplacement_2p/grids IPCM_Snapshot_14
cp -r brinedisplacement_2p/grids IPCM_Snapshot_15
cp -r brinedisplacement_2p/grids IPCM_Snapshot_16
cp -r brinedisplacement_2p/grids IPCM_Snapshot_17
cp -r brinedisplacement_2p/grids IPCM_Snapshot_18
cp -r brinedisplacement_2p/grids IPCM_Snapshot_19
cp -r brinedisplacement_2p/grids IPCM_Snapshot_20


cp Submit_brine_2p_I1 IPCM_Snapshot_1/
cp Submit_brine_2p_I2 IPCM_Snapshot_2/
cp Submit_brine_2p_I3 IPCM_Snapshot_3/
cp Submit_brine_2p_I4 IPCM_Snapshot_4/
cp Submit_brine_2p_I5 IPCM_Snapshot_5/
cp Submit_brine_2p_I6 IPCM_Snapshot_6/
cp Submit_brine_2p_I7 IPCM_Snapshot_7/
cp Submit_brine_2p_I8 IPCM_Snapshot_8/
cp Submit_brine_2p_I9 IPCM_Snapshot_9/
cp Submit_brine_2p_I10 IPCM_Snapshot_10/
#
cp Submit_brine_2p_I11 IPCM_Snapshot_11/
cp Submit_brine_2p_I12 IPCM_Snapshot_12/
cp Submit_brine_2p_I13 IPCM_Snapshot_13/
cp Submit_brine_2p_I14 IPCM_Snapshot_14/
cp Submit_brine_2p_I15 IPCM_Snapshot_15/

cp Submit_brine_2p_I16 IPCM_Snapshot_16/
cp Submit_brine_2p_I17 IPCM_Snapshot_17/
cp Submit_brine_2p_I18 IPCM_Snapshot_18/
cp Submit_brine_2p_I19 IPCM_Snapshot_19/
cp Submit_brine_2p_I20 IPCM_Snapshot_20/


cp alugrid.cfg IPCM_Snapshot_1/
cp alugrid.cfg IPCM_Snapshot_2/
cp alugrid.cfg IPCM_Snapshot_3/
cp alugrid.cfg IPCM_Snapshot_4/
cp alugrid.cfg IPCM_Snapshot_5/
cp alugrid.cfg IPCM_Snapshot_6/
cp alugrid.cfg IPCM_Snapshot_7/
cp alugrid.cfg IPCM_Snapshot_8/
cp alugrid.cfg IPCM_Snapshot_9/
cp alugrid.cfg IPCM_Snapshot_10/
#
cp alugrid.cfg IPCM_Snapshot_11/
cp alugrid.cfg IPCM_Snapshot_12/
cp alugrid.cfg IPCM_Snapshot_13/
cp alugrid.cfg IPCM_Snapshot_14/
cp alugrid.cfg IPCM_Snapshot_15/

cp alugrid.cfg IPCM_Snapshot_16/
cp alugrid.cfg IPCM_Snapshot_17/
cp alugrid.cfg IPCM_Snapshot_18/
cp alugrid.cfg IPCM_Snapshot_19/
cp alugrid.cfg IPCM_Snapshot_20/


#cd IPCM_Snapshot_1
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I1
#aqsub -q wab -W depend=afterany:272539 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_Submit_brine_2p_I6
#cd ..
#cd IPCM_Snapshot_2
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I2
#cd ..
#cd IPCM_Snapshot_3
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I3
#cd ..
#cd IPCM_Snapshot_4
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I4
#cd ..
#cd IPCM_Snapshot_5
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I5
#cd ..

#cd IPCM_Snapshot_6
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I6
#qsub -q wab -W depend=afterany:272539 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I6
#cd ..
#cd IPCM_Snapshot_7
#qsub -q wab -W depend=afterany:272540 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I7
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I7
#cd ..
#cd IPCM_Snapshot_8
#qsub -q wab -W depend=afterany:272541 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I8
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I8
#cd ..
#cd IPCM_Snapshot_9
#qsub -q wab -W depend=afterany:272542 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I9
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I9
#cd ..
#cd IPCM_Snapshot_10
#qsub -q wab -W depend=afterany:272543 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I10
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I10



#cd IPCM_Snapshot_11
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I11
#qsub -q wab -W depend=afterany:272617 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I11
#cd ..
#cd IPCM_Snapshot_12
#qsub -q wab -W depend=afterany:272617 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I12
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I12
#cd ..
#cd IPCM_Snapshot_13
#qsub -q wab -W depend=afterany:272617 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I13
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I13
#cd ..
#cd IPCM_Snapshot_14
#qsub -q wab -W depend=afterany:277181 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I14
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I4
#cd ..
#cd IPCM_Snapshot_15
#qsub -q wab -W depend=afterany:277183 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I15
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I5

#cd ..
#cd IPCM_Snapshot_16
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I16
#qsub -q wab -W depend=afterany:2728 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I16
#cd ..
#cd IPCM_Snapshot_17
#qsub -q wab -W depend=afterany:272517 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I17
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I17
#cd ..
#cd IPCM_Snapshot_18
#qsub -q wab -W depend=afterany:272518 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I18
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I18
#cd ..
#cd IPCM_Snapshot_19
#qsub -q wab -W depend=afterany:272519 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I19
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I19
#cd ..
#cd IPCM_Snapshot_20
#qsub -q wab -W depend=afterany:272520 -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I20
#qsub -q wab -l nodes=3:bwgrid:ppn=8,walltime=200:00:00 Submit_brine_2p_I20
