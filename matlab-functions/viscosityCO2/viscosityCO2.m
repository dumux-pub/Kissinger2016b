% Viscosity of CO2 according to Fenghour 1998
% Temperature in K
% Pressure in Pa

function [visco_CO2] = viscosityCO2(temperature, pressure)
    
        a0 = 0.235156;
        a1 = -0.491266;
        a2 = 5.211155E-2;
        a3 = 5.347906E-2;
        a4 = -1.537102E-2;

        d11 = 0.4071119E-2;
        d21 = 0.7198037E-4;
        d64 = 0.2411697E-16;
        d81 = 0.2971072E-22;
        d82 = -0.1627888E-22;

        ESP = 251.196;

        
        
        

        if(temperature < 275.) % regularisation
            % temperature = 275;
            error('Temperature below 275 Kelvin, CO2 viscosity calculation failed')
        end


        TStar = temperature/ESP;

        % mu0: viscosity in zero-density limit */
        SigmaStar = exp(a0 + a1*log(TStar) ...
                        + a2*log(TStar)*log(TStar) ...
                        + a3*log(TStar)*log(TStar)*log(TStar) ...
                        + a4*log(TStar)*log(TStar)*log(TStar)*log(TStar) );

        mu0 = 1.00697*sqrt(temperature) / SigmaStar;

        % dmu : excess viscosity at elevated density 
        rho = densityCO2(temperature, pressure/1e6); % CO2 mass density [kg/m^3] pressure in MPa

        dmu = d11*rho + d21*rho*rho + d64*rho^6/(TStar*TStar*TStar) ...
            + d81*rho^8 + d82*rho^8/TStar;

        % dmucrit : viscosity increase near the critical point 

        % False (Lybke 2July2007)
        %e1 = 5.5930E-3;
        %e2 = 6.1757E-5;
        %e4 = 2.6430E-11;
        %dmucrit = e1*rho + e2*rho*rho + e4*rho*rho*rho;
        %visco_CO2 = (mu0 + dmu + dmucrit)/1.0E6;   /* conversion to [Pa s] */

        visco_CO2 = (mu0 + dmu)/1.0E6;   % conversion to [Pa s] 
        
end